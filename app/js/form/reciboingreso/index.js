var form = {
	nuevo: function() {
		
	},
	editar: function(id) {
		// alert(id);
	},
	eliminar: function(id) {
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				grilla.reload(_default_grilla);
			});
		});
	},
	imprimir: function() {
		var id = grilla.get_id(_default_grilla);
		if(id != null) {
			alert(id);
		}
	},
	guardar: function(datos_aux) {
		var data = $("#form_"+_controller).serialize();
		    // data+= "&"+$("#form_more").serialize()
			// data += "&tipodocumento="+$( "#idtipodocumento option:selected" ).text();
			if(datos_aux) {
				data += "&"+datos_aux;
			}

		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				redirect(_controller);
			});
		});
	},
	cancelar: function() {
		
	}
};

// validate();

// $("#descripcion").letras({'permitir':' '});
$("#monto,.numero").numero_real();
$("#serie_doc,#numero_doc").numero_entero();

function callbackRI(nRow, aData, iDisplayIndex) {
	$('td:eq(3)', nRow).html(fecha_es(aData['fecha']));
	$('td:eq(4)', nRow).html("<div style='text-align:right;'>"+aData['monto']+"</div>");
}