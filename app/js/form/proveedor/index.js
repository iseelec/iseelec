modulo.register("<?php echo $controller;?>", {
	nuevo: function() {
	},
	editar: function(id) {
	},
	eliminar: function(id) {
		var sel = this;
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				sel.callback(res);
			});
		}, sel._controller);
	},
	imprimir: function() {
	},
	guardar: function() {
		var sel = this;
		var data = $(sel._form).serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				sel.callback(res);
			});
		}, sel._controller);
	},
	cancelar: function() {
		
	}
	,callback(res, redir) {
		if(typeof redir != "boolean")
			redir = ! dialog.inModal(this._form);
		
		if(redir)
			redirect(this._controller);
		else {
			grilla.reload(_default_grilla);
			dialog.close("#modal-"+this._controller);
		}
	}
	,init: function() { // inicializar las funciones y eventos del modulo
		validate(this._controller);
		if( ! dialog.inModal(this._form))
			$("#"+this._prefix+"ruc").focus();
	}
	,hide_modal: function() { // funcion a ejecutar cuando se cierra la ventana modal
		clear_form(this._form);
	}
	,show_modal: function() { // funcion a ejecutar cuando se muestra la ventana modal
		
	}
});

$("#<?php echo $prefix;?>ruc").numero_entero();

$("#btn-search-ruc").on("click", function() {
	if( ! $("#<?php echo $prefix;?>ruc").required())
		return;
	$.ajax({
		data: { "nruc" : $("#<?php echo $prefix;?>ruc").val() },
		type: "POST",
		dataType: "json",
		url: "<?php echo $url_consultas_ruc; ?>",
	}).done(function( data, textStatus, jqXHR ){
		if(data['success'] != "false" && data['success'] != false) {
			if(typeof(data['result']) != 'undefined') {
				$("#<?php echo $prefix;?>nombre").val(data["result"]["razon_social"]);
				$("#<?php echo $prefix;?>direccion").val(data["result"]["direccion"]);
			}
			else
				ventana.alert({titulo: "", mensaje: "No se puede obtener los datos, consulte nuevamente", tipo:"error"});
		}
		else if(typeof(data['msg'])!='undefined')
			ventana.alert({titulo: "", mensaje: data['msg'], tipo:"error"});
		else if(typeof(data['message'])!='undefined')
			ventana.alert({titulo: "", mensaje: data['message'], tipo:"error"});
		else
			ventana.alert({titulo: "", mensaje: "No se puede obtener los datos &iquest;el ruc es valido?", tipo:"error"});
	}).fail(function( jqXHR, textStatus, errorThrown ){
		ventana.alert({titulo: "", mensaje: "Solicitud fallida:" + textStatus, tipo:"error"});
	});
});
