	var form = {
	nuevo: function() {
		
	},
	editar: function(id) {
		
	},
	eliminar: function(id) {
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				grilla.reload(_default_grilla);
			});
		});
	},
	imprimir: function() {
		var id = grilla.get_id(_default_grilla);
		if(id != null) {
			alert(id);
		}
	},
	guardar: function() {
		Save_action("form_"+_controller);
		return;
		var data = $("#form_"+_controller).serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				redirect(_controller);
			});
		});
	},
	cancelar: function() {
		
	}
	,usuario_responsable: function(id) {
	}
	,guardar_usuario: function() {
		var data = $("#form_usuario").serialize();
		model.save(data, function(res) {
			$("#modal-usuario").modal("hide");
			reload_combo("#idusuario", {controller: "usuario"}, function() {
				$("#idusuario").val(res.idusuario);
			});
		}, "usuario");
	}
	,guardar_fase: function() {
		var data = $("#form_fase").serialize();
		model.save(data, function(res) {
			$("#modal-fase").modal("hide");
			reload_combo("#idfase", {controller: "fase"}, function() {
				$("#idfase").val(res.idfase);
			});
		}, "fase");
	}
};

validate();

function Save_action(id){//id= id del formulario
	var fd = new FormData(document.getElementById(id));
		$.ajax({
			url: _base_url+"proyecto/guardar",
			type: "POST",
			data: fd,
			enctype: 'multipart/form-data',
			processData: false,  // tell jQuery not to process the data
			contentType: false   // tell jQuery not to set contentType
		}).done(function( data ) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				redirect(_controller);
			});
		});
		return false;
}

$("#btn_cancel").click(function() {
	redirect(_controller);
	return false;
});

$("#linea").focus();

$(".float-number").numero_real();
$(".int-number").numero_entero();


$("#load_photo").click(function() {
    $("#file").click();
});

$(".btn-registrar-usuario").on("click", function() {
	$("#modal-usuario").modal("show");
	return false;
});

$(".btn-registrar-fase").on("click", function() {
	$("#modal-fase").modal("show");
	return false;
});

$('#fecha_inicio').datepicker({
	todayBtn: "linked",
	keyboardNavigation: false,
	forceParse: false,
	autoclose: true,
	language: 'es',
	endDate: parseDate(_current_date)
});

$('#fecha_fin').datepicker({
	todayBtn: "linked",
	keyboardNavigation: false,
	forceParse: false,
	autoclose: true,
	language: 'es',
	//endDate: parseDate(_current_date)
});

// $('#deta_fecha_i').datepicker({
	// todayBtn: "linked",
	// keyboardNavigation: false,
	// forceParse: false,
	// autoclose: true,
	// language: 'es',
	// endDate: parseDate(_current_date)
// });

// $('#deta_fecha_f').datepicker({
	// todayBtn: "linked",
	// keyboardNavigation: false,
	// forceParse: false,
	// autoclose: true,
	// language: 'es',
	////endDate: parseDate(_current_date)
// });

input.autocomplete({
	selector: "#descripcion"
	,controller: "proyecto"
	,method: "autocomplete_descripcion"
	,label: "[descripcion]"
	,value: "[descripcion]"
	,highlight: true
});

function leerarchivoprod(f) {
    var imagenAR = document.getElementById("file");
    if (imagenAR.files.length != 0 && imagenAR.files[0].type.match(/image.*/)) {
		var lecimg = new FileReader();
		lecimg.onload = function(e) { 
			var img = document.getElementById("photo");
			img.src = e.target.result;
		} 
		lecimg.onerror = function(e) { 
			alert("Error leyendo la imagen!!");
		}
		lecimg.readAsDataURL(imagenAR.files[0]);
    } else {
		alert("Seleccione una imagen!!")
    }
}

// $("#idusuario").on("change", function() {
	// var t = $("option:selected", this).text();
	// if(t == "") {
		// t = "&iquest;?";
	// }
	
	// $("table.tabla_usuario_responsable abbr").html(t);
	// update_combo_usuario_temp();
// });

// $("#idfase").on("change", function() {
	// var t = $("option:selected", this).text();
	// if(t == "") {
		// t = "&iquest;?";
	// }
	
	// $("table.tabla_fases_py abbr").html(t);
	// update_combo_fases_temp();
// });

function add_usuario_responsable() {
	var id = $("#combo_asignar_usuario_responsabled").val();
	var desc = $("#combo_asignar_usuario_responsabled option:selected").text();
	
	if($("table.tabla_usuario_responsable tr[index="+id+"]").length) {
		// ventana.alert({titulo: "Usuario de responsable asignado", 
		// mensaje: "La usuario de responsable "+desc+" ya se encuentra en la tabla"});
		$("table.tabla_usuario_responsable tr[index="+id+"] input.proy_sueldo_usuario").focus();
		return;
	}
	
	var uni = $("#idusuario option:selected").text();
	if(uni == "") {
		uni = "&iquest;?";
	}
	
	var html = '<tr index="'+id+'">';
	html += '<td><input type="hidden" name="proy_usuario[]" class="proy_usuario" value="'+id+'">'+desc+'</td>';
	html += '<td>Dias <strong> de trabajo: </strong></td>';
	html += '<td><input type="text" name="proy_dias_usuario[]" class="proy_dias_usuario form-control input-xs"></td>';
	html += '<td>Sueldo <strong>x Día:</strong></td>';
	html += '<td><input type="text" name="proy_sueldo_usuario[]" class="proy_sueldo_usuario form-control input-xs"></td>';
	html += '<td>Viaticos <strong>x Día:</strong></td>';
	html += '<td><input type="text" name="proy_viatico_usuario[]" class="proy_viatico_usuario form-control input-xs"></td>';
	// html += '<td>RESPONSABLE <strong>del equipo en Proyecto : </strong></td>';
	// html += '<td><abbr title="Responsable del Proyecto o Jefe de Grupo">'+uni+'</abbr></td>';
	html += '<td><button class="btn btn-danger btn-xs btn-del-usuario"><i class="fa fa-trash"></i></button></td>';
	html += '</tr>';
	
	$("table.tabla_usuario_responsable").append(html);
}

$("#btn-asignar-usuario").on("click", function() {
	if($("#combo_asignar_usuario_responsabled").required()) {
		add_usuario_responsable();
	}
	return false;
});
// combo temporal
function get_usuario_temp() {
	var html = '', id = 0;
	if($("#idusuario").val() != '') {
		id = $("#idusuario").val();
		html += '<option value="'+id+'">'+$("#idusuario option:selected").text()+'</option>';
	}
	if($(".tabla_usuario_responsable tbody tr").length) {
		$(".tabla_usuario_responsable tbody tr").each(function() {
			if(id != $("input.proy_usuario[]", this).val()) {
				html += '<option value="'+$("input.proy_usuario[]", this).val()+'">'+$("td:eq(1)", this).text()+'</option>';
			}
		});
	}
	return html;
}
//formulario para agregar las fases
function add_fases_py() {
	var id = $("#combo_asignar_fases_pyd").val();
	var desc = $("#combo_asignar_fases_pyd option:selected").text();
	
	if($("table.tabla_fases_py tr[index="+id+"]").length) {
		// ventana.alert({titulo: "Usuario de responsable asignado", 
		// mensaje: "La usuario de responsable "+desc+" ya se encuentra en la tabla"});
		$("table.tabla_fases_py tr[index="+id+"] input.proy_sueldo_usuario").focus();
		return;
	}
	var fechai = new Date();
	var fechaf = "";
	var fas = $("#idfase option:selected").text();
	if(fas == "") {
		fas = "&iquest;?";
	}
	
	var html = '<tr index="'+id+'">';
	html += '<td><input type="hidden" name="proy_fase[]" class="proy_fase" value="'+id+'"><font color="red">'+desc+'</font></td>';
	html += '<td>Fecha <strong> Inicio: </strong></td>';
	html += '<td><input type="text" class="form-control input-xs deta_fecha_i" name="deta_fecha_i[]" value="'+dateFormat(fechai, 'd/m/Y')+'"></td>';
	html += '<td>Fecha <strong>Fin:</strong></td>';
	html += '<td><input type="text" class="form-control input-xs deta_fecha_f" name="deta_fecha_f[]" value="'+dateFormat(fechaf, 'd/m/Y')+'"></td>';
	html += '<td><strong>Responsable:</strong></td>';
	html += '<td><select name="deta_usuario[]" class="form-control input-xs deta_usuario">'+$("#idusuario").html()+'</select></td>';	
	//html += '<td><select name="deta_usuario[]" class="form-control input-xs deta_usuario">'+get_usuario_temp+'</select></td>';	
	// html += '<td>RESPONSABLE <strong>del equipo en Proyecto : </strong></td>';
	// html += '<td><abbr title="Responsable del Proyecto o Jefe de Grupo">'+fas+'</abbr></td>';
	html += '<td><button class="btn btn-danger btn-xs btn-del-fase"><i class="fa fa-trash"></i></button></td>';
	html += '</tr>';
	
	//$("table.tabla_fases_py").append(html);
	//update_combo_fase_temp();
	$("table.tabla_fases_py").append(html);
	set_events_table();
	return false;
}

$("#btn-asignar-fases").on("click", function() {
	if($("#combo_asignar_fases_pyd").required()) {
		add_fases_py();
	}
	return false;
});

$("#cliente_razonsocial").autocomplete({
	source: function( request, response ) {
		ajax.post({url: _base_url+"cliente/autocomplete", data: "maxRows=50&startsWith="+request.term, dataType: 'json'}, function(data) {
			response( $.map( data, function( item ) {
				return {
					label: item.nombres + " " + item.apellidos
				   ,value: item.nombres + " " + item.apellidos
				   ,nombres: item.nombres
				   ,apellidos: item.apellidos
				   ,dni: item.dni
				   ,ruc: item.ruc
				   ,id: item.idcliente
				}
			}));
		});
	},
	select: function( event, ui ) {
		if(ui.item) {
			$("#proyecto_idcliente").val(ui.item.id);
			// get_saldo($("#compra_idcliente").val());
			is_activo();
		}
	}
}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	var html = "";
	if(item.ruc) {
		html += "<strong>RUC: "+item.ruc+"</strong>| ";
	}
	else if(item.dni) {
		html += "<strong>DNI: "+item.dni+"</strong>| ";
	}
	html += item.nombres+" "+item.apellidos;
	
	return $( "<li>" )
	.data( "ui-autocomplete-item", item )
	.append( html )
	.appendTo( ul );
};

$(document).on("click", "button.btn-del-usuario", function() {
	$(this).closest("tr").remove();
	return false;
});

$(document).on("click", "button.btn-del-fases", function() {
	$(this).closest("tr").remove();
	return false;
});

function set_events_table() {
	$("input.deta_fecha_i", ".tabla_fases_py").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		autoclose: true,
		language: 'es'
	});
	
	$("input.deta_fecha_f", ".tabla_fases_py").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		autoclose: true,
		language: 'es'
		//endDate: parseDate(_current_date)
	});
	$("input.fecha_vencimiento", "table.table_tarea_fase").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		autoclose: true,
		language: 'es'
	});
	//$("input.total", "table.tabla_fases_py").numero_real();
}

$("#btn-buscar-cliente").click(function() {
	jFrame.create({
		title: "Buscar Cliente"
		,controller: "cliente"
		,method: "grilla_popup"
		,msg: ""
		,widthclass: "modal-lg"
		,onSelect: function(datos) {
			$("#cliente_razonsocial").val(datos.cliente);
			$("#proyecto_idcliente").val(datos.idcliente);
			// get_saldo($("#compra_idcliente").val());
			is_activo();
		}
	});
	
	jFrame.show();
	return false;
});

// $("#btn-registrar-cliente").on("click", function() {
	// open_modal_cliente(true);
	// setTimeout(function(){
		// $("#"+prefix_cliente+"tipo").focus();
	// },1000);
  // return false;
// });

$("#btn-registrar-cliente").on("click", function() {
	modulo.get("cliente").nuevo();
});

modulo.register("cliente", {
	callback: function(res, redir) {
		$("#modal-"+this._controller).modal("hide");
		ajax.post({url: _base_url+"cliente/get_post",data:"id="+res}, function(rpt) {
			$("#proyecto_idcliente").val(rpt.idcliente);
			$("#cliente_razonsocial").val(rpt.nombres+" "+rpt.apellidos);
		});
	}
});

function is_activo(p) {
	return true;
	if(typeof p != "boolean")
		p = true;
	
	$(".msg-about-cliente").html("");
	$("#estado_cliente").val("");
	
	if(p) {
		var r = $("#idtipodocumento option:selected").data("ruc_obligatorio");
		if(r !== "S") {
			return;
		}
	}
	
	if($.trim($("#proyecto_idcliente").val()) != "") {
		ajax.post({url: _base_url+"cliente/is_activo/"+$("#proyecto_idcliente").val()}, function(res) {
			$("#estado_cliente").val(res.code);
			if(res.code == "ok") {
				$(".msg-about-cliente").html('<div class="alert alert-success">'+res.msg+'</div>').fadeIn();
			}
			else {
				$(".msg-about-cliente").html('<div class="alert alert-danger">'+res.msg+'</div>').fadeIn();
			}
			
			setTimeout(function(){
				$(".msg-about-cliente").fadeOut();
			},4000);
		});
	}
	else {
		ventana.alert({titulo: "Error", mensaje: "Seleccione un cliente de la lista para continuar."});
	}
}
function get_fase_temp() {
	var html = '', id = 0;
	if($("#idfase").val() != '') {
		id = $("#idfase").val();
		html += '<option value="'+id+'">'+$("#idfase option:selected").text()+'</option>';
	}
	if($(".tabla_fases_py tbody tr").length) {
		$(".tabla_fases_py tbody tr").each(function() {
			if(id != $("input.proy_fase", this).val()) {
				html += '<option value="'+$("input.proy_fase", this).val()+'">'+$("td:eq(0)", this).text()+'</option>';
			}
		});
	}
	return html;
}

$("#add_tarea_fase").on("click", function() {
	var fechavenc = $("#fecha_vencimiento").val();
	var html = '<tr>';
	html += '<td><select name="deta_idfase[]" class="deta_idfase form-control input-xs">'+get_fase_temp()+'</select></td>';
	html += '<td><input type="text" name="deta_descripcion_fase[]" class="deta_descripcion_fase form-control input-xs"></td>';
	html += '<td><select name="deta_usuariot[]" class="form-control input-xs deta_usuariot">'+$("#idusuario").html()+'</select></td>';
	html += '<td><select name="deta_estatus[]" class="form-control input-xs deta_estatus">'+$("#idestatus").html()+'</select></td>';
	html += '<td><input type="text" name="deta_tiempo_dias[]" class="deta_tiempo_dias form-control input-xs"></td>';
	html += '<td><input type="text" class="form-control input-xs deta_fecha_vencimiento" name="deta_fecha_vencimiento[]" value="'+dateFormat(fechavenc, 'd/m/Y')+'"></td>';
	html += '<td><select name="deta_prioridad[]" class="form-control input-xs deta_prioridad">'+$("#idprioridad").html()+'</select></td>';
	html += '<td><button class="btn btn-danger btn-xs btn-del-tarea-fase"><i class="fa fa-trash"></i></button></td>';
	html += '</tr>';
	
	$("table.table_tarea_fase").append(html);
	set_events_table();
	return false;
});
$(document).on("click", "button.btn-del-tarea-fase", function() {
	$(this).closest("tr").remove();
	return false;
});

function update_combo_fase_temp() {
	$("#fase_temp").html(get_fase_temp());
}

update_combo_fase_temp();

function update_combo_usuario_temp() {
	$("#usuario_temp").html(get_usuario_temp());
}
update_combo_usuario_temp();