if(typeof form == 'undefined') {
	form = {};
}

if( !$.isFunction(form.guardar_proyecto) ) {
	form.guardar_proyecto = function() {
		var data = $("#form_proyecto").serialize();
		model.save(data, function(res) {
			$("#modal-proyecto").modal("hide");
		}, "proyecto");
	}
}
form.guardar_usuario = function() {
	var data = $("#form_usuario").serialize();
	model.save(data, function(res) {
		$("#modal-usuario").modal("hide");
		reload_combo("#idusuario", {controller: "usuario"}, function() {
			$("#idusuario").val(res.idusuario);
		});
	}, "usuario");
}

// form guardar fase
form.guardar_fase = function() {
	var data = $("#form_fase").serialize();
	model.save(data, function(res) {
		$("#modal-fase").modal("hide");
		reload_combo("#idfase", {controller: "fase"}, function() {
			$("#idfase").val(res.idfase);
		});
	}, "fase");
}
validate("#form_proyecto", form.guardar_proyecto);

$("#modal-proyecto").on('shown.bs.modal', function () {
	$("#prod_descripcion").focus();
});

$("#modal-proyecto").on('hidden.bs.modal', function () {
	clear_form("#form_proyecto");
});

$(".float-number").numero_real();
$(".int-number").numero_entero();

$("#btn-registrar-marca").on("click", function() {
	$("#modal-marca").modal("show");
	return false;
});


$("#btn-registrar-usuario").on("click", function() {
	$("#modal-usuario").modal("show");
	return false;
});
