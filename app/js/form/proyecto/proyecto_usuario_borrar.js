function actualizarComboUsuarioes() {
	var exists = [];
	if($("#tabla_usuario_responsable input.idusuario").length) {
		$("#tabla_usuario_responsable input.idusuario").each(function() {
			exists.push(parseInt($(this).val()));
		});
	}
	
	var options = '';
	
	$.each(UNIDAD_MEDIDA, function(i, value) {
		id = parseInt(value.idusuario);
		if(exists.indexOf(id) != -1) {
			return true;
		}
		options += '<option value="'+value.idusuario+'" desc="'+value.descripcion+'" abbr="'+value.abreviatura+'">'+value.descripcion+' ('+value.abreviatura+')</option>';
	});
	
	$("#usuario_responsabled_filtro").html(options);
}

if(typeof form == 'undefined') {
	form = {};
}

if(typeof form.guardar_usuario_responsable != 'function') {
	form.guardar_usuario_responsable = function() {
		var data = $("#form_usuario_responsable").serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"});
		}, "proyecto", "guardar_usuario");
	}
}

function appendUsuarioResponsable(arr) {
	if(arr.length) {
		var html = '', data;
		for(var i in arr) {
			data = arr[i];
			html += '<tr data-idusuario="'+data.idusuario+'">';
			html += '<td><input type="hidden" name="idusuario[]" class="idusuario" value="'+data.idusuario+'">'+data.descripcion+' ('+data.abreviatura+')</td>';
			html += '<td><input type="text" name="cantidad_usuario[]" class="cantidad_usuario form-control input-sm" value="'+data.cantidad_usuario+'" readonly></td>';
			html += '<td><input type="text" name="cantidad_usuario_min[]" class="cantidad_usuario_min form-control input-sm" value="'+data.cantidad_usuario_min+'"></td>';
			html += '<td><button type="button" class="btn btn-default btn-xs btn_delete_usuario_responsable">Eliminar</button></td>';
			html += '</tr>';
			
		}
		$("#tabla_usuario_responsable tbody").append(html);
	}
}

actualizarComboUsuarioes();

$("#btn-add-usuario").click(function() {
	if($("#usuario_responsabled_filtro").required()) {
		if($("#tabla_usuario_responsable tbody tr[data-idusuario='"+$("#usuario_responsabled_filtro").val()+"']").length) {
			return false;
		}
		
		var data = {
			idusuario: $("#usuario_responsabled_filtro").val()
			,descripcion: $("#usuario_responsabled_filtro option:selected").attr("desc")
			,abreviatura: $("#usuario_responsabled_filtro option:selected").attr("abbr")
			,cantidad_usuario: 1
			,cantidad_usuario_min: ""
		};
		var arr = [];
		arr.push(data);
		
		appendUsuarioResponsable(arr);
		actualizarComboUsuarioes();
	}
	return false;
});

$(document).on("click", "button.btn_delete_usuario_responsable", function() {
	$(this).closest("tr").remove();
	actualizarComboUsuarioes();
	return false;
});

$("#form_usuario_responsable").submit(function() {
	return false;
});

$("#uni_prod_btn_save").on("click", function() {
	if( $("#tabla_usuario_responsable .cantidad_usuario").required() && 
	$("#tabla_usuario_responsable .cantidad_usuario_min").required() ) {
		form.guardar_usuario_responsable();
	}
	return false;
});