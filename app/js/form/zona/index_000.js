var form = {
	nuevo: function() {
		
	},
	editar: function(id) {
		// alert(id);
	},
	eliminar: function(id) {
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				grilla.reload(_default_grilla);
			});
		});
	},
	imprimir: function() {
		
	},
	guardar: function() {
		var data = $("#form_"+_controller).serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				redirect(_controller);
			});
		});
	},
	cancelar: function() {
		
	}
};

validate();

$("#descripcion").focus();

$("#btn_cancel").click(function() {
	redirect(_controller);
	return false;
});

$("#btn_ubigeo").click(function() {
	// ubigeo.set("220303"); // indicar el ubigeo inicial seleccionado
	ubigeo.ok(function(data) {
		$("#ubigeo_descr").val(data.departamento+' - '+data.provincia+' - '+data.distrito);
		$("#idubigeo").val(data.idubigeo);
	});
	ubigeo.show();
	return false;
});