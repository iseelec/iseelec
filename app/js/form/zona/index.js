modulo.register("<?php echo $controller;?>", {
	nuevo: function() {
		dialog.open("#modal-"+this._controller);
	},
	editar: function(id) {
		var sel = this;
		model.get(id, function(res) {
			$.each(res, function(k, v) {
				$("#"+sel._prefix+k, sel._form).val(v);
			});
            dialog.open("#modal-"+sel._controller);
		}, sel._controller);
	},
	eliminar: function(id) {
		var sel = this;
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				sel.callback(res);
			});
		}, sel._controller);
	},
	guardar: function() {
		var sel = this;
		var data = $(sel._form).serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				sel.callback(res);
			});
		}, sel._controller);
	}
	,callback(res, redir) {
		if(typeof redir != "boolean")
			redir = ! dialog.inModal(this._form);
		
		if(redir)
			redirect(this._controller);
		else {
			grilla.reload(_default_grilla);
			dialog.close("#modal-"+this._controller);
		}
	}
	,cancelar: function() {
		dialog.close("#modal-"+this._controller);
	}
	,init: function() { // inicializar las funciones y eventos del modulo
		validate(this._controller);
		if( ! dialog.inModal(this._form))
			$("#"+this._prefix+"descripcion").focus();
	}
	,hide_modal: function() { // funcion a ejecutar cuando se cierra la ventana modal
		clear_form(this._form);
	}
	,show_modal: function() { // funcion a ejecutar cuando se muestra la ventana modal
		
	}
});

$("#<?php echo $prefix; ?>btn_ubigeo").click(function() {
	ubigeo.ok(function(data) {
		$("#<?php echo $prefix; ?>ubigeo_descr").val(data.distrito+" - "+data.provincia+" - "+data.departamento);
		$("#<?php echo $prefix; ?>idubigeo").val(data.idubigeo);
	});
	ubigeo.show();
	return false;
});