$("#idproyecto").on("change", function() {
	var opt = {
		controller: "hojatiempo"
		,method: "options_empleado"
		,data: $("#formFiltros").serialize()
		,firstOption: {value:"", label:""}
	};
	
	reload_combo("#idusuario", opt);
	
	opt.method = "options_fase";
	reload_combo("#idfase", opt);
});

function diff_hours(hi, hf) {
	if(/\d{2}:\d{2}/.test(hi) != true || /\d{2}:\d{2}/.test(hf) != true)
		return 0;
	
	var di = new Date("<?php echo $date;?>T"+hi+":00Z");
	var df = new Date("<?php echo $date;?>T"+hf+":00Z");
	
	var diff = (df.getTime() - di.getTime()) / 1000;
	diff /= (60 * 60);
	// return round(Math.abs(diff), 2);
	return round(diff, 2);
}

$("#btnAddRow").on("click", function() {
	var t = createTr();
	t.data("id", 0);
	setTr($("#datosHojaTiempo>.ibox-content>.table-hojatiempo"), t);
	addEvents($("#datosHojaTiempo>.ibox-content>.table-hojatiempo"), t);
});

$("#btnGenerar").on("click", function() {
	var tabla = $("#datosHojaTiempo>.ibox-content>.table-hojatiempo");
	
	$("#tmpComboActividad>option").remove();
	$("#datosAnteriores>.faq-item").remove();
	$("#datosHojaTiempo>.ibox-title>.date-name").html("");
	$("#datosHojaTiempo>.ibox-title>.label").addClass("hide");
	$("tbody>tr", tabla).remove();
	$("#btnAddRow").addClass("hide").prop("disabled", true);
	$("#btnSave").addClass("hide").prop("disabled", true);
	
	ajax.post({url:_base_url+"hojatiempo/resume/", data:$("#formFiltros").serialize()}, function(res) {
		$("#datosHojaTiempo>.ibox-title>.date-name").html(res.string_date+' <small><a href="javascript:void(0)" class="change-date">(Cambiar fecha)</a></small>');
		$("#datosHojaTiempo>.ibox-title>.label").toggleClass("hide", (res.today != res.current_date));
		tabla.data(res.query);
		
		createComboItems(res.tasks);
		createTableHojaTiempo(tabla, res.current_rows);
		createTablePrevHojaTiempo(res.previous_rows);
		registrarEventoDate(".change-date");
		
		$("#btnAddRow").removeClass("hide").prop("disabled", false);
		$("#btnSave").removeClass("hide").prop("disabled", false);
		
		if(res.current_rows <= 0)
			$("#btnAddRow").trigger("click");
		else
			calcularHoras(tabla);
	});
});

$("#btnSave").on("click", function() {
	var table = $("#datosHojaTiempo>.ibox-content>.table-hojatiempo");
	
	if($("tbody>tr", table).length <= 0)
		return;
	
	if( ! $(".hora_inicio", table).required() || ! $(".hora_fin", table).required() || ! $(".idtarea", table).required())
		return;
	
	var t = [];
	$("tbody>tr", table).each(function() {
		t.push({
			idhojatiempo: $(this).data("id")
			,idtarea: $(".idtarea", this).val()
			,hora_inicio: $(".hora_inicio", this).val()
			,hora_fin: $(".hora_fin", this).val()
			,observaciones: $(".observaciones", this).val()
		});
	});
	
	var str = $.param(table.data()) + "&" + $.param({tareas: t});
	ajax.post({url:_base_url+"hojatiempo/guardar/", data:str}, function(res) {
		ventana.alert({mensaje: "Datos guardados correctamente", tipo: "success"});
	});
});

function createComboItems(a) {
	var html = '';
	if(a.length > 0) {
		for(var i in a) {
			html += '<option value="'+a[i].idtarea+'">'+a[i].descripcion+'</option>';
		}
	}
	$("#tmpComboActividad").html(html);
}

function createTr(b) {
	if(typeof b != "boolean")
		b = true;
	
	var t = $("<tr></tr>");
	t.html('<td class="text-center"></td>'+
			'<td class="text-center"></td>'+
			'<td class="text-center"></td>'+
			'<td></td>'+
			'<td class="text-center text-navy text-horas"></td>'+
			'<td></td>');
	
	if(b === true) {
		$("td:eq(0)", t).html('<a href="javascript:void(0)" title="Eliminar"><i class="fa fa-close text-danger del-row"></i></a>');
		$("td:eq(1)", t).html('<input type="text" class="form-control input-xs text-center input-time hora_inicio" placeholder="hh:mm">');
		$("td:eq(2)", t).html('<input type="text" class="form-control input-xs text-center input-time hora_fin" placeholder="hh:mm">');
		$("td:eq(3)", t).html('<select class="form-control input-xs idtarea">'+$("#tmpComboActividad").html()+'</select>');
		$("td:eq(5)", t).html('<input type="text" class="form-control input-xs observaciones">');
	}
	
	return t;
}

function setTr(tbl, t) {
	if(t.prop("tagName") == "TR")
		$("tbody", tbl).append(t);
}

function addEvents(tbl, t) {
	if($(".input-time", t).length) {
		$(".input-time", t).clockpicker({autoclose:true, afterDone:function(){calcularHoras(tbl, t);}});
		$(".input-time", t).on("keyup", function() {
			calcularHoras(tbl, t);
		});
	}
	if($(".del-row", t).length) {
		$(".del-row", t).on("click", function() {
			$(this).closest("tr").remove();
		});
	}
}

function calcularHoras(tbl, tr) {
	if(typeof tr != "undefined")
		$(".text-horas", tr).text(diff_hours($(".hora_inicio", tr).val(), $(".hora_fin", tr).val()));
	
	var t = 0;
	if($("tbody>tr", tbl).length) {
		$("tbody>tr", tbl).each(function() {
			t += Number($(".text-horas", this).text());
		});
		
		t = round(t, 2);
	}
	
	if($(".text-horas-total", tbl).length)
		$(".text-horas-total", tbl).text(t);
	
	return t;
}

function createTableHojaTiempo(tbl, a) {
	if(a.length <= 0)
		return;
	
	for(var i in a) {
		let t = createTr();
		setTr(tbl, t);
		addEvents(tbl, t);
		if(a[i].idhojatiempo)
			t.data("id", a[i].idhojatiempo);
		else
			t.data("id", 0);
		if(a[i].hora_inicio)
			$(".hora_inicio", t).val(String(a[i].hora_inicio).substr(0, 5));
		if(a[i].hora_fin)
			$(".hora_fin", t).val(String(a[i].hora_fin).substr(0, 5));
		if(a[i].idtarea)
			$(".idtarea", t).val(a[i].idtarea);
		if(a[i].horas)
			$(".text-horas", t).text(round(a[i].horas, 2));
		if(a[i].observaciones)
			$(".observaciones", t).val(a[i].observaciones);
	}
}

function createTablePrevHojaTiempo(a) {
	if(a.length <= 0)
		return;
	
	// var h = '';//50, nues
	
	$.each(a, function(i, v) {
		let div = $('<div class="faq-item"></div>');
		
		div.html('<div class="row">'+
			'<div class="col-md-6"><a data-toggle="collapse" href="#'+i+'" class="faq-question ts-fecha"></a></div>'+
			'<div class="col-md-3"><span class="small font-bold">Total horas</span><div class="tag-list"><span class="tag-item ts-horas"></span></div></div>'+
			'<div class="col-md-3 text-right"><span class="small font-bold">Entradas</span><div class="tag-list"><span class="tag-item ts-filas"></span></div></div>'+
			'</div>'+
			
			'<div class="row">'+
			'<div class="col-lg-12">'+
			'<div id="'+i+'" class="panel-collapse collapse">'+
			
			'<table class="table table-striped ts-table m-b-none m-t-sm">'+
			'<thead>'+
			'<tr>'+
			'<th style="width:25px;">&nbsp;</th>'+
			'<th style="width:65px;">Inicio</th>'+
			'<th style="width:65px;">Fin</th>'+
			'<th>Tarea</th>'+
			'<th style="width:60px;">Horas</th>'+
			'<th>Observaciones</th>'+
			'</tr>'+
			'</thead>'+
			'<tbody></tbody>'+
			'</table>'+
			
			'</div>'+
			'</div>'+
			'</div>'+
			'');
		
		$("#datosAnteriores").append(div);
		$(".ts-filas", div).text(v.length);
		
		let th = 0, sf = "";
		
		for(let j in v) {
			let t = createTr(false);
			
			$("td", t).removeClass("text-center");
			
			if(v[j].hora_inicio)
				$("td:eq(1)", t).text(String(v[j].hora_inicio).substr(0, 5));
			if(v[j].hora_fin)
				$("td:eq(2)", t).text(String(v[j].hora_fin).substr(0, 5));
			if(v[j].tarea)
				$("td:eq(3)", t).text(v[j].tarea);
			if(v[j].observaciones)
				$("td:eq(5)", t).text(v[j].observaciones);
			if(v[j].horas) {
				$("td:eq(4)", t).text(round(v[j].horas, 2));
				th += round(v[j].horas, 2);
			}
			if(v[j].string_date)
				sf = v[j].string_date;
			
			$(".ts-table>tbody", div).append(t);
		}
		
		$(".ts-fecha", div).text(sf);
		$(".ts-horas", div).text(th);
	});
}

function registrarEventoDate(elem) {
	$(elem).datepicker({
		autoclose: true
		,language: 'es'
		,format: 'dd/mm/yyyy'
		,todayHighlight: true
		,endDate: '0d'
	}).on('changeDate', function(e){
		$("#fecha").val(e.format());
		$("#btnGenerar").trigger("click");
    });
}