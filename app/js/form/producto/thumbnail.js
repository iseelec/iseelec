function getRecords(page) {
	if(typeof page == "undefined")
		page = 0;
	
	var data = {
		page: page
		,query: $("#inputQuery").val()
	};
	
	ajax.post({url:_base_url+"producto/get_records/", data:data}, function(res) {
		$(".btn-paginate").data("more", res.more);
		$(".btn-paginate").data("page", res.page);
		setFilas(res.rows);
		$("#infoPage").text(res.begin+"-"+res.end+" / "+res.total);
	});
}

function setFilas(arr) {
	var panel = $("#product-list");
	$("div.item-product-box", panel).remove();
	
	if(arr.length <= 0)
		return;
	
	var div = null, i;
	
	for(i in arr) {
		div = $('<div class="col-md-4 col-lg-3 col-sm-2 item-product-box"></div>');
		div.data(arr[i]);
		panel.append(div);
		
		div.html(''+
			'<div class="contact-box"><a href="javascript:void(0)">'+
			'<div class="col-sm-4">'+
			'<div class="text-center"><img alt="..." class="m-t-xs img-responsive" src="'+_base_url+'app/img/producto/'+arr[i].imagen+'"></div>'+
			'</div>'+
			'<div class="col-sm-8">'+
			'<h6><strong>'+arr[i].producto+'</strong></h6><p>Precio: '+arr[i].precio+'</p>'+
			'</div>'+
			'<div class="clearfix"></div></a></div>'+
			'');
	}
	
	$(window).scrollTop(0);
}

$("#btnSearch").on("click", function() {
	getRecords();
});

$("#inputQuery").on("keypress", function(e) {
	if(e.which == 13) {
		e.preventDefault();
		$("#btnSearch").trigger("click");
	}
});

$(".btn-paginate").on("click", function() {
	if($(this).hasClass("next") && $(this).data("more") != true)
		return;
	
	var page = $(this).data("page") + $(this).data("dir");
	if(page < 0)
		return;
	
	getRecords(page);
});

$("#product-list").on("click", ".item-product-box>.contact-box>a", function() {
	var thisDiv = $(this).closest(".contact-box");
	$("#product-list>.item-product-box>.contact-box").not(thisDiv).removeClass("selected");
	thisDiv.toggleClass("selected");
});

(function() {
	getRecords();
})();