function actualizarComboSecciones() {
	var exists = [];
	if($("#tabla_seccion_medida input.idseccion").length) {
		$("#tabla_seccion_medida input.idseccion").each(function() {
			exists.push(parseInt($(this).val()));
		});
	}
	
	var options = '';
	
	$.each(SECCION_MEDIDA, function(i, value) {
		id = parseInt(value.idseccion);
		if(exists.indexOf(id) != -1) {
			return true;
		}
		options += '<option value="'+value.idseccion+'" desc="'+value.descripcion+'" abbr="'+value.abreviatura+'">'+value.descripcion+' ('+value.abreviatura+')</option>';
	});
	
	$("#seccion_medidad_filtro").html(options);
}

if(typeof form == 'undefined') {
	form = {};
}

if(typeof form.guardar_seccion_medida != 'function') {
	form.guardar_seccion_medida = function() {
		var data = $("#form_seccion_medida").serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"});
		}, "producto", "guardar_seccion");
	}
}

function appendSeccionMedida(arr) {
	if(arr.length) {
		var html = '', data;
		for(var i in arr) {
			data = arr[i];
			html += '<tr data-idseccion="'+data.idseccion+'">';
			html += '<td><input type="hidden" name="idseccion[]" class="idseccion" value="'+data.idseccion+'">'+data.descripcion+' ('+data.abreviatura+')</td>';
			html += '<td><input type="text" name="cantidad_seccion[]" class="cantidad_seccion form-control input-sm" value="'+data.cantidad_seccion+'" readonly></td>';
			html += '<td><input type="text" name="cantidad_seccion_min[]" class="cantidad_seccion_min form-control input-sm" value="'+data.cantidad_seccion_min+'"></td>';
			html += '<td><button type="button" class="btn btn-default btn-xs btn_delete_seccion_medida">Eliminar</button></td>';
			html += '</tr>';
			
		}
		$("#tabla_seccion_medida tbody").append(html);
	}
}

actualizarComboSecciones();

$("#btn-add-seccion").click(function() {
	if($("#seccion_medidad_filtro").required()) {
		if($("#tabla_seccion_medida tbody tr[data-idseccion='"+$("#seccion_medidad_filtro").val()+"']").length) {
			return false;
		}
		
		var data = {
			idseccion: $("#seccion_medidad_filtro").val()
			,descripcion: $("#seccion_medidad_filtro option:selected").attr("desc")
			,abreviatura: $("#seccion_medidad_filtro option:selected").attr("abbr")
			,cantidad_seccion: 1
			,cantidad_seccion_min: ""
		};
		var arr = [];
		arr.push(data);
		
		appendSeccionMedida(arr);
		actualizarComboSecciones();
	}
	return false;
});

$(document).on("click", "button.btn_delete_seccion_medida", function() {
	$(this).closest("tr").remove();
	actualizarComboSecciones();
	return false;
});

$("#form_seccion_medida").submit(function() {
	return false;
});

$("#uni_prod_btn_save").on("click", function() {
	if( $("#tabla_seccion_medida .cantidad_seccion").required() && 
	$("#tabla_seccion_medida .cantidad_seccion_min").required() ) {
		form.guardar_seccion_medida();
	}
	return false;
});