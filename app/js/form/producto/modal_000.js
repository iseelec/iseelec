if(typeof form == 'undefined') {
	form = {};
}

if( !$.isFunction(form.guardar_producto) ) {
	form.guardar_producto = function() {
		var data = $("#form_producto").serialize();
		model.save(data, function(res) {
			$("#modal-producto").modal("hide");
		}, "producto");
	}
}

form.guardar_marca = function() {
	var data = $("#form_marca").serialize();
	model.save(data, function(res) {
		$("#modal-marca").modal("hide");
		reload_combo("#idmarca", {controller: "marca"}, function() {
			$("#idmarca").val(res.idmarca);
		});
	}, "marca");
}

form.guardar_modelo = function() {
	var data = $("#form_modelo").serialize();
	model.save(data, function(res) {
		$("#modal-modelo").modal("hide");
		reload_combo("#idmodelo", {controller: "modelo", data: "idmarca="+$("#idmarca").val()}, function() {
			$("#idmodelo").val(res.idmodelo);
		});
	}, "modelo");
}

form.guardar_unidad = function() {
	var data = $("#form_unidad").serialize();
	model.save(data, function(res) {
		$("#modal-unidad").modal("hide");
		reload_combo("#idunidad", {controller: "unidad"}, function() {
			$("#idunidad").val(res.idunidad);
		});
	}, "unidad");
}

validate("#form_producto", form.guardar_producto);

$("#modal-producto").on('shown.bs.modal', function () {
	$("#prod_descripcion").focus();
});

$("#modal-producto").on('hidden.bs.modal', function () {
	clear_form("#form_producto");
});

$(".float-number").numero_real();
$(".int-number").numero_entero();

$("#btn-registrar-marca").on("click", function() {
	$("#modal-marca").modal("show");
	return false;
});

$("#btn-registrar-modelo").on("click", function() {
	if( $("#idmarca").required() ) {
		$("#mod_idmarca").val($("#idmarca").val());
		$("#modal-modelo").modal("show");
	}
	return false;
});

$(document).on("change", "#idmarca", function() {
	$("#idmodelo").html('<option value=""></option>');
	if($(this).val() != "") {
		reload_combo("#idmodelo", {controller: "modelo", data: "idmarca="+$(this).val()});
	}
});

$("#btn-registrar-unidad").on("click", function() {
	$("#modal-unidad").modal("show");
	return false;
});