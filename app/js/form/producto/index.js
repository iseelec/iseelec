modulo.register("<?php echo $controller;?>", {
	nuevo: function() {
		dialog.open("#modal-"+this._controller);
	},
	editar: function(id) {
		if(id == 0) {
			if($("#product-list>.item-product-box>.contact-box.selected").length <= 0) {
				ventana.alert({titulo: "Aviso", mensaje: "Seleccione un registro de la tabla"});
				return;
			}
			id = $("#product-list>.item-product-box>.contact-box.selected").closest(".item-product-box").data("idproducto");
		}
		
		var sel = this;
		model.get(id, function(res) {
			$.each(res.producto, function(k, v) {
				if($("#"+sel._prefix+k, sel._form).is(":checkbox"))
					$("#"+sel._prefix+k, sel._form).prop("checked", (v == "S"));
				else
					$("#"+sel._prefix+k, sel._form).val(v);
			});
			$("#"+sel._prefix+"corr_temp").val(res.producto.nro_codigo_producto);
			if(res.categoria)
				$("#"+sel._prefix+"categoria").val(res.categoria.descripcion);
			if(res.modelo)
				$("#"+sel._prefix+"modelo").val(res.modelo.descripcion);
			if(res.marca)
				$("#"+sel._prefix+"marca").val(res.marca.descripcion);
			if(res.color)
				$("#"+sel._prefix+"color").val(res.color.descripcion);
			if(res.material)
				$("#"+sel._prefix+"material").val(res.material.descripcion);
			if(res.tamanio)
				$("#"+sel._prefix+"tamanio").val(res.tamanio.descripcion);
			
			if(res.producto.imagen_producto)
				$("#"+sel._prefix+"load_photo").html('<img src="'+_base_url+'app/img/producto/'+res.producto.imagen_producto+'" class="img-responsive img-thumbnail" />');
			
            dialog.open("#modal-"+sel._controller);
		}, sel._controller, "get_edit_ajax");
	},
	eliminar: function(id) {
		if(id == 0) {
			if($("#product-list>.item-product-box>.contact-box.selected").length <= 0) {
				setTimeout(function() {
					ventana.alert({titulo: "Aviso", mensaje: "Seleccione un registro de la tabla"});
				}, 200);
				return;
			}
			id = $("#product-list>.item-product-box>.contact-box.selected").closest(".item-product-box").data("idproducto");
		}
		
		var sel = this;
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				sel.callback(res);
			});
		}, sel._controller);
	},
	guardar: function() {
		if( ! $("#"+this._prefix+"descripcion_detallada").required())
			return;
		
		// validamos tab 1
		var b = true && $("#"+this._prefix+"idtipo_producto").required();
		b = b && $("#"+this._prefix+"categoria").required();
		if($("#"+this._prefix+"idtipo_producto").val() != "2") {
			// b = b && $("#"+this._prefix+"modelo").required();
			b = b && $("#"+this._prefix+"marca").required();
		}
		b = b && $("#"+this._prefix+"descripcion").required();
		//b = b && $("#"+this._prefix+"codigo_barras").required();
		b = b && $("#"+this._prefix+"precio_compra").required({numero:true, tipo:"float", aceptaCero:true});
		b = b && $("#"+this._prefix+"margen_venta").required({numero:true, tipo:"float", aceptaCero:true});
		b = b && $("#"+this._prefix+"precio_mercado").required({numero:true, tipo:"float"});
		b = b && $("#"+this._prefix+"idunidad").required();
		if( ! b) {
			$("#tabs_"+this._controller+" a[href='#tab-1']").tab("show");
			return;
		}
		if( ! $("#"+this._prefix+"idcategoria").required()) {
			ventana.alert({mensaje: "Seleccione una categoria de la lista o registre una nueva categoria"});
			$("#tabs_"+this._controller+" a[href='#tab-1']").tab("show");
			return;
		}
		if($("#"+this._prefix+"idtipo_producto").val() != "2") {
			// if( ! $("#"+this._prefix+"idmodelo").required()) {
				// ventana.alert({mensaje: "Seleccione un modelo de la lista o registre un nuevo modelo"});
				// $("#tabs_"+this._controller+" a[href='#tab-1']").tab("show");
				// return;
			// }
			if( ! $("#"+this._prefix+"idmarca").required()) {
				ventana.alert({mensaje: "Seleccione una marca de la lista o registre una nueva marca"});
				$("#tabs_"+this._controller+" a[href='#tab-1']").tab("show");
				return;
			}
		}
		
		// tab 2 no es necesario validar segun las indicaciones
		// validamos tab 3
		// quitamos la validación del tab 3
		// b = b && $("#"+this._prefix+"descripcion_cli").required();
		// b = b && $("#"+this._prefix+"descripcion_pro").required();
		// if( ! b) {
			// $("#tabs_"+this._controller+" a[href='#tab-3']").tab("show");
			// return;
		// }
		
		if(b)
			Save_action("form_"+this._controller);
	},
	cancelar: function() {
		dialog.close("#modal-"+this._controller);
	}
	,callback(res, redir) {
		if(typeof redir != "boolean")
			redir = ! dialog.inModal(this._form);
		
		if(redir)
			redirect(this._controller);
		else {
			if(typeof _default_grilla != "undefined")
				grilla.reload(_default_grilla);
			else if(typeof getRecords == "function")
				getRecords();
			dialog.close("#modal-"+this._controller);
		}
	}
	,hide_modal: function() { // funcion a ejecutar cuando se cierra la ventana modal
		clear_form(this._form);
		$("#"+this._prefix+"load_photo>img").remove();
		$("#tabs_"+this._controller+" a[href='#tab-1']").tab("show");
	}
	,show_modal: function() { // funcion a ejecutar cuando se muestra la ventana modal
		
	}
	,init: function() {
		if( ! dialog.inModal(this._form))
			$("#"+this._prefix+"descripcion_detallada").focus();
	}
});

function calCostosoles() {
	if( $.isNumeric($("#<?php echo $prefix;?>precio_dolar").val()) ) {
		var costod = parseFloat($("#<?php echo $prefix;?>precio_dolar").val());
		var valor_dolar = parseFloat($("#<?php echo $prefix;?>valor_moneda").val());

		total = costod * valor_dolar;
		$("#<?php echo $prefix;?>precio_compra").val(total.toFixed(2));
		return;
	}
	$("#<?php echo $prefix;?>precio_compra").val("");
}
$("#<?php echo $prefix;?>precio_dolar").on("keyup", function() {
	calCostosoles();
	
});

function calPrecioventa() {
	if( $.isNumeric($("#<?php echo $prefix;?>precio_compra").val()) ) {
		var precioc = parseFloat($("#<?php echo $prefix;?>precio_compra").val());
		var margenv = parseFloat($("#<?php echo $prefix;?>margen_venta").val());

		precio_m = precioc * (1+(margenv/100));
		$("#<?php echo $prefix;?>precio_mercado").val(precio_m.toFixed(2));
		return;
	}
	$("#<?php echo $prefix;?>precio_mercado").val("");
}
$("#<?php echo $prefix;?>margen_venta").on("keyup", function() {
	calPrecioventa();
	
});

$("#<?php echo $prefix;?>load_photo").click(function() {
    $("#<?php echo $prefix;?>file").click();
});

$("#<?php echo $prefix;?>btn_save").click(function() {
	modulo.get("<?php echo $controller;?>").guardar();
	return false;
});

$(".float-number").numero_real();
$("#<?php echo $prefix;?>peso").numero_real();
$(".int-number").numero_entero();

function Save_action(id){//id= id del formulario
	var fd = new FormData(document.getElementById(id));
	fd.append("response", "ajax");
	fd.append("type", "json");
	$.ajax({
		url: _base_url+"producto/guardar",
		type: "POST",
		data: fd,
		dataType: "json",
		enctype: 'multipart/form-data',
		processData: false,  // tell jQuery not to process the data
		contentType: false   // tell jQuery not to set contentType
	}).done(function( res ) {
		if(res.code == "ERROR") {
			ventana.alert({mensaje:res.message, tipo:"error"});
			return;
		}
		ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
			modulo.get("<?php echo $controller;?>").callback(res.data);
		});
	});
	return false;
}

$(".btn-registrar-unidad").on("click", function() {
	$("#modal-unidad").modal("show");
	return false;
});

function get_prefijo() {
	var str = "";
	if(typeof $("#<?php echo $prefix;?>idcategoria").data("data") != "undefined")
		str += $.trim($("#<?php echo $prefix;?>idcategoria").data("data").prefijo);
	if(typeof $("#<?php echo $prefix;?>idmarca").data("data") != "undefined")
		str += $.trim($("#<?php echo $prefix;?>idmarca").data("data").prefijo);
	// if(typeof $("#<?php echo $prefix;?>idmodelo").data("data") != "undefined")
		// str += $.trim($("#<?php echo $prefix;?>idmodelo").data("data").prefijo);
	
	if($.trim(str) != "")
		$("#<?php echo $prefix;?>pref_codigo_producto").val(str);
}


function leerarchivoprod(f) {
    if (f.files.length != 0 && f.files[0].type.match(/image.*/)) {
		var lecimg = new FileReader();
		lecimg.onload = function(e) {
			var img = $('<img class="img-responsive img-thumbnail" />');
			img.prop("src", e.target.result);
			$("#<?php echo $prefix;?>load_photo").html(img);
		};
		lecimg.onerror = function(e) { 
			alert("Error leyendo la imagen!!");
		};
		lecimg.readAsDataURL(f.files[0]);
    } else {
		$("#<?php echo $prefix;?>load_photo>img").remove();
		alert("Seleccione una imagen!!");
    }
}

$(document).ready(function(){
	input.autocomplete({
		selector: "#<?php echo $prefix;?>categoria"
		,controller: "categoria"
		,label: "[descripcion]"
		,value: "[descripcion]"
		,highlight: true
		,show_new_item: true
		,appendTo: $("#<?php echo $prefix;?>categoria").closest(".form-group")
		,onSelect: function(item) {
			$("#<?php echo $prefix;?>idcategoria").val(item.idcategoria).data("data", item);
			get_prefijo();
		}
		,onNewItem: function(term) {
			$("#cat_descripcion").val(term);
			$("#modal-categoria").modal("show");
		}
	});

	input.autocomplete({
		selector: "#<?php echo $prefix;?>marca"
		,controller: "marca"
		,label: "[descripcion]"
		,value: "[descripcion]"
		,highlight: true
		,show_new_item: true
		,appendTo: $("#<?php echo $prefix;?>marca").closest(".form-group")
		,onSelect: function(item) {
			$("#<?php echo $prefix;?>idmarca").val(item.idmarca).data("data", item);
			get_prefijo();
		}
		,onNewItem: function(term) {
			$("#mar_descripcion").val(term);
			$("#modal-marca").modal("show");
		}
	});

	// input.autocomplete({
		// selector: "#<?php echo $prefix;?>modelo"
		// ,controller: "modelo"
		// ,label: "[descripcion]"
		// ,value: "[descripcion]"
		// ,highlight: true
		// ,show_new_item: true
		// ,appendTo: $("#<?php echo $prefix;?>modelo").closest(".form-group")
		// ,onSelect: function(item) {
			// $("#<?php echo $prefix;?>idmodelo").val(item.idmodelo).data("data", item);
			// get_prefijo();
		// }
		// ,onNewItem: function(term) {
			// $("#mod_descripcion").val(term);
			// $("#modal-modelo").modal("show");
		// }
	// });

	input.autocomplete({
		selector: "#<?php echo $prefix;?>color"
		,controller: "color"
		,label: "[descripcion]"
		,value: "[descripcion]"
		,highlight: true
		,show_new_item: true
		,appendTo: $("#<?php echo $prefix;?>color").closest(".form-group")
		,onSelect: function(item) {
			$("#<?php echo $prefix;?>idcolor").val(item.idcolor);
		}
		,onNewItem: function(term) {
			$("#col_descripcion").val(term);
			$("#modal-color").modal("show");
		}
	});

	input.autocomplete({
		selector: "#<?php echo $prefix;?>material"
		,controller: "material"
		,label: "[descripcion]"
		,value: "[descripcion]"
		,highlight: true
		,show_new_item: true
		,appendTo: $("#<?php echo $prefix;?>material").closest(".form-group")
		,onSelect: function(item) {
			$("#<?php echo $prefix;?>idmaterial").val(item.idmaterial);
		}
		,onNewItem: function(term) {
			$("#mat_descripcion").val(term);
			$("#modal-material").modal("show");
		}
	});

	input.autocomplete({
		selector: "#<?php echo $prefix;?>tamanio"
		,controller: "tamanio"
		,label: "[descripcion]"
		,value: "[descripcion]"
		,highlight: true
		,show_new_item: true
		,appendTo: $("#<?php echo $prefix;?>tamanio").closest(".form-group")
		,onSelect: function(item) {
			$("#<?php echo $prefix;?>idtamanio").val(item.idtamanio);
		}
		,onNewItem: function(term) {
			$("#tam_descripcion").val(term);
			$("#modal-tamanio").modal("show");
		}
	});

	input.autocomplete({
		selector: "#<?php echo $prefix;?>descripcion"
		,controller: "producto"
		,method: "autocomplete_descripcion"
		,label: "[descripcion]"
		,value: "[descripcion]"
		,highlight: true
		,appendTo: $("#<?php echo $prefix;?>descripcion").closest(".form-group")
	});
});

modulo.register("categoria", {
	callback: function(res, redir) {
		$("#modal-categoria").modal("hide");
		$("#<?php echo $prefix;?>categoria").val(res.descripcion);
		$("#<?php echo $prefix;?>idcategoria").val(res.idcategoria).data("data", res);
		get_prefijo();
	}
});

modulo.register("marca", {
	callback: function(res, redir) {
		$("#modal-marca").modal("hide");
		$("#<?php echo $prefix;?>marca").val(res.descripcion);
		$("#<?php echo $prefix;?>idmarca").val(res.idmarca).data("data", res);
		get_prefijo();
	}
});

// modulo.register("modelo", {
	// callback: function(res, redir) {
		// $("#modal-modelo").modal("hide");
		// $("#<?php echo $prefix;?>modelo").val(res.descripcion);
		// $("#<?php echo $prefix;?>idmodelo").val(res.idmodelo).data("data", res);
		// get_prefijo();
	// }
// });

modulo.register("color", {
	callback: function(res, redir) {
		$("#modal-color").modal("hide");
		$("#<?php echo $prefix;?>color").val(res.descripcion);
		$("#<?php echo $prefix;?>idcolor").val(res.idcolor);
	}
});

modulo.register("tamanio", {
	callback: function(res, redir) {
		$("#modal-tamanio").modal("hide");
		$("#<?php echo $prefix;?>tamanio").val(res.descripcion);
		$("#<?php echo $prefix;?>idtamanio").val(res.idtamanio);
	}
});

modulo.register("material", {
	callback: function(res, redir) {
		$("#modal-material").modal("hide");
		$("#<?php echo $prefix;?>material").val(res.descripcion);
		$("#<?php echo $prefix;?>idmaterial").val(res.idmaterial);
	}
});

modulo.register("unidad", {
	callback: function(res, redir) {
		$("#modal-unidad").modal("hide");
		reload_combo("#<?php echo $prefix;?>idunidad", {controller: "unidad"}, function() {
			$("#<?php echo $prefix;?>idunidad").val(res.idunidad);
		});
	}
});