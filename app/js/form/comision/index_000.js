$("#idempresa").on("change", function() {
	reload_combo("#idsucursal", {controller:"sucursal", data:"idempresa="+$(this).val()}, function() {
		$("#idsucursal").trigger("change");
	});
});

$("#idsucursal").on("change", function() {
	reload_combo("#anio", {controller:"comision", method:"get_anios", data:"idsucursal="+$(this).val()});
	reload_combo("#idempleado", {controller:"comision", method:"get_empleados", data:"idsucursal="+$(this).val()}, function() {
		$("#idempleado").trigger("chosen:updated");
	});
});

$("#idempleado").chosen();

$("#btn-listar").on("click", function(e) {
	e.preventDefault();
	if($("#idempresa").required()) {
		verificarEstado();
		reloadTabla();
	}
});

function verificarEstado() {
	if($("#idsucursal").required() && $("#anio").required() && $("#mes").required()) {
		$("#msg-info-mes").html("");
		$("#msg-param-comision").html("");
		
		var str = $("#frm-filtros").serialize();
		
		ajax.post({url: _base_url+"comision/get_estado", data: str}, function(res) {
			$("#msg-info-mes").html(res.info);
			$("#msg-param-comision").html(res.tabla);
		});
	}
}
function reloadTabla() {
if($("#idsucursal").required() && $("#anio").required() && $("#mes").required()) {
		$("#msg-info-mes").html("");
		$("#msg-param-comision").html("");
		
		var str = $("#frm-filtros").serialize()+"&idsucursal="+$("#idsucursal").val();
		
		ajax.post({url: _base_url+"comision/get_tabladet", data: str}, function(res) {
			$("#table-letras tbody").empty();

			if(res){
				var html ='';
				$(res.detalle_pagos).each(function(i,j){
					html+="<tr>";
					html+="	<td><small>"+j.fecha_venta+"</small></td>";
					html+="	<td><small>"+j.comprobante+"</small></td>";
					html+="	<td><small>"+j.totventa+"</small></td>";
					html+="	<td><small>"+j.fecha_venta+"</small></td>";
					html+="	<td align = 'right'><small>"+j.monto+"</small></td>";
					html+="	<td><small>"+j.fecha_venta+"</small></td>";
					html+="	<td align = 'right'><small>"+j.nrodias+"</small></td>";
					html+="	<td align = 'right'><small>"+j.porcentaje+"%</small></td>";
					html+="	<td align = 'right'><small>"+j.comision+"</small></td>";
					//continua las delas columnas, copia y pega no mas
					
					html+="</tr>";
				});
				$("#table-letras tbody").html(html);
			}
		});

		// aqui jalar los registros, mostrar boton [Guardar y Cerrar]
	}	
}
(function() {
	verificarEstado();
})();