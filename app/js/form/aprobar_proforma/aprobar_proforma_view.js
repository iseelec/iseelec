if(typeof form == 'undefined') {
	form = {};
}

$("#form_aprobar_proforma").submit(function() {
	return false;
});

function detele_aprobar_proforma(indice) {
	idproforma = $("#idproforma").val();
	kardex = $("#kardex"+indice).val();
	alma =   $("#alma"+indice).val();
	produc = $("#produc"+indice).val();
	tipo_docu = $("#tipo_docu"+indice).val();
	serie = $("#serie"+indice).val();
	numero = $("#numero"+indice).val();
	
	ventana.confirm({titulo:"Confirmar",
		mensaje:"¿Desea eliminar el registro seleccionado?",
		textoBotonAceptar: "Eliminar"}, function(ok){
			if(ok) {
				str = "idproforma="+idproforma+"&kardex="+kardex+"&alma="+alma+"&produc="+produc+"&tipo_docu="+tipo_docu+"&serie="+serie+"&numero="+numero;
				form.eliminar_aprobar_proforma(str);
			}
		});
}