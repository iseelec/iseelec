modulo.register("<?php echo $controller;?>", {
	nuevo: function() {
		
	},
	editar: function(id) {
		// alert(id);
	},
	eliminar: function(id) {
		var sel = this;
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				sel.callback(res);
			});
		}, sel._controller);
	},
	imprimir: function() {
		
	},
	guardar: function() {
		var sel = this;
		var data = $(sel._form).serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				sel.callback(res, true);
			});
		}, sel._controller);
	}
	,callback(res, redir) {
		if(typeof redir != "boolean")
			redir = false;
		
		if(redir)
			redirect(this._controller);
		else
			grilla.reload(_default_grilla);
	}
	,cancelar: function() {
		
	}
	,init: function() { // inicializar las funciones y eventos del modulo
		validate(this._controller);
		$("#"+this._prefix+"ocupacion").focus();
	}
	,hide_modal: function() { // funcion a ejecutar cuando se cierra la ventana modal
		clear_form(this._form);
	}
	,show_modal: function() { // funcion a ejecutar cuando se muestra la ventana modal
		
	}
});