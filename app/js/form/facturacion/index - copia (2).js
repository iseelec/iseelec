var ListaSituacion = [
	{"id":"01","nombre":"Por Generar XML"},
    {"id":"02","nombre":"XML Generado"},
    {"id":"03","nombre":"Enviado y Aceptado SUNAT"},
    {"id":"04","nombre":"Enviado y Aceptado SUNAT con Obs."},
    {"id":"05","nombre":"Enviado y Anulado por SUNAT"},
    {"id":"06","nombre":"Con Errores"},
    {"id":"07","nombre":"Por Validar XML"},
    {"id":"08","nombre":"Enviado a SUNAT Por Procesar"},
    {"id":"09","nombre":"Enviado a SUNAT Procesando"},
    {"id":"10","nombre":"Rechazado por SUNAT"}
];

var TipoComprobante = [
    {"id":"01","nombre":"Factura"},
    {"id":"03","nombre":"Boleta de Venta"},
    {"id":"07","nombre":"Nota de Credito"},
    {"id":"08","nombre":"Nota de Debito"},
    {"id":"RA","nombre":"Comunicación de Baja"}
];

function getSituacion(v) {
	for(var i in ListaSituacion) {
		if(ListaSituacion[i].id == v)
			return ListaSituacion[i].nombre;
	}
	
	return "-";
}

function getComprobante(v) {
	for(var i in TipoComprobante) {
		if(TipoComprobante[i].id == v)
			return TipoComprobante[i].nombre;
	}
	
	return "-";
}

function updateRow(tr, data) {
	$("td:eq(0)", tr).text(data.num_ruc);
	$("td:eq(1)", tr).text(getComprobante(data.tip_docu));
	$("td:eq(2)", tr).text(data.num_docu);
	$("td:eq(4)", tr).text(data.fec_carg);
	$("td:eq(5)", tr).text(data.fec_gene);
	$("td:eq(6)", tr).text(data.fec_envi);
	$("td:eq(7)", tr).text(getSituacion(data.ind_situ));
	$("td:eq(8)", tr).text(data.des_obse);
}

function getRecords() {
	ajax.post({url:_base_url+"facturacion/get_records", data:$("#form-data").serialize()}, function(res) {
		$("#tabla-result tbody").html(res);
	});
}

function init() {
	getRecords();
}

$("select").change(function(e) {
	$("#btnsearch").trigger("click");
});

$("#btnsearch").click(function(e) {
	e.preventDefault();
	getRecords();
});

$("#tabla-result").on("click", "tbody tr", function() {
	if($(this).hasClass("empty-rs"))
		return;
	
	if($(this).hasClass("active"))
		$("#tabla-result tbody tr").removeClass("active");
	else {
		$("#tabla-result tbody tr").removeClass("active");
		$(this).addClass("active");
	}
});

$("#btnupdate").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	var str = "idreferencia="+tr.data("idref")+"&referencia="+tr.data("ref");
	
	ajax.post({url:_base_url+"facturacion/update", data: str}, function(res) {
		updateRow(tr, res);
	});
});

$("#btnprint").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	open_url(tr.data("ref")+"/imprimir/"+tr.data("idref"));
});

$("#btngenerar").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	var str = "idreferencia="+tr.data("idref")+"&referencia="+tr.data("ref");
	
	ajax.post({url:_base_url+"facturacion/generar", data: str}, function(res) {
		updateRow(tr, res);
	});
});

$("#btnsend").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	var str = "idreferencia="+tr.data("idref")+"&referencia="+tr.data("ref");
	
	ajax.post({url:_base_url+"facturacion/send", data: str}, function(res) {
		updateRow(tr, res);
	});
});

$("#btnbaja").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	
	if(tr.data("tdoc") != "RA") {
		ventana.prompt({titulo:"",
			mensaje:"&iquest;Desea dar de baja al comprobante "+$("td:eq(2)",tr).text()+"?",
			tipo: false,
			textoBotonAceptar: "Dar de baja",
			placeholder: 'Ingrese algun motivo'
		}, function(inputValue){
			if(inputValue === false)
				return false;
			
			if (inputValue === "") {
				swal.showInputError("Ingrese el motivo para dar de baja");
				return false
			}
			
			var str = "idreferencia="+tr.data("idref")+"&referencia="+tr.data("ref")+"&motivo="+inputValue;
			
			ajax.post({url:_base_url+"facturacion/baja", data: str}, function(res) {
				getRecords();
			});
		});
	}
	else {
		ventana.alert({titulo:'',mensaje:'Seleccione el comprobante que desea dar de baja.'});
	}
});

init();