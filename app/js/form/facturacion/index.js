$('#fechai,#fechaf,#fecha_resumen_diario').datepicker({
	todayHighlight: true
	,autoclose: true
	,language: 'es'
	,format: 'dd/mm/yyyy'
});

$("#btnsearch").click(function(e) {
	e.preventDefault();
	getRecords();
});

$("#tabla-result").on("click", "tbody>tr", function() {
	if($(this).hasClass("empty-rs"))
		return;
	$("#tabla-result>tbody>tr").not(this).removeClass("active");
	$(this).toggleClass("active");
});

function updateRow(tr, data) {
	if(data) {
		for(var i in table_columns) {
			$("td:eq("+i+")", tr).text(data[table_columns[i]]);
		}
	}
}

function getRecords() {
	ajax.post({url:_base_url+"facturacion/get_records", data:$("#form-data").serialize()}, function(res) {
		createTr(res);
	});
}

function createTr(arr) {
	$("#tabla-result>tbody>tr").remove();
	if(arr.length) {
		$.each(arr, function(i, v) {
			var h = '';
			for(var j in table_columns)
				h += '<td>'+v[table_columns[j]]+'</td>';
			tr = $('<tr></tr>');
			tr.data(v);
			tr.html(h);
			$("#tabla-result>tbody").append(tr);
		});
	}
	else {
		var tr = '<tr class="empty-rs"><td colspan="'+table_columns.length+
			'"><i>No se han encontrado resultados segun los criterios de b&uacute;squeda.</i></td></tr>';
		$("#tabla-result>tbody").append(tr);
	}
}

function init() {
	getRecords();
}

$("#btnupdate").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	var str = "idreferencia="+tr.data("idreferencia")+"&referencia="+tr.data("referencia")+"&udt=1";
	
	ajax.post({url:_base_url+"facturacion/update", data: str}, function(res) {
		updateRow(tr, res);
	});
});

$("#btngenerar").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	var str = "idreferencia="+tr.data("idreferencia")+"&referencia="+tr.data("referencia");
	
	ajax.post({url:_base_url+"facturacion/generar", data: str}, function(res) {
		updateRow(tr, res);
	});
});

$("#btnsend").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	var str = "idreferencia="+tr.data("idreferencia")+"&referencia="+tr.data("referencia");
	
	ajax.post({url:_base_url+"facturacion/send", data: str}, function(res) {
		updateRow(tr, res);
	});
});

$("#btnprint").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	open_url(tr.data("referencia")+"/imprimir/"+tr.data("idreferencia"));
});

$("#btndescargar").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	var tr = $("#tabla-result tbody tr.active");
	open_url("facturacion/download?idreferencia="+tr.data("idreferencia")+"&referencia="+tr.data("referencia"));
});

$("#btnresumen").on("click", function() {
	$("#modal-resumenDiario").modal("show");
});
$("#btnSaveResumenDiario").on("click", function() {
	if( ! $("#fecha_resumen_diario").required())
		return false;
	
	var str = "fecha=" + $("#fecha_resumen_diario").val();
	ajax.post({url:_base_url+"facturacion/resumen_diario", data:str}, function(res) {
		getRecords();
		$("#modal-resumenDiario").modal("hide");
	});
});

$("#btnreset").click(function(e) {
	e.preventDefault();
	if($("#tabla-result tbody tr.active").length <= 0) {
		ventana.alert({titulo:"", mensaje:"Seleccione un registro de la tabla."});
		return;
	}
	
	ventana.confirm({titulo:"", mensaje:"Al formatear el comprobante se volver&aacute; a su situaci&oacute;n inicial.<br><br>"+
		"Por lo general esta operaci&oacute;n se realiza cuando el comprobante ha tenido algun error al generar o enviar a Sunat "+
		"o se ha modificado alguna informaci&oacute;n despues de haberse generado.<br><br>"+
		"&iquest;Desea formatear el comprobante?", 
	textoBotonAceptar: "Formatear"}, function(b) {
		if(b) {
			var tr = $("#tabla-result tbody tr.active");
			var str = "idreferencia="+tr.data("idreferencia")+"&referencia="+tr.data("referencia");
			ajax.post({url:_base_url+"facturacion/restablecer", data: str}, function(res) {
				updateRow(tr, res);
			});
		}
	});
});

init();