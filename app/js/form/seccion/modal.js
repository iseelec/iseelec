if(typeof form.guardar_seccion != 'function') {
	form.guardar_seccion = function() {
		var data = $("#form_seccion").serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				$("#modal-seccion").modal("hide");
			});
		}, "seccion");
	}
}

$("#modal-seccion").on('shown.bs.modal', function () {
	$('#uni_descripcion').focus();
});

$("#modal-seccion").on('hidden.bs.modal', function () {
	clear_form("#form_seccion");
});

validate("seccion", form.guardar_seccion);