modulo.register("<?php echo $controller;?>", {
	nuevo: function() {
		dialog.open("#modal-"+this._controller);
	},
	editar: function(id) {
		// alert(id);
	},
	eliminar: function(id) {
		var sel = this;
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				sel.callback(res);
			});
		}, sel._controller);
	},
	guardar: function() {
		var b = true && $("#"+this._prefix+"tipo").required();
		b = b && $("#"+this._prefix+"nombres").required();
		
		if($("#"+this._prefix+"tipo").val() == "J") {
			b = b && $("#"+this._prefix+"ruc").required();
			b = b && $(".list_direcciones .direccion").required();
		}
		else {
			b = b && $("#"+this._prefix+"dni").required();
			b = b && $("#"+this._prefix+"apellidos").required();
		}
		
		if($.trim($("#"+this._prefix+"ruc").val()) != "") {
			if(String($("#"+this._prefix+"ruc").val()).length != 11) {
				ventana.alert({mensaje: "El ruc debe tener 11 digitos"});
				b = false;
			}
		}
		
		if($.trim($("#"+this._prefix+"dni").val()) != "") {
			if(String($("#"+this._prefix+"dni").val()).length != 8) {
				ventana.alert({mensaje: "El dni debe tener 8 digitos"});
				b = false;
			}
		}
		
		if( ! b) {
			$("#tabs_"+this._controller+" a[href='#tab-<?php echo $controller;?>-1']").tab("show");
			return;
		}
		
		if(b)
			Save_action_<?php echo $controller;?>("form_"+this._controller);
	},
	cancelar: function() {
		dialog.close("#modal-"+this._controller);
	}
	,callback(res, redir) {
		if(typeof redir != "boolean")
			redir = ! dialog.inModal(this._form);
		
		if(redir)
			redirect(this._controller);
		else {
			grilla.reload(_default_grilla);
			dialog.close("#modal-"+this._controller);
		}
	}
	,init: function() {
		$("#"+this._prefix+"tipo").trigger("change");
		if($("#"+this._prefix+"idcliente").val() == '')
			open_modal_cliente(false);
		else
			getDirecciones();
		if( ! dialog.inModal(this._form))
			$("#"+this._prefix+"tipo").focus();
	}
	,hide_modal: function() { // funcion a ejecutar cuando se cierra la ventana modal
		clear_form(this._form);
		$("#"+this._prefix+"load_photo>img").remove();
		$("#tabs_"+this._controller+" a[href='#tab-"+this._controller+"-1']").tab("show");
	}
	,show_modal: function() { // funcion a ejecutar cuando se muestra la ventana modal
		
	}
});

var prefix_cliente = "<?php echo $prefix;?>";

$('#<?php echo $prefix;?>fecha_nac').datepicker({
	keyboardNavigation: false,
	forceParse: false,
	autoclose: true,
	language: 'es',
	format: "dd/mm/yyyy",
	todayHighlight: true,
	endDate: parseDate(_current_date)
});

$('#<?php echo $prefix;?>nombres').letras({'permitir':' &.'});
$('#<?php echo $prefix;?>apellidos').letras({'permitir':' '});
$('#<?php echo $prefix;?>direccion_trabajo').alfanumerico({'permitir':' -#./'});
$('#<?php echo $prefix;?>cliente_email').alfanumerico({'permitir':'@-_.'});
$('#<?php echo $prefix;?>dni, #<?php echo $prefix;?>ruc').numero_entero();
$('#<?php echo $prefix;?>ingreso_mensual').numero_real();

$("#<?php echo $prefix;?>tipo").change(function() {
	if($(this).val() == "N") {
		$("#<?php echo $prefix;?>apellidos").prop('readonly', false).removeClass("hide");
		$('.label_ruc').removeClass('required');
		$(".info_natural").show();
		$(".info_juridico").hide();
		$(".label_secundario").html("Datos Adicionales");
	}else{
		$("#<?php echo $prefix;?>apellidos").val('').prop('readonly', true).addClass("hide");
		$('.label_ruc').addClass('required');
		$(".info_juridico").show();
		$(".info_natural").hide();
		$(".label_secundario").html("Datos Representante");
	}
});

$("#<?php echo $prefix;?>load_photo").click(function() {
    $("#<?php echo $prefix;?>file").click();
});

$("#<?php echo $prefix;?>btn_save_cliente").click(function() {
	modulo.get("<?php echo $controller;?>").guardar();
	return false;
});

function leerarchivobin(f) {
	if (f.files.length != 0 && f.files[0].type.match(/image.*/)) {
		var lecimg = new FileReader();
		lecimg.onload = function(e) {
			var img = $('<img class="img-responsive img-thumbnail" />');
			img.prop("src", e.target.result);
			$("#<?php echo $prefix;?>load_photo").html(img);
		} ;
		lecimg.onerror = function(e) {
			alert("Error leyendo la imagen!!");
		};
		lecimg.readAsDataURL(f.files[0]);
	} else {
		$("#<?php echo $prefix;?>load_photo>img").remove();
		alert("Seleccione una imagen");
	}
}

$(".list_direcciones").on('click', "#addDireccion", function() {
	if($('.list_direcciones .direccion').required()) {
		var array = [];
		var data = {
				descripcion: ''
				,dir_principal: 'S'
				,direccion: ''
				,estado: 'A'
				,idcliente: ''
				,idclientedireccion: ''
				,tipo: ''
			};

		array.push(data);
		direcciones_grid(array, false, "<?php echo $prefix;?>");
	}
});

$(".list_direcciones").on('click', '.delete_direccion', function() {
	$(this).closest('div.input-group').remove();
	if($(".list_direcciones :radio.dir_principal:checked").length <= 0)
		$(".list_direcciones :radio.dir_principal:first").prop("checked", true).trigger("change");
});

$(".list_direcciones").on("change", ":radio.dir_principal", function() {
	$(".list_direcciones .dir_principal_val").val("N");
	if($(this).is(":checked")) {
		var div = $(this).closest("div.input-group");
		$(".dir_principal_val", div).val("S");
	}
});

$(".list_telefonos").on('click', "#addTelefono", function() {
	if( $('.list_telefonos .telefono').required() ){
		var array = [];
		var data = {
				idclientetelefono: ''
				,idcliente: ''
				,descripcion: ''
				,estado: 'A'
				,telefono: ''
			};
		array.push(data);
		telefonos_grid(array, false, "<?php echo $prefix;?>");
	}
});

$(".list_telefonos").on('click', '.delete_telefono', function() {
	$(this).closest('div.input-group').remove();
});

$(".list_representantes").on('click', "#addRepresentante", function() {
	var s = true && $('.list_representantes .nombre_representante').required();
	s = s && $('.list_representantes .apellidos_representante').required();
	s = s && $('.list_representantes .dni_representante').required();
	
	if( s ) {
		var array = [];
		var data = {
				idcliente_representante: ''
				,idcliente: ''
				,nombre_representante: ''
				,apellidos_representante: ''
				,dni_representante: ''
				,email_representante: ''
				,estado: 'A'
			};
		array.push(data);
		representante_grid(array,true,prefix_cliente);
	}
});

$(".list_representantes").on('click', '.delete_repres', function() {
	$(this).closest('div.div-row-repres').remove();
});

function getDirecciones() {
	if( ! $("#<?php echo $prefix;?>idcliente").required())
		return;
	
	ajax.post({url: _base_url+"cliente/get_all/", data:{id:$("#<?php echo $prefix;?>idcliente").val()}}, function(response) {
		if(!response.direccion.length){
			var array = [];
			var data = {
					descripcion: ''
					,dir_principal: 'S'
					,direccion: ''
					,estado: 'A'
					,idcliente: ''
					,idclientedireccion: ''
					,tipo: ''
				};

			array.push(data);
			response.direccion = array;
		}

		if(!response.telefonos.length){
			var array = [];
			var data = {
					idclientetelefono: ''
					,idcliente: ''
					,descripcion: ''
					,estado: 'A'
					,telefono: ''
				};
			array.push(data);
			response.telefonos = array;
		}
		
		if(!response.representantes.length){
			var array = [];
			var data = {
					idcliente_representante: ''
					,idcliente: ''
					,nombre_representante: ''
					,apellidos_representante: ''
					,dni_representante: ''
					,email_representante: ''
					,estado: 'A'
				};
			array.push(data);
			response.representantes = array;
		}
		
		direcciones_grid(response.direccion,false,"<?php echo $prefix;?>");
		telefonos_grid(response.telefonos,false,"<?php echo $prefix;?>");
		representante_grid(response.representantes,false,"<?php echo $prefix;?>");
	});
}

$("#btn-search-ruc").on("click", function() {
	if( ! $("#<?php echo $prefix;?>ruc").required())
		return;
	$.ajax({
		data: { "nruc" : $("#<?php echo $prefix;?>ruc").val() },
		type: "POST",
		dataType: "json",
		url: "<?php echo $url_consultas_ruc; ?>",
	}).done(function( data, textStatus, jqXHR ){
		if(data['success'] != "false" && data['success'] != false) {
			if(typeof(data['result']) != 'undefined') {
				$("#<?php echo $prefix;?>nombres").val(data["result"]["razon_social"]);
				$(".list_direcciones .direccion:first").val(data["result"]["direccion"]);
				$("#<?php echo $prefix;?>apellidos").val("");
				$("#<?php echo $prefix;?>dni").val("");
			}
			else
				ventana.alert({titulo: "", mensaje: "No se puede obtener los datos, consulte nuevamente", tipo:"error"});
		}
		else if(typeof(data['msg'])!='undefined')
			ventana.alert({titulo: "", mensaje: data['msg'], tipo:"error"});
		else if(typeof(data['message'])!='undefined')
			ventana.alert({titulo: "", mensaje: data['message'], tipo:"error"});
		else
			ventana.alert({titulo: "", mensaje: "No se puede obtener los datos &iquest;el ruc es valido?", tipo:"error"});
	}).fail(function( jqXHR, textStatus, errorThrown ){
		ventana.alert({titulo: "", mensaje: "Solicitud fallida:" + textStatus, tipo:"error"});
	});
});

$("#btn-search-dni").on("click", function() {
	if( ! $("#<?php echo $prefix;?>dni").required())
		return;
	$.ajax({
		data: { "dni" : $("#<?php echo $prefix;?>dni").val() },
		type: "GET",
		dataType: "json",
		url: "<?php echo $url_consultas_dni; ?>",
	}).done(function( data, textStatus, jqXHR ){
		if(data.estado == true) {
			$("#<?php echo $prefix;?>nombres").val(data.nombres);
			$("#<?php echo $prefix;?>apellidos").val(data.apellidos);
			$(".list_direcciones .direccion:first").val(data.distrito);
			$("#<?php echo $prefix;?>ruc").val("");
		}
		else
			ventana.alert({titulo: "", mensaje: "Ha ocurrido algun error, consulte nuevamente", tipo:"error"});
	}).fail(function( jqXHR, textStatus, errorThrown ){
		ventana.alert({titulo: "", mensaje: "Solicitud fallida:" + textStatus, tipo:"error"});
	});
});

$("#btn-registrar-zona").on("click", function() {
  $("#modal-zona").modal("show");
  return false;
});

modulo.register("zona", {
	callback: function(res, redir) {
		$("#modal-zona").modal("hide");
		reload_combo("#<?php echo $prefix;?>idzona", {controller: "zona"}, function() {
			$("#<?php echo $prefix;?>idzona").val(res.idzona);
		});
	}
});

$("#btn-registrar-ocupacion").on("click", function() {
	$("#modal-ocupacion").modal("show");
	return false;
});

modulo.register("ocupacion", {
	callback: function(res, redir) {
		$("#modal-ocupacion").modal("hide");
		reload_combo("#<?php echo $prefix;?>idocupacion", {controller: "ocupacion"}, function() {
			$("#<?php echo $prefix;?>idocupacion").val(res.idocupacion);
		});
	}
});

function Save_action_<?php echo $controller;?>(id){
	// buscamos si existe la direccion principal
	if($(".list_direcciones :radio.dir_principal:checked").length <= 0)
		$(".list_direcciones :radio.dir_principal:first").prop("checked", true);
	
	$(".list_direcciones>.row-direccion").each(function() {
		var v = $(":radio.dir_principal", this).is(":checked") ? "S" : "N";
		$(".dir_principal_val", this).val(v);
	});
	
	var fd = new FormData(document.getElementById(id));
	fd.append("response", "ajax");
	fd.append("type", "json");
	$.ajax({
		url: _base_url+"cliente/guardar",
		type: "POST",
		data: fd,
		dataType: "json",
		enctype: 'multipart/form-data',
		processData: false,  // tell jQuery not to process the data
		contentType: false   // tell jQuery not to set contentType
	}).done(function( res ) {
		if(res.code == "ERROR") {
			ventana.alert({mensaje:res.message, tipo:"error"});
			return;
		}
		
		ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
			modulo.get("<?php echo $controller;?>").callback(res.data);
		});
	});
	return false;
}