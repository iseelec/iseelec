modulo.register("<?php echo $controller;?>", {
	nuevo: function() {
		open_modal_cliente(false, this._prefix);
		$("#"+this._prefix+"tipo").trigger("change");
		dialog.open("#modal-"+this._controller);
	},
	editar: function(id) {
		var sel = this;
		model.get(id, function(res) {
			$.each(res, function(k, v) {
				$("#"+sel._prefix+k, sel._form).val(v);
			});
			$("#"+sel._prefix+"tipo").trigger("change");
			if(res.foto)
				$("#"+sel._prefix+"load_photo").html('<img src="'+_base_url+'app/img/cliente/'+res.foto+'" class="img-responsive img-thumbnail" />');
			
			getDirecciones_<?php echo $controller;?>();
			getContactos_<?php echo $controller;?>();
			
			if(res.idcliente_ref) {
				$("#tab-<?php echo $controller;?>-2").addClass("hide");
				$("a[href='#tab-<?php echo $controller;?>-2']").closest("li").addClass("hide");
			}
			else {
				$("#tab-<?php echo $controller;?>-2").removeClass("hide");
				$("a[href='#tab-<?php echo $controller;?>-2']").closest("li").removeClass("hide");
			}
			
            dialog.open("#modal-"+sel._controller);
		}, sel._controller);
	},
	eliminar: function(id) {
		var sel = this;
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				sel.callback(res);
			});
		}, sel._controller);
	},
	guardar: function() {
		var b = true && $("#"+this._prefix+"tipo").required();
		b = b && $("#"+this._prefix+"nombres").required();
		
		<?php if($controller == "cliente") {?>
		if($("#"+this._prefix+"tipo").val() == "J") {
			b = b && $("#"+this._prefix+"ruc").required();
			b = b && $("."+this._prefix+"list_direcciones .direccion").required();
		}
		else {
			b = b && $("#"+this._prefix+"dni").required();
			b = b && $("#"+this._prefix+"apellidos").required();
		}
		
		if($.trim($("#"+this._prefix+"ruc").val()) != "") {
			if(String($("#"+this._prefix+"ruc").val()).length != 11) {
				ventana.alert({mensaje: "El ruc debe tener 11 digitos"});
				b = false;
			}
		}
		
		if($.trim($("#"+this._prefix+"dni").val()) != "") {
			if(String($("#"+this._prefix+"dni").val()).length != 8) {
				ventana.alert({mensaje: "El dni debe tener 8 digitos"});
				b = false;
			}
		}
		<?php }?>
		
		if( ! b) {
			$("#tabs_"+this._controller+" a[href='#tab-<?php echo $controller;?>-1']").tab("show");
			return;
		}
		
		if(b)
			Save_action_<?php echo $controller;?>("form_"+this._controller);
	},
	cancelar: function() {
		dialog.close("#modal-"+this._controller);
	}
	,callback(res, redir) {
		if(typeof redir != "boolean")
			redir = ! dialog.inModal(this._form);
		
		if(redir)
			redirect(this._controller);
		else {
			grilla.reload(_default_grilla);
			dialog.close("#modal-"+this._controller);
		}
	}
	,init: function() {
		if( ! dialog.inModal(this._form)) {
			$("#"+this._prefix+"tipo").trigger("change");
			if($("#"+this._prefix+"idcliente").val() == '')
				open_modal_cliente(false, this._prefix);
			else {
				getDirecciones_<?php echo $controller;?>();
				getContactos_<?php echo $controller;?>();
			}
			$("#"+this._prefix+"tipo").focus();
		}
	}
	,hide_modal: function() { // funcion a ejecutar cuando se cierra la ventana modal
		clear_form(this._form);
		$("#"+this._prefix+"load_photo>img").remove();
		$("."+this._prefix+"list_direcciones>.row-direccion").remove();
		$("."+this._prefix+"list_telefonos>div.input-group").remove();
		$("#contact-list-"+this._controller+">.item-contact-box").remove();
		$("#tabs_"+this._controller+" a[href='#tab-"+this._controller+"-1']").tab("show");
	}
	,show_modal: function() { // funcion a ejecutar cuando se muestra la ventana modal
		
	}
});

var prefix_<?php echo $controller;?> = "<?php echo $prefix;?>";

$('#<?php echo $prefix;?>fecha_nac').datepicker({
	keyboardNavigation: false,
	forceParse: false,
	autoclose: true,
	language: 'es',
	format: "dd/mm/yyyy",
	todayHighlight: true,
	endDate: parseDate(_current_date)
});

$('#<?php echo $prefix;?>nombres').letras({'permitir':' &.'});
$('#<?php echo $prefix;?>apellidos').letras({'permitir':' '});
// $('#<?php echo $prefix;?>direccion_trabajo').alfanumerico({'permitir':' -#./'});
$('#<?php echo $prefix;?>cliente_email').alfanumerico({'permitir':'@-_.'});
$('#<?php echo $prefix;?>dni, #<?php echo $prefix;?>ruc').numero_entero();
// $('#<?php echo $prefix;?>ingreso_mensual').numero_real();

$("#<?php echo $prefix;?>tipo").change(function() {
	if($(this).val() == "N") {
		$("#<?php echo $prefix;?>apellidos").prop('readonly', false).removeClass("hide");
		// $('.label_ruc').removeClass('required');
		$(".<?php echo $prefix;?>info_natural").show();
		$(".<?php echo $prefix;?>info_juridico").hide();
		// $(".label_secundario").html("Datos Adicionales");
	}else{
		$("#<?php echo $prefix;?>apellidos").val('').prop('readonly', true).addClass("hide");
		// $('.label_ruc').addClass('required');
		$(".<?php echo $prefix;?>info_juridico").show();
		$(".<?php echo $prefix;?>info_natural").hide();
		// $(".label_secundario").html("Datos Representante");
	}
});

$("#<?php echo $prefix;?>load_photo").click(function() {
    $("#<?php echo $prefix;?>file").click();
});

$("#<?php echo $prefix;?>btn_save_cliente").click(function() {
	modulo.get("<?php echo $controller;?>").guardar();
	return false;
});

function leerarchivobin_<?php echo $controller;?>(f) {
	if (f.files.length != 0 && f.files[0].type.match(/image.*/)) {
		var lecimg = new FileReader();
		lecimg.onload = function(e) {
			var img = $('<img class="img-responsive img-thumbnail" />');
			img.prop("src", e.target.result);
			$("#<?php echo $prefix;?>load_photo").html(img);
		} ;
		lecimg.onerror = function(e) {
			alert("Error leyendo la imagen!!");
		};
		lecimg.readAsDataURL(f.files[0]);
	} else {
		$("#<?php echo $prefix;?>load_photo>img").remove();
		alert("Seleccione una imagen");
	}
}

$(".<?php echo $prefix;?>list_direcciones").on('click', "#<?php echo $prefix;?>addDireccion", function() {
	if($('.<?php echo $prefix;?>list_direcciones .direccion').required()) {
		var array = [];
		var data = {
				descripcion: ''
				,dir_principal: 'S'
				,direccion: ''
				,estado: 'A'
				,idcliente: ''
				,idclientedireccion: ''
				,tipo: ''
			};

		array.push(data);
		direcciones_grid(array, false, "<?php echo $prefix;?>");
	}
});

$(".<?php echo $prefix;?>list_direcciones").on('click', '.delete_direccion', function() {
	$(this).closest('div.input-group').remove();
	if($(".<?php echo $prefix;?>list_direcciones :radio.dir_principal:checked").length <= 0)
		$(".<?php echo $prefix;?>list_direcciones :radio.dir_principal:first").prop("checked", true).trigger("change");
});

$(".<?php echo $prefix;?>list_direcciones").on("change", ":radio.dir_principal", function() {
	$(".<?php echo $prefix;?>list_direcciones .dir_principal_val").val("N");
	if($(this).is(":checked")) {
		var div = $(this).closest("div.input-group");
		$(".dir_principal_val", div).val("S");
	}
});

$(".<?php echo $prefix;?>list_telefonos").on('click', "#<?php echo $prefix;?>addTelefono", function() {
	if( $('.<?php echo $prefix;?>list_telefonos .telefono').required() ){
		var array = [];
		var data = {
				idclientetelefono: ''
				,idcliente: ''
				,descripcion: ''
				,estado: 'A'
				,telefono: ''
			};
		array.push(data);
		telefonos_grid(array, false, "<?php echo $prefix;?>");
	}
});

$(".<?php echo $prefix;?>list_telefonos").on('click', '.delete_telefono', function() {
	$(this).closest('div.input-group').remove();
});

function getDirecciones_<?php echo $controller;?>() {
	if( ! $("#<?php echo $prefix;?>idcliente").required())
		return;
	
	ajax.post({url: _base_url+"<?php echo $controller;?>/get_all/", data:{id:$("#<?php echo $prefix;?>idcliente").val()}}, function(response) {
		if(!response.direccion.length){
			var array = [];
			var data = {
					descripcion: ''
					,dir_principal: 'S'
					,direccion: ''
					,estado: 'A'
					,idcliente: ''
					,idclientedireccion: ''
					,tipo: ''
				};

			array.push(data);
			response.direccion = array;
		}

		if(!response.telefonos.length){
			var array = [];
			var data = {
					idclientetelefono: ''
					,idcliente: ''
					,descripcion: ''
					,estado: 'A'
					,telefono: ''
				};
			array.push(data);
			response.telefonos = array;
		}
		
		direcciones_grid(response.direccion,false,"<?php echo $prefix;?>");
		telefonos_grid(response.telefonos,false,"<?php echo $prefix;?>");
	});
}

function getContactos_<?php echo $controller;?>() {
	if($("#contact-list-<?php echo $controller;?>").length <= 0)
		return;
	
	if( ! $("#<?php echo $prefix;?>idcliente").required())
		return;
	
	ajax.post({url: _base_url+"<?php echo $controller;?>/get_all_contact/", data:{id:$("#<?php echo $prefix;?>idcliente").val()}}, function(res) {
		if(res.length <= 0)
			return;
		
		for(var i in res) {
			modulo.get("contacto").callback(res[i]);
		}
	});
}

$("#<?php echo $prefix;?>btn-search-ruc").on("click", function() {
	if( ! $("#<?php echo $prefix;?>ruc").required())
		return;
	$.ajax({
		data: { "nruc" : $("#<?php echo $prefix;?>ruc").val() },
		type: "POST",
		dataType: "json",
		url: "<?php echo $url_consultas_ruc; ?>",
	}).done(function( data, textStatus, jqXHR ){
		if(data['success'] != "false" && data['success'] != false) {
			if(typeof(data['result']) != 'undefined') {
				$("#<?php echo $prefix;?>nombres").val(data["result"]["razon_social"]);
				$(".list_direcciones .direccion:first").val(data["result"]["direccion"]);
				$("#<?php echo $prefix;?>apellidos").val("");
				$("#<?php echo $prefix;?>dni").val("");
			}
			else
				ventana.alert({titulo: "", mensaje: "No se puede obtener los datos, consulte nuevamente", tipo:"error"});
		}
		else if(typeof(data['msg'])!='undefined')
			ventana.alert({titulo: "", mensaje: data['msg'], tipo:"error"});
		else if(typeof(data['message'])!='undefined')
			ventana.alert({titulo: "", mensaje: data['message'], tipo:"error"});
		else
			ventana.alert({titulo: "", mensaje: "No se puede obtener los datos &iquest;el ruc es valido?", tipo:"error"});
	}).fail(function( jqXHR, textStatus, errorThrown ){
		ventana.alert({titulo: "", mensaje: "Solicitud fallida:" + textStatus, tipo:"error"});
	});
});

$("#<?php echo $prefix;?>btn-search-dni").on("click", function() {
	if( ! $("#<?php echo $prefix;?>dni").required())
		return;
	$.ajax({
		data: { "dni" : $("#<?php echo $prefix;?>dni").val() },
		type: "GET",
		dataType: "json",
		url: "<?php echo $url_consultas_dni; ?>",
	}).done(function( data, textStatus, jqXHR ){
		if(data.estado == true) {
			$("#<?php echo $prefix;?>nombres").val(data.nombres);
			$("#<?php echo $prefix;?>apellidos").val(data.apellidos);
			$(".<?php echo $prefix;?>list_direcciones .direccion:first").val(data.distrito);
			$("#<?php echo $prefix;?>ruc").val("");
		}
		else
			ventana.alert({titulo: "", mensaje: "Ha ocurrido algun error, consulte nuevamente", tipo:"error"});
	}).fail(function( jqXHR, textStatus, errorThrown ){
		ventana.alert({titulo: "", mensaje: "Solicitud fallida:" + textStatus, tipo:"error"});
	});
});

$("#<?php echo $prefix;?>btn-registrar-contacto").on("click", function() {
	modulo.get("contacto").nuevo();
	var c = modulo.get("contacto");
	$("."+c._prefix+"list_direcciones .direccion:first").val($(".<?php echo $prefix;?>list_direcciones .direccion:first").val());
	$("."+c._prefix+"list_telefonos .telefono:first").val($(".<?php echo $prefix;?>list_telefonos .telefono:first").val());
});

modulo.register("contacto", {
	callback: function(res, redir) {
		var dir = [];
		if(res.direccion.length) {
			$.each(res.direccion, function(i, v) {
				dir.push(v.direccion+" ("+v.tipo+")");
			});
		}
		
		var tel = [];
		if(res.telefonos.length) {
			$.each(res.telefonos, function(i, v) {
				tel.push(v.telefono);
			});
		}
		
		var exists = ($("#contact-list-<?php echo $controller;?> > .item-contact-box[aria-idcliente='"+res.cliente.idcliente+"']").length >= 1);
		
		var div;
		if(exists)
			div = $("#contact-list-<?php echo $controller;?> > .item-contact-box[aria-idcliente='"+res.cliente.idcliente+"']");
		else 
			div = $('<div class="col-md-4 item-contact-box" aria-idcliente="'+res.cliente.idcliente+'"></div>');
		
		div.html(''+
			'<div class="contact-box" style="padding:5px;"><a href="javascript:void(0)" class="edit-item">'+
			'<div class="col-sm-4">'+
			'<div class="text-center"><img alt="..." class="img-circle m-t-xs img-responsive" src="'+_base_url+'app/img/cliente/'+res.cliente.foto+'"></div>'+
			'</div>'+
			'<div class="col-sm-8">'+
			'<p><strong class="contact-name">'+res.cliente.nombres+' '+res.cliente.apellidos+'</strong></p>'+
			(res.cliente.cliente_email ? '<p><span class="fa fa-envelope m-r-xs"></span> '+res.cliente.cliente_email+'</p>' : '')+
			'<p><span class="fa fa-home m-r-xs"></span> '+dir.join("<br>")+'</p>'+
			'<p><span class="fa fa-phone m-r-xs"></span> '+tel.join(", ")+'</p>'+
			'</div>'+
			'<div class="clearfix"></div></a></div>'+
			'<a href="javascript:void(0)" class="text-danger trash-item"><span class="fa fa-trash-o"></span></a>'+
			'');
		
		if( ! exists)
			$("#contact-list-<?php echo $controller;?>").append(div);
		
		dialog.close("#modal-"+this._controller);
	}
});

$("#contact-list-<?php echo $controller;?>").on("click", ".item-contact-box>.contact-box>a.edit-item", function() {
	modulo.get("contacto").editar($(this).closest(".item-contact-box").attr("aria-idcliente"));
});

$("#contact-list-<?php echo $controller;?>").on("click", ".item-contact-box>a.trash-item", function() {
	var div = $(this).closest(".item-contact-box");
	ventana.confirm({
		titulo:"Confirmar"
		,mensaje:"¿Desea eliminar el contacto "+$(".contact-name", div).text()+"?"
		,textoBotonAceptar: "Eliminar"
	}, function(ok) {
		if(ok) {
			div.remove();
		}
	});
});

function Save_action_<?php echo $controller;?>(id){
	// buscamos si existe la direccion principal
	if($(".<?php echo $prefix;?>list_direcciones :radio.dir_principal:checked").length <= 0)
		$(".<?php echo $prefix;?>list_direcciones :radio.dir_principal:first").prop("checked", true);
	
	$(".<?php echo $prefix;?>list_direcciones>.row-direccion").each(function() {
		var v = $(":radio.dir_principal", this).is(":checked") ? "S" : "N";
		$(".dir_principal_val", this).val(v);
	});
	
	
	var fd = new FormData(document.getElementById(id));
	fd.append("response", "ajax");
	fd.append("type", "json");
	
	if($("#contact-list-<?php echo $controller;?> > .item-contact-box").length > 0) {
		var a = [];
		$("#contact-list-<?php echo $controller;?> > .item-contact-box").each(function() {
			a.push($(this).attr("aria-idcliente"));
		});
		fd.append("contactos", a.join(";"));
	}
	
	$.ajax({
		url: _base_url+"<?php echo $controller;?>/guardar",
		type: "POST",
		data: fd,
		dataType: "json",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false
	}).done(function(res) {
		if(res.code == "ERROR") {
			ventana.alert({mensaje:res.message, tipo:"error"});
			return;
		}
		
		ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
			modulo.get("<?php echo $controller;?>").callback(res.data);
		});
	});
	return false;
}