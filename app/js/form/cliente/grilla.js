function callbackCliente(nRow, aData, iDisplayIndex) {
	var fotito =_base_url+'app/img/imgdef.png';
	if($.trim(aData.foto) != "")
		fotito = _base_url+'app/img/cliente/'+aData.foto;
	if( ! ("telefono" in aData))
		aData.telefono = "";
	$('td', nRow).eq(1).html('<div style="display:inline-block;"><div class="client-avatar" style="display:inline-block;"><img src="'+fotito+'" /></div>'+aData.cliente+'</div>');
	$('td', nRow).eq(4).html('<div><i class="fa fa-phone">&nbsp;</i>'+aData.telefono+'</div>');
}

$("#dt<?php echo $tabla_vista;?>").on('click', "tbody tr", function() {
	var idcliente = $("td:eq(0)", this).text();
	if( ! $.isNumeric(idcliente))
		return;
	
	ajax.post({url: _base_url+_controller+"/retornar_detalle/", data: {idcliente:idcliente}}, function(res) {
		if( ! res.cliente.apellidos)
			res.cliente.apellidos = '';
		$('.title_cliente').html($.trim(res.cliente.nombres+' '+res.cliente.apellidos));
		$('.referencia').html($.trim(res.cliente.observacion));
		if(res.cliente.cliente_email){
			$('.title_mail').html($.trim(res.cliente.cliente_email)).show();			
		}else
			$('.title_mail').hide();
		
		var fotito = _base_url+'app/img/imgdef.png';
		if($.trim(res.cliente.foto))
			fotito = _base_url+'app/img/cliente/'+res.cliente.foto;
		
		$('.thumb_image').attr('src',fotito);
		$(".more_info").html(res.info);
	});
});