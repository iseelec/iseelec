var form = {
	nuevo: function() {
		
	},
	editar: function(id) {
		// alert(id);
	},
	eliminar: function(id) {
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				grilla.reload(_default_grilla); // _default_grilla=perfil, si no se indico otro nombre
			});
		});
	},
	imprimir: function() {
		grilla.set_where(_default_grilla, "idperfil", "=", "1");
		grilla.reload(_default_grilla);
	},
	guardar: function() {
		var data = $("#form_"+_controller).serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				redirect(_controller);
			});
		});
	},
	cancelar: function() {
		
	}
};

validate();

$("#btn_cancel").click(function() {
	redirect(_controller);
	return false;
});

$("#idbanco").focus();

if (typeof prefix_cuentas_bancarias === 'undefined')
    prefix_cuentas_bancarias='';

keyboardSequence([	"#idbanco"+prefix_cuentas_bancarias
						,"#idsucursal"+prefix_cuentas_bancarias
						,"#idmoneda"+prefix_cuentas_bancarias
						,"#nro_cuenta"+prefix_cuentas_bancarias
						,retornar_boton("cuentas_bancarias",prefix_cuentas_bancarias)
				], "#form_cuentas_bancarias");

$("#idproyecto").on("change", function() {
	var t = $("option:selected", this).text();
	if(t == "") {
		t = "&iquest;?";
	}
	
	$("table.tabla_proyecto abbr").html(t);
	update_combo_proyectos_temp();
});
function set_events_table() {
	$("input.fecha_vencimiento", "table.tabla_proyecto").datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		autoclose: true,
		language: 'es'
	});
	
	$("input.total", "table.tabla_proyecto").numero_real();
}
function formatoFechaGrilla(nRow, aData, iDisplayIndex) {
	$("td:eq(0)",nRow).html(fecha_es(aData.fecha_credito, false));
}
function add_proyectos() {
	var id = $("#combo_asignar_proyectos").val();
	var ia = $("#combo_asignar_fase").val();
	var desc = $("#combo_asignar_proyectos option:selected").text();
	var dare = $("#combo_asignar_fase option:selected").text();
	//var sucursal = $("#sucursal").val();
	
	if($("table.tabla_proyecto tr[index="+id+"][index1="+ia+"]").length) {
		ventana.alert({titulo: "Proyecto y Sección Asignado", 
		mensaje: "La Sección <font color='red'>"+dare+"</font> ya se encuentra Agregado A <font color='red'>"+desc+"</font>  en la lista"});
	//$("table.tabla_proyecto tr[index="+id+"] input.prod_cantidad_proyectos_min").focus();
		return;
	}
	var fechavenc = $("#fecha_vencimiento").val();

	var gra = $("#idproyecto option:selected").text();
	if(gra == "") {
		gra = "&iquest;?";
	}
	var html = '<tr index="'+id+'" index1="'+ia+'">';
	html += '<td><strong><input type="hidden" name="tot_proyectos[]" class="tot_proyectos" value="'+id+'"><input type="hidden" name="proyectos[]" class="proyectos" value="'+desc+'"><font color="red">'+desc+'</font></strong></td>';
	html += '<td><strong><input type="hidden" name="tot_fase[]" class="tot_fase" value="'+ia+'"><font color="red">'+dare+'</font></strong></td>';

	//html += '<td><abbr title="Ingreso del Total de estudiantes por Edad y/o Proyectos para el calculo de los Compromisos de Gestion Escolar">'+gra+'</abbr></td>';
	
	//html += '<td><label class="col-lg-2 control-label required">Matriculados 2017</label></td>';
	html += '<td><input type="text" name="nombre_sec[]" class="nombre_sec form-control input-xs required"></td>';
	
	//html += '<td><label class="col-lg-2 control-label required">Concluyeron 2017</label></td>';
	//html += '<td><input type="text" name="cantidad[]" class="cantidad form-control input-xs required"></td>';
	html += '<td><select name="deta_usuario[]" class="form-control input-xs deta_usuario">'+$("#idusuario").html()+'</select></td>';

	html += '<td><select name="deta_estatus[]" class="form-control input-xs deta_estatus">'+$("#idestatus").html()+'</select></td>';
	//html += '<td><input type="text" name="cantidad[]" class="cantidad form-control input-xs required"></td>';
	html += '<td><input type="text" name="cantidad[]" class="cantidad form-control input-xs required"></td>';
	html += '<td><input type="text" class="form-control input-xs fecha_vencimiento" name="fecha_vencimiento[]" value="'+dateFormat(fechavenc, 'd/m/Y')+'"></td>';
	//html += '<td><input type="text" name="cantidad[]" class="cantidad form-control input-xs required"></td>';
	html += '<td><select name="deta_prioridad[]" class="form-control input-xs deta_prioridad">'+$("#idprioridad").html()+'</select></td>';
	html += '<td><button class="btn btn-danger btn-xs btn-del-proyectos data-toggle="tooltip" title="Eliminar registro""><i class="fa fa-trash"></i></button></td>';
	html += '</tr>';
	
	$("table.tabla_proyecto").append(html);
	$("#cantidad").numero_entero();
	//alert data();
	/*$("#table.tabla_proyecto").numero_entero();
	$("#concluyeron1").numero_entero();
	$("#matriculados2").numero_entero();
	$("#concluyeron2").numero_entero();*/
	//$("#cantidad").numero_entero();
	
	update_combo_proyectos_temp();
	set_events_table();
}

$("#btn-asignar-proyectos").on("click", function() {
	if($("#combo_asignar_proyectos").required()) {
		add_proyectos();
	}
	return false;
});

$(document).on("click", "button.btn-del-proyectos", function() {
	$(this).closest("tr").remove();
	return false;
});

function get_proyectos_temp() {
	var html = '', id = 0;
	if($("#idproyecto").val() != '') {
		id = $("#idproyecto").val();
		html += '<option value="'+id+'">'+$("#idproyecto option:selected").text()+'</option>';
	}
	if($(".tabla_proyecto tbody tr").length) {
		$(".tabla_proyecto tbody tr").each(function() {
			if(id != $("input.tot_proyectos", this).val()) {
				html += '<option value="'+$("input.tot_proyectos", this).val()+'">'+$("td:eq(1)", this).text()+'</option>';
			}
		});
	}
	return html;
}
$("#tbl-detalle").on("keyup", "input.cantidad", function() {
	var tr = $(this).closest("tr");
	calcularDatos(tr);
});

function calcularDatos(tr) {
	//calcularImporte(tr);
	/////calcularSubtotal();
	//calcularIgv();
	//calcularTotal();
}
$(document).on("click", "button.btn-del-proyectos", function() {
	$(this).tooltip('destroy');
	$(this).closest("tr").remove();
	////calcularSubtotal();
	//calcularIgv();
	//calcularTotal();
});
$("#subtotal,#btn_save_preventa,#total").hover(function(e) {
	////calcularSubtotal();
	//calcularTotal();
});
// function calcularSubtotal() {
	// if( $("#tbl-detalle tbody tr").length ) {
		// var m = 0; 	var c = 0; 	var m2 = 0;
		// var c2 = 0; var p = 0;
		// var ct = 0; var qt = 0; var st = 0;
	
		// $("#tbl-detalle tbody tr").each(function() {
		////if (sucursal==''){
			// if($.isNumeric($("input.cantidad", this).val()) && ($("input.proyectos", this).val()=='3 AÑOS') ) {
				// m += parseFloat($("input.cantidad", this).val());
			// }
			// if($.isNumeric($("input.cantidad", this).val()) && ($("input.proyectos", this).val()=='4 AÑOS') ) {
				// c += parseFloat($("input.cantidad", this).val());
			// }
			// if($.isNumeric($("input.cantidad", this).val()) && ($("input.proyectos", this).val()=='5 AÑOS') ) {
				// m2 += parseFloat($("input.cantidad", this).val());
			// }
		////}
			// if($.isNumeric($("input.cantidad", this).val()) && ($("input.proyectos", this).val()=='PRIMERO') ) {
				// m += parseFloat($("input.cantidad", this).val());
			// }
			// if($.isNumeric($("input.cantidad", this).val()) && ($("input.proyectos", this).val()=='SEGUNDO') ) {
				// c += parseFloat($("input.cantidad", this).val());
			// }
			// if($.isNumeric($("input.cantidad", this).val()) && ($("input.proyectos", this).val()=='TERCERO') ) {
				// m2 += parseFloat($("input.cantidad", this).val());
			// }
			// if($.isNumeric($("input.cantidad", this).val()) && ($("input.proyectos", this).val()=='CUARTO') ) {
				// ct += parseFloat($("input.cantidad", this).val());
			// }
			// if($.isNumeric($("input.cantidad", this).val()) && ($("input.proyectos", this).val()=='QUINTO') ) {
				// qt += parseFloat($("input.cantidad", this).val());
			// }
			// if($.isNumeric($("input.cantidad", this).val()) && ($("input.proyectos", this).val()=='SEXTO') ) {
				// st += parseFloat($("input.cantidad", this).val());
			// }
		
			// if($.isNumeric($("input.cantidad", this).val())) {
				// c2 += parseFloat($("input.cantidad", this).val());
			// }
			// if($.isNumeric($("input.cantidad", this).val())) {
				// p += 1;
			// }
		// });
		// $("#tot_proyecto1").val(m.toFixed(0));
		// $("#tot_proyecto2").val(c.toFixed(0));
		// $("#tot_proyecto3").val(m2.toFixed(0));
		// $("#tot_proyecto4").val(ct.toFixed(0));
		// $("#tot_proyecto5").val(qt.toFixed(0));
		// $("#tot_proyecto6").val(st.toFixed(0));
		// $("#total_est").val(c2.toFixed(0));
		// $("#total_aulas").val(p.toFixed(0));
		// return;
	// }
	// $("#tot_proyecto1").val("");
	// $("#tot_proyecto2").val("");
	// $("#tot_proyecto3").val("");
	// $("#tot_proyecto4").val("");
	// $("#tot_proyecto5").val("");
	// $("#tot_proyecto6").val("");
	// $("#total_est").val("");
	// $("#total_aulas").val("");
// }
$("#cantidad").numero_entero();

function update_combo_proyectos_temp() {
	$("#proyectos_temp").html(get_proyectos_temp());
}

update_combo_proyectos_temp();

