function actualizarComboGrados() {
	var exists = [];
	if($("#tabla_grado input.idgrados").length) {
		$("#tabla_grado input.idgrados").each(function() {
			exists.push(parseInt($(this).val()));
		});
	}
	
	var options = '';
	
	$.each(GRADOS, function(i, value) {
		id = parseInt(value.idgrados);
		if(exists.indexOf(id) != -1) {
			return true;
		}
		options += '<option value="'+value.idgrados+'" desc="'+value.descripcion+'" abbr="'+value.pref+'">'+value.descripcion+' ('+value.pref+')</option>';
	});
	
	$("#grados_filtro").html(options);
}

if(typeof form == 'undefined') {
	form = {};
}

/*if(typeof form.guardar_grados != 'function') {
	form.guardar_grados = function() {
		var data = $("#form_grados").serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"});
		}, "producto", "guardar_grados");
	}
*/

/*function appendGrado(arr) {
	if(arr.length) {
		var html = '', data;
		for(var i in arr) {
			data = arr[i];
			html += '<tr data-idgrados="'+data.idgrados+'">';
			html += '<td><input type="hidden" name="idgrados[]" class="idgrados" value="'+data.idgrados+'">'+data.descripcion+' ('+data.pref+')</td>';
			html += '<td><input type="text" name="cantidad_grados[]" class="cantidad_grados form-control input-sm" value="'+data.cantidad_grados+'" readonly></td>';
			html += '<td><input type="text" name="cantidad_grados_min[]" class="cantidad_grados_min form-control input-sm" value="'+data.cantidad_grados_min+'"></td>';
			html += '<td><button type="button" class="btn btn-default btn-xs btn_delete_grados">Eliminar</button></td>';
			html += '</tr>';
			
		}
		$("#tabla_grado tbody").append(html);
	}
}*/

actualizarComboGrados();

$("#btn-add-grados").click(function() {
	if($("#grados_filtro").required()) {
		if($("#tabla_grado tbody tr[data-idgrados='"+$("#grados_filtro").val()+"']").length) {
			return false;
		}
		
		var data = {
			idgrados: $("#grados_filtro").val()
			,descripcion: $("#grados_filtro option:selected").attr("desc")
			,abreviatura: $("#grados_filtro option:selected").attr("abbr")
			,cantidad_grados: 1
			,cantidad_grados_min: ""
		};
		var arr = [];
		arr.push(data);
		
		appendGrado(arr);
		actualizarComboGrados();
	}
	return false;
});

$(document).on("click", "button.btn_delete_grados", function() {
	$(this).closest("tr").remove();
	actualizarComboGrados();
	return false;
});

$("#form_grados").submit(function() {
	return false;
});

$("#uni_prod_btn_save").on("click", function() {
	if( $("#tabla_grado .cantidad_grados").required() && 
	$("#tabla_grado .cantidad_grados_min").required() ) {
		form.guardar_grados();
	}
	return false;
});