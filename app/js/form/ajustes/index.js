var form = {
	nuevo: function() {
		
	},
	editar: function(id) {
		// alert(id);
	},
	eliminar: function(id) {
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				grilla.reload(_default_grilla);
			});
		});
	},
	imprimir: function() {
		
	},
	guardar: function() {
		var data = $("#form_"+_controller).serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				redirect(_controller);
			});
		});
	},
	cancelar: function() {
		
	}
};

$("#btn_save_condiciones").click(function(e) {
	e.preventDefault();
	if($("#idparam").required()) {
		ajax.post({url: _base_url+"param/guardar_terminosycondiciones", 
		data: "idparam="+$("#idparam").val()+"&valor="+$("#descripcion").val()}, function() {
			ventana.alert({titulo:"", mensaje:"Terminos y Condiciones guardadas"});
		});
	}
});

validate();

$("#descripcion").focus();

//// :::::::::configuracion almacen ::::::::::::::::
$("#btn-buscar-almacen").click(function() {
	jFrame.create({
		title: "Buscar Almacen"
		,controller: "almacen"
		,method: "grilla_popup"
		,msg: ""
		,widthclass: "modal-lg"
		,onSelect: function(datos) {
			$("#almacen").val(datos.descripcion);
			$("#idsucursal").val(datos.idsucursal);			
			$("#direccion").val(datos.direccion);
			$("#telefono").val(datos.telefono);
			}
	});
	
	jFrame.show();
	return false;
});

$("#btn-registrar-almacen").on("click", function() {
	$("#modal-almacen").modal("show");
	return false;
});
// $("#btn-registrar-almacen").on("click", function() {
	// open_modal_almacen(true);
	// setTimeout(function(){
		// $("#"+prefix_almacen+"sucursal").focus();
	// },1000);
	// return false;
// });

$("#btn-edit-cliente").on("click", function() {
	id = $("#compra_idcliente").val();
	form_cli = "#form_"+prefix_cliente;
	if(id!=''){
		obtenerDatosCliente(id, prefix_cliente, form_cli);
	}else{
		ventana.alert({titulo: "", mensaje: "Debe seleccionar un cliente"});
	}
});
//// :::::::::::fin de almacen ::::::::::::::
