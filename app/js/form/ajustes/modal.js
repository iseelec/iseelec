if(typeof form.guardar_ajustes != 'function') {
	form.guardar_ajustes = function() {
		var data = $("#form_ajustes").serialize();
		model.save(data, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				$("#modal-ajustes").modal("hide");
			});
		}, "ajustes");
	}
}

$("#modal-ajustes").on('shown.bs.modal', function () {
	$('#tam_descripcion').focus();
});

$("#modal-ajustes").on('hidden.bs.modal', function () {
	clear_form("#form_ajustes");
});
// $("#btn_save_condiciones").click(function(e) {
	// e.preventDefault();
	// if($("#idparam").required()) {
		// ajax.post({url: _base_url+"param/guardar_terminosycondiciones", 
		// data: "idparam="+$("#idparam").val()+"&valor="+$("#descripcion").val()}, function() {
			// ventana.alert({titulo:"", mensaje:"Terminos y Condiciones guardadas"});
		// });
	// }
// });

validate("ajustes", form.guardar_ajustes);