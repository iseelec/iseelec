var form = {
	nuevo: function() {
		
	},
	editar: function(id) {
		// alert(id);
	},
	eliminar: function(id) {
		model.del(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Registro eliminado correctamente", tipo:"success"}, function() {
				grilla.reload(_default_grilla);
			});
		});
	},
	imprimir: function() {
		var id = grilla.get_id(_default_grilla);
		if(id != null) {
			open_url(_controller+"/cotizacion/"+id);
		}
	},
	guardar: function(datos_aux) {
		var str = "";
		$("form[aria-control='"+_controller+"']").each(function() {
			str += "&" + $(this).serialize();
		});
		
		str += "&tipodocumento="+$( "#idtipodocumento option:selected" ).text();
		if(datos_aux) {
			str += "&"+datos_aux;
		}
		
		model.save(str, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Datos guardados correctamente", tipo:"success"}, function() {
				redirect(_controller);
			});
		});
	},
	cancelar: function() {		
	},
	aprobar: function(id) {
		model.aprobar_preventa(id, function(res) {
			ventana.alert({titulo: "En horabuena!", mensaje: "Cotizacion aprobado correctamente", tipo:"success"}, function() {
				//redirect(_controller);
			grilla.reload(_default_grilla);
			});
		});
	}
	,mail: function(id) {
		$("#mail-idpreventa").val(id);
		dialog.open("#dialogMail");
		/* ventana.prompt({
			mensaje:"Ingrese los correos electr&oacute;nicos separados por una coma (,)",
			tipo: false,
			textoBotonAceptar: "Enviar correo"
			,placeholder: 'E-mail'
			// ,value: email
		}, function(inputValue) {
			if(inputValue === false)
				return false;
			
			var correo = $.trim(inputValue);
			
			if(correo === "") {
				swal.showInputError("Ingrese un correo electr&oacute;nico");
				return false
			}
			
			correo = correo.replace(/\s+/g, '');
			
			var arr = correo.split(",");
			for(var i in arr) {
				if( ! isEmail(arr[i])) {
					swal.showInputError("El correo electr&oacute;nico "+arr[i]+" no es v&aacute;lido");
					return false
				}
			}
			
			ajax.post({url: _base_url+_controller+"/sendmail", data:{idpreventa:id, correos:correo}}, function(res) {
				if(res === true)
					ventana.alert({mensaje:"La cotizacion se ha enviado a los correos ingresados", tipo:"success"});
				else
					ventana.alert({mensaje:res, tipo:"error"});
			});
		}); */
	}
};

$("#btn_aprobar").click(function(e) {
	e.preventDefault();
	if($("#idpreventa").required()) {
		ajax.post({url: _base_url+"preventa/aprobar_preventa", 
		data: "idpreventa="+$("#idpreventa").val()+"&aprobado="+$("#aprobado").val()}, function() {
						ventana.alert({titulo: "En horabuena!", mensaje: "La Cotizacioón a sido Aprobada", tipo:"success"}, function() {
			// ventana.alert({titulo:"", mensaje:"Observaci&oacute;n guardada"});
			redirect(_controller);
			return false;
			});
		});
	}
});

$("#email").on("click", function() {
	var id = grilla.get_id(_default_grilla);
	if(id != null)
		form.mail(id);
});

$("select[filter]").on("change", function() {
	grilla.set_filter(_default_grilla, $(this).attr("filter"), "=", $(this).val());
	grilla.reload(_default_grilla);
});
/*evento autocomplete seccion*/
	input.autocomplete({
		selector: "#seccion"
		,controller: "seccion"
		,label: "[descripcion]"
		,value: "[descripcion]"
		,highlight: true
		,show_new_item: true
		,onSelect: function(item) {
			$("#idseccion").val(item.idseccion).data("data", item);
			get_prefijo();
		}
		,onNewItem: function(term) {
			$("#mar_descripcion").val(term);
			$("#modal-seccion").modal("show");
		}
	});

/*Eventos acceso directo*/
$(document).ready(function(){
	// $("#btn_nuevo").button("option", "label", "<sub class='hotkey white'>(F1)</sub> Nuevo");
	$("#btn_nuevo").html("<i class='fa fa-file-o'></i> Nuevo <sub class='hotkey white'>(F1)</sub>");
	$("sub").css({"bottom":"0"});

	$(document).keydown(function(e) {
		var i = $("div.modal.fade.in").length;
		
		switch(e.keyCode) {
			case 27:// "Esc"
				e.preventDefault();
				e.stopPropagation();

				if(i<=0){
					redirect(_controller);
				}
				break;
			
			// case 13:// "Enter"
				// e.preventDefault();
				// e.stopPropagation();
					// if($("div.modal.fade.in#modal-pay").length>0){
						// $("button.btn-accept-pay").trigger("click");
					// }else if( $("div.showSweetAlert.visible").length>0 ){
						// $("div.showSweetAlert.visible .confirm").trigger("click");
					// }else if( $("div#modal-popup.modal.fade.in").length>0  ){
						// $("div#modal-popup.modal.fade.in button.select-modal-popup").trigger("click");
					// }else if( $("div#modal-precio-tempp.modal.fade.in").length>0 ){
						// addPrecio($("#ptemp"));
					// }else{
						// console.log("enter en otra part confirm");
					// }
				// break;
				
			// case 120:// "F9"
				// e.preventDefault();
				// e.stopPropagation();
				
				// $("#btn-search-preventa").trigger("click");
				
				// break;
			case 112:// "F1"
				e.preventDefault();
				e.stopPropagation();
				
				$("#btn_nuevo").trigger("click");				
				break;
				
			case 113://
				e.preventDefault();
				e.stopPropagation();
				
				$("#producto_descripcion").focus();
				break;
				
			case 114://
				e.preventDefault();
				e.stopPropagation();
				break;
				
			case 117://
				e.preventDefault();
				e.stopPropagation();
				break;
			case 118://
				e.preventDefault();
				e.stopPropagation();
				break;
				
			case 119://
				e.preventDefault();
				e.stopPropagation();
				break;
				
			case 116://
				e.preventDefault();
				e.stopPropagation();				
				console.log("no hacer nada F5");
				break;
			case 115: // "F4"
				e.preventDefault();
				e.stopPropagation();
				$("#btn_save_preventa").trigger('click');
				break;
			default: 
				// e.preventDefault();
				// e.stopPropagation();
				break;
		}
	});
	
	$(".summernote").summernote({
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture', 'video']]
          // ['view', ['fullscreen', 'codeview', 'help']]
        ]
	});
});

/*Eventos acceso directo*/

keyboardSequence([	"#idtipodocumento"
	,"#idtipoventa"
	,"#idmoneda"
	,"#idvendedor"
	,"#idalmacen"
	,"#codtipo_operacion"
	,"#idmodalidad"
	,"#cliente_razonsocial"
	,"#subtotal"
	,"#igv"
	,"#total"
	,retornar_boton("preventa",'',"btn_save_preventa")
], "#form_preventa");

$("#btnCancelMail").on("click", function() {
	dialog.close("#dialogMail");
});

$("#btnSendMail").on("click", function() {
	if( ! $("#mail-destinatario").required() || ! $("#mail-asunto").required())
		return;
	
	var correo = String($.trim($("#mail-destinatario").val())).replace(/\s+/g, '');
	var arr = correo.split(";");
	for(var i in arr) {
		if( ! isEmail(arr[i])) {
			ventana.alert({mensaje:"El correo electr&oacute;nico "+arr[i]+" no es v&aacute;lido"});
			return;
		}
	}
	
	var str = $("#mail-form").serialize();
	str += "&mensaje=" + encodeURIComponent($(".summernote:first").code());
	
	ajax.post({url: _base_url+_controller+"/sendmail", data:str}, function(res) {
		if(res === true) {
			ventana.alert({mensaje:"La cotizacion se ha enviado a los correos ingresados", tipo:"success"});
			dialog.close("#dialogMail");
		}
		else
			ventana.alert({mensaje:res, tipo:"error"});
	});
});

function drawCallbackPreventa() {
	var api = this.api();
	var footer = api.table().footer();
	$("tr>th", footer).text("");
	$("tr>th:eq(4)", footer).text("TOTAL");
	
	var a = api.column(5, {page:'current'}).data(), t = 0, i;
	if(a.length > 0) {
		for(i=0; i < a.length; i++) {
			if($.isNumeric(a[i]))
				t += Number(a[i]);
		}
	}
	
	$("tr>th:eq(5)", footer).text((round(t, 2)).toFixed(2));
}
