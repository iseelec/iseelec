$(function() {
	$(".btn-config-save").on("click", function(e) {
		e.preventDefault();
		var form = $(this).closest("form");
		
		var local = {}, post = [];
		$(".input-config", form).each(function() {
			if($.trim($(this).val()) != "")
				local[$(this).attr("name")] = $(this).val();
			
			post.push({clave: $(this).attr("name"), valor: $(this).val()});
		});
		
		ajax.post({url: _base_url+"home/default_values", data: {datos:post}}, function(res) {
			// localStorage, aceptara un array como value?
			// saveStorage("default_values", JSON.stringify(datos));
			setDefaultValue(local);
			toastr.success('Datos guardados correctamente.');
		});
		
		return false;
	});
	
	$('.img-circle').tooltip({placement: "right"});
	$(document).on("click","a#change_clave",function(){
		$("#modal-change-pass input").val('');
		$("#clave_nueva").attr('readonly','readonly');
		$("#modal-change-pass").modal("show");
		setTimeout(function(){
			$("#clave_anterior").focus();
		},500);
	});
	
	$("#btn-verificar-pass").click(function(){
		if($("#clave_anterior").required()){
			ajax.post({
				url: _base_url+"usuario/verificar_clave", 
				data: "clave_anterior="+$("#clave_anterior").val()
			},function(res) {
				// alert('En mantenimiento....');
				$(".sms_pass").empty().hide();
				if(res){
					$("#clave_nueva").removeAttr('readonly');
					// $(".sms_pass").html().show();
				}else{
					$("#clave_nueva").attr('readonly','readonly');
					$(".sms_pass").html("Clave Actual Incorrecto...!").show();
				}
			});
		}
	});
	
	$("#save_clave").click(function(){
		s = true && $("#clave_anterior").required();
		s = s    && $("#clave_nueva").required();
		if(s){
			ajax.post({
				url: _base_url+"usuario/change_pass", 
				data: "clave="+$("#clave_nueva").val()
			},function(res) {
				
				$("#modal-change-pass").modal("hide");
			});
		}
	});
	
	$(document).on("click","a#change_avatar",function(){
		$("#modal-change-avatar").modal("show");
	});
	
	$("#load_avatar").click(function() {
		$("#file_avatar").click();
	});
	
	$(document).on("click","#avatar_session",function() {
		$("#modal-change-avatar").modal("show");
	});
	
	$("#save_avatar").click(function(){
		Save_avatar("form-change-avatar");
	});
});

function leer_archivo(f) {
    id='photoAvatar';

    var imagenAR = document.getElementById("file_avatar");
    if (imagenAR.files.length != 0 && imagenAR.files[0].type.match(/image.*/)) {
    var lecimg = new FileReader();
    lecimg.onload = function(e) { 
      var img = document.getElementById(id);
      img.src = e.target.result;
    } 
    lecimg.onerror = function(e) { 
	  ventana.alert({titulo: "Hey!", mensaje: "Error leyendo la imagen!!", tipo:"warning"});
    }
    lecimg.readAsDataURL(imagenAR.files[0]);
    } else {
      ventana.alert({titulo: "Hey!", mensaje: "Seleccione una imagen", tipo:"warning"});
    }
}

function Save_avatar(id){//id= id del formulario
  var fd = new FormData(document.getElementById(id));
    $.ajax({
      url: _base_url+"usuario/change_avatar",
      type: "POST",
      data: fd,
      enctype: 'multipart/form-data',
      processData: false,  // tell jQuery not to process the data
      contentType: false   // tell jQuery not to set contentType
    }).done(function( data ) {
		data = jQuery.parseJSON(data);
		console.log(data);
		if($.isNumeric(data.idusuario)){
			$("#avatar_session").attr("src",'app/img/usuario/'+data.avatar);
			$(".sms_avatar").html("Avatar cambiado correctamente...!").removeClass('alert-danger').addClass('alert-success').show();
			
			setTimeout(function(){
				$("#modal-change-avatar").modal("hide");
			},1000);
		}else{
			$(".sms_avatar").html(data).removeClass('alert-success').addClass('alert-danger').show();
			// ventana.alert({titulo: "Upss...!", mensaje: data, tipo:"warning"}, function() {
				
			// });
		}
    });
    return false;
}