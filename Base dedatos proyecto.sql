-- secuencia de tabla proyecto 
CREATE SEQUENCE proyecto.proyecto_idproyecto_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 78
  CACHE 1;
ALTER TABLE proyecto.proyecto_idproyecto_seq
  OWNER TO corpor23;
GRANT ALL ON SEQUENCE proyecto.proyecto_idproyecto_seq TO corpor23;
GRANT ALL ON SEQUENCE proyecto.proyecto_idproyecto_seq TO corpor23_isai;

-- TABLA PROYECTO RESPONSABLE
CREATE TABLE proyecto.proyecto_responsable
(
  idproyecto integer NOT NULL,
  idusuario integer NOT NULL,
  sueldo_proy_responsable_dia double precision,
  tiempo_proy double precision,
  observacion character varying(100),
  CONSTRAINT proyecto_responsable_pkey PRIMARY KEY (idproyecto, idusuario)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE proyecto.proyecto_responsable
  OWNER TO corpor23;
GRANT ALL ON TABLE proyecto.proyecto_responsable TO corpor23 WITH GRANT OPTION;
GRANT ALL ON TABLE proyecto.proyecto_responsable TO corpor23_isai WITH GRANT OPTION;
----------------------------------
----------------------------------
----TABLA PROYECTO-------------
CREATE TABLE proyecto.proyecto
(
  idproyecto serial NOT NULL,
  idusuario integer,
  descripcion character varying(250),
  descripcion_detallada character varying(500),
  responsable character varying(205),
  direccion character varying(250),
  fecha_inicio timestamp without time zone,
  fecha_fin timestamp without time zone,
  presupuesto double precision,
  estado character(1), -- 1 = activo...
  idempresa integer DEFAULT 0,
  idsucursal integer,
  idcliente integer,
  CONSTRAINT pk_proyecto PRIMARY KEY (idproyecto),
  CONSTRAINT fk_proyecto_usuario FOREIGN KEY (idusuario)
      REFERENCES seguridad.usuario (idusuario) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE proyecto.proyecto
  OWNER TO corpor23;
GRANT ALL ON TABLE proyecto.proyecto TO corpor23;
COMMENT ON COLUMN proyecto.proyecto.estado IS '1 = activo

0 = inactivo';

---- AGREGAMOS UN CAMPO A TABLA PROYECTO
ALTER TABLE proyecto.proyecto ADD COLUMN codigo_proyecto character varying(30);
