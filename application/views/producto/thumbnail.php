<div class="row wrapper border-bottom white-bg page-heading fixed-button-top form-inline">
	<div class="col-sm-6 col-md-6 text-left">
		<?php
		if(count($botones)) {
			foreach($botones as $k){
				echo "\n".$k;
			}
		}
		?>
	</div>
	<div class="col-sm-6 col-md-6 text-right">
		<form id="frmSearch" class="form-inline">
			<div class="form-group m-r-md">
				<div class="input-group input-group-sm">
					<input type="text" class="form-control" id="inputQuery" name="query" placeholder="Buscar" style="width:300px;">
					<span class="input-group-btn">
						<button type="button" class="btn btn-primary" id="btnSearch"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-sm btn-link btn-paginate" data-dir="-1"><i class="fa fa-chevron-left"></i></button>
				<span class="badge badge-default" id="infoPage" style="font-size:14px;"></span>
				<button type="button" class="btn btn-sm btn-link btn-paginate next" data-dir="1"><i class="fa fa-chevron-right"></i> </button>
			</div>
		</form>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
	<div id="product-list" class="row"></div>
</div>

<?php echo $form;?>

<style>
.contact-box.selected {
	background-color: #b4c5ea;
}
</style>