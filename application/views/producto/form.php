<form id="form_<?php echo $controller; ?>" class="app-form form-uppercase" enctype="multipart/form-data" autocomplete="off" data-prefix="<?php echo $prefix;?>" data-modal="<?php echo $modal;?>">

	<input type="hidden" name="idproducto" id="<?php echo $prefix; ?>idproducto" value="<?php echo (isset($producto["idproducto"])) ? $producto["idproducto"] : ""; ?>">
	<input type="hidden" name="corr_temp" id="<?php echo $prefix; ?>corr_temp" value="<?php echo (isset($corr_temp)) ? $corr_temp : ""; ?>">
	<input type="file" name="file" id="<?php echo $prefix; ?>file" style="display: none;" onchange='leerarchivoprod(this)' />
	<input type="hidden" id="imagen_producto" name="imagen_producto" value="<?php echo (!empty($producto["imagen_producto"])) ? $producto["imagen_producto"] : "default_producto.png"; ?>"/>
	<input type="hidden" id="imagen_producto_new" name="imagen_producto_new" value="<?php echo (!empty($producto["imagen_producto"])) ? $producto["imagen_producto"] : "default_producto.png"; ?>">
	<input type="hidden" name="valor_moneda" id="<?php echo $prefix; ?>valor_moneda" value="<?php echo $moneda; ?>" class="form-control prueba2 input-xs fixed-value">
	
	<div class="row">
		<div class="col-md-3">
			<div class="form-group text-center">
				<div id="<?php echo $prefix;?>load_photo" class="app-img-temp">
					<?php
					$n_producto = FCPATH.'app/img/producto/default_producto.png';
					if( ! empty($producto["idproducto"]) && ! empty($producto["imagen_producto"]))
						$n_producto = FCPATH.'app/img/producto/'.$producto["imagen_producto"];
					if(file_exists($n_producto)) {
					?>
					<img src="<?php echo base_url("/app/img/producto/".$producto["imagen_producto"]);?>" class="img-responsive img-thumbnail" />
					<?php }?>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="form-group">
				<label class="required">Nombre del Producto</label>
				<input type="text" style="font-size:22px; color: #009bdd;" name="descripcion_detallada" id="<?php echo $prefix; ?>descripcion_detallada" value="<?php echo (isset($producto["descripcion_detallada"])) ? $producto["descripcion_detallada"] : ""; ?>" class="form-control" required="">
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label>Puede Ser Vendido</label>
						<div class="onoffswitch">
							<input type="checkbox" name="mostrar_en_venta" id="<?php echo $prefix; ?>mostrar_en_venta" class="onoffswitch-checkbox" value="N" <?php echo (isset($producto["mostrar_en_venta"]) && $producto["mostrar_en_venta"] == 'S') ? "checked" : ""; ?>>
							<label class="onoffswitch-label" for="<?php echo $prefix; ?>mostrar_en_venta">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
					<div class="col-sm-4">
						<label>Puede ser Comprado</label>
						<div class="onoffswitch">
							<input type="checkbox" name="mostrar_en_compra" id="<?php echo $prefix; ?>mostrar_en_compra" class="onoffswitch-checkbox" value="N" <?php echo (isset($producto["mostrar_en_compra"]) && $producto["mostrar_en_compra"] == 'S') ? "checked" : ""; ?>>
							<label class="onoffswitch-label" for="<?php echo $prefix; ?>mostrar_en_compra">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
					<div class="col-sm-4">
						<label>Puede ser un Gasto</label>
						<div class="onoffswitch">
							<input type="checkbox" name="mostrar_en_reciboegreso" id="<?php echo $prefix; ?>mostrar_en_reciboegreso" class="onoffswitch-checkbox" value="N" <?php echo (isset($producto["mostrar_en_reciboegreso"]) && $producto["mostrar_en_reciboegreso"] == 'S') ? "checked" : ""; ?>>
							<label class="onoffswitch-label" for="<?php echo $prefix; ?>mostrar_en_reciboegreso">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row form-group">
		<div class="col-lg-12">
			<div class="tabs-container">
				<ul id="tabs_<?php echo $controller;?>" class="nav nav-tabs">
					<li class="active" style="color: #009bdd;"><a data-toggle="tab" href="#tab-1" style="color: #009bdd;">Informaci&oacute;n General</a></li>
					<li class="" style="color: #009bdd;"><a data-toggle="tab" href="#tab-2" style="color: #009bdd;">Otros Datos</a></li>
					<li class="" style="color: #009bdd;"><a data-toggle="tab" href="#tab-3" style="color: #009bdd;">Notas</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="tab-1" class="tab-pane active">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="row form-group">
										<label class="col-md-3 control-label required">Tipo de Producto</label>
										<div class="col-md-9">
											<?php echo $tipo; ?>
										</div>
									</div>
									<div class="row form-group">
										<label class="col-md-3 control-label required">Categoria</label>
										<div class="col-md-9">
											<input type="hidden" name="idcategoria" id="<?php echo $prefix; ?>idcategoria" value="<?php echo (isset($producto["idcategoria"])) ? $producto["idcategoria"] : ""; ?>">
											<input type="text" name="categoria" id="<?php echo $prefix; ?>categoria" value="<?php echo (isset($categoria["descripcion"])) ? $categoria["descripcion"] : ""; ?>" class="form-control prueba2 input-xs" required="">
										</div>
									</div>
									<div class="row form-group">
										<label class="col-md-3 control-label">Modelo</label>
										<div class="col-md-9">
											<input type="hidden" name="idmodelo" id="<?php echo $prefix; ?>idmodelo" value="<?php echo (isset($producto["idmodelo"])) ? $producto["idmodelo"] : ""; ?>">
											<input type="text" name="modelo" id="<?php echo $prefix; ?>modelo" value="<?php echo (isset($modelo["descripcion"])) ? $modelo["descripcion"] : ""; ?>" class="form-control prueba2 input-xs">
										</div>
									</div>
									<div class="row form-group">
										<label class="col-md-3 control-label">Marca</label>
										<div class="col-md-9">
											<input type="hidden" name="idmarca" id="<?php echo $prefix; ?>idmarca" value="<?php echo (isset($producto["idmarca"])) ? $producto["idmarca"] : ""; ?>">
											<input type="text" name="marca" id="<?php echo $prefix; ?>marca" value="<?php echo (isset($marca["descripcion"])) ? $marca["descripcion"] : ""; ?>" class="form-control prueba2 input-xs">
										</div>
									</div>
									<div class="row form-group">
										<label class="col-md-3 control-label required">Referencia Interna</label>
										<div class="col-md-9">
											<input type="text" name="descripcion" id="<?php echo $prefix; ?>descripcion" value="<?php echo (isset($producto["descripcion"])) ? $producto["descripcion"] : ""; ?>" class="form-control prueba2 input-xs" required="">
										</div>
									</div>
									<div class="row form-group">
										<label class="col-md-3 control-label required">C&oacute;digo de Barras</label>
										<div class="col-md-9">
											<input type="text" name="codigo_barras" id="<?php echo $prefix; ?>codigo_barras" value="<?php echo (isset($producto["codigo_barras"])) ? $producto["codigo_barras"] : ""; ?>" class="form-control prueba2 input-xs">
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="row form-group">
										<label class="col-md-3 control-label">Costo en Dolares</label>
										<div class="col-md-9">
											<input type="text" name="precio_dolar" id="<?php echo $prefix;?>precio_dolar" value="<?php echo (isset($producto["precio_dolar"])) ? $producto["precio_dolar"] : ""; ?>" class="form-control float-number prueba2 input-xs precio_dolar">
										</div>
									</div>
									<div class="row form-group">
										<label class="col-md-3 control-label required">Costo en Soles</label>
										<div class="col-md-9">
											<input type="text" name="precio_compra" id="<?php echo $prefix; ?>precio_compra" value="<?php echo (isset($producto["precio_compra"])) ? $producto["precio_compra"] : ""; ?>" class="form-control float-number prueba2 input-xs">
										</div>
									</div>
									<div class="row form-group">
										<label class="col-md-3 control-label required">Margen de Venta %</label>
										<div class="col-md-9">
											<input type="text" name="margen_venta" id="<?php echo $prefix; ?>margen_venta" value="<?php echo (isset($producto["margen_venta"])) ? $producto["margen_venta"] : ""; ?>" class="form-control float-number prueba2 input-xs">
										</div>
									</div>
									<div class="row form-group">
										<label class="col-md-3 control-label required">Precio de Venta</label>
										<div class="col-md-9">
											<input type="text" name="precio_mercado" id="<?php echo $prefix; ?>precio_mercado" value="<?php echo (isset($producto["precio_mercado"])) ? $producto["precio_mercado"] : ""; ?>" class="form-control float-number prueba2 input-xs">
										</div>
									</div>
									<div class="row form-group">
										<label class="col-md-3 control-label required">Unidad de Medida</label>
										<div class="col-md-9">
											<div class="input-group">							
												<?php echo $unidad; ?>
												<span class="input-group-btn tooltip-demo text-left">
													<button type="button" class="btn btn-outline btn-primary btn-registrar-unidad input-xs" data-toggle="tooltip" title="Registrar unidad de medida">
														<i class="fa fa-edit"></i>
													</button>
												</span>
												<input type="hidden" name="pref_codigo_producto" id="<?php echo $prefix; ?>pref_codigo_producto" value="<?php echo (isset($producto["pref_codigo_producto"])) ? $producto["pref_codigo_producto"] : ""; ?>" class="form-control prueba2 input-xs">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="tab-2" class="tab-pane">
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Color</label>
										<input type="hidden" name="idcolor" id="<?php echo $prefix; ?>idcolor" value="<?php echo (isset($producto["idcolor"])) ? $producto["idcolor"] : ""; ?>">
										<input type="text" name="color" id="<?php echo $prefix; ?>color" value="<?php echo (isset($color["descripcion"])) ? $color["descripcion"] : ""; ?>" class="form-control">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Material</label>
										<input type="hidden" name="idmaterial" id="<?php echo $prefix; ?>idmaterial" value="<?php echo (isset($producto["idmaterial"])) ? $producto["idmaterial"] : ""; ?>">
										<input type="text" name="material" id="<?php echo $prefix; ?>material" value="<?php echo (isset($material["descripcion"])) ? $material["descripcion"] : ""; ?>" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Tama&ntilde;o</label>
										<input type="hidden" name="idtamanio" id="<?php echo $prefix; ?>idtamanio" value="<?php echo (isset($producto["idtamanio"])) ? $producto["idtamanio"] : ""; ?>">
										<input type="text" name="tamanio" id="<?php echo $prefix; ?>tamanio" value="<?php echo (isset($tamanio["descripcion"])) ? $tamanio["descripcion"] : ""; ?>" class="form-control">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Peso (Kg)</label>
										<input type="text" name="peso" id="<?php echo $prefix; ?>peso" value="<?php echo (isset($producto["peso"])) ? $producto["peso"] : ""; ?>" class="form-control">
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="tab-3" class="tab-pane">
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-6">	
									<label style="font-size:24px; color:#009bdd;">Descripci&oacute;n para Clientes</label>
									<div class="form-group prueba3" style="margin-right:0px;margin-left:0px;margin-bottom:0px">
										<textarea name="descripcion_cli" id="<?php echo $prefix; ?>descripcion_cli" value="<?php echo (isset($producto["descripcion_cli"])) ? $producto["descripcion_cli"] : ""; ?>" placeholder="Esta nota aparecera en los pedidos de venta" class="form-control prueba3" rows="3"><?php echo (isset($producto["descripcion_cli"])) ? $producto["descripcion_cli"] : ""; ?></textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<label style="font-size:24px; color:#009bdd;">Descripci&oacute;n para proveedores</label>
									<div class="form-group prueba3" style="margin-right:0px;margin-left:0px;margin-bottom:0px">
										<textarea name="descripcion_pro" id="<?php echo $prefix; ?>descripcion_pro" value="<?php echo (isset($producto["descripcion_pro"])) ? $producto["descripcion_pro"] : ""; ?>" placeholder="Esta nota, aparecera en los pedidos de compra" class="form-control prueba3" rows="3"><?php echo (isset($producto["descripcion_pro"])) ? $producto["descripcion_pro"] : ""; ?></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="row form-group">
		<div class="col-lg-12">
			<button type="button" id="<?php echo $prefix; ?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
			<button type="button" id="<?php echo $prefix; ?>btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
		</div>
	</div>
</form>

<?php echo $form_categoria; ?>
<?php echo $form_marca; ?>
<?php echo $form_modelo; ?>
<?php echo $form_color; ?>
<?php echo $form_tamanio; ?>
<?php echo $form_material; ?>
<?php echo $form_unidad; ?>

<style>
.outlinenone {
    outline: none;
    background-color: #D2D2FF;
    border: 0;
	}
.prueba2 {
	border: 0;
    border-radius: 0;
	background-color: #F3F3F4;
    border-bottom: 1px solid #f71a1a;
	}
.prueba3 {
	border: 0;
    border-radius: 0;
    border-bottom: 1px solid #f71a1a;
	}
.separador {
	padding-top: 7px;
}
.app-img-temp {
    background-image: url(<?php echo base_url("/app/img/imgdef.png");?>);
    background-repeat: no-repeat;
    background-position: center center;
	min-height: 100px;
    width: 100%;
	cursor: pointer;
}
#<?php echo $prefix;?>load_photo .img-thumbnail {
	max-height: 130px;
    width: auto;
}
</style>