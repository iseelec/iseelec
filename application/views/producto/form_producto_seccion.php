<form id="form_seccion_medida" class="app-form">
	<input type="hidden" name="idproducto" id="sec_idproducto" value="<?php echo (isset($producto["idproducto"])) ? $producto["idproducto"]:""; ?>">
	<div class="row">
		<div class="col-sm-8">
			<select id="seccion_medidad_filtro" class="input-sm form-control input-s-sm inline"></select>
		</div>
		<div class="col-sm-4">
			<div class="btn-group">
				<button id="btn-add-seccion" class="btn btn-sm btn-white parent" type="button">Agregar item</button>
			</div>
		</div> 	
	</div>
	<div class="clients-list">
		<!-- <div class="full-height-scroll"> -->
			<div class="table-responsive">
				<table id="tabla_seccion_medida" class="tabla_modulos table table-striped">
					<thead>
						<tr>
							<th>Seccion</th>
							<th>Cantidad</th>
							<th class="tooltip-demo">Equivalencia (<a id="ref_seccion_medida" data-toggle="tooltip" title="<?php echo (isset($seccion["descripcion"])) ? $seccion["descripcion"]:""; ?>"><?php echo (isset($seccion["descripcion"])) ? $seccion["abreviatura"]:""; ?></a>)</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($producto_seccion)) {
							foreach($producto_seccion as $val){
								echo '<tr data-idseccion="'.$val["idseccion"].'">';
								echo '<td><input type="hidden" name="idseccion[]" class="idseccion" value="'.$val["idseccion"].'">'.$val["descripcion"].' ('.$val["abreviatura"].')</td>';
								echo '<td><input type="text" name="cantidad_seccion[]" class="cantidad_seccion form-control input-sm" value="'.$val["cantidad_seccion"].'" readonly></td>';
								echo '<td><input type="text" name="cantidad_seccion_min[]" class="cantidad_seccion_min form-control input-sm" value="'.$val["cantidad_seccion_min"].'"></td>';
								echo '<td><button type="button" class="btn btn-default btn-xs btn_delete_seccion_medida">Eliminar</button></td>';
								echo '</tr>';
							}
						}
						?>
					</tbody>
				</table>
			</div>
		<!-- </div> -->
	</div>
	<div class="row">
		<div class="form-group">
			<div class="col-lg-4">
				<button id="sec_prod_btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
				<button type="submit" id="sec_prod_btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
			</div>
		</div>
	</div>
</form>
<script>
SECCION_MEDIDA = <?php echo json_encode($secciones); ?>;
</script>