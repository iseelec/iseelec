<form id="form_<?php echo $controller; ?>" class="app-form form-uppercase" autocomplete="off" data-prefix="<?php echo $prefix;?>" data-modal="<?php echo $modal;?>">
	<input type="hidden" name="idcategoria" id="<?php echo $prefix; ?>idcategoria" value="<?php echo (!empty($idcategoria)) ? $idcategoria : ""; ?>">
	<div class="form-group">
		<label class="control-label required">Descripci&oacute;n</label>
		<input type="text" name="descripcion" id="<?php echo $prefix;?>descripcion" value="<?php echo (!empty($descripcion)) ? $descripcion : ""; ?>" class="form-control" required="">
	</div>
	<div class="form-group">
		<label class="control-label">Prefijo</label>
		<input type="text" name="prefijo" id="<?php echo $prefix;?>prefijo" value="<?php echo (!empty($prefijo)) ? $prefijo : ""; ?>" class="form-control">
	</div>
	<div class="form-group">
		<button type="button" id="<?php echo $prefix; ?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller;?>">Cancelar</button>
		<button type="submit" id="<?php echo $prefix; ?>btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
	</div>
</form>