<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cotizacion <?php echo $cotizacion["idpreventa"];?></title>
		<style>
		/*
table { overflow: visible !important; }
thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }
		*/
		body{
    margin: 5px;
    font-size: 12x;
    font-family: monospace;
    color: #000000;
    background-color: #ffffff;
    width: 20cm;
	padding-left: 7px;
}

.head{
    width: 100%;
}
.left{
    float: left;
}
.right{
    float: right;
}
.clearfix::after {
	display: block;
	content: "";
	clear: both;
}
@page{
    margin-top: 40px;
    margin-bottom: 10px;
    margin-right: 50px;
    margin-left: 50px;
}
.logo {height:110px;}
.documento {
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    font-weight: bold;
	border-width: 1px;
	border-color: #999999;
	border-style: solid;
}
.detalle {width:100%;border-width:1px;border-color:#999999;border-style:solid;border-collapse: collapse;}
.border-all {border-width:1px;border-color:#999999;border-style:solid;}
.border-all1 {border-width:2px;border-color:blue;border-style:solid;}
.border {
	border-right-width:1px;
	border-right-color:#999999;
	border-right-style:solid;
	border-left-width:1px;
	border-left-color:#999999;
	border-left-style:solid;
}
.head-detalle {border-width:1px;border-color:#999999;border-style:solid;background-color:#dedede;font-weight:bold;text-align:center;}
.head-title {font-size:20px;font-weight:bold;}
.text-right {text-align:right;}
.text-center {text-align:center;}
.detalle-title {font-weight:bold;}
.border-bottom-double {
	border-bottom-width:3px;
	border-bottom-color:#999999;
	border-bottom-style:solid;
}
.background {background-color:#dedede;}
.table-padding tr td{padding-top:2px;padding-bottom:2px;}
.div-foot {font-size:10px;}
		</style>
    </head>
    <body>
		<table class="head">
				<tr><?php if( ! empty($logo)) {?>
					<td rowspan="3" style="width: 100px;">
					<img src="data:image/png;base64, <?php echo $logo;?>" class="logo" alt="..."/>
					</td>
					<?php }?>
					
					<td><div style="font-weight:bold; font-size:40px; color:blue; text-decoration: underline overline; font-family:georgia;">Inga Seguridad Electrónica</div></td>
					<td><div> <b style="font-weight:bold; font-size:14px; text-decoration:none; color:blue;">.S</b><br><b style="font-weight:bold; font-size:14px; color:blue;"b>.A</b><br><b style="font-weight:bold; font-size:14px; color:blue;">.C</b></div></td>
					<td rowspan="3" class="border-all1" style="width:300px;padding-right:10px;padding-left:10px;font-weight:bold;"><div>
						VENTA, INSTALACIÓN Y MANTENIMIENTO DEL SISTEMA DE ALARMA CONTRA ROBO, DETECCIÓN DE INCENDIO, CONTROL DE 
						ACCESO, CIRCUITO CERRADO DE TV (CCTV), CABLEADO ESTRUCTURADO, RED HÚMEDA.
					</div></td>
				</tr>
				
				<tr>
					<td><div style="font-weight:bold;">RUC: <?php echo $empresa["ruc"];?></div></td><td></td>
				</tr>
				<tr>
					<td style="width:480px;"><div>DIRECCION: <?php echo $empresa["direccion"];?></div></td><td></td>
				</tr>
				
		
	
		
		<!-- Borrar luego  -->
			<!-- <tr> -->
				<!-- <?php if( ! empty($logo)) {?> -->
				<!-- <td style="width: 150px;"> -->
					<!-- <img src="data:image/png;base64, <1?php echo $logo;?>" class="logo" alt="..."/> -->
				<!-- </td> -->
				<!-- <?php }?> -->
				<!-- <td> -->
					<!-- <div> -->
						<!-- <div style="font-weight:bold;"><?php echo $empresa["descripcion"];?></div> -->
						<!-- <div style="font-weight:bold; font-size:40px; color:blue; text-decoration: underline overline; font-family:georgia;">Inga Seguridad Electrónica</div> -->
						<!-- <div style="font-weight:bold;">RUC: <?php echo $empresa["ruc"];?></div> -->
						<!-- <div><b>DIRECCION: <?php echo $empresa["direccion"];?></b></div> -->
					<!-- </div> -->
				<!-- </td> -->
				<!-- <td> -->
				<!-- <div> <b style="font-weight:bold; font-size:15px; text-decoration:none; color:blue;">.S</b><br><b style="font-weight:bold; font-size:15px;"b>.A</b><br><b style="font-weight:bold; font-size:15px;">.C</b></div> -->
				<!-- </td> -->
				<!-- <td class="border-all1" style="width:300px;padding-right:10px;padding-left:10px;font-weight:bold;"> -->
					<!-- <div> -->
						<!-- VENTA, INSTALACIÓN Y MANTENIMIENTO DEL SISTEMA DE ALARMA CONTRA ROBO, DETECCIÓN DE INCENDIO, CONTROL DE  -->
						<!-- ACCESO, CIRCUITO CERRADO DE TV (CCTV), CABLEADO ESTRUCTURADO, RED HÚMEDA. -->
					<!-- </div> -->
				<!-- </td> -->
			<!-- </tr> -->
			<!-- antiguo solo le dejo temporalmente  -->
		</table>
		<hr style="border-width:1px;border-color:#999999;border-style:solid;">
		<?php 
		$date = new DateTime($cotizacion["fecha"]);
		$format = "d/m/Y h:ia";
		$w = "100px";
		$w1 = "80px";
		//$w2 = "80px";
		?>
		<br>
		<table class="head">
			<tr>
				<td style="vertical-align:top;">
					<div>
						<div style="font-weight:bold;">DATOS DEL CLIENTE</div>
						<div style="font-weight:bold;">Razon Social:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $cotizacion["cliente"];?></div>
					</div>
					<div>
						<!-- <div style="font-weight:bold;">Cliente - </div> -->
						<div style="font-weight:bold;">Ruc:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $cotizacion["ruc"];?></div>
					</div>
					<div>
						<!-- <div style="font-weight:bold;">Cliente - Direcci&oacute;n:</div> -->
						<div>Direcci&oacute;n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $cotizacion["direccion"];?></div>
					</div>
				</td>
				<!-- <td style="width: 50px;">&nbsp;</td> -->
				<!-- <td style="vertical-align:top;"> -->
					<!-- <1?php echo $cotizacion["direccion"];?> -->
				<!-- </td> -->
			</tr>
		</table>
		<br>
		<div class="head-title">Cotizaci&oacute;n N&deg;<?php echo $cotizacion["idpreventa"]; if ($cotizacion["n_modificacion"] !=0) {?> Modi <?php echo $cotizacion["n_modificacion"];}?> </div>
		<br>
		<div class="border-all">
			<table class="head">
				<tr>
					<td style="vertical-align:top;width:25%;">
						<div>
							<div style="font-weight:bold;">Referencia:</div>
							<div><?php echo $cotizacion["referencia"];?></div>
						</div>
					</td>
					<td style="vertical-align:top;width:25%;">
						<div>
							<div style="font-weight:bold;">Fecha del presupuesto:</div>
							<div><?php echo $date->format($format);?></div>
						</div>
					</td>
					<td style="vertical-align:top;width:25%;">
						<div>
							<div style="font-weight:bold;">Comercial:</div>
							<div><?php echo $cotizacion["vendedor"];?></div>
						</div>
					</td>
					<td style="vertical-align:top;width:25%;">
						<div>
							<div style="font-weight:bold;">Plazo de pago:</div>
							<div><?php echo $cotizacion["tipopago"];?></div>
						</div>
					</td>
				</tr>
			</table>
		</div>
        <br>
        <table class="detalle">
		<?php if($cotizacion["descuento"] > 0) {?>
			<tr class="border-all border-bottom-double">
				<td class="detalle-title" style="width:30px;">#</td>
				<?php if($cotizacion["mostrar_imagen"] == 'S') {?>
				<td class="detalle-title" style="width:<?php echo $w1;?>;">IMAGEN REFERENCIAL</td>
				<?php } ?>
				<td class="detalle-title">DESCRIPCION</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">MARCA</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">MODELO</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">CANTIDAD</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">PRECIO UNITARIO</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">DESCUENTO %</td>
				<td class="detalle-title text-right" style="width:<?php echo $w;?>;">IMPORTE</td>
			</tr>			
			<?php } else { ?>
			<tr class="border-all border-bottom-double">
				<td class="detalle-title" style="width:30px;">#</td>
				<?php if($cotizacion["mostrar_imagen"] == 'S') {?>
				<td class="detalle-title" style="width:<?php echo $w;?>;">IMAGEN REFERENCIAL</td>
				<?php } ?>
				<td class="detalle-title">DESCRIPCION</td>
				<td class="detalle-title text-right" style="width:<?php echo $w;?>;">MARCA</td>
				<td class="detalle-title text-right" style="width:<?php echo $w;?>;">MODELO</td>
				<td class="detalle-title text-right" style="width:<?php echo $w;?>;">CANTIDAD</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">PRECIO UNITARIO</td>
				<!-- <td class="detalle-title text-right" style="width:<1?php echo $w1;?>;">DESCUENTO %</td> -->
				<td class="detalle-title text-right" style="width:<?php echo $w;?>;">IMPORTE</td>
			</tr>
			<?php }
			if( ! empty($conceptos)) {
				$i = 0;
				foreach($conceptos as $v) {
					$i ++;
				if($cotizacion["descuento"] > 0) {	
				if($cotizacion["mostrar_imagen"] == 'N') {
			?>
			<tr>
				<td colspan="8" class="border-all background" style="text-align:right;"><span style="font-weight:bold;">Subtotal:</span> <?php echo $cotizacion["moneda"]." ".number_format($v["total"],2,".",",");?></td>
			</tr>
			<?php } else {?>
			<tr>
				<td colspan="9" class="head-detalle" style="text-align:left;padding-top:5px;padding-bottom:5px;"><?php echo $i.". ".$v["seccion"];?></td>
			</tr>
			<?php } } else { ?>
			<?php if($cotizacion["mostrar_imagen"] == 'N') {?>
			<tr>
				<td colspan="7" class="head-detalle" style="text-align:left;padding-top:5px;padding-bottom:5px;"><?php echo $i.". ".$v["seccion"];?></td>
			</tr>
			
			<?php }
			}
			if( ! empty($v["items"])) {
				foreach($v["items"] as $j=>$r) {
			if($cotizacion["descuento"] > 0) {
			?>
			<tr>
				<td class="border-all text-center" style="width:30px;vertical-align:top;"><?php echo $i.".".($j+1);?></td>
				<?php if($cotizacion["mostrar_imagen"] == 'S') {?>
				<td class="border-all text-center" style="width:<?php echo $w;?>;vertical-align:top;">
					<?php if( ! empty($r["imagen"])) {?>
					<img src="data:image/png;base64, <?php echo $r["imagen"];?>" alt="..." style="height:60px;"/>
					<?php }?>
				</td>
				<?php }?>
				<td class="border-all" style="vertical-align:top;"><?php echo $r["descripcion_cli"];?></td>
				<td class="border-all" style="width:<?php echo $w1;?>;vertical-align:top;"><?php echo $r["marca"];?></td>
				<td class="border-all" style="width:<?php echo $w1;?>;vertical-align:top;"><?php echo $r["modelo"];?></td>
				<td class="border-all text-right" style="width:<?php echo $w1;?>;vertical-align:top;"><?php echo number_format($r["cantidad"],3,".","")."<br>".$r["unidad"];?></td>
				<td class="border-all text-right" style="width:<?php echo $w1;?>;vertical-align:top;"><?php echo number_format($r["precio"],2,".",",");?></td>
				<!-- <1?php if($r["descuento"]>0) {?> -->
				<td class="border-all text-right" style="width:<?php echo $w1;?>;vertical-align:top;"><?php echo number_format($r["descuento"],2,".",",");?></td>
				<!-- <1?php } ?> -->
				<td class="border-all text-right" style="width:<?php echo $w;?>;vertical-align:top;"><?php echo $cotizacion["moneda"]." ".number_format($r["importe"],2,".",",");?></td>
			</tr>
			<?php } else { ?>
			<tr>
				<td class="border-all text-center" style="width:30px;vertical-align:top;"><?php echo $i.".".($j+1);?></td>
			<?php if($cotizacion["mostrar_imagen"] == 'S') {?>
				<td class="border-all text-center" style="width:<?php echo $w;?>;vertical-align:top;">
					<?php if( ! empty($r["imagen"])) {?>
					<img src="data:image/png;base64, <?php echo $r["imagen"];?>" alt="..." style="height:60px;"/>
					<?php }?>
				</td>
				<?php }?>
				<td class="border-all" style="vertical-align:top;"><?php echo $r["descripcion_cli"];?></td>
				<td class="border-all" style="width:<?php echo $w;?>;vertical-align:top;"><?php echo $r["marca"];?></td>
				<td class="border-all" style="width:<?php echo $w;?>;vertical-align:top;"><?php echo $r["modelo"];?></td>
				<td class="border-all text-right" style="width:<?php echo $w;?>;vertical-align:top;"><?php echo number_format($r["cantidad"],3,".","")."<br>".$r["unidad"];?></td>
				<td class="border-all text-right" style="width:<?php echo $w;?>;vertical-align:top;"><?php echo number_format($r["precio"],2,".",",");?></td>
				<!-- <1?php if($r["descuento"]>0) {?> -->
				<!-- <td class="border-all text-right" style="width:<1?php echo $w1;?>;vertical-align:top;"><?php echo number_format($r["descuento"],2,".",",");?></td> -->
				<!-- <1?php } ?> -->
				<td class="border-all text-right" style="width:<?php echo $w;?>;vertical-align:top;"><?php echo $cotizacion["moneda"]." ".number_format($r["importe"],2,".",",");?></td>
			</tr>
			<?php }
				}
			}
			if($cotizacion["descuento"] > 0) {
			?>
			<?php if($cotizacion["mostrar_imagen"] == 'N') {?>
			<tr>
				<td colspan="8" class="border-all background" style="text-align:right;"><span style="font-weight:bold;">Subtotal:</span> <?php echo $cotizacion["moneda"]." ".number_format($v["total"],2,".",",");?></td>
			</tr>
			<?php } else {?>
			<tr>
				<td colspan="9" class="border-all background" style="text-align:right;"><span style="font-weight:bold;">Subtotal:</span> <?php echo $cotizacion["moneda"]." ".number_format($v["total"],2,".",",");?></td>
			</tr>
			<?php } } else { ?>
			<?php if($cotizacion["mostrar_imagen"] == 'N') {?>
			<tr>
				<td colspan="7" class="border-all background" style="text-align:right;"><span style="font-weight:bold;">Subtotal:</span> <?php echo $cotizacion["moneda"]." ".number_format($v["total"],2,".",",");?></td>
			</tr>
			<?php }
				} 
				}
			}
			?>
		</table>
        <br>
		<table class="head">
			<tr>
				<td style="width:20%;"></td>
				<td style="width:80%;text-align:right;">
					<table class="head border-all table-padding" style="border-collapse:collapse;width:300px;float:right;">
						<tr>
							<td style="font-weight:bold;">Subtotal</td>
							<td class="text-right"><?php echo $cotizacion["moneda"]." ".number_format($cotizacion["subtotal"],2,".",",");?></td>
						</tr>
						<tr>
							<td class="border-all">IGV 18%</td>
							<td class="border-all text-right background"><?php echo $cotizacion["moneda"]." ".number_format($cotizacion["igv"],2,".",",");?></td>
						</tr>
						<tr>
							<td style="background-color:#333;color:#fff;">Total</td>
							<td style="background-color:#333;color:#fff;text-align:right;"><?php echo $cotizacion["moneda"]." ".number_format($cotizacion["total"],2,".",",");?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
		<div class="div-foot">
			<!-- <div>GARANTÍA DE EQUIPOS: <1?php echo $cotizacion["garantia_equi"];?></div> -->
			<!-- <div>GARANTÍA DEL SERVICIO: <1?php echo $cotizacion["garantia_ser"];?></div> -->
			<br>
			<div>SON  <b><?php echo $this->numeroletra->convertir($cotizacion["total"])."  ".$cotizacion["desmoneda"];?>; INCLUIDO IGV</b></div>
			<div>OJO.<br><?php echo $cotizacion["condiciones_ser"];?></div>
		<br>
			<div style="font-size:11px;">Plazo de pago: <?php echo $cotizacion["tipopago"];?></div>
		</div>
		<script>
			<?php if($print) {?>
			window.print();
			<?php }?>
			<?php if($close) {?>
			window.close();
			<?php }?>
		</script>
    </body>
</html>