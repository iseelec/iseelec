<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cotizacion <?php echo $cotizacion["idpreventa"];?></title>
		<style>
		/*
table { overflow: visible !important; }
thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }
		*/
		body{
    margin: 5px;
    font-size: 11x;
    font-family: sans-serif;
    color: #000000;
    background-color: #ffffff;
    width: 20cm;
	padding-left: 7px;
}

.head{
    width: 100%;
}
.left{
    float: left;
}
.right{
    float: right;
}
.clearfix::after {
	display: block;
	content: "";
	clear: both;
}
@page{
	size: auto;
    margin-top: 160px;
    margin-bottom: 80px;
    margin-right: 20px;
    margin-left: 20px;
	odd-header-name: html_MyHeader1;
	odd-footer-name: html_MyFooter1;
}
.logo {height:80px;}
.documento {
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    font-weight: bold;
	border-width: 1px;
	border-color: #999999;
	border-style: solid;
}
.detalle {width:100%;border-width:1px;border-color:#999999;border-style:solid;border-collapse: collapse;}
.border-all {border-width:1px;border-color:#999999;border-style:solid;}
.border {
	border-right-width:1px;
	border-right-color:#999999;
	border-right-style:solid;
	border-left-width:1px;
	border-left-color:#999999;
	border-left-style:solid;
}
.head-detalle {border-width:1px;border-color:#999999;border-style:solid;background-color:#dedede;font-weight:bold;text-align:center;}
.head-title {font-size:20px;font-weight:bold;}
.text-right {text-align:right;}
.text-center {text-align:center;}
.detalle-title {font-weight:bold;}
.border-bottom-double {
	border-bottom-width:3px;
	border-bottom-color:#666666;
	border-bottom-style:solid;
}
.background {background-color:#dedede;}
.table-padding tr td{padding-top:2px;padding-bottom:2px;}
.div-foot {font-size:10px;}
		</style>
    </head>
    <body>
		<htmlpageheader name="MyHeader1">
		<table class="head">
			<tr>
				<?php if( ! empty($logo)) {?>
				<td style="width: 120px;">
					<img src="data:image/png;base64, <?php echo $logo;?>" class="logo" alt="..."/>
				</td>
				<?php }?>
				<td>
					<div>
						<div style="font-weight:bold;"><?php echo $empresa["descripcion"];?></div>
						<div style="font-weight:bold;">RUC: <?php echo $empresa["ruc"];?></div>
						<div>DIRECCION: <?php echo $empresa["direccion"];?></div>
					</div>
				</td>
				<td class="border-all" style="width:300px;padding-right:10px;padding-left:10px;font-weight:bold;">
					<div>
						VENTA, INSTALACIÓN Y MANTENIMIENTO DEL SISTEMA DE ALARMA CONTRA ROBO, DETECCIÓN DE INCENDIO, CONTROL DE 
						ACCESO, CIRCUITO CERRADO DE TV (CCTV), CABLEADO ESTRUCTURADO, RED HÚMEDA.
					</div>
				</td>
			</tr>
		</table>
		</htmlpageheader>
		<htmlpagefooter name="MyFooter1">
		<div style="font-size:10px;text-align:center;border-top-width:3px;border-top-color:#666666;border-top-style:solid;">
		Tel.:<?php echo $sucursal["telefono"];?> &nbsp;&nbsp;&nbsp; Correo: ventas@iseelec.com &nbsp;&nbsp;&nbsp; Web: http://www.iseelec.com &nbsp;&nbsp;&nbsp; RUC: <?php echo $empresa["ruc"];?>
		<br>P&aacute;gina: {PAGENO}/{nbpg}
		</div>
		</htmlpagefooter>
		<?php 
		$date = new DateTime($cotizacion["fecha"]);
		$format = "d/m/Y h:ia";
		$w = "100px";
		$w1 = "80px";
		?>
		<table class="head">
			<tr>
				<td style="vertical-align:top;">
					<div>
						<div style="font-weight:bold;">Direcci&oacute;n de facturaci&oacute;n:</div>
						<div><?php echo $cotizacion["direccion"];?></div>
					</div>
					<br>
					<div>
						<div style="font-weight:bold;">Direcci&oacute;n de despacho:</div>
						<div><?php echo $cotizacion["direccion"];?></div>
					</div>
				</td>
				<td style="width: 50px;">&nbsp;</td>
				<td style="vertical-align:top;">
					<?php echo $cotizacion["direccion"];?>
				</td>
			</tr>
		</table>
		<br>
		<div class="head-title">Cotizaci&oacute;n # N&deg;<?php echo $cotizacion["idpreventa"];?></div>
		<br>
		<div class="border-all">
			<table class="head">
				<tr>
					<td style="vertical-align:top;width:25%;">
						<div>
							<div style="font-weight:bold;">Referencia:</div>
							<div><?php echo $cotizacion["referencia"];?></div>
						</div>
					</td>
					<td style="vertical-align:top;width:25%;">
						<div>
							<div style="font-weight:bold;">Fecha del presupuesto:</div>
							<div><?php echo $date->format($format);?></div>
						</div>
					</td>
					<td style="vertical-align:top;width:25%;">
						<div>
							<div style="font-weight:bold;">Comercial:</div>
							<div><?php echo $cotizacion["cliente"];?></div>
						</div>
					</td>
					<td style="vertical-align:top;width:25%;">
						<div>
							<div style="font-weight:bold;">Plazo de pago:</div>
							<div><?php echo $cotizacion["tipopago"];?></div>
						</div>
					</td>
				</tr>
			</table>
		</div>
        <br>
        <table class="detalle">
			<tr class="border-all border-bottom-double">
				<td class="detalle-title" style="width:30px;">#</td>
				<td class="detalle-title" style="width:<?php echo $w1;?>;">IMAGEN REFERENCIAL</td>
				<td class="detalle-title">DESCRIPCION</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">MARCA</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">MODELO</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">CANTIDAD</td>
				<td class="detalle-title text-right" style="width:<?php echo $w1;?>;">PRECIO UNITARIO</td>
				<td class="detalle-title text-right" style="width:<?php echo $w;?>;">IMPORTE</td>
			</tr>
			<?php
			if( ! empty($conceptos)) {
				$i = 0;
				foreach($conceptos as $v) {
					$i ++;
			?>
			<tr>
				<td colspan="8" class="head-detalle" style="text-align:left;padding-top:5px;padding-bottom:5px;"><?php echo $i.". ".$v["seccion"];?></td>
			</tr>
			<?php
			if( ! empty($v["items"])) {
				foreach($v["items"] as $j=>$r) {
			?>
			<tr>
				<td class="border-all text-center" style="width:30px;vertical-align:top;"><?php echo $i.".".($j+1);?></td>
				<td class="border-all text-center" style="width:<?php echo $w;?>;vertical-align:top;">
					<?php if( ! empty($r["imagen"])) {?>
					<img src="data:image/png;base64, <?php echo $r["imagen"];?>" alt="..." style="height:60px;"/>
					<?php }?>
				</td>
				<td class="border-all" style="vertical-align:top;"><?php echo $r["producto"];?></td>
				<td class="border-all" style="width:<?php echo $w1;?>;vertical-align:top;"><?php echo $r["marca"];?></td>
				<td class="border-all" style="width:<?php echo $w1;?>;vertical-align:top;"><?php echo $r["modelo"];?></td>
				<td class="border-all text-right" style="width:<?php echo $w1;?>;vertical-align:top;"><?php echo number_format($r["cantidad"],3,".","")."<br>".$r["unidad"];?></td>
				<td class="border-all text-right" style="width:<?php echo $w1;?>;vertical-align:top;"><?php echo number_format($r["precio"],2,".",",");?></td>
				<td class="border-all text-right" style="width:<?php echo $w;?>;vertical-align:top;"><?php echo $cotizacion["moneda"]." ".number_format($r["importe"],2,".",",");?></td>
			</tr>
			<?php 
				}
			}
			?>
			<tr>
				<td colspan="8" class="border-all background" style="text-align:right;"><span style="font-weight:bold;">Subtotal:</span> <?php echo $cotizacion["moneda"]." ".number_format($v["total"],2,".",",");?></td>
			</tr>
			<?php 
				}
			}
			?>
		</table>
        <br>
		<table class="head">
			<tr>
				<td style="width:20%;"></td>
				<td style="width:80%;text-align:right;">
					<table class="head border-all table-padding" style="border-collapse:collapse;width:300px;float:right;">
						<tr>
							<td style="font-weight:bold;">Subtotal</td>
							<td class="text-right"><?php echo $cotizacion["moneda"]." ".number_format($cotizacion["subtotal"],2,".",",");?></td>
						</tr>
						<tr>
							<td class="border-all">IGV 18%</td>
							<td class="border-all text-right background"><?php echo $cotizacion["moneda"]." ".number_format($cotizacion["igv"],2,".",",");?></td>
						</tr>
						<tr>
							<td style="background-color:#333;color:#fff;">Total</td>
							<td style="background-color:#333;color:#fff;text-align:right;"><?php echo $cotizacion["moneda"]." ".number_format($cotizacion["total"],2,".",",");?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
		<div class="div-foot">
			<div>GARANTÍA DE EQUIPOS: 24 MESES</div>
			<div>GARANTÍA DEL SERVICIO: 12 MESES</div>
			<br>
			<div>OJO.<br>si de aprobarse el presupuesto ya no se está cobrando la limpieza de averías del panel simplex se estará revisando como un agregado .</div>
			<br>
			<div>Plazo de pago: <?php echo $cotizacion["tipopago"];?></div>
		</div>
    </body>
</html>