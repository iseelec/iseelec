<form id="form_proyectos" class="app-form">
	<input type="hidden" name="idtotalestudiantes" id="uni_idtotalestudiantes" value="<?php echo (isset($totalestudiantes["idtotalestudiantes"])) ? $totalestudiantes["idtotalestudiantes"]:""; ?>">
	<div class="row">
		<div class="col-sm-8">
			<select id="proyectos_filtro" class="input-sm form-control input-s-sm inline"></select>
		</div>
		<div class="col-sm-4">
			<div class="btn-group">
				<button id="btn-add-proyectos" class="btn btn-sm btn-white parent" type="button">Agregar item</button>
			</div>
		</div> 	
	</div>
	<div class="clients-list">
		<!-- <div class="full-height-scroll"> -->
			<div class="table-responsive">
				<table id="tabla_proyecto" class="tabla_modulos table table-striped">
					<thead>
						<tr>
							<th>Unidad medida</th>
							<th>Cantidad</th>
							<th class="tooltip-demo">Equivalencia (<a id="ref_proyectos_medida" data-toggle="tooltip" title="<?php echo (isset($proyectos["descripcion"])) ? $proyectos["descripcion"]:""; ?>"><?php echo (isset($proyectos["descripcion"])) ? $proyectos["abreviatura"]:""; ?></a>)</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($totalestudiantes_proyectos)) {
							foreach($totalestudiantes_proyectos as $val){
								echo '<tr data-idproyecto="'.$val["idproyecto"].'">';
								echo '<td><input type="hidden" name="idproyecto[]" class="idproyecto" value="'.$val["idproyecto"].'">'.$val["descripcion"].' ('.$val["abreviatura"].')</td>';
								echo '<td><input type="text" name="cantidad_proyectos[]" class="cantidad_proyectos form-control input-sm" value="'.$val["cantidad_proyectos"].'" readonly></td>';
								echo '<td><input type="text" name="cantidad_proyectos_min[]" class="cantidad_proyectos_min form-control input-sm" value="'.$val["cantidad_proyectos_min"].'"></td>';
								echo '<td><button type="button" class="btn btn-default btn-xs btn_delete_proyectos_medida">Eliminar</button></td>';
								echo '</tr>';
							}
						}
						?>
					</tbody>
				</table>
			</div>
		<!-- </div> -->
	</div>
	<div class="row">
		<div class="form-group">
			<div class="col-lg-4">
				<button id="uni_prod_btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
				<button type="submit" id="uni_prod_btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
			</div>
		</div>
	</div>
</form>
<script>
GRADOS = <?php echo json_encode($proyectoss); ?>;
</script>