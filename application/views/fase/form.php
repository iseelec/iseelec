	<!--<form id="form_<?php echo $controller; ?>" action="<?php echo $controller; ?>/guardar" class="form-horizontal app-form" enctype="multipart/form-data"> -->
	<form id="form_<?php echo $controller; ?>" class="form-horizontal app-form" enctype="multipart/form-data">
		<input type="hidden" name="idfase" id="idfase" value="<?php echo (!empty($idfase)) ? $idfase : ""; ?>">
		<div id='input_extras'></div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-lg-3 control-label required">Desripción</label>
					<div class="col-lg-9">
						<input type="text" name="descripcion" id="descripcion" value="<?php echo (!empty($descripcion)) ? $descripcion : ""; ?>" class="form-control" required="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-3 control-labelrequired">Fecha Inicio</label>
						<div class="col-lg-9">
						<input type="text" name="fecha_inicio" id="fecha_inicio" value="<?php echo (!empty($fase["fecha_inicio"])) ? dateFormat($fase["fecha_inicio"], "d/m/Y") : date("d/m/Y"); ?>" class="form-control input-sm" placeholder="<?php echo date("d/m/Y"); ?>" required="">
						</div>
				</div>
			</div>

			<div class="col-md-6">			
				<div class="form-group">
					<label class=" col-lg-3 control-label required">Personal responsable del Grupo</label>
					<div class="col-lg-9">
					<span class="input-group-btn tooltip-demo">
					<?php echo $usuario; ?>
					</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-3 control-labelrequired">Fecha Fin Proyectada</label>
						<div class="col-lg-9">
						<input type="text" name="fecha_fin" id="fecha_fin" value="<?php echo (!empty($fase["fecha_fin"])) ? dateFormat($fase["fecha_fin"], "d/m/Y") : date("d/m/Y"); ?>" class="form-control input-sm" placeholder="<?php echo date("d/m/Y"); ?>" required="">
						</div>
				</div>
			</div>
		</div>
		
		<div class="row">
				<div class="form-group">
					<div class="col-lg-offset-4 col-lg-8">
						<button id="btn_cancel" class="btn btn-sm btn-white btn_cancel" >Cancelar</button>
						<button type="submit" id="btn_save" class="btn btn-sm btn-primary" >Guardar</button>
					</div>
				</div>
		</div>
	</form>

	<style type="text/css">
		.msg{
			color:red;
		}
	</style>