<form id="form_<?php echo $controller; ?>" class="app-form form-uppercase" autocomplete="off" data-prefix="<?php echo $prefix;?>" data-modal="<?php echo $modal;?>">
	<input type="hidden" name="idajustes" id="<?php echo $prefix; ?>idajustes" value="<?php echo (!empty($idajustes)) ? $idajustes : ""; ?>">
	<div class="row">
		<!-- // <div class="col-md-6"> -->
			<!-- // <div class="form-group"> -->
				<!-- // <label class="control-label required">Terminos y Condiciones en la Cotizaci&oacute;n</label> -->
				<!-- <input type="text" name="descripcion" id="<1?php echo $terminos; ?>" value="<1?php echo $terminos; ?>" class="form-control" required=""> -->
			<!-- // </div> -->
		<!-- // </div> -->
	
				<div class="col-md-6">
								<div class="form-group">
									<label>Terminos y Observaciones en la Cotizaci&oacute;n</label>
									<div class="input-group">
										<input type="hidden" name="idparam" id="idparam" value="123456">
										<textarea name="descripcion" id="descripcion" class="form-control input-xs"><?php echo $terminos; ?></textarea>
										<span class="input-group-btn">
											<button id="btn_save_condiciones" class="btn btn-white btn-xs" style="white-space:normal;vertical-align:bottom;font-size:13.4px;" >Grabar Terminos</button> 
										</span>
										<form id="form">
							</form>
									</div>
								</div>
				</div>
				
				
				<div class="col-md-5">
					<label>Configuración de Almacen</label>
					<div class="input-group">
						<span class="input-group-btn tooltip-demo">
								<!-- <input type="hidden" name="idalmacen" id="idalmacen" value="<?php echo (!empty($compra["idproveedor"])) ? $compra["idproveedor"] : ""; ?>"> -->
								
								<input type="text" name="almacen" id="almacen" value="<?php echo (!empty($venta["full_nombres"])) ? $venta["full_nombres"] : ""; ?>" class="form-control input-xs" placeholder="Descripción">

								<button type="button" id="btn-buscar-almacen" class="btn btn-outline btn-primary btn-xs" data-toggle="tooltip" title="Buscar Almacen">
									<i class="fa fa-search"></i>
								</button>
								<button type="button" id="btn-registrar-almacen" class="btn btn-outline btn-primary btn-xs" data-toggle="tooltip" title="&iquest;No existe el almacen? Registrar aqui">
									<i class="fa fa-file-o"></i>
								</button>
								
								<button type="button" id="btn-edit-cliente" class="btn btn-outline btn-primary btn-xs" data-toggle="tooltip" title="Editar Cliente">
									<i class="fa fa-edit"></i>
								</button>
							</span>
						</div>
					</div>
		</div>					


	
	<div class="form-group">
		<button id="<?php echo $prefix; ?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
		<button type="submit" id="<?php echo $prefix; ?>btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
	</div>
</form>
<!----------formulario ALMACEN -------------->
<div id="modal-almacen" class="modal fade" data-keyboard="false" aria-hidden="true" aria-labelledby="myLargeModalAlmacen" data-backdrop="static">
	<div class="modal-dialog modal-lg" style="margin-top: 10px;">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Registrar Almacen</h4>
			</div>
			<div class="modal-body" style="padding: 0px 30px 0px 29px;">
				<div class="row">
					<?php echo $form_almacen; ?>
				</div>
			</div>
		</div>
	</div>
</div>