<form id="form_<?php echo $controller; ?>" class="app-form form-uppercase" autocomplete="off" data-prefix="<?php echo $prefix;?>" data-modal="<?php echo $modal;?>">
	<input type="hidden" name="idseccion" id="<?php echo $prefix; ?>idseccion" value="<?php echo (!empty($idseccion)) ? $idseccion : ""; ?>">
	<div class="row form-group">
		<div class="col-md-12">
			<label class="control-label required">Descripci&oacute;n</label>
			<input type="text" name="descripcion" id="<?php echo $prefix; ?>descripcion" value="<?php echo (!empty($descripcion)) ? $descripcion : ""; ?>" class="form-control text-uppercase" required="">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label class="control-label required">Abreviatura</label>
			<input type="text" name="abreviatura" id="<?php echo $prefix; ?>abreviatura" value="<?php echo (!empty($abreviatura)) ? $abreviatura : ""; ?>" class="form-control" required="">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<button type="button" id="<?php echo $prefix;?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
			<button type="submit" id="<?php echo $prefix;?>btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
		</div>
	</div>
</form>