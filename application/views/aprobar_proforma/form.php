<div style="display:none;"><?php echo $proyecto; ?></div>
<form id="form_<?php echo $controller; ?>" class="app-form">
	<input type="hidden" name="idaprobar_proforma" id="idaprobar_proforma" value="<?php echo (!empty($aprobar_proforma["idaprobar_proforma"])) ? $aprobar_proforma["idaprobar_proforma"] : ""; ?>">
	<input type="hidden" name="idproforma" id="idproforma" value="">

	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="required">Buscar Proforma</label>
						<div class="input-group">
							<input type="text" name="proforma" id="buscar_proforma" value="<?php echo (!empty($aprobar_proforma["descripcion"])) ? $aprobar_proforma["descripcion"] : ""; ?>" class="form-control" required="">
							<span class="input-group-btn tooltip-demo">
								<button type="button" id="btn-buscar-proforma" class="btn btn-outline btn-primary" data-toggle="tooltip" title="Buscar Orden de Compra">
									<i class="fa fa-search"></i>
								</button>								
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label class="required">Tipo documento</label>
						<?php echo $tipodocumento; ?>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label class="required">Serie</label>
						<input type="text" name="serie" id="serie" value="<?php echo (!empty($aprobar_proforma["serie"])) ? $aprobar_proforma["serie"] : ""; ?>" class="form-control" required="">
					</div>
				</div>
				
			</div>
			
		</div>
		
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="required">Observaciones</label>
						<input type="text" name="observacion" id="observacion" value="<?php echo (!empty($aprobar_proforma["observacion"])) ? $aprobar_proforma["observacion"] : ""; ?>" class="form-control">
					</div>
				</div>
				
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="required">Nro. documento</label>
					<input type="text" name="numero" id="numero" value="<?php echo (!empty($aprobar_proforma["numero"])) ? $aprobar_proforma["numero"] : ""; ?>" class="form-control">
				</div>
			</div>
			
		</div>
	</div>
	
	
	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Detalle de productos a aprobar</div>
				<div class="panel-body">
					
					<div class="table-responsive">
						<table id="tbl-detalle" class="table table-striped tooltip-demo detail-table">
							<thead>
								<tr>
									<th style="width: 3%;"><input type="checkbox" id="check_all"></th>
									<th style="width: 37%;">Producto</th>
									<th style="width: 15%;">Proyecto</th>
									<th style="width: 10%;">U.Medida</th>
									<th style="width: 8%;">Cantidad</th>
									<th style="width: 8%;">Cant. Aprobada</th>
									<th style="width: 8%;">Cant. Pendiente</th>
									<th style="width: 8%;">Cant. a Aprobar</th>									
									<th style="width: 3%;"></th>
									<th style="display:none;"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="form-group">
			<div class="col-sm-6 text-left">
				<button class="btn btn-sm btn-warning btn_cancel" data-controller="<?php echo $controller; ?>"><sub class='hotkey white'>(Esc)</sub> Cancelar</button>
			</div>
			<div class="col-sm-6 text-right">
				<button class="btn btn-sm btn-primary" id="btn_save_aprobar_proforma" data-controller="<?php echo $controller; ?>"><sub class='hotkey white'>(F4)</sub> Guardar</button>
				<!--<input type="submit" id="btn_save_aprobar_proforma" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>" value="Guardar">-->
			</div>
		</div>
	</div>
</form>

<div id="modal-series" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="input-group"><input id="input-text-serie" placeholder="Ingrese la serie" class="input-sm form-control text-uppercase" type="text" />
							<span class="input-group-btn"><button id="btn-add-serie" type="button" class="btn btn-sm btn-primary">Agregar</button></span></div>
					</div>
				</div>
				<div class="table-responsive div_scroll" style="max-height:300px;">
					<table id="table-serie" class="table table-striped">
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="btn-close-serie" class="btn btn-primary">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<style>
	.numero{text-align:right;}
	.hotkey.white {
		color: #ccc;
	}
	table#dtaprobar_proforma_view_popup tbody>tr>td{padding: 4px !important;}
	sub.hotkey{bottom: 0;}
	
	.block_content {
		position: absolute;
		top: 80px;
		bottom: 0;
		right: 0;
		left: 0;
	}
</style>