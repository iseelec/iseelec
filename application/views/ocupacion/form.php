<form id="form_<?php echo $controller; ?>" class="app-form form-uppercase" autocomplete="off" data-prefix="<?php echo $prefix;?>" data-modal="<?php echo $modal;?>">
	<input type="hidden" name="idocupacion" id="<?php echo $prefix; ?>idocupacion" value="<?php echo (!empty($idocupacion)) ? $idocupacion : ""; ?>">
	<div class="form-group">
		<label class="control-label required">Descripci&oacute;n</label>
		<input type="text" name="ocupacion" id="<?php echo $prefix; ?>ocupacion" value="<?php echo (!empty($ocupacion)) ? $ocupacion : ""; ?>" class="form-control" required="">
	</div>
	<div class="form-group">
		<button type="button" id="<?php echo $prefix; ?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
		<button type="submit" id="<?php echo $prefix; ?>btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
	</div>
</form>