<?php 
if( ! empty($proforma["aprobado"]) && $proforma["aprobado"] == "S") {
?>
<div class="alert alert-danger">
	<strong class="alert-link">¡Proforma Aprobado!</strong> la proforma ya esta aprobado, no se puede modificar.
</div>
<?php 
}
?>
<form id="form_<?php echo $controller; ?>" class="app-form form-uppercase">
	<input type="hidden" name="idproforma" id="idproforma" value="<?php echo (!empty($proforma["idproforma"])) ? $proforma["idproforma"] : ""; ?>">
	<input type="hidden" name="idproveedor" id="proveedor_idproveedor" value="<?php echo (!empty($proforma["idproveedor"])) ? $proforma["idproveedor"] : ""; ?>">

	<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="required">Proveedor</label>
								<div class="input-group">
									<input type="text" name="proveedor" id="proveedor_descripcion" value="<?php echo (!empty($proveedor["nombre"])) ? $proveedor["nombre"] : ""; ?>" class="form-control input-sm" placeholder="Razon social o RUC" required="">
									<span class="input-group-btn tooltip-demo">
										<button type="button" id="btn-buscar-proveedor" class="btn btn-outline btn-primary btn-sm" data-toggle="tooltip" title="Buscar proveedores">
											<i class="fa fa-search"></i>
										</button>
										<button type="button" id="btn-registrar-proveedor" class="btn btn-outline btn-primary btn-sm" data-toggle="tooltip" title="&iquest;No existe el proveedor? Registrar aqui">
											<i class="fa fa-edit"></i>
										</button>
									</span>
								</div>
							</div>
						</div>		
						<div class="col-sm-2">
							<div class="form-group">
								<label class="required">Tipo documento</label>
								<?php  echo $tipodocumento; ?>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="required" style="font-size: 12px;">Serie</label>
								<input type="text" name="serie" id="serie" value="<?php echo (!empty($proforma["serie"])) ? $proforma["serie"] : ""; ?>" class="form-control input-sm" maxlength="4" required="">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="required">Numero</label>
								<input type="text" name="numero" id="numero" value="<?php echo (!empty($proforma["numero"])) ? $proforma["numero"] : ""; ?>" class="form-control input-sm" maxlength="8" required="">
							</div>
						</div>
		</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="required">Descripcion</label>
						<input type="text" name="descripcion" id="descripcion" value="<?php echo (!empty($proforma["descripcion"])) ? $proforma["descripcion"] : ""; ?>" class="form-control" required="">
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label class="required">Fecha emision</label>
						<div class="input-group date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type="text" name="fecha_proforma" id="fecha_proforma" value="<?php echo (!empty($proforma["fecha_proforma"])) ? dateFormat($proforma["fecha_proforma"], "d/m/Y") : date("d/m/Y"); ?>" class="form-control" required="">
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label class="required">Almacen</label>
						<?php echo $almacen; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Detalle proforma</div>
				<div class="panel-body">
					<div class="row m-b-sm m-t-sm">
						<div class="col-md-2">
							<button type="button" id="btn-buscar-producto" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Buscar Producto</button>
						</div>
						<div class="col-md-10">
							<div class="input-group">
								<input type="hidden" name="producto_idproducto" id="producto_idproducto">
								<input type="text" name="producto" id="producto_descripcion" placeholder="Nombre o codigo del producto" class="input-sm form-control">
								<span class="input-group-btn tooltip-demo">
									<button type="button" id="btn-agregar-producto" class="btn btn-sm btn-outline btn-primary" data-toggle="tooltip" title="Agregar producto a la tabla">
										<i class="fa fa-share"></i> <i class="fa fa-shopping-cart"></i>
									</button>
									<button type="button" id="btn-registrar-producto" class="btn btn-sm btn-outline btn-primary" data-toggle="tooltip" title="&iquest;No existe el producto? Registrar aqui">
										<i class="fa fa-edit"></i>
									</button>
								</span>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table id="tbl-detalle" class="table table-striped tooltip-demo detail-table">
							<thead>
								<tr>
									<th style="width: 4%"></th>
									<th style="width: 22%;">Producto</th>
									<th style="width: 7%;">Marca</th>
									<th style="width: 7%;">Modelo</th>
									<th style="width: 15%;">Proyecto</th>
									<th style="width: 10%;">U.Med.</th>
									<th style="width: 10%;">Precio</th>
									<th style="width: 10%;">Cant.</th>
									<th style="width: 10%;">Importe</th>
									<th style="width: 5%"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<div class="row">
		<div class="form-group">
			<div class="col-lg-12">
				<button class="btn btn-sm btn-white btn_cancel" data-controller="<?php echo $controller; ?>">Cancelar</button>
				<?php if(empty($proforma["aprobado"]) || $proforma["aprobado"] == "N") { ?>
				<button id="btn_save_proforma" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
				<?php } ?>
			</div>
		</div>
	</div>
</form>
<?php echo $form_producto; ?>
<?php echo $form_proveedor; ?>

<div style="display:none;"><?php echo $combo_proyecto ?></div>

<!-- // <div id="modal-proveedor" class="modal fade" aria-hidden="true" aria-labelledby="myLargeModalLabel" data-backdrop="static"> -->
	<!-- // <div class="modal-dialog modal-lg"> -->
		<!-- // <div class="modal-content"> -->
			<!-- // <div class="modal-header"> -->
				<!-- // <h4 class="modal-title">Registrar Proveedor</h4> -->
			<!-- // </div> -->
			<!-- // <div class="modal-body"> -->
				<!-- // <div class="row"> -->
					<!-- // <1?php echo $form_proveedor; ?> -->
				<!-- // </div> -->
			<!-- // </div> -->
		<!-- // </div> -->
	<!-- // </div> -->
<!-- // </div> -->
<!-- <div id="modal-producto" class="modal fade" aria-hidden="true"> -->
	<!-- <div class="modal-dialog"> -->
		<!-- <div class="modal-content"> -->
			<!-- <div class="modal-header"> -->
				<!-- <h4 class="modal-title">Registrar producto</h4> -->
			<!-- </div> -->
			<!-- <div class="modal-body"> -->
				<!-- <div class="row"> -->
					<!-- <1?php echo $form_producto; ?> -->
				<!-- </div> -->
			<!-- </div> -->
		<!-- </div> -->
	<!-- </div> -->
<!-- </div> -->

<div id="modal-unidad_medida" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Unidad de medida <small id="uni_producto_descripcion"></small></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<?php echo $form_producto_unidad; ?>
				</div>
			</div>
		</div>
	</div>
</div>