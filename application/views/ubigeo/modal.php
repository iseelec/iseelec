<div id="modal-ubigeo" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Seleccionar ubigeo</h4>
			</div>
			<div class="modal-body">
				<form id="form-ubigeo" class="form-uppercase">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Departamento</label>
								<?php echo $departamento; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Provincia</label>
								<?php echo $provincia; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Distrito</label>
								<?php echo $distrito; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 text-right">
							<button class="btn btn-sm btn-default btn-cancel-ubigeo">Cancelar</button>
							<button class="btn btn-sm btn-primary btn-accept-ubigeo">Aceptar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>