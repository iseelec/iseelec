<div class="row wrapper border-bottom white-bg page-heading fixed-button-top form-inline">
	<div class="col-md-12">
		<form id="form-data" autocomplete="off">
			<label>Fechas</label>
			<div class="input-group">
				<input type="text" class="form-control input-xs" id="fechai" name="fechai" placeholder="dd/mm/aaaa" style="width:90px;" value="<?php echo date("d/m/Y");?>">
				<span class="input-group-addon" style="padding:0 5px;">-</span>
				<input type="text" class="form-control input-xs" id="fechaf" name="fechaf" placeholder="dd/mm/aaaa" style="width:90px;">
			</div>
			<label>Tipo Doc.</label>
			<?php echo $tipodocumento;?>
			<label>Nro. Doc.</label>
			<input type="text" class="form-control input-xs" name="q" style="width:90px;">
			<label>Situaci&oacute;n</label>
			<?php echo $situacion;?>
			<label>Sucursal</label>
			<?php echo $sucursal;?>
			<div class="tooltip-demo" style="display:inline-block;margin-left:5px;">
			<button type="button" id="btnsearch" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Buscar Registros"><i class="fa fa-search"></i></button>
			</div><br>
			<div class="tooltip-demo" style="display:inline-block;margin-left:5px;">
				<button type="button" id="btnupdate" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Actualizar Estado del Comprobante"><i class="fa fa-refresh"></i> Actualizar</button>
				<button type="button" id="btngenerar" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="bottom" title="Generar Comprobante"><i class="fa fa-gears"></i> Generar</button>
				<button type="button" id="btnsend" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="Enviar Comprobante a SUNAT"><i class="fa fa-upload"></i> Enviar</button>
				<button type="button" id="btnprint" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Imprimir Comprobante"><i class="fa fa-print"></i> Imprimir</button>
				<button type="button" id="btndescargar" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Descargar Comprobante"><i class="fa fa-download"></i> Descargar</button>
				<button type="button" id="btnresumen" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="bottom" title="Resumen Diario de Boletas"><i class="fa fa-list-ol"></i> Resumen</button>
				<button type="button" id="btnreset" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="bottom" title="Formatear Comprobante"><i class="fa fa-undo"></i> Restaurar</button>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12" style="padding:0;">
		<!-- datos del credito -->
		<div class="ibox float-e-margins" style="margin-bottom: 0px;">
			<div class="ibox-content">
				<!--<div class="row">
					<div class="col-md-4 col-md-offset-8 m-b-xs">
						<div class="input-group">
							<input type="text" placeholder="Buscar" name="query" id="txtQuery" class="input-sm form-control">
							<span class="input-group-btn"><button type="button" class="btn btn-sm btn-primary" id="btnQuery"><i class="fa fa-search"></i></button> </span>
						</div>
					</div>
				</div>-->
				<div class="table-responsive">
					<table id="tabla-result" class="table table-bordered" style="width: 100%;margin-bottom:0;">
						<thead>
							<tr>
								<?php foreach($cols as $col) {?>
									<th><?php echo $col;?></th>
								<?php }?>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--<div class="loader"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>-->
<form id="modal-resumenDiario" class="modal fade" aria-hidden="true" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Generar Resumen Diario</h4>
			</div>
			<div class="modal-body">
				<div class="row form-group">
					<div class="col-sm-12">
						<label>Fecha de emisi&oacute;n o anulaci&oacute;n</label>
						<input type="text" class="form-control input-sm" id="fecha_resumen_diario" placeholder="dd/mm/aaaa">
					</div>
				</div>
				<p>Las boletas de venta emitidas o anuladas en la fecha ingresada se enviar&aacute;n a SUNAT</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
				<button type="button" id="btnSaveResumenDiario" class="btn btn-primary btn-sm">Generar Resumen Diario</button>
			</div>
		</div>
	</div>
</form>
<style>
.wrapper-content {padding:0;}
#tabla-result tbody tr {cursor:pointer;}
</style>