<div class="row wrapper border-bottom white-bg page-heading fixed-button-top form-inline">
	<div class="col-md-12">
		<form id="form-data">
			<label>A&ntilde;o</label>
			<?php echo $anios;?>
			<label>Mes</label>
			<?php echo $meses;?>
			<label>Comprobante</label>
			<?php echo $tipodocumento;?>
			<label>Sucursal</label>
			<?php echo $sucursal;?>
			<div class="tooltip-demo" style="display:inline-block;margin-left:5px;">
				<button type="button" id="btnsearch" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Buscar Registros"><i class="fa fa-search"></i></button>
				<button type="button" id="btnupdate" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Actualizar Estado"><i class="fa fa-refresh"></i></button>
				<button type="button" id="btngenerar" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="bottom" title="Generar Comprobante"><i class="fa fa-gears"></i></button>
				<button type="button" id="btnsend" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="Enviar Comprobante a SUNAT"><i class="fa fa-upload"></i></button>
				<button type="button" id="btnprint" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Imprimir Comprobante"><i class="fa fa-print"></i></button>
				<button type="button" id="btnbaja" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Comunicacion de Baja"><i class="fa fa-times"></i></button>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12" style="padding:0;">
		<!-- datos del credito -->
		<div class="ibox float-e-margins" style="margin-bottom: 0px;">
			<div class="ibox-content">
				<!--<div class="row">
					<div class="col-md-4 col-md-offset-8 m-b-xs">
						<div class="input-group">
							<input type="text" placeholder="Buscar" name="query" id="txtQuery" class="input-sm form-control">
							<span class="input-group-btn"><button type="button" class="btn btn-sm btn-primary" id="btnQuery"><i class="fa fa-search"></i></button> </span>
						</div>
					</div>
				</div>-->
				<div class="table-responsive">
					<table id="tabla-result" class="table table-bordered" style="width: 100%;margin-bottom:0;">
						<thead>
							<tr>
								<th style="width:10%;">Nro. RUC</th>
								<th style="width:11%;">Tipo Doc.</th>
								<th style="width:11%;">Numero Doc.</th>
								<th style="width:10%;">Doc. Ref.</th>
								<th style="width:8%;">F. Carga</th>
								<th style="width:10%;">F. Generaci&oacute;n</th>
								<th style="width:10%;">F. Envio</th>
								<th style="width:12%;">Situaci&oacute;n</th>
								<th style="width:18%;">Observaciones</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--<div class="loader"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>-->
<style>
.wrapper-content {padding:0;}
#tabla-result tbody tr {cursor:pointer;}
</style>