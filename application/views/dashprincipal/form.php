
    </div>
    </div>
    </div>
    </div>

<div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-sm-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Mes</span>
                                <h5>VENTAS</h5>
                            </div>
                            <div class="ibox-content">
                                <h2 class="no-margins">S./ 40'886,200</h2>
                                <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                                <small>Total income</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">D&iacute;a</span>
                                <h5>VENTAS CR&Eacute;DITOS</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">275,800</h1>
                                <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                                <small>New orders</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Today</span>
                                <h5>COMPRAS</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">106,120</h1>
                                <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                                <small>New visits</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Low</span>
                                <h5>COMPRAS POR PAGAR</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">80,600</h1>
                                <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>
                                <small>In first month</small>
                            </div>
                        </div>
            		</div>
            		<div class="col-sm-3">
		                <div class="widget style1 navy-bg">
		                    <div class="row">
		                        <div class="col-xs-4">
		                            <i class="fa fa-shopping-cart fa-2x"></i>
		                        </div>
		                        <div class="col-xs-8 text-right">
		                            <span> Ventas del D&iacute;a </span>
		                            <h2 class="font-bold">26'C</h2>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div class="col-sm-3">
		                <div class="widget style1 navy-bg">
		                    <div class="row">
		                        <div class="col-xs-4">
		                            <i class="fa fa-clipboard fa-2x"></i>
		                        </div>
		                        <div class="col-xs-8 text-right">
		                            <span> Inventario Valuado</span>
		                            <h2 class="font-bold">26'C</h2>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div class="col-sm-3">
		                <div class="widget style1 navy-bg">
		                    <div class="row">
		                        <div class="col-xs-4">
		                            <i class="fa fa-line-chart fa-2x"></i>
		                        </div>
		                        <div class="col-xs-8 text-right">
		                            <span> Gastos </span>
		                            <h2 class="font-bold">26'C</h2>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div class="col-sm-3">
		                <div class="widget style1 navy-bg">
		                    <div class="row">
		                        <div class="col-xs-4">
		                            <i class="fa fa-money fa-2x"></i>
		                            <!-- <span> Cuentas por Cobrar </span> -->
		                        </div>
		                         <div class="col-xs-8">
		                            <!-- <i class="fa fa-shopping-cart fa-2x"></i> -->
		                            <span> Cuentas por Cobrar </span>
		                        </div>
		                        <div class="col-xs-12 text-right">
		                            <h2 class="font-bold">S./ 	1 823 923</h2>
		                        </div>
		                    </div>
		                </div>
		            </div>
        </div>
        <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Orders</h5>
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-white active">Today</button>
                                        <button type="button" class="btn btn-xs btn-white">Monthly</button>
                                        <button type="button" class="btn btn-xs btn-white">Annual</button>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                <div class="col-lg-9">
                                    <div class="flot-chart">
                                        <div class="flot-chart-content" id="container"></div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <ul class="stat-list">
                                        <li>
                                            <h2 class="no-margins">2,346</h2>
                                            <small>Total orders in period</small>
                                            <div class="stat-percent">48% <i class="fa fa-level-up text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 48%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">4,422</h2>
                                            <small>Orders in last month</small>
                                            <div class="stat-percent">60% <i class="fa fa-level-down text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 60%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">9,180</h2>
                                            <small>Monthly income from orders</small>
                                            <div class="stat-percent">22% <i class="fa fa-bolt text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 22%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                                </div>

                            </div>
                        </div>
        </div>
        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
    
<!--<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>-->
<script src="<?php echo base_url(); ?>app/js/plugins/highcharts/highcharts.v8.js"></script>
<script src="<?php echo base_url(); ?>app/js/plugins/highcharts/exporting.v8.js"></script>