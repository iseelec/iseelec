<form id="form_<?php echo $controller; ?>" class="app-form form-uppercase form_clientito" autocomplete="off" data-prefix="<?php echo $prefix;?>" data-modal="<?php echo $modal;?>">
	<input type="hidden" name="idcliente" id="<?php echo $prefix;?>idcliente" value="<?php  echo (!empty($idcliente)) ? $idcliente : "";?>"/>
	<input type="hidden" name="foto" id="<?php echo $prefix;?>foto" value="<?php echo (!empty($foto)) ? $foto : "";?>"/>
	<input type="file" name="file" id="<?php echo $prefix; ?>file" style="display: none;" onchange='leerarchivobin(this)' />
	<input type="hidden" name="linea_credito" id="<?php echo $prefix;?>linea_credito" value="<?php  echo (!empty($linea_credito)) ? $linea_credito : 'N'; ?>"/>
	<input type="hidden" name="limite_credito" id="<?php echo $prefix;?>limite_credito" value="<?php  echo (!empty($limite_credito)) ? $limite_credito : '0'; ?>"/>
	
	<div class="row form-group">
		<div class="col-md-12">
		
			<div class="tabs-container">
				<ul id="tabs_<?php echo $controller;?>" class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#tab-<?php echo $controller;?>-1"> <i class="fa fa-folder-open-o" aria-hidden="true"></i>Datos Generales</a></li>
					<li class=""><a data-toggle="tab" href="#tab-<?php echo $controller;?>-2"><i class="fa fa-building-o" aria-hidden="true"></i>Datos Particulares</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="tab-<?php echo $controller;?>-1" class="tab-pane active">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group text-center">
												<div id="<?php echo $prefix;?>load_photo" class="app-img-temp">
													<?php
													$n_producto = FCPATH.'app/img/cliente/anonimo.jpg';
													if( ! empty($idcliente) && ! empty($foto))
														$n_producto = FCPATH.'app/img/cliente/'.$foto;
													if(file_exists($n_producto)) {
													?>
													<img src="<?php echo base_url("/app/img/cliente/".$foto);?>" class="img-responsive img-thumbnail"/>
													<?php }?>
												</div>
											</div>
										</div>
										<div class="col-md-9">
											<div class="row form-group">
												<div class="col-md-6">
													<label class="required">Tipo Cliente</label>
													<?php echo $combo_tipo;?>
												</div>
												<div class="col-md-6">
													<label class="label_ruc">RUC</label>
													<div class="input-group">
														<input type="text" class="form-control input-xs" name="ruc" id="<?php echo $prefix; ?>ruc" maxlength="11" value="<?php  echo (!empty($ruc)) ? $ruc : "" ?>"/>
														<span class="input-group-btn tooltip-demo">
															<button type="button" id="btn-search-ruc" class="btn btn-success btn-xs" data-toggle="tooltip" title="Buscar RUC">
																<i class="fa fa-search"></i>
															</button>
														</span>
													</div>
												</div>
											</div>
											<div class="row form-group">
												<div class="col-md-12">
													<label class="required">Nombres</label>
													<input type="text" name="nombres" class="form-control input-xs" id="<?php echo $prefix; ?>nombres" placeholder="Nombres / Razon social" value="<?php  echo (!empty($nombres)) ? $nombres : "" ?>" />
													<input type="text" name="apellidos" class="form-control input-xs" id="<?php echo $prefix; ?>apellidos" placeholder="Apellidos" value="<?php  echo (!empty($apellidos)) ? $apellidos : "" ?>" />
												</div>
											</div>
											<div class="row form-group">
												<div class="col-md-6">
													<label>E-Mail</label>
													<input type="text" class="form-control input-xs" name="cliente_email" id="<?php echo $prefix;?>cliente_email" value="<?php  echo (!empty($cliente_email)) ? $cliente_email : "" ?>">
												</div>
												<div class="col-md-6 info_natural">
													<label>DNI</label>
													<div class="input-group">
														<input name="dni" class="form-control input-xs" id="<?php echo $prefix;?>dni" maxlength="8" value="<?php  echo (!empty($dni)) ? $dni : "" ?>"/>
														<span class="input-group-btn tooltip-demo">
															<button type="button" id="btn-search-dni" class="btn btn-success btn-xs" data-toggle="tooltip" title="Buscar DNI">
																<i class="fa fa-search"></i>
															</button>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="row form-group">
										<div class="col-md-8">						
											<label>Referencia</label>
											<textarea name="observacion" id="<?php echo $prefix; ?>observacion" class="form-control input-xs"><?php  echo (!empty($observacion)) ? $observacion : "" ?></textarea>
										</div>
										
										<div class="col-md-4">
											<label>Telefono</label>
											<div class="list_telefonos tooltip-demo"></div>
										</div>
										
										<!--<div class="col-md-4">
											<label>Es Especial?</label>
											<div class="onoffswitch">
												<input type="checkbox" name="especial" id="<?php echo $prefix; ?>especial" class="onoffswitch-checkbox" value="1" <?php echo (isset($especial) && $especial == 'S') ? "checked" : ""; ?> >
												<label class="onoffswitch-label" for="<?php echo $prefix; ?>especial">
													<span class="onoffswitch-inner"></span>
													<span class="onoffswitch-switch"></span>
												</label>
											</div>
										</div>-->
									</div>
									<div class="row form-group">
										<div class="col-md-12">
											<label class="required">Direcci&oacute;n</label>
											<div class="list_direcciones tooltip-demo"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="tab-<?php echo $controller;?>-2" class="tab-pane">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<fieldset class="">
										<legend style="font-size:15px;"><label>Datos Cliente</label></legend>
										<div class="row" style="">
											<div class="col-md-12">
												<label class="">Zona</label>
												<div class="input-group">
													<?php echo $zona_combo;?>
													<span class="input-group-btn tooltip-demo">
														<button type="button" id="btn-registrar-zona" class="btn btn-outline btn-success btn-xs" data-toggle="tooltip" title="&iquest;No existe la zona? Registrar aqui">
															<i class="fa fa-edit"></i>
														</button>
													</span>
												</div>
												<br>
											</div>
										</div>
										
										<div class="row info_natural" style="">
											<div class="col-md-4 ">
												<label>Estado Civil</label>
												<?php echo $estado_civil;?>
											</div>
												
											<div class="col-md-4 ">
												<label>Sexo</label>
												<?php echo $combo_sexo;?>
											</div>

											<div class="col-md-4 ">
												<label>Fecha Nac.</label>
												<div class="input-group input-group-xs">
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													<input name="fecha_nac" id="<?php echo $prefix; ?>fecha_nac" type="text" class="form-control fecha_nac input-xs" value="<?php  echo (!empty($fecha_nac)) ? dateFormat($fecha_nac,"d/m/Y") : "" ?>" placeholder="dd/mm/aaaa" />
												</div>
											</div>
										</div>
									
									</fieldset>
								</div>
								
								<div class="col-md-6">
									<fieldset class="">
										<legend style="font-size:15px;"><label class="label_secundario">Datos Adicionales</label></legend>
										<div class="form-group" style="margin-right:0px;margin-left:0px;">
											<div class="info_natural" style="display:block;">
												<div class="row" style="">
													<div class="col-md-6 ">
														<label class="">Situacion Laboral</label>
														<?php echo $situacion; ?>
														<br>
													</div>
													
													<div class="col-md-6 ">
														<label class="">Centro de Trabajo</label>
														<input type="text" name="centro_laboral" id="<?php echo $prefix; ?>centro_laboral" value="<?php  echo (!empty($centro_laboral)) ? $centro_laboral : "" ?>" class="form-control centro_laboral input-xs">
														<br>
													</div>
												</div>
												
												<div class="row" style="">
													<div class="col-md-6">
														<label class="">Ocupacion / Cargo</label>
														<div class="input-group">
															<?php echo $ocupacion_cli;?>
															<span class="input-group-btn tooltip-demo">
																<button type="button" id="btn-registrar-ocupacion" class="btn btn-outline btn-success btn-xs" data-toggle="tooltip" title="&iquest;No existe el Cargo/Ocupacion? Registrar aqui">
																	<i class="fa fa-edit"></i>
																</button>
															</span>
														</div>
														<br>
													</div>

													<div class="col-md-6">
														<div class="">
															<label class="">Direccion Trabajo</label>
															<input type="text" name="direccion_trabajo" id="<?php echo $prefix; ?>direccion_trabajo" value="<?php  echo (!empty($direccion_trabajo)) ? $direccion_trabajo : "" ?>"  class="form-control direccion_trabajo input-xs">
															<br>
														</div>
													</div>
												</div>

												<div class="row" style="">
													<div class="col-md-4">
														<div class="">
															<label class="">Ingreso Mensual</label>
															<input type="text" name="ingreso_mensual" id="<?php echo $prefix; ?>ingreso_mensual" value="<?php  echo (!empty($ingreso_mensual)) ? $ingreso_mensual : "0.00" ?>" class="form-control ingreso_mensual numerillo input-xs">
														</div>
													</div>
												</div>
											</div>
											
											<div class="info_juridico" style="display:none;">
												<div class="row">
													<div class="list_representantes"></div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="row form-group">
		<div class="col-md-12">
			<button type="button" id="<?php echo $prefix; ?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
			<button type="button" id="<?php echo $prefix; ?>btn_save_cliente" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
		</div>
	</div>
</form>

<?php echo $form_zona;?>
<?php echo $form_ocupacion;?>

<style>
.app-img-temp {
    background-image: url(<?php echo base_url("/app/img/imgdef.png");?>);
    background-repeat: no-repeat;
    background-position: center center;
	min-height: 130px;
    width: 100%;
	cursor: pointer;
}
#<?php echo $prefix;?>load_photo .img-thumbnail {
	max-height: 160px;
    width: auto;
}
</style>