<form id="form_<?php echo $controller; ?>" class="app-form form_clientito" autocomplete="off" data-prefix="<?php echo $prefix;?>" data-modal="<?php echo $modal;?>">
	<input type="hidden" name="idcliente" id="<?php echo $prefix;?>idcliente" value="<?php  echo (!empty($idcliente)) ? $idcliente : "";?>"/>
	<input type="hidden" name="foto" id="<?php echo $prefix;?>foto" value="<?php echo (!empty($foto)) ? $foto : "";?>"/>
	<input type="file" name="file" id="<?php echo $prefix; ?>file" style="display: none;" onchange='leerarchivobin_<?php echo $controller;?>(this)' />
	<input type="hidden" name="linea_credito" id="<?php echo $prefix;?>linea_credito" value="<?php  echo (!empty($linea_credito)) ? $linea_credito : 'N'; ?>"/>
	<input type="hidden" name="limite_credito" id="<?php echo $prefix;?>limite_credito" value="<?php  echo (!empty($limite_credito)) ? $limite_credito : '0'; ?>"/>
	
	<div class="row form-group">
		<div class="col-md-12">
		
			<div class="tabs-container">
				<ul id="tabs_<?php echo $controller;?>" class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#tab-<?php echo $controller;?>-1"> <i class="fa fa-folder-open-o" aria-hidden="true"></i>Datos Generales</a></li>
					<?php if($controller == "cliente") {?>
					<li class=""><a data-toggle="tab" href="#tab-<?php echo $controller;?>-2"><i class="fa fa-building-o" aria-hidden="true"></i>Contactos</a></li>
					<?php }?>
				</ul>
				
				<div class="tab-content">
					<div id="tab-<?php echo $controller;?>-1" class="tab-pane active">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group text-center">
										<div id="<?php echo $prefix;?>load_photo" class="app-img-temp">
											<?php
											$n_producto = FCPATH.'app/img/cliente/anonimo.jpg';
											if( ! empty($idcliente) && ! empty($foto))
												$n_producto = FCPATH.'app/img/cliente/'.$foto;
											if(file_exists($n_producto)) {
											?>
											<img src="<?php echo base_url("/app/img/cliente/".$foto);?>" class="img-responsive img-thumbnail"/>
											<?php }?>
										</div>
									</div>
								</div>
								
								<div class="col-md-9">
									<?php if($controller == "cliente") {?>
									<div class="row form-group">
										<div class="col-md-4">
											<label>Tipo Cliente</label>
											<?php echo $combo_tipo;?>
										</div>
										<div class="col-md-4">
											<label class="label_ruc">RUC</label>
											<div class="input-group">
												<input type="text" class="form-control input-xs" name="ruc" id="<?php echo $prefix; ?>ruc" maxlength="11" value="<?php  echo (!empty($ruc)) ? $ruc : "" ?>"/>
												<span class="input-group-btn tooltip-demo">
													<button type="button" id="<?php echo $prefix; ?>btn-search-ruc" class="btn btn-success btn-xs" data-toggle="tooltip" title="Buscar RUC">
														<i class="fa fa-search"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="col-md-4 <?php echo $prefix; ?>info_natural">
											<label>DNI</label>
											<div class="input-group">
												<input name="dni" class="form-control input-xs" id="<?php echo $prefix;?>dni" maxlength="8" value="<?php  echo (!empty($dni)) ? $dni : "" ?>"/>
												<span class="input-group-btn tooltip-demo">
													<button type="button" id="<?php echo $prefix; ?>btn-search-dni" class="btn btn-success btn-xs" data-toggle="tooltip" title="Buscar DNI">
														<i class="fa fa-search"></i>
													</button>
												</span>
											</div>
										</div>
									</div>
									<?php } else {?>
									<input type="hidden" name="tipo" id="<?php echo $prefix;?>tipo" value="<?php echo (!empty($tipo)) ? $tipo : "N";?>" class="fixed-value"/>
									<input type="hidden" name="dni" id="<?php echo $prefix;?>dni" value="<?php echo (!empty($dni)) ? $dni : "";?>"/>
									<input type="hidden" name="ruc" id="<?php echo $prefix;?>ruc" value="<?php echo (!empty($ruc)) ? $ruc : "";?>"/>
									<?php }?>
									<div class="row form-group">
										<div class="col-md-8">
											<label>Nombres</label>
											<input type="text" name="nombres" class="form-control input-xs" id="<?php echo $prefix; ?>nombres" placeholder="Nombres / Razon social" value="<?php  echo (!empty($nombres)) ? $nombres : "" ?>" />
											<input type="text" name="apellidos" class="form-control input-xs" id="<?php echo $prefix; ?>apellidos" placeholder="Apellidos" value="<?php  echo (!empty($apellidos)) ? $apellidos : "" ?>" />
										</div>
										<div class="col-md-4">
											<label>Email</label>
											<input type="text" class="form-control input-xs" name="cliente_email" id="<?php echo $prefix;?>cliente_email" value="<?php  echo (!empty($cliente_email)) ? $cliente_email : "" ?>">
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-12">
											<label>Observaciones / Notas</label>
											<input type="text" class="form-control input-xs" name="observacion" id="<?php echo $prefix;?>observacion" value="<?php  echo (!empty($observacion)) ? $observacion : "" ?>">
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-3">
									<label>Telefono</label>
									<div class="<?php echo $prefix;?>list_telefonos tooltip-demo"></div>
								</div>
								<div class="col-md-9">
									<label>Direcci&oacute;n</label>
									<div class="<?php echo $prefix;?>list_direcciones tooltip-demo"></div>
								</div>
							</div>
						</div>
					</div>
					<?php if($controller == "cliente") {?>
					<div id="tab-<?php echo $controller;?>-2" class="tab-pane">
						<div class="panel-body">
							<div class="m-b-sm"><button type="button" class="btn btn-primary btn-xs" id="<?php echo $prefix;?>btn-registrar-contacto"><i class="fa fa-plus"></i> Crear contacto</button></div>
							<div id="contact-list-<?php echo $controller;?>" class="row"></div>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="row form-group">
		<div class="col-md-12">
			<button type="button" id="<?php echo $prefix; ?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
			<button type="button" id="<?php echo $prefix; ?>btn_save_cliente" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
		</div>
	</div>
</form>

<?php /*echo $form_zona;?>
<?php echo $form_ocupacion;*/?>
<?php if($controller == "cliente") {echo $form_contacto;}?>

<style>
.app-img-temp {
    background-image: url(<?php echo base_url("/app/img/imgdef.png");?>);
    background-repeat: no-repeat;
    background-position: center center;
	min-height: 130px;
    width: 100%;
	cursor: pointer;
}
#<?php echo $prefix;?>load_photo .img-thumbnail {
	max-height: 160px;
    width: auto;
}
.item-contact-box .trash-item {
	position: absolute;
    top: 6px;
    right: 24px;
}
</style>