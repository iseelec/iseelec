<div id="<?php echo $id;?>" class="modal fade <?php if($style !== false){echo "modal-".$style;}?>" role="dialog" aria-hidden="true" data-backdrop="static" <?php if($closeOnEscape !== false){echo 'data-keyboard="false"';}?> style="display: none;">
	<div class="modal-dialog <?php if($width !== false){echo $width;}?>">
		<div class="modal-content">
			 <?php if($title !== false){?>
			<div class="modal-header">
				 <?php if($closeIcon === true){?>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				 <?php }?>
				<h4 class="modal-title"><?php echo $title;?></h4>
			</div>
			 <?php }?>
			<div class="modal-body"><?php echo $content;?></div>
			<?php if( ! empty($buttons)){?>
			<div class="modal-footer">
				<?php echo implode("", $buttons);?>
			</div>
			<?php }?>
		</div>
	</div>
</div>