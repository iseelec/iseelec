	<!--<form id="form_<?php echo $controller; ?>" action="<?php echo $controller; ?>/guardar" class="form-horizontal app-form" enctype="multipart/form-data"> -->
	<form id="form_<?php echo $controller; ?>" class="form-horizontal app-form" enctype="multipart/form-data">
		<input type="hidden" name="idgasto" id="idgasto" value="<?php echo (!empty($idgasto)) ? $idgasto : ""; ?>">
		<input type="file" name="file" id="file" style="display: none;" onchange='leerarchivobin(this)' />
		<input type="hidden" id="imagen_doc" name="imagen_doc" value="<?php echo (!empty($imagen_doc)) ? $imagen_doc : "default_imagen_doc.png"; ?>"/>
		<input type="hidden" id="imagen_doc_new" name="imagen_doc_new" value="<?php echo (!empty($imagen_doc)) ? $imagen_doc : "default_imagen_doc.png"; ?>"/>
		<div id='input_extras'></div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-lg-3 control-label required">Descripción del Gasto</label>
					<div class="col-lg-9">
						<input type="text" name="descripcion" id="descripcion" value="<?php echo (!empty($descripcion)) ? $descripcion : ""; ?>" class="form-control input-xs" required="">
					</div>
				</div>
			</div>	
			<div class="col-md-6">					
				<div class="form-group">
					<label class="col-lg-3 control-label required">Gasto en Soles</label>
					<div class="col-lg-9">
						<input type="text" name="gasto" id="gasto" value="<?php echo (!empty($gasto)) ? $gasto : ""; ?>" class="form-control input-xs" required="">
					</div>
				</div>
			</div>	
			<div class="col-md-6">	
				<div class="form-group">
					<label class="col-lg-3 control-label required">Fecha del Gasto</label>
					<div class="col-lg-9">
					<input type="text" name="fecha_gasto" id="fecha_gasto" value="<?php echo (!empty($gasto["fecha_gasto"])) ? dateFormat($gasto["fecha_gasto"], "d/m/Y") : date("d/m/Y"); ?>" class="form-control input-sm input-xs" placeholder="<?php echo date("d/m/Y"); ?>" required="">
					</div>
				</div>
			</div>	
			<div class="col-md-6">	
			
				<div class="form-group">
					<label class="col-lg-3 control-label required">Documento de Referencia</label>
					<div class="col-lg-9">
						<input type="text" name="referencia_documento" id="referencia_documento" value="<?php echo (!empty($referencia_documento)) ? $referencia_documento : ""; ?>" class="form-control input-xs" required="">
					</div>
				</div>
				</div>	
			<div class="col-md-6">
					<div class="form-group">
						<label class="col-lg-3 control-label">Proyecto</label>
						<div class="col-lg-9">
							<?php echo $proyecto; ?>			
						</div>
					</div>
				</div>	
			<div class="col-md-6">
					<div class="form-group">
						<label class="col-lg-3 control-label">Observacion y/o Nota</label>
						<div class="col-lg-9">
						<textarea name="observacion" id="observacion" value="<?php echo (isset($gasto["observacion"])) ? $gasto["observacion"] : ""; ?>" placeholder="Aqui algun registro o recordatorio con referencia al Gasto" class="form-control prueba3" rows="3"><?php echo (!empty($observacion)) ? $observacion : ""; ?></textarea>
						<!-- <textarea name="observacion" id="<?php echo $prefix; ?>observacion" value="<?php echo (!empty($observacion)) ? $observacion : ""; ?>"  placeholder="Aqui algun registro o recordatorio con referencia al Gasto" class="form-control" rows="3"><?php echo (!empty($observacion)) ? $observacion : ""; ?></textarea> -->
							<!-- <input type="text" name="observacion" id="observacion" value="<1?php echo (!empty($observacion)) ? $observacion : ""; ?>" class="form-control input-xs" > -->
						</div>
					</div>
			</div>

			<!-- // <div class="col-md-6">			 -->
				<!-- // <div class="form-group"> -->
					<!-- // <label class="col-lg-2 control-label">Imagen_doc</label> -->
					<!-- // <div class="col-lg-9"> -->
						<!-- // <div id="load_photo" class="app-img-temp img-thumbnail"> -->
							<!-- <1?php -->
								<!-- $n_imagen_doc = '../app/img/gasto/default_imagen_doc.png'; -->
								<!-- if(!empty($idgasto)) -->
									<!-- // $n_imagen_doc = '../../app/img/gasto/'.$imagen_doc; -->
							<!-- // ?> -->
							<!-- // <img id="photo" src="<1?php echo $n_imagen_doc;?>" class="img-responsive img-thumbnail" style="background:#f3f3f4;"/> -->
						<!-- // </div> -->
					<!-- // </div> -->
				<!-- // </div>	 -->
			<!-- // </div> -->
		</div>
		
		<div class="row">
				<div class="form-group">
					<div class="col-lg-offset-4 col-lg-8">
						<button id="btn_cancel" class="btn btn-sm btn-white btn_cancel" >Cancelar</button>
						<button type="submit" id="btn_save" class="btn btn-sm btn-primary" >Guardar</button>
					</div>
				</div>
		</div>
	</form>

	<style type="text/css">
		.msg{
			color:red;
		}
	</style>