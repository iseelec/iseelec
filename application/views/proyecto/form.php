<!-- datos temporales -->
<div style="display:none;"><?php echo $tipo_precio_temp; ?></div>

<form id="form_<?php echo $controller; ?>" class="app-form form-uppercase" enctype="multipart/form-data">


	<input type="hidden" name="idproyecto" id="<?php echo $prefix; ?>idproyecto" value="<?php echo (isset($proyecto["idproyecto"])) ? $proyecto["idproyecto"] : ""; ?>">
	<input type="hidden" name="idcliente" id="proyecto_idcliente" value="<?php echo (!empty($proyecto["idcliente"])) ? $proyecto["idcliente"] : ""; ?>">
	<input type="hidden" name="estado_cliente" id="estado_cliente">

	<input type="hidden" name="corr_temp" id="<?php echo $prefix; ?>corr_temp" value="<?php echo (isset($corr_temp)) ? $corr_temp : ""; ?>">


       
       <input type="file" name="file" id="file" style="display: none;" onchange='leerarchivoprod(this)' />
		
		<input type="hidden" id="imagen_proyecto_new" name="imagen_proyecto_new" value="<?php echo (!empty($imagen_proyecto)) ? $imagen_proyecto : "default_proyecto.png"; ?>">


	   <div class="row">
		<div class="col-lg-12">
			<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#tab-1"> Datos b&aacute;sicos</a></li>
					<!--li class=""><a data-toggle="tab" href="#tab-31"> Secciones</a></li-->
					<li class=""><a data-toggle="tab" href="#tab-2"> Responsables del Py</a></li>
					<li class=""><a data-toggle="tab" href="#tab-3"> Fases del Py</a></li>
					<li class=""><a data-toggle="tab" href="#tab-4"> Tareas por Fase</a></li>
					<!--<li class=""><a data-toggle="tab" href="#tab-5"> Combos | Regalos</a></li>-->
				</ul>
				<div class="tab-content">
					<div id="tab-1" class="tab-pane active">
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="required">Descripci&oacute;n Corta</label>
										<input type="text" name="descripcion" id="<?php echo $prefix; ?>descripcion" value="<?php echo (isset($proyecto["descripcion"])) ? $proyecto["descripcion"] : ""; ?>" class="form-control" required="">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="required">Personal responsable del Grupo</label>
										<div class="input-group">
										<span class="input-group-btn tooltip-demo">
											<?php echo $usuario; ?>
										</span>
										</div>
									</div>
								</div>
							</div>
						
						
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="required">Descripci&oacute;n detallada</label>
										<input type="text" name="descripcion_detallada" id="<?php echo $prefix; ?>descripcion_detallada" value="<?php echo (isset($proyecto["descripcion_detallada"])) ? $proyecto["descripcion_detallada"] : ""; ?>" class="form-control" required="">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="required">Direci&oacute;n</label>
										<input type="text" name="direccion" id="<?php echo $prefix; ?>direccion" value="<?php echo (isset($proyecto["direccion"])) ? $proyecto["direccion"] : ""; ?>" class="form-control" required="">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Cliente</label>
										<div class="input-group">
											<input type="text" name="cliente" id="cliente_razonsocial" value="<?php echo (!empty($proyecto["cliente"])) ? $proyecto["cliente"] : ""; ?>" class="form-control" placeholder="Nombre, DNI, razon social o RUC">
											<span class="input-group-btn tooltip-demo">
												<button type="button" id="btn-buscar-cliente" class="btn btn-outline btn-primary" data-toggle="tooltip" title="Buscar clientes">
													<i class="fa fa-search"></i>
												</button>
												<!--button type="button" id="btn-registrar-cliente" class="btn btn-outline btn-primary" data-toggle="tooltip" title="&iquest;No existe el cliente? Registrar aqui">
													<i class="fa fa-edit"></i>
												</button-->
												<!--button type="button" id="btn-consultar-ruc" class="btn btn-outline btn-default" data-toggle="tooltip" title="Consultar RUC">
													<img src="<1?php echo base_url("app/img/sunat.png");?>" style="width:15px;">
												</button-->
											</span>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="required">Fecha Inicio</label>
										<input type="text" name="fecha_inicio" id="fecha_inicio" value="<?php echo (!empty($proyecto["fecha_inicio"])) ? dateFormat($proyecto["fecha_inicio"], "d/m/Y") : date("d/m/Y"); ?>" class="form-control input-sm" placeholder="<?php echo date("d/m/Y"); ?>" required="">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="required">Fecha Fin Proyectada</label>
										<input type="text" name="fecha_fin" id="fecha_fin" value="<?php echo (!empty($proyecto["fecha_fin"])) ? dateFormat($proyecto["fecha_fin"], "d/m/Y") : date("d/m/Y"); ?>" class="form-control input-sm" placeholder="<?php echo date("d/m/Y"); ?>" required="">
									</div>
								</div>
							</div>
						</div>
					</div>
				
					<div id="tab-2" class="tab-pane">
						<!-- usuarios de responsable -->
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-8">
									<div class="input-group">
										<select id="combo_asignar_usuario_responsabled" class="input-sm form-control input-s-sm inline"><?php echo $usuarioes; ?></select>
										<span class="input-group-btn tooltip-demo">
											<button type="button" class="btn btn-outline btn-primary btn-sm btn-registrar-usuario" data-toggle="tooltip" title="Registrar usuario de responsable">
												<i class="fa fa-edit"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="btn-group">
										<button id="btn-asignar-usuario" class="btn btn-sm btn-white parent" type="button">Agregar</button>
									</div>
								</div>
							</div>
							<div class="clients-list">
								<div class="">
									<div class="table-responsive" style="">
										<table border="0" class="tabla_modulos table table-striped tabla_usuario_responsable"><?php echo (!empty($tr_usuario))?$tr_usuario:''; ?></table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- tab 3 fases del proyecto -->
					<div id="tab-3" class="tab-pane">
						<!-- usuarios de responsable -->
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-8">
									<div class="input-group">
										<select id="combo_asignar_fases_pyd" class="input-sm form-control input-s-sm inline"><?php echo $fasees; ?></select>
										<span class="input-group-btn tooltip-demo">
											<button type="button" class="btn btn-outline btn-primary btn-sm btn-registrar-fases" data-toggle="tooltip" title="Registrar fases del proyecto">
												<i class="fa fa-edit"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="btn-group">
										<button id="btn-asignar-fases" class="btn btn-sm btn-white parent" type="button">Agregar</button>
									</div>
								</div>
							</div>
							<div class="clients-list">
								<div class="">
									<div class="table-responsive" style="">
										<table border="0" class="tabla_modulos table table-striped tabla_fases_py"><?php echo (!empty($tr_fases))?$tr_fases:''; ?></table>
									</div>
								</div>
							</div>
						</div>
					</div>
										<div id="tab-4" class="tab-pane">
						<!-- precios -->
						<div class="panel-body">
							<!-- <div class="row"> -->
								<!-- <div class="col-md-6"> -->
									<!-- <label>Precio compra unitario</label> -->
									<!-- <input type="text" name="precio_compra" id="<?php echo $prefix; ?>precio_compra" value="<?php echo (isset($precio["precio_compra"])) ? $precio["precio_compra"] : ""; ?>" class="form-control float-number"> -->
								<!-- </div> -->
								<!-- <div class="col-md-6"> -->
									<!-- <label>Precio venta unitario</label> -->
									<!-- <input type="text" name="precio_venta" id="<?php echo $prefix; ?>precio_venta" value="<?php echo (isset($precio["precio_venta"])) ? $precio["precio_venta"] : ""; ?>" class="form-control float-number"> -->
								<!-- </div> -->
							<!-- </div> -->
							<div class="row" style="margin-top:10px;">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">Tareas por cada fase del proyecto</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-4">
													<button class="btn btn-white btn-sm btn-block" id="add_tarea_fase"><i class="fa fa-plus"></i> Agregar nueva TAREA</button>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<table class="table table-stripped table-bordered table_tarea_fase">
														<thead>
															<tr>
																<th>Selec Fase.</th>
																<th>Descripcion</th>
																<th>Responsable</th>
																<th>Estatus</th>
																<th>Tiempo Días</th>
																<th>F.Fin</th>
																<th>Prioridad</th>
																<!-- <th class="tooltip-demo">Porcentaje  -->
																	<!-- <i class="fa fa-info-circle" data-toggle="tooltip" title="Porcentaje para el calculo del precio de venta a partir del precio de compra"></i> -->
																<!-- </th> -->
																<th>&nbsp;</th>
															</tr>
														</thead>
														<tbody><?php echo (!empty($tr_tareaf))?$tr_tareaf:''; ?></tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<!--<div class="col-lg-offset-10 col-lg-2">-->
			<div class="col-lg-12">
				<button id="<?php echo $prefix; ?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
				<button type="submit" id="<?php echo $prefix; ?>btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
			</div>
		</div>
	</div>
</form>
<div style="display:none;"><?php echo $usuario ?></div>
<div style="display:none;"><?php echo $combo_estatus ?></div>
<div style="display:none;"><?php echo $combo_prioridad ?></div>

<div id="modal-usuario" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Registrar usuario de responsable</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<?php echo $form_usuario; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $form_cliente; ?>
<script>
var validar_ruc = <?php echo ($validar_ruc == "S") ? "true" : "false"; ?>;
</script>
<style>
	.numero{text-align:right;}
	.hotkey.white {
		color: #ccc;
	}
	table#dtcliente_view_popup tbody>tr>td{padding: 4px !important;}
	sub.hotkey{bottom: 0;}
	
	.block_content {
		position: absolute;
		top: 80px;
		bottom: 0;
		right: 0;
		left: 0;
	}
</style>



