<form id="form_usuario_responsable" class="app-form">
	<input type="hidden" name="idproyecto" id="uni_idproyecto" value="<?php echo (isset($proyecto["idproyecto"])) ? $proyecto["idproyecto"]:""; ?>">
	<div class="row">
		<div class="col-sm-8">
			<select id="usuario_responsabled_filtro" class="input-sm form-control input-s-sm inline"></select>
		</div>
		<div class="col-sm-4">
			<div class="btn-group">
				<button id="btn-add-usuario" class="btn btn-sm btn-white parent" type="button">Agregar item</button>
			</div>
		</div> 	
	</div>
	<div class="clients-list">
		<!-- <div class="full-height-scroll"> -->
			<div class="table-responsive">
				<table id="tabla_usuario_responsable" class="tabla_modulos table table-striped">
					<thead>
						<tr>
							<th>Usuario responsable</th>
							<th>Cantidad</th>
							<th class="tooltip-demo">Equivalencia (<a id="ref_usuario_responsable" data-toggle="tooltip" title="<?php echo (isset($usuario["descripcion"])) ? $usuario["descripcion"]:""; ?>"><?php echo (isset($usuario["descripcion"])) ? $usuario["abreviatura"]:""; ?></a>)</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($proyecto_usuario)) {
							foreach($proyecto_usuario as $val){
								echo '<tr data-idusuario="'.$val["idusuario"].'">';
								echo '<td><input type="hidden" name="idusuario[]" class="idusuario" value="'.$val["idusuario"].'">'.$val["descripcion"].' ('.$val["abreviatura"].')</td>';
								echo '<td><input type="text" name="cantidad_usuario[]" class="cantidad_usuario form-control input-sm" value="'.$val["cantidad_usuario"].'" readonly></td>';
								echo '<td><input type="text" name="cantidad_usuario_min[]" class="cantidad_usuario_min form-control input-sm" value="'.$val["cantidad_usuario_min"].'"></td>';
								echo '<td><button type="button" class="btn btn-default btn-xs btn_delete_usuario_responsable">Eliminar</button></td>';
								echo '</tr>';
							}
						}
						?>
					</tbody>
				</table>
			</div>
		<!-- </div> -->
	</div>
	<div class="row">
		<div class="form-group">
			<div class="col-lg-4">
				<button id="uni_prod_btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
				<button type="submit" id="uni_prod_btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
			</div>
		</div>
	</div>
</form>
<script>
UNIDAD_MEDIDA = <?php echo json_encode($usuarioes); ?>;
</script>