<?php 
if( ! empty($preventa["aprobado"]) && $preventa["aprobado"] == "S") {
?>
<div class="alert alert-danger">
	<strong class="alert-link">¡Cotización Aprobada!</strong> la Cotización ya esta aprobada, no se puede modificar.
</div>
<?php 
}
?>
<div class="row">
				<div class="ibox-title">
				<div class="row">
					<div class="form-group">
						<div class="col-sm-10 text-right">
							<button id="btn_save_preventa" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>"><sub class='hotkey white'>(F4)</sub> Guardar</button>
						</div>
						<div class="col-sm-2 text-left">
							<button class="btn btn-sm btn-warning btn_cancel" data-controller="<?php echo $controller; ?>"><sub class='hotkey white'>(Esc)</sub> Cancelar</button>
						</div>
						
					</div>
				</div>
				</div>

	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Datos de la Cotización</h5>
				<!--div class="ibox-tools">
					<a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#" id="buscar-pedido"><i class="fa fa-search"></i> Buscar pedido</a></li>
					</ul>
				</div-->
			<?php if(!empty($preventa["idpreventa"])) { ?>
				<div class="ibox-tools">
					<button type="button" id="btn_aprobar" class="btn btn-xs btn-info btn_aprobar"><i class="fa fa-search"></i> Aprobar Cotización</button>
				</div>
				<?php } ?>	
				<div class="ibox-tools">
					<a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				</div>
				
			</div>
			
				<div class="ibox-content">
					<form aria-control="<?php echo $controller; ?>" class="app-form form-uppercase">

					<!-- <form id="form_<1?php echo $controller; ?>" class="app-form"> -->
					<input type="hidden" name="idpreventa" id="idpreventa" value="<?php echo (!empty($preventa["idpreventa"])) ? $preventa["idpreventa"] : ""; ?>">
					<input type="hidden" name="idcliente" id="compra_idcliente" value="<?php echo (!empty($preventa["idcliente"])) ? $preventa["idcliente"] : ""; ?>">
					<input type="hidden" name="estado_cliente" id="estado_cliente">
					<input type="hidden" name="aprobado" id="aprobado" value="S">

					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="required">Fecha Caducidad</label>
								<input type="text" name="fecha_caducidad" id="fecha_caducidad" value="<?php echo (!empty($preventa["fecha_caducidad"])) ? dateFormat($preventa["fecha_caducidad"], "d/m/Y") : date("d/m/Y"); ?>" class="form-control input-xs" placeholder="<?php echo date("d/m/Y"); ?>" required="">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="required">Tipo documento</label>
								<?php echo $tipodocumento; ?>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="required">Tipo venta</label>
								<?php echo $tipoventa; ?>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label>Forma de Pago</label>
								<?php echo $forma_pago; ?>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-9">
							<div class="form-group">
								<label>Cliente</label>
								<div class="input-group">
									<input type="text" name="cliente" id="cliente_razonsocial" value="<?php echo (!empty($preventa["cliente"])) ? $preventa["cliente"] : ""; ?>" class="form-control input-xs" placeholder="Nombre, DNI, razon social o RUC">
									<span class="input-group-btn tooltip-demo">
										<button type="button" id="btn-buscar-cliente" class="btn btn-outline btn-primary input-xs" data-toggle="tooltip" title="Buscar clientes">
											<i class="fa fa-search"></i>
										</button>
										<button type="button" id="btn-registrar-cliente" class="btn btn-outline btn-primary input-xs" data-toggle="tooltip" title="&iquest;No existe el cliente? Registrar aqui">
											<i class="fa fa-edit"></i>
										</button>
										<!--button type="button" id="btn-consultar-ruc" class="btn btn-outline btn-default" data-toggle="tooltip" title="Consultar RUC">
											<img src="<1?php echo base_url("app/img/sunat.png");?>" style="width:15px;">
										</button-->
										</span>
								</div>
								<div class="col-sm-4 msg-about-cliente"></div>
							</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="required">Moneda</label>
									<?php echo $moneda; ?>
								</div>
							</div>
					</div>
				
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">
								<label class="">Referencia</label>
								<input type="text" name="referencia" id="referencia" value="<?php echo (!empty($preventa["referencia"])) ? $preventa["referencia"] : ""; ?>" class="form-control input-xs">
							</div>
						</div>
						<?php if(!empty($preventa["idpreventa"])) { ?>
						<div class="col-sm-2">
								<div class="form-group">
									<label style="font-size:13px; color: #009bdd;">Modificación N°</label>
									<input type="text" name="n_modificacion" id="n_modificacion" value="<?php echo (!empty($preventa["n_modificacion"])) ? $preventa["n_modificacion"] : ""; ?>" class="form-control input-xs numero">
								</div>
							</div>
							<?php } ?>	
							<div class="col-sm-2">
								<div class="form-group">
									<label>Cambio moneda</label>
									<input type="text" name="cambio_moneda" id="cambio_moneda" value="<?php echo (!empty($compra["cambio_moneda"])) ? $compra["cambio_moneda"] : ""; ?>" readonly="" class="form-control input-xs numero valor_cambio">
								</div>
							</div>
							
					</div>
					<!-- <div class="row"> -->
						<!-- <div class="col-sm-12"> -->
							<!-- <div class="form-group"> -->
								<!-- <label class="" style="font-size:13px; color: #009bdd;">Saludo del Email</label> -->
								<!-- <textarea name="saludo_email" id="saludo_email" value="<?php echo (isset($preventa["saludo_email"])) ? $preventa["saludo_email"] : ""; ?>" placeholder="Esto aparecerá como su saludo al momento de enviar el mensaje, recuerde que no es necesario hacer mención a la empresa, ya que automatico se concatenará" class="form-control prueba3" rows="2"><?php echo (isset($preventa["saludo_email"])) ? $preventa["saludo_email"] : ""; ?></textarea> -->
							<!-- </div> -->
						<!-- </div> -->
					<!-- </div> -->
				</form>
			</div><!-- ibox-content -->
		</div><!-- ibox -->
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Detalle de la Cotización</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
				 <form aria-control="<?php echo $controller; ?>" class="app-form form-uppercase">
				 <!-- <form id="form_<1?php echo $controller; ?>" class="app-form"> -->
						<div class="row m-b-sm m-t-sm">
						<div class="col-md-9">
							<input type="hidden" id="producto_idproducto">
							<input type="hidden" id="producto_has_serie">
							<input type="hidden" id="producto_idseccion">
							<input type="hidden" id="producto_idunidad">
							<input type="hidden" id="producto_idalmacen">
							<input type="hidden" id="producto_serie">
							<!-- <input type="hidden" id="producto_precio_compra"> -->
							<!-- <input type="hidden" id="producto_precio_venta"> -->
							<div class="input-group tooltip-demo">
								<span class="input-group-addon" data-toggle="tooltip" title="Buscar por serie o c&oacute;digo de barras">
									<input type="checkbox" value="1" id="buscar_serie" name="buscar_serie">
								</span>
								<input type="text" name="producto" id="producto_descripcion" class="form-control" placeholder="Ingrese el nombre o codigo del producto">
								<span class="input-group-addon" >
									<sub class='hotkey white'>(F2)</sub> 
								</span>
							</div>
						</div>
						<!--<div class="col-md-3">
							<button type="button" id="btn-buscar-producto" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Buscar Producto</button>
						</div>-->
					</div>
					
					<div class="table-responsive">
						<table id="tbl-detalle" class="table table-hover tooltip-demo detail-table">
							<thead>
								<tr>
									<th style="width: 2%;"></th>
									<th style="width:5%">Marca</th>
									<th style="width:5%">Modelo</th>
									<th style="width:5%">Sección.</th>
									<th style="width:20%">Descrip.</th>
									<th style="width:5%">Cant.</th>
									<th style="width:5%">U.Med.</th>
									<th style="width:6%">Precio.U.</th>
									<th style="width:5%">Costo</th>
									<th style="width:5%">Decuento %</th>
									<!-- <th style="width:5%">Stock</th> -->
									<th style="width:6%">Importe</th>
									<th style="width:2%"></th>
									<th style="display:none;"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					
					<div class="row">
						<!-- <div class="col-sm-3"></div> -->
						<div class="col-sm-10 text-right">
							<!-- <div class="form-group text-right"> -->
								<label class="required text-right">Subtotal</label>
							<!-- </div> -->
						</div>
						<div class="col-sm-2">
						<input type="text" name="subtotal" id="subtotal" value="<?php echo (!empty($preventa["subtotal"])) ? $preventa["subtotal"] : ""; ?>" class="form-control numero prueba2 input-xs" required="" readonly="" placeholder="0.00">
						</div>
					</div>
					<div class="separador"></div>
					<div class="row">
						<div class="col-sm-10 text-right">
							<!-- <div class="form-group text-right"> -->
								<label class="required text-right">IGV</label>
								<!--<div class="input-group">
									<span class="input-group-addon"><input type="checkbox" name="valor_igv" id="valor_igv" value="<?php //echo $valor_igv;?>" <?php //echo (!empty($preventa["igv"]) && floatval($preventa["igv"]) > 0) ? "checked" : ""; ?>></span>
									<input type="text" name="igv" id="igv" value="<?php //echo (!empty($preventa["igv"])) ? $preventa["igv"] : ""; ?>" class="form-control numero" readonly="">
								</div>-->
							<!-- </div> -->
							</div>
							<div class="col-sm-2">
							<input type="text" name="igv" id="igv" value="<?php echo (!empty($preventa["igv"])) ? $preventa["igv"] : ""; ?>" class="form-control numero prueba2 input-xs" readonly="" placeholder="0.00">
							</div>
					</div>
					<div class="separador"></div>
					<div class="row">
						<div class="col-sm-10 text-right">
							<!-- <div class="form-group text-right"> -->
								<label class="required text-right">Total</label>
							<!-- </div> -->
						</div>	
							<div class="col-sm-2">							
								<input type="text" name="total" id="total" value="<?php echo (!empty($preventa["total"])) ? $preventa["total"] : ""; ?>" class="form-control numero prueba2 input-xs" required="" readonly="" placeholder="0.00">
							</div>
					</div>
					<div class="separador"></div>
					<div class="row">
						<div class="col-sm-10 text-right">
							<!-- <div class="form-group text-right"> -->
								<label class="required text-right">Margen</label>
							<!-- </div>	 -->
						</div>
							<div class="col-sm-2">	
								<input type="text" name="margen" id="margen" value="<?php echo (!empty($preventa["margen"])) ? $preventa["margen"] : ""; ?>" class="form-control  prueba2 input-xs text-center" required="" readonly="" placeholder="0.00">
								<input type="hidden" name="descuento" id="descuento" value="<?php echo (!empty($preventa["descuento"])) ? $preventa["descuento"] : ""; ?>" class="form-control  prueba2 input-xs text-center" required="" readonly="" placeholder="0.00">
							</div>
					</div>
					
					<div class="row">
						<div class="col-sm-12" id="info-saldo-cliente"></div>
					</div>
					
					<!-- <div class="row"> -->
						<!-- <div class="form-group"> -->
							<!-- <div class="col-sm-6 text-left"> -->
								<!-- <button class="btn btn-sm btn-warning btn_cancel" data-controller="<?php echo $controller; ?>"><sub class='hotkey white'>(Esc)</sub> Cancelar</button> -->
							<!-- </div> -->
							<!-- <1?php if($editable) { ?> -->
							<!-- <div class="col-sm-6 text-right"> -->
								<!-- <button id="btn_save_compra" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>"><sub class='hotkey white'>(F4)</sub> Guardar</button> -->
							<!-- </div> -->
							<!-- <1?php } ?> -->
						<!-- </div> -->
					<!-- </div> -->
				</form>
			</div><!-- ibox-content -->
		</div><!-- ibox -->
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Terminos y Condiciones</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
				 <form aria-control="<?php echo $controller; ?>" class="app-form form-uppercase">
				 <!-- <form id="form_<1?php echo $controller; ?>" class="app-form"> -->
				<div class="row">
						<div class="col-sm-12">
						<label class="" style="font-size:16px; color: #009bdd;">Condiciones</label>
							<div class="form-group">
						<!-- <textarea name="condiciones_ser" id="condiciones_ser" value="<1?php echo (isset($preventa["condiciones_ser"])) ? $preventa["descripcion_pro"] : ""; ?>" placeholder="Esta parte aparecera en la parte final del tu cotización" class="form-control" rows="3" cols="8"><1?php echo (isset($preventa["condiciones_ser"])) ? $preventa["condiciones_ser"] : ""; ?></textarea> -->
							<textarea name="condiciones_ser" id="condiciones_ser" value="<?php echo (isset($preventa["condiciones_ser"])) ? $preventa["condiciones_ser"] : ""; ?>" placeholder="Esta parte aparecera en la parte final del tu cotización" class="form-control prueba3" rows="2"><?php echo (isset($preventa["condiciones_ser"])) ? $preventa["condiciones_ser"] : ""; ?></textarea>
							</div>
						</div>
							<!-- <div class="col-sm-3"> -->
								<!-- <div class="form-group"> -->
									<!-- <label style="font-size:13px; color: #009bdd;">Garantía de EQUIPOS</label> -->
									<!-- <input type="text" name="garantia_equi" id="garantia_equi" value="<?php echo (!empty($preventa["garantia_equi"])) ? $preventa["garantia_equi"] : ""; ?>" class="form-control input-xs numero"> -->
								<!-- <label style="font-size:13px; color: #009bdd;">Garantía del SERVICIO</label> -->
									<!-- <input type="text" name="garantia_ser" id="garantia_ser" value="<?php echo (!empty($preventa["garantia_ser"])) ? $preventa["garantia_ser"] : ""; ?>" class="form-control input-xs numero"> -->
								<!-- </div> -->
							<!-- </div> -->
					</div>		
				</form>
			</div><!-- ibox-content -->
		</div><!-- ibox -->
	</div>
</div>















			
			
			
			
		
			
				

		</div>
	</div>
</form>

<div id="modal-seccion_medida" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Secciones <small id="sec_producto_descripcion"></small></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<?php echo $form_producto_seccion; ?>
				</div>
			</div>
		</div>
	</div>
</div>




<div id="modal-product-list" data-keyboard="false" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Resultado de la b&uacute;squeda</h4>
			</div>
			<div class="modal-body">
				<p>Se han encontrado <span class="count-result-list"></span> resultados. Seleccione el item que corresponde.</p>
				<div class="list-group result-list"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<div id="modal-series" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="input-group">
							<input id="input-text-serie" placeholder="Ingrese la serie" class="input-sm form-control text-uppercase" type="text" />
							<span class="input-group-btn">
								<button id="btn-search-serie" type="button" class="btn btn-sm btn-white"><i class="fa fa-search"></i> Buscar</button>
							</span>
						</div>
					</div>
				</div>
				<div class="table-responsive div_scroll" style="max-height:300px;">
					<table id="table-serie" class="table table-striped">
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="btn-close-serie" class="btn btn-primary">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<div id="modal-precio-tempp" class="modal fade" data-keyboard="false" aria-hidden="true" aria-labelledby="myLargeModalLabel" data-backdrop="static">
	<div class="modal-dialog modal-sm" >
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Agregar precio</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12"><input type="text" id="ptemp" class="form-control"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal-cliente" class="modal fade" data-keyboard="false" aria-hidden="true" aria-labelledby="myLargeModalCliente" data-backdrop="static">
	<div class="modal-dialog modal-lg" style="margin-top: 10px;">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Registrar Cliente</h4>
			</div>
			<div class="modal-body" style="padding: 0px 30px 0px 29px;">
				<div class="row">
					<?php echo $form_cliente; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="display:none;"><?php echo $combo_seccion ?></div>
<div style="display:none;"><?php echo $combo_grupo_igv.'</br>'.$combo_tipo_igv;?></div>
<script>
var default_grupo_igv = <?php echo (!empty($default_igv)) ? "'$default_igv'" : "false"; ?>;
var validar_ruc = <?php echo ($validar_ruc == "S") ? "true" : "false"; ?>;
</script>

<style>
	.numero{text-align:right;}
	.hotkey.white {
		color: #ccc;
	}
	table#dtcliente_view_popup tbody>tr>td{padding: 4px !important;}
	sub.hotkey{bottom: 0;}
	
	.block_content {
		position: absolute;
		top: 80px;
		bottom: 0;
		right: 0;
		left: 0;
	}
	.outlinenone {
    outline: none;
    background-color: #D2D2FF;
    border: 0;
	}
.prueba2 {
	border: 0;
    border-radius: 0;
	background-color: #F3F3F4;
    border-bottom: 1px solid #f71a1a;
	}
.prueba3 {
	border: 0;
    border-radius: 0;
    border-bottom: 1px solid #f71a1a;
	}
.separador {
	padding-top: 2px;
}
</style>