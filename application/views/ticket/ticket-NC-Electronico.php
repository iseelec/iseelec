<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ticket</title>
        <style>
		body{
    margin: 5px;
    font-size: 13px;
    font-family: monospace;
    color: #000000;
    background-color: #ffffff;
    width: 278px;
	padding-left: 7px;
}

#head{
    text-align: center;
}
.line{
    display: block;    
    border-bottom: #000000 dashed 1px;
    margin: 2px 0px;
}
.left{
    float: left;
}
.right{
    float: right;
}
.clearfix::after {
	display: block;
	content: "";
	clear: both;
}
.span-total {
	display: inline-block;
	width: 60px;
}


@page{
    margin: 0;
}

/* nuevos estilos para el codigo de barras */
.text-right {text-align: right;}
.content-bloque2 {width: 570px;}
.bloque2 {
    padding: 5px 20px 19.5px 20px;
    border: solid 0px #999933;
    display: inline-block;
    /*margin-right: 3px;*/
    vertical-align: top;
    font-size: 10px;
	margin-right: 10px;
	margin-bottom: 0;
}
.bloque2 .left {float:left;text-align:center;}
.bloque2 .left img.bar {display:block;}
.bloque2 .left span.bar {font-size:13px;}

.bloque2 .right {float:right;text-align:right;}
.bloque2 .right .precio {font-size: 16px; font-weight:bold;}
.bloque2 .item {font-size: 11px; font-weight:bold;}
.desc-resolucion {
	font-size: 11px;
	margin: 0;
	/* text-align: justify; */
}
.logo {height:80px;}
		</style>
    </head>
    <body>
        <div id="head">
			<?php if( ! empty($logo)) {?>
			<div><img src="data:image/png;base64, <?php echo $logo;?>" class="logo" alt="..."/></div>
			<?php }?>
            <div><?php echo $empresa["descripcion"];?></div>
			<?php if( ! empty($empresa["nombre_comercial"]) && $empresa["nombre_comercial"] != $empresa["descripcion"]) {?>
            <div><?php echo $empresa["nombre_comercial"];?></div>
			<?php }?>
            <div>RUC N° <?php echo $empresa["ruc"];?></div>
            <div><?php echo $empresa["direccion"];?></div>
            <div><span><?php echo $empresa["distrito"];?></span></div>
            <div><span>TELF: <?php echo $sucursal["telefono"];?></span></div>
        </div>
		<?php 
		$date = new DateTime($comprobante["fecha_registro"]);
		?>
        <!-- <div class="line"></div> -->
		<br>
        <div style="text-align: center; font-weight: bold;"><?php echo $comprobante["tipo_documento"];?></div>
		<div style="text-align: center; font-weight: bold;"><?php echo $comprobante["serie"];?> - <?php echo str_pad($comprobante["numero"], 8, "0", STR_PAD_LEFT);?></div>
		<br>
        <div>
			<div>Fecha emisi&oacute;n: <?php echo $date->format("d/m/Y h:i a");?></div>
			<div>Cliente: <?php echo $comprobante["cliente"];?></div>
			<div>Direcci&oacute;n: <?php echo $comprobante["direccion"];?></div>
			<?php
			if( ! empty($facturacion["num_docu_cliente"])) {
				if($facturacion["num_docu_cliente"] != "00000000" && $facturacion["num_docu_cliente"] != "00000000000") {
					echo '<div>DNI/RUC: '.$facturacion["num_docu_cliente"].'</div>';
				}
			}
			?>
        </div>
        <br>
		<div style="font-weight: bold;">DOCUMENTO QUE MODIFICA</div>
		<div>
			<div>Tipo: <?php echo $comprobante["tipo_documento_ref"];?></div>
			<div>N&uacute;mero: <?php echo $comprobante["serie_ref"]."-".$comprobante["correlativo_ref"];?></div>
		</div>
        <br>
		<?php
		$w = array("cant"=>"20%", "um"=>"20%", "pu"=>"23%", "importe"=>"35%");
		?>
        <div class="line"></div>
        <div class="clearfix">
			<div class="left" style="width: <?php echo $w["cant"];?>; font-weight: bold;">Cant</div>
            <div class="left" style="width: <?php echo $w["um"];?>; font-weight: bold;">U.M.</div>
            <div class="left" style="width: <?php echo $w["pu"];?>; text-align: right; font-weight: bold;">P.U.</div>
            <div class="left" style="width: <?php echo $w["importe"];?>; text-align: right; font-weight: bold;">Importe</div>
        </div>
        <div class="line"></div>
        <div id="detalle">
		<?php
		if( ! empty($conceptos)) {
			foreach($conceptos as $v) {
		?>
			<div><?php echo $v["detalle"];?></div>
			<div class="clearfix">
				<div class="left" style="width: <?php echo $w["cant"];?>;"><?php echo number_format($v["cantidad"],2,".","");?></div>
				<div class="left" style="width: <?php echo $w["um"];?>;"><?php echo $v["um"];?></div>
				<div class="left" style="width: <?php echo $w["pu"];?>; text-align: right;"><?php echo number_format($v["precio"],2,".","");?></div>
				<div class="left" style="width: <?php echo $w["importe"];?>; text-align: right;"><?php echo number_format($v["importe"],2,".","");?></div>
			</div>
		<?php 
			}
		}
		$wr = array(23, 40, 10, 25);
		?>
        </div>
        <div class="line"></div>
        
		<div class="clearfix">
			<div class="left" style="width: <?php echo $wr[0];?>%;">&nbsp;</div>
			<div class="left" style="width: <?php echo $wr[1];?>%;">Op.Exonerada</div>
			<div class="left" style="width: <?php echo $wr[2];?>%;"><?php echo $moneda["simbolo"];?></div>
			<div class="left" style="width: <?php echo $wr[3];?>%; text-align: right;"><?php echo number_format($facturacion["exonerado"],2,".","");?></div>
		</div>
		<div class="clearfix">
			<div class="left" style="width: <?php echo $wr[0];?>%;">&nbsp;</div>
			<div class="left" style="width: <?php echo $wr[1];?>%;">Op.Inafecta</div>
			<div class="left" style="width: <?php echo $wr[2];?>%;"><?php echo $moneda["simbolo"];?></div>
			<div class="left" style="width: <?php echo $wr[3];?>%; text-align: right;"><?php echo number_format($facturacion["inafecto"],2,".","");?></div>
		</div>
		<div class="clearfix">
			<div class="left" style="width: <?php echo $wr[0];?>%;">&nbsp;</div>
			<div class="left" style="width: <?php echo $wr[1];?>%;">Op.Gravada</div>
			<div class="left" style="width: <?php echo $wr[2];?>%;"><?php echo $moneda["simbolo"];?></div>
			<div class="left" style="width: <?php echo $wr[3];?>%; text-align: right;"><?php echo number_format($facturacion["gravado"],2,".","");?></div>
		</div>
		<div class="clearfix">
			<div class="left" style="width: <?php echo $wr[0];?>%;">&nbsp;</div>
			<div class="left" style="width: <?php echo $wr[1];?>%;">IGV (18.00 %)</div>
			<div class="left" style="width: <?php echo $wr[2];?>%;"><?php echo $moneda["simbolo"];?></div>
			<div class="left" style="width: <?php echo $wr[3];?>%; text-align: right;"><?php echo number_format($facturacion["suma_igv"],2,".","");?></div>
		</div>
		<?php if(floatval($facturacion["gratuito"]) > 0) {?>
		<div class="clearfix">
			<div class="left" style="width: <?php echo $wr[0];?>%;">&nbsp;</div>
			<div class="left" style="width: <?php echo $wr[1];?>%;">Op.Gratuitas</div>
			<div class="left" style="width: <?php echo $wr[2];?>%;"><?php echo $moneda["simbolo"];?></div>
			<div class="left" style="width: <?php echo $wr[3];?>%; text-align: right;"><?php echo number_format($facturacion["gratuito"],2,".","");?></div>
		</div>
		<?php }?>
		<?php if(floatval($facturacion["descuento_global"]) > 0 || floatval($facturacion["suma_descuento"]) > 0) {
				$totalDscto = floatval($facturacion["suma_descuento"]) + floatval($facturacion["descuento_global"]);
			?>
		<div class="clearfix">
			<div class="left" style="width: <?php echo $wr[0];?>%;">&nbsp;</div>
			<div class="left" style="width: <?php echo $wr[1];?>%;">Total Dscto.</div>
			<div class="left" style="width: <?php echo $wr[2];?>%;"><?php echo $moneda["simbolo"];?></div>
			<div class="left" style="width: <?php echo $wr[3];?>%; text-align: right;"><?php echo number_format($totalDscto,2,".","");?></div>
		</div>
		<?php }?>
		<div class="clearfix">
			<div class="left" style="width: <?php echo $wr[0];?>%;">&nbsp;</div>
			<div class="left" style="width: <?php echo $wr[1];?>%;">Importe Total</div>
			<div class="left" style="width: <?php echo $wr[2];?>%;"><?php echo $moneda["simbolo"];?></div>
			<div class="left" style="width: <?php echo $wr[3];?>%; text-align: right;"><?php echo number_format($comprobante["total"],2,".","");?></div>
		</div>
        
		<div style="padding-top: 5px;">Son: <?php echo $total_letras." ".$comprobante["moneda"];?></div>
        
		<br>
		<div class="clearfix">
			<p class="desc-resolucion">
			<?php if( ! empty($facturacion["imagen_qr"])) {?>
			<img src="data:image/png;base64, <?php echo $facturacion["imagen_qr"];?>" alt="..." class="left" style="margin-right: 5px;"/>
			<?php }?>
			<br>Representaci&oacute;n impresa de la <?php echo $comprobante["tipo_documento"];?>, esta puede ser consultada en <?php echo $webconsulas;?>
			</p>
		</div>
		<script>
			<?php if($print) {?>
			window.print();
			<?php }?>
			<?php if($close) {?>
			window.close();
			<?php }?>
		</script>
    </body>
</html>