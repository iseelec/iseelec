<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ticket</title>
		<style>
		/*
table { overflow: visible !important; }
thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }
		*/
		body{
    margin: 5px;
    font-size: 13px;
    font-family: monospace;
    color: #000000;
    background-color: #ffffff;
    width: 20cm;
	padding-left: 7px;
}

.head{
    width: 100%;
}
.left{
    float: left;
}
.right{
    float: right;
}
.clearfix::after {
	display: block;
	content: "";
	clear: both;
}
@page{
    margin: 0;
}
.logo {height:80px;}
.documento {
	background-color: #dedede;
    padding: 10px 0;
    text-align: center;
    font-weight: bold;
    font-size: 15px;
    border-radius: 5px;
	line-height: 18px;
	border: solid 1px #999999;
}
.detalle {width:100%;border-width:1px;border-color:#999999;border-style:solid;border-collapse: collapse;}
.border-all {border-width:1px;border-color:#999999;border-style:solid;}
.border {
	border-right-width:1px;
	border-right-color:#999999;
	border-right-style:solid;
	border-left-width:1px;
	border-left-color:#999999;
	border-left-style:solid;
}
.head-detalle {border-width:1px;border-color:#999999;border-style:solid;background-color:#dedede;font-weight:bold;text-align:center;}
		</style>
    </head>
    <body>
		<table class="head">
			<tr>
				<?php if( ! empty($logo)) {?>
				<td style="width: 140px;">
					<img src="data:image/png;base64, <?php echo $logo;?>" class="logo" alt="..."/>
				</td>
				<?php }?>
				<td>
					<div>
						<div style="font-weight:bold;"><?php echo $empresa["descripcion"];?></div>
						<?php if( ! empty($empresa["nombre_comercial"]) && $empresa["nombre_comercial"] != $empresa["descripcion"]) {?>
						<div><?php echo $empresa["nombre_comercial"];?></div>
						<?php }?>
						<div><?php echo $empresa["direccion"]." - ".$empresa["distrito"];?></div>
						<div><span>TELF: <?php echo $sucursal["telefono"];?></span></div>
					</div>
				</td>
				<td style="width: 190px;">
					<div class="documento">
						<div><?php echo $comprobante["tipo_documento"];?></div>
						<div>RUC <?php echo $empresa["ruc"];?></div>
						<div><?php echo $facturacion["serie"];?> - <?php echo $facturacion["numero"];?></div>
					</div>
				</td>
			</tr>
		</table>
		<?php 
		$date = new DateTime($comprobante["fecha_registro"]);
		$format = "d/m/Y h:ia";
		$w = "100px";
		?>
		<br>
		<table class="head">
			<tr>
				<td style="width: 90px;"><span style="font-weight:bold;">SEÑOR(ES)</span></td>
				<td>: <?php echo $comprobante["full_nombres"];?></td>
				<td style="width: 53px;"><span style="font-weight:bold;">FECHA</span></td>
				<td style="width: 152px;">: <?php echo $date->format($format);?></td>
			</tr>
			<tr>
				<td style="width: 90px;"><span style="font-weight:bold;">RUC/DNI</span></td>
				<td>:
				<?php
				if( ! empty($facturacion["num_docu_cliente"])) {
					if($facturacion["num_docu_cliente"] != "00000000" && $facturacion["num_docu_cliente"] != "00000000000") {
						echo $facturacion["num_docu_cliente"];
					}
				}
				?>
				</td>
				<td style="width: 53px;"><span style="font-weight:bold;">MONEDA</span></td>
				<td style="width: 152px;">: <?php echo $comprobante["moneda"];?></td>
			</tr>
			<tr>
				<td style="width: 90px;"><span style="font-weight:bold;">DIRECCION</span></td>
				<td colspan="3">: <?php echo $comprobante["direccion"];?></td>
			</tr>
		</table>
        <br>
        <table class="detalle">
			<tr>
				<td class="head-detalle" style="width:<?php echo $w;?>;">CANTIDAD</td>
				<td class="head-detalle" style="width:<?php echo $w;?>;">U.MEDIDA</td>
				<td class="head-detalle">DESCRIPCION</td>
				<td class="head-detalle" style="width:<?php echo $w;?>;">P.UNITARIO</td>
				<td class="head-detalle" style="width:<?php echo $w;?>;">IMPORTE</td>
			</tr>
			<?php
			if( ! empty($conceptos)) {
				foreach($conceptos as $v) {
			?>
			<tr>
				<td class="border" style="width: <?php echo $w;?>; text-align: center;"><?php echo $v["cantidad"];?></td>
				<td class="border" style="width: <?php echo $w;?>; text-align:center;"> <?php echo $v["um"];?> </td>
				<td class="border"><?php echo $v["detalle"];?></td>
				<td class="border" style="width: <?php echo $w;?>; text-align: right;"><?php echo number_format($v["precio"],2,".","");?></td>
				<td class="border" style="width: <?php echo $w;?>; text-align: right;"><?php echo number_format($v["importe"],2,".","");?></td>
			</tr>
			<?php 
				}
			}
			?>
			<tr>
				<td colspan="5" class="border-all">Son: <?php echo $total_letras." ".$comprobante["moneda"];?></td>
			</tr>
		</table>
        <br>
		<table class="head">
			<tr>
				<td>Recibido: <?php echo number_format($pago["entregado"],2,".","");?></td>
				<td style="width: 110px;">Op.Exonerada</td>
				<td style="width: 40px; text-align:right;"><?php echo $moneda["simbolo"];?></td>
				<td style="width: 100px; text-align:right;"><?php echo number_format($facturacion["exonerado"],2,".","");?></td>
			</tr>
			<tr>
				<td>Vuelto: <?php echo number_format($pago["vuelto"],2,".","");?></td>
				<td style="width: 110px;">Op.Inafecta</td>
				<td style="width: 40px; text-align:right;"><?php echo $moneda["simbolo"];?></td>
				<td style="width: 100px; text-align:right;"><?php echo number_format($facturacion["inafecto"],2,".","");?></td>
			</tr>
			<tr>
				<td>Tipo pago: <?php echo $pago["descripcion"];?></td>
				<td style="width: 110px;">Op.Gravada</td>
				<td style="width: 40px; text-align:right;"><?php echo $moneda["simbolo"];?></td>
				<td style="width: 100px; text-align:right;"><?php echo number_format($facturacion["gravado"],2,".","");?></td>
			</tr>
			<tr>
				<td>Vendedor: <?php echo $comprobante["vendedor"];?></td>
				<td style="width: 110px;">IGV(18 %)</td>
				<td style="width: 40px; text-align:right;"><?php echo $moneda["simbolo"];?></td>
				<td style="width: 100px; text-align:right;"><?php echo number_format($facturacion["suma_igv"],2,".","");?></td>
			</tr>
			<?php if(floatval($facturacion["gratuito"]) > 0) {?>
			<tr>
				<td>&nbsp;</td>
				<td style="width: 110px;">Op.Gratuitas</td>
				<td style="width: 40px; text-align:right;"><?php echo $moneda["simbolo"];?></td>
				<td style="width: 100px; text-align:right;"><?php echo number_format($facturacion["gratuito"],2,".","");?></td>
			</tr>
			<?php }?>
			<?php if(floatval($facturacion["descuento_global"]) > 0 || floatval($facturacion["suma_descuento"]) > 0) {
				$totalDscto = floatval($facturacion["suma_descuento"]) + floatval($facturacion["descuento_global"]);
			?>
			<tr>
				<td>&nbsp;</td>
				<td style="width: 110px;">Total Dscto.</td>
				<td style="width: 40px; text-align:right;"><?php echo $moneda["simbolo"];?></td>
				<td style="width: 100px; text-align:right;"><?php echo number_format($totalDscto,2,".","");?></td>
			</tr>
			<?php }?>
			<tr>
				<td>&nbsp;</td>
				<td style="width: 110px;">Importe Total</td>
				<td style="width: 40px; text-align:right;"><?php echo $moneda["simbolo"];?></td>
				<td style="width: 100px; text-align:right;"><?php echo number_format($comprobante["total"],2,".","");?></td>
			</tr>
		</table>
		<br>
		<table class="head">
			<tr>
				<?php if( ! empty($facturacion["imagen_qr"])) {?>
				<td style="width: 100px;">
					<img src="data:image/png;base64, <?php echo $facturacion["imagen_qr"];?>" alt="..." class="left" style="margin-right: 5px;"/>
				</td>
				<?php }?>
				<td>
					<div style="font-size:12px;">
						Representaci&oacute;n impresa de la <?php echo $comprobante["tipo_documento"];?>, esta puede ser consultada en <?php echo $webconsulas;?>
					</div>
					<br>
					<div style="text-align: center;">Gracias por su Compra</div>
					<div style="text-align: center;">BIENES TRANSFERIDOS EN LA AMAZONIA PARA SER CONSUMIDOS EN LA MISMA</div>
				</td>
			</tr>
		</table>
		
		<script>
			<?php if($print) {?>
			window.print();
			<?php }?>
			<?php if($close) {?>
			window.close();
			<?php }?>
		</script>
    </body>
</html>