<div id="right-sidebar">
	<div class="sidebar-container">

		<ul class="nav nav-tabs navs-3">
			<li class="active">
				<a data-toggle="tab" href="#tab-1">Datos</a>
			</li>
			<li>
				<a data-toggle="tab" href="#tab-2"><i class="fa fa-gear"></i></a>
			</li>
		</ul>

		<div class="tab-content">
			<div id="tab-1" class="tab-pane active">
				<div class="sidebar-title">
					<h3><i class="fa fa-cube"></i> Datos predeterminados</h3>
					<!--<small><i class="fa fa-tim"></i> Configure los valores predeterminados.</small>-->
				</div>
				<form class="form-config">
					<div class="setings-item">
						<span>Tipo venta</span>
						<div class="switch"><?php echo $tipo_venta;?></div>
					</div>
					<div class="setings-item">
						<span>Tipo documento</span>
						<div class="switch"><?php echo $tipo_documento;?></div>
					</div>
					<div class="setings-item">
						<span>Serie documento</span>
						<div class="switch"><input type="text" class="form-control input-xs input-config" name="serie" value="<?php echo $serie;?>"></div>
					</div>
					<div class="setings-item">
						<span>Almacen</span>
						<div class="switch"><?php echo $almacen;?></div>
					</div>
					<div class="setings-item">
						<span>Tipo pago</span>
						<div class="switch"><?php echo $tipo_pago;?></div>
					</div>
					<div class="setings-item">
						<span>Moneda</span>
						<div class="switch"><?php echo $moneda;?></div>
					</div>
					<div class="setings-item">
						<span>Vendedor</span>
						<div class="switch"><?php echo $vendedor;?></div>
					</div>
					<div class="setings-item">
						<div class="text-right"><button class="btn btn-primary btn-xs btn-config-save"><i class="fa fa-check"></i> Guardar</button></div>
					</div>
				</form>
			</div>
			<div id="tab-2" class="tab-pane">
				<div class="sidebar-title">
					<h3><i class="fa fa-gears"></i> Configuraciones</h3>
				</div>

				<div class="setings-item">
					<span>
						Show notifications
					</span>
					<div class="switch">
						<div class="onoffswitch">
							<input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example">
							<label class="onoffswitch-label" for="example">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="setings-item">
					<span>
						Disable Chat
					</span>
					<div class="switch">
						<div class="onoffswitch">
							<input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example2">
							<label class="onoffswitch-label" for="example2">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="setings-item">
					<span>
						Offline users
					</span>
					<div class="switch">
						<div class="onoffswitch">
							<input type="checkbox" checked="" name="collapsemenu" class="onoffswitch-checkbox" id="example5">
							<label class="onoffswitch-label" for="example5">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>