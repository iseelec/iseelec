<div class="fh-with-tab">
	<div id="jiframe-ymenu_header" class="ymenu-tab">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#ymtab-1"><?php echo $title; ?></a></li>
		</ul>
	</div>	
	
	<div class="full-height">
		<div class="full-height-scroll white-bg border-left">
			<div id="jiframe-ymenu_body" class="element-detail-box">
				<div class="tab-content">
					<div id="ymtab-1" class="tab-pane active">
						<?php echo $content; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>