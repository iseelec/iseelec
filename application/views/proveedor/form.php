<form id="form_<?php echo $controller; ?>" class="app-form" autocomplete="off" data-prefix="<?php echo $prefix;?>" data-modal="<?php echo $modal;?>">
	<input type="hidden" name="idproveedor" id="<?php echo $prefix; ?>idproveedor" value="<?php echo (!empty($idproveedor)) ? $idproveedor : ""; ?>">
	
	<div class="row form-group">
		<div class="col-md-4">
			<label class="required">RUC</label>
			<div class="input-group input-group-sm">
				<input type="text" name="ruc" id="<?php echo $prefix; ?>ruc" value="<?php echo (!empty($ruc)) ? $ruc : ""; ?>" class="form-control input-sm" required="" maxlength="11">
				<span class="input-group-btn tooltip-demo">
					<button type="button" id="btn-search-ruc" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Buscar RUC">
						<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</div>
		<div class="col-md-8">
			<label class="required">Razon Social</label>
			<input type="text" name="nombre" id="<?php echo $prefix; ?>nombre" value="<?php echo (!empty($nombre)) ? $nombre : ""; ?>" class="form-control input-sm" required=""/>
		</div>
	</div>
	
	<div class="row form-group">
		<div class="col-md-12">
			<label class="required">Direcci&oacute;n</label>
			<div class="input-group">
				<input type="text" name="direccion" id="<?php echo $prefix; ?>direccion" value="<?php echo (!empty($direccion)) ? $direccion : ""; ?>" class="form-control input-sm" required="">
				<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
			</div>
			<br>
		</div>
	</div>
	
	<div class="row form-group">
		<div class="col-md-4">
			<label>Tel&eacute;fono</label>
			<div class="input-group">
				<input type="text" name="telefono" id="<?php echo $prefix; ?>telefono" value="<?php echo (!empty($telefono)) ? $telefono : ""; ?>" class="form-control input-sm">
				<span class="input-group-addon"><i class="fa fa-phone"></i></span>
			</div>
		</div>
		<div class="col-md-4">
			<label>E-MAIL</label>
			<div class="input-group">
				<input type="text" type="email" name="email" id="<?php echo $prefix; ?>email" value="<?php echo (!empty($email)) ? $email : ""; ?>" class="form-control input-sm" >
				<span class="input-group-addon">@</span>
			</div>
		</div>
	</div>

	<div class="row form-group">
		<div class="col-md-12">
			<button type="button" id="<?php echo $prefix; ?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>">Cancelar</button>
			<button type="submit" id="<?php echo $prefix; ?>btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
		</div>
	</div>
</form>