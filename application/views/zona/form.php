<form id="form_<?php echo $controller; ?>" class="app-form form-uppercase" autocomplete="off" data-prefix="<?php echo $prefix;?>" data-modal="<?php echo $modal;?>">
	<input type="hidden" name="idzona" id="<?php echo $prefix; ?>idzona" value="<?php echo (!empty($idzona)) ? $idzona : ""; ?>">
	<input type="hidden" name="idubigeo" id="<?php echo $prefix; ?>idubigeo" value="<?php echo (!empty($idubigeo)) ? $idubigeo : ""; ?>">
	<div class="form-group">
		<label class="control-label required">Ubigeo</label>
		<div class="input-group">
			<input type="text" name="" id="<?php echo $prefix; ?>ubigeo_descr" value="<?php echo (!empty($ubigeo_descr)) ? $ubigeo_descr : ""; ?>" class="form-control" required="" readonly="">
			<span class="input-group-btn tooltip-demo">
				<button type="button" id="<?php echo $prefix; ?>btn_ubigeo" class="btn btn-outline btn-success" data-toggle="tooltip" title="&iquest;Buscar Ubigeo">
					<i class="fa fa-search"></i>
				</button>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label required">Zona</label>
		<input type="text" name="zona" id="<?php echo $prefix; ?>zona" value="<?php echo (!empty($zona)) ? $zona : ""; ?>" class="form-control" required="">
	</div>
	
	<div class="form-group">
		<button type="button" id="<?php echo $prefix; ?>btn_cancel" class="btn btn-sm btn-white btn_cancel<?php echo $modal?" modal-form":""; ?>" data-controller="<?php echo $controller; ?>" >Cancelar</button>
		<button type="submit" id="<?php echo $prefix; ?>btn_save" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Guardar</button>
	</div>
</form>

<?php echo $ubigeo; ?>