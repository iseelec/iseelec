<form id="form_<?php echo $controller; ?>" class="app-form">
	<input type="hidden" name="idproveedor" id="idproveedor" value="">
	<input type="hidden" name="idproducto" id="idproducto" value="">
	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label>Proveedor</label>
						<div class="input-group">
							<input type="text" name="proveedor" id="proveedor_descripcion" value="" class="form-control">
							<span class="input-group-btn tooltip-demo">
								<button type="button" id="btn-buscar-proveedor" class="btn btn-outline btn-primary" data-toggle="tooltip" title="Buscar proveedores">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					<div class="form-group">
						<label>A&ntilde;o</label>
						<?php echo $annio; ?>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label>Periodo</label>
						<?php echo $periodo; ?>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Del Almacen:</label>
						<?php  echo $almacen_i; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label>Producto</label>
						<div class="input-group">
							<input type="text" name="producto" id="producto_descripcion" value="" class="form-control">
							<span class="input-group-btn tooltip-demo">
								<button type="button" id="btn-buscar-proveedor" class="btn btn-outline btn-primary" data-toggle="tooltip" title="Buscar proveedores">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-6">					
					
					<div class="form-group">
						<label>Fecha</label>
						<div class="input-group date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type="text" name="fecha" id="fecha" value="<?php echo date("d/m/Y"); ?>" class="form-control" placeholder="<?php echo date("d/m/Y"); ?>" required="">
						</div>
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Hasta el Almacen:</label>
						<?php  echo $almacen_f; ?>
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
					
					<fieldset>
					  <legend  style="font-size: 13px; max-width: 100%; font-weight: 700;">Formato</legend>
					  <input type="radio" name="opc_tipo" class="opc_tipo" id="tipo_1" value="1" checked /><label for="tipo_1">PDF&nbsp;&nbsp;</label>
					  <input type="radio" name="opc_tipo" class="opc_tipo" id="tipo_2" value="2" /><label for="tipo_2">Excel&nbsp;</label>
					</fieldset>
					</div>
				</div>
				
			</div>			
		</div>		
	</div>

	<div class="row">
		<div class="form-group">
			<div class="col-lg-6">
				<button id="btn_generar_report" class="btn btn-sm btn-primary" data-controller="<?php echo $controller; ?>">Generar Archivo</button>
			</div>
		</div>
	</div>
</form>

