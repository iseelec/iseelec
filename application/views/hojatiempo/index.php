<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-md-4">
			<div class="ibox float-e-margins">
				<div class="ibox-content p-m">
					<form id="formFiltros" role="form">
						<input type="hidden" name="fecha" id="fecha">
						<div class="form-group">
							<label>Proyecto</label>
							<select class="form-control" name="idproyecto" id="idproyecto">
								<option value=""></option>
								<?php echo $options_proyecto;?>
							</select>
						</div>
						<div class="form-group">
							<label>Empleado</label>
							<select class="form-control" name="idusuario" id="idusuario"></select>
						</div>
						<div class="form-group">
							<label>Fase</label>
							<select class="form-control" name="idfase" id="idfase"></select>
						</div>
						<div>
							<button type="button" class="btn btn-sm btn-primary" id="btnGenerar">Consultar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-md-8">
		
			<div id="datosHojaTiempo" class="ibox m-b">
				<div class="ibox-title">
					<span class="label label-primary pull-right hide">Hoy</span>
					<h3 class="date-name"></h3>
				</div>
				<div class="ibox-content">
				
					<table class="table table-striped table-hojatiempo m-b-none">
						<thead>
							<tr>
								<th style="width:25px;">&nbsp;</th>
								<th class="text-center" style="width:65px;">Inicio</th>
								<th class="text-center" style="width:65px;">Fin</th>
								<th class="text-center">Tarea</th>
								<th class="text-center" style="width:60px;">Horas</th>
								<th class="text-center">Observaciones</th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<th colspan="4"><button type="button" class="btn btn-white btn-xs hide" id="btnAddRow"><i class="fa fa-plus"></i> Agregar fila</button></th>
								<th class="text-center text-horas-total"></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
					<div class="text-right"><button type="button" class="btn btn-primary btn-sm hide" id="btnSave">Guardar</button></div>
				</div>
			</div>
			
			<div id="datosAnteriores"></div>
			
		</div>
	</div>
</div>
<select id="tmpComboActividad" class="hide"></select>
<style>
.tag-list {
    display: inline-block;
    margin-left: 10px;
}
.faq-item .tag-list>.tag-item {
	font-size: 14px;
}
</style>