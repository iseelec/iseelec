
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">

            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Bienvenido al Sistema Administrable SysAgroPalm</span>
                </li>
                <li>
                    <a href="<?php echo base_url()?>login/salir">
                        <i class="fa fa-sign-out"></i> Salir
                    </a>
                </li>
            </ul>
            </nav>
        </div>
        <div class="wrapper wrapper-content">

        </div>
