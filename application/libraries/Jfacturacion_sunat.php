<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/libraries/Jfacturacion_base.php"; 

class Jfacturacion_sunat extends Jfacturacion_base {
	
	protected $tipo_operacion_venta = "0101"; // Catalogo 51, venta interna
	protected $name_igv_default = "EXO";
	protected $codsunat_igv_default = "9997";
	protected $codinter_igv_default = "VAT";
	
	public function get_data_venta($id) {
		// cabecera
		$sql = "select coalesce(t.codtipo_operacion, '".$this->tipo_operacion_venta."') as top, 
			d.codsunat as tdoc, t.serie, t.correlativo as numero, t.fecha_venta as fecha,
			to_char(t.fecha_registro, 'HH24:MI:SS') as hora, c.dni, c.ruc, c.nombres||coalesce(' '||c.apellidos,'') as razon, 
			m.abreviatura as moneda, t.igv, t.subtotal, t.subtotal+t.igv as total, t.idcliente, 
			coalesce(t.descuento, 0) as descuento, t.estado, c.direccion_principal as direccion
			from venta.venta t
			join venta.tipo_documento d on d.idtipodocumento=t.idtipodocumento
			join venta.cliente c on c.idcliente=t.idcliente
			join general.moneda m on m.idmoneda=t.idmoneda
			where t.idventa=?";
		$query = $this->ci->db->query($sql, array($id));
		$res["cab"] = $query->row_array();
		
		$w = ($res["cab"]["estado"] == "A") ? "=" : "<>";
		
		// detalle
		$sql = "select u.codsunat as unidmed, t.cantidad, t.idproducto as codproducto, t.descripcion as producto, 
		
			t.precio as valor_unit, 
			case when t.oferta='S' then 0.00 else 0.00*t.cantidad end as sum_dscto, 
			case when t.oferta='S' then 0.00 else t.precio*t.cantidad*t.igv end as sum_igv,
			case when t.oferta='S' then 0.00 else t.precio*(1+t.igv) end as precio_venta, 
			t.precio*t.cantidad as valor_venta,
			
			case when t.codtipo_igv is not null then t.codtipo_igv else
				case when t.oferta='S' then '".$this->tipo_igv_oferta."'::text else 
					case when coalesce(v.igv,0)>0 then '10'::text else '".$this->tipo_igv_default."'::text end
				end
			end as tipo_igv, t.oferta, t.precio as pu_real, t.serie,
			coalesce(t.codgrupo_igv, '".$this->grupo_igv_default."') as codgrupo_igv
			,coalesce(g.nombre, '".$this->name_igv_default."') as name_igv
			,coalesce(g.codsunat, '".$this->codsunat_igv_default."') as codsunat_igv
			,coalesce(g.codinternacional, '".$this->codinter_igv_default."') as codinter_igv
			from venta.detalle_venta t 
			join venta.venta v on v.idventa=t.idventa
			join compra.unidad u on u.idunidad=t.idunidad
			left join general.grupo_igv g on g.codgrupo_igv=t.codgrupo_igv
			where t.idventa=? and t.estado{$w}'A'
			order by t.iddetalle_venta";
		$query = $this->ci->db->query($sql, array($id));
		$res["det"] = $query->result_array();
		
		return $res;
	}
	
	public function get_data_nota_credito($id) {
		// cabecera
		$sql = "select '".$this->tipo_operacion_venta."' as top, d.codsunat as tdoc, t.serie, t.numero as numero, 
			to_char(t.fecha_registro,'YYYY-MM-DD') as fecha, to_char(t.fecha_registro, 'HH24:MI:SS') as hora, 
			c.dni, c.ruc, c.nombres||coalesce(' '||c.apellidos,'') as razon, m.abreviatura as moneda, t.igv, t.subtotal, t.descuento, 
			t.subtotal+t.igv-t.descuento as total, t.descripcion as motivo, d2.codsunat as tdoc_ref, t.serie_ref, 
			t.numero_ref as numero_ref, t.estado, t.idcliente, c.direccion_principal as direccion,
			t.idtipo_notacredito as tnota, tn.descripcion as tnota_desc
			from venta.notacredito t
			join venta.cliente c on c.idcliente=t.idcliente
			join venta.tipo_documento d on d.idtipodocumento=t.idtipodocumento
			join general.moneda m on m.idmoneda=t.idmoneda
			join venta.tipo_documento d2 on d2.idtipodocumento=t.iddocumento_ref
			join general.tipo_notacredito tn on tn.idtipo_notacredito=t.idtipo_notacredito
			where t.idnotacredito=?";
		$query = $this->ci->db->query($sql, array($id));
		$res["cab"] = $query->row_array();
		
		$w = ($res["cab"]["estado"] == "A") ? "=" : "<>";
		
		// detalle
		$sql = "select u.codsunat as unidmed, t.cantidad, t.idproducto as codproducto, t.descripcion as producto, 
		
			t.precio as valor_unit, 
			0.00 as sum_dscto, 
			t.precio*t.igv*t.cantidad as sum_igv,
			t.precio*(1+t.igv) as precio_venta, t.precio*t.cantidad as valor_venta,
			
			case when t.codtipo_igv is not null then t.codtipo_igv else
				case when coalesce(t.igv,0)>0 then '10'::text else '".$this->tipo_igv_default."'::text end
			end as tipo_igv
			,'N'::text as oferta, t.precio as pu_real, t.serie,
			coalesce(t.codgrupo_igv, '".$this->grupo_igv_default."') as codgrupo_igv
			,coalesce(g.nombre, '".$this->name_igv_default."') as name_igv
			,coalesce(g.codsunat, '".$this->codsunat_igv_default."') as codsunat_igv
			,coalesce(g.codinternacional, '".$this->codinter_igv_default."') as codinter_igv
			from venta.detalle_notacredito t 
			join compra.unidad u on u.idunidad=t.idunidad
			left join general.grupo_igv g on g.codgrupo_igv=t.codgrupo_igv
			where t.idnotacredito=? and t.estado{$w}'A'
			order by t.iddetalle_notacredito";
		$query = $this->ci->db->query($sql, array($id));
		$res["det"] = $query->result_array();
		
		return $res;
	}
	
	public function make_file_det($tipo_doc, $serie, $numero, $data) {
		$content = "";
		if( ! empty($data)) {
			foreach($data as $i=>$row) {
				$producto = trim($row["producto"]);
				if( ! empty($row["serie"])) {
					$txtSeries = trim($row["serie"]);
					$txtSeries = str_replace("|", ",", $txtSeries);
					$producto .= " S/N: ".$txtSeries;
				}
				
				if($row["oferta"] == "S") {
					// testear esta parte para los items gratuitos
					$row["codsunat_igv"] = "9996";
					$row["name_igv"] = "GRA";
					$row["codinter_igv"] = "FRE";
					// $row["sum_igv"] = 0;
				}
				
				if(strlen($producto) > 500) {
					$producto = substr($producto, 0, 500);
				}
				
				$producto = str_replace("|","/",$producto);
				$producto = str_replace("\\","/",$producto);
				$producto = str_replace("&", "&amp;", $producto);
				
				$linea = array();
				$linea[] = $row["unidmed"]; // cod unidad medida (NIU)													1
				$linea[] = number_format($row["cantidad"],4,".",""); // cantidad de unid x item							2
				$linea[] = $row["codproducto"]; // codigo producto														3
				$linea[] = "-"; // codigo producto sunat																4
				$linea[] = $producto; // descripcion item																5
				$linea[] = number_format($row["valor_unit"],4,".",""); // valor unitario x item							6
				$linea[] = number_format($row["sum_igv"],2,".",""); // sumatoria tributos (igv + isc + otros)			7
				// IGV
				$linea[] = $row["codsunat_igv"]; // tipo tributos igv (IGV)												8
				$linea[] = number_format($row["sum_igv"],2,".",""); // monto igv x item									9
				$linea[] = "0.00"; // base imponible igv x item (>0)													10
				$linea[] = $row["name_igv"]; // nombre tributo x item													11
				$linea[] = $row["codinter_igv"]; // tipo tributo x item													12
				$linea[] = $row["tipo_igv"]; // afectacion al igv x item (catalogo 7)									13
				$linea[] = number_format($this->porcentaje_igv,2,".",""); // porcentaje igv								14
				// ISC
				$linea[] = "-"; // tipo tributo isc (catalogo 5)														15
				$linea[] = "0.00"; // monto isc x item																	16
				$linea[] = "0.00"; // base imponible isc x item (>0)													17
				$linea[] = "ISC"; // nombre tributo x item																18
				$linea[] = "2000"; // tipo tributo x item (isc)															19
				$linea[] = "01"; // tipo sistema isc (catalogo 8)														20
				$linea[] = "15.00"; // porcentaje isc																	21
				// OTRO
				$linea[] = "-"; // tipo tributo otro (catalogo 5)														22
				$linea[] = "0.00"; // monto otro x item																	23
				$linea[] = "0.00"; // base imponible otro x item (>0)													24
				$linea[] = "OTROS"; // nombre tributo x item															25
				$linea[] = "9999"; // tipo tributo x item (isc)															26
				$linea[] = "15.00"; // porcentaje otro																	27
				
				$linea[] = number_format($row["precio_venta"],4,'.',''); // precio venta unitario x item				28
				$linea[] = number_format($row["valor_venta"],2,".",""); // valor venta x item							29
				$linea[] = "0.00"; // Valor referencial unitario (gratuitos)											30
				
				$content .= implode("|", $linea);
				$content .= "\r\n";
			}
		}
		
		// guardamos el archivo detalle
		$this->save_file($tipo_doc, $serie, $numero, "DET", $content);
	}
	
	public function make_file_venta($cab, $det) {
		// datos para el archivo
		$serie = $cab["serie"];
		if(is_numeric($serie)) {
			$serie = intval($serie);
			$serie = str_pad($serie, 4, "0", STR_PAD_LEFT);
		}
		$numero = $cab["numero"];
		if(is_numeric($numero)) {
			$numero = intval($numero);
			$numero = str_pad($numero, $this->pad_length, "0", STR_PAD_LEFT);
		}
		
		$tdoc = $ruc_dni = "-";
		$razon = trim($cab["razon"]);
		
		if($cab["tdoc"] == "01") { // modifica una factura
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
		}
		else if( ! empty($cab["dni"])) {
			$tdoc = 1;
			$ruc_dni = $cab["dni"];
		}
		else if( ! empty($cab["ruc"])) {
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
		}
		else {
			$tdoc = ($cab["tdoc"] == "01") ? 6 : 1;
			$ruc_dni = ($tdoc == 6) ? str_repeat("0", 11) : str_repeat("0", 8);
			$razon = "VARIOS";
		}
		
		if(strlen($razon) > 300)
			$razon = substr($razon, 0, 300);
		$razon = str_replace("&", "&amp;", $razon);
		
		// obtenemos el total descuento desde los items
		$strAde = "";
		$totalDscto = $totalGra = $totalIna = $totalExo = $sumaIgv = $totalOferta = 0;
		if( ! empty($det)) {
			foreach($det as $i=>$row) {
				if($row["oferta"] == "S")
					$totalOferta += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "GRA")
					$totalGra += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "EXO")
					$totalExo += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "INA")
					$totalIna += $row["valor_venta"];
				
				$sumaIgv += $row["sum_igv"];
				$totalDscto += $row["sum_dscto"];
			}
		}
		
		// $importeTotal = $totalGra + $totalIna + $totalExo + $sumaIgv - $totalDscto - $cab["descuento"];
		$totalValorVenta = $totalGra + $totalIna + $totalExo;
		$totalPrecioVenta = $totalValorVenta + $sumaIgv;
		$totalDescuento = $totalDscto + $cab["descuento"];
		$sumaOtrosCargos = 0;
		$totalAnticipos = 0;
		$importeTotal = $totalPrecioVenta - $totalDescuento + $sumaOtrosCargos - $totalAnticipos;
		
		$linea = array();
		$linea[] = $cab["top"]; // tipo operacion (Catalogo 51, venta interna)									1
		$linea[] = $cab["fecha"]; // fecha emision  (YYYY-MM-DD)												2
		$linea[] = $cab["hora"]; // hora emision (HH:MM:SS)														3
		$linea[] = "-"; // fecha vencimiento																	4
		$linea[] = ""; // codigo domicilio fiscal o de local anexo del emisor(N3)								5
		$linea[] = $tdoc; // tipo doc cliente (DNI o RUC)														6
		$linea[] = $ruc_dni; // numero doc cliente (numero DNI o RUC)											7
		$linea[] = $razon; // nombre cliente, razon social														8
		$linea[] = $cab["moneda"]; // moneda (PEN,USD,...)														9
		$linea[] = number_format($sumaIgv,2,".",""); // sum igv													10
		$linea[] = number_format($totalValorVenta,2,".",""); // total valor venta								11
		$linea[] = number_format($totalPrecioVenta,2,".",""); // total precio venta								12
		$linea[] = number_format($totalDescuento,2,'.',''); // total descuento									13
		$linea[] = number_format($sumaOtrosCargos,2,'.',''); // suma otros cargos								14
		$linea[] = number_format($totalAnticipos,2,'.',''); // total anticipos									15
		$linea[] = number_format($importeTotal,2,".",""); // importe total venta								16
		$linea[] = "2.1"; // version UBL																		17
		$linea[] = "2.0"; // custom documento																	18
		
		// guardamos el archivo cabecera
		$file = $this->save_file($cab["tdoc"], $serie, $numero, "CAB", implode("|", $linea));
		
		// armamos el archivo detalle
		$this->make_file_det($cab["tdoc"], $serie, $numero, $det);
		
		// creamos archivo leyenda
		$this->make_file_ley($cab["tdoc"], $serie, $numero, $importeTotal);
		
		$res["tipo_doc"] = $cab["tdoc"];
		$res["tip_docu"] = $cab["tdoc"];
		$res["fec_carg"] = date("d/m/Y");
		$res["ind_situ"] = "01";
		$res["serie"] = $serie;
		$res["numero"] = $numero;
		$res["num_docu"] = $serie."-".$numero;
		$res["archivo"] = basename($file, ".CAB");
		$res["ruta"] = dirname($file);
		$res["tip_docu_cliente"] = $tdoc;
		$res["num_docu_cliente"] = $ruc_dni;
		$res["razon_cliente"] = $razon;
		$res["direccion_cliente"] = $cab["direccion"];
		$res["gravado"] = $totalGra;
		$res["inafecto"] = $totalIna;
		$res["exonerado"] = $totalExo;
		$res["suma_igv"] = $sumaIgv;
		$res["suma_descuento"] = $totalDscto;
		$res["descuento_global"] = $cab["descuento"];
		$res["gratuito"] = $totalOferta;
		$res["valor_venta"] = $totalValorVenta;
		return $res;
	}
	
	public function make_file_nota($cab, $det) {
		// datos para el archivo
		$serie = trim($cab["serie"]);
		if(is_numeric($serie)) {
			$serie = intval($serie);
			$serie = str_pad($serie, 4, "0", STR_PAD_LEFT);
		}
		$numero = $cab["numero"];
		if(is_numeric($numero)) {
			$numero = intval($numero);
			$numero = str_pad($numero, $this->pad_length, "0", STR_PAD_LEFT);
		}
		
		// datos del contenido del archivo cabecera
		$serie_ref = trim($cab["serie_ref"]);
		if(is_numeric($serie_ref)) {
			$serie_ref = intval($serie_ref);
			$serie_ref = str_pad($serie_ref, 4, "0", STR_PAD_LEFT);
		}
		$numero_ref = $cab["numero_ref"];
		if(is_numeric($numero_ref)) {
			$numero_ref = intval($numero_ref);
			$numero_ref = str_pad($numero_ref, 8, "0", STR_PAD_LEFT);
		}
		
		// datos del contenido del archivo cabecera
		$tdoc = $ruc_dni = "-";
		$razon = trim($cab["razon"]);
		
		if($cab["tdoc_ref"] == "01") { // factura
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
		}
		else if( ! empty($cab["dni"])) {
			$tdoc = 1;
			$ruc_dni = $cab["dni"];
		}
		else if( ! empty($cab["ruc"])) {
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
		}
		else {
			$tdoc = ($cab["tdoc_ref"] == "01") ? 6 : 1;
			$ruc_dni = ($tdoc == 6) ? str_repeat("0", 11) : str_repeat("0", 8);
			$razon = "VARIOS";
		}
		
		if(strlen($razon) > 100)
			$razon = substr($razon, 0, 100);
		$razon = str_replace("&", "&amp;", $razon);
		
		$motivo = $cab["motivo"];
		if(strlen($motivo) > 250)
			$motivo = substr($motivo, 0, 250);
		$motivo = str_replace("|","/",$motivo);
		
		// obtenemos el total descuento desde los items
		$totalGra = $totalIna = $totalExo = $sumaIgv = $totalOferta = 0;
		if( ! empty($det)) {
			foreach($det as $i=>$row) {
				if($row["oferta"] == "S")
					$totalOferta += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "GRA")
					$totalGra += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "EXO")
					$totalExo += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "INA")
					$totalIna += $row["valor_venta"];
				
				$sumaIgv += $row["sum_igv"];
			}
		}
		
		$totalValorVenta = $totalGra + $totalIna + $totalExo;
		$totalPrecioVenta = $totalValorVenta + $sumaIgv;
		$totalDescuento = 0;
		$sumaOtrosCargos = 0;
		$totalAnticipos = 0;
		$importeTotal = $totalPrecioVenta - $totalDescuento + $sumaOtrosCargos - $totalAnticipos;
		
		$linea = array();
		$linea[] = $cab["top"]; // tipo operacion (catalogo 51)													1
		$linea[] = $cab["fecha"]; // fecha emision (YYYY-MM-DD)													2
		$linea[] = $cab["hora"]; // hora emision (HH:MM:SS)														3
		$linea[] = ""; // codigo domicilio fiscal																4
		$linea[] = $tdoc; // tipo doc cliente (DNI o RUC)														5
		$linea[] = $ruc_dni; // numero doc cliente (numero DNI o RUC)											6
		$linea[] = $razon; // nombre cliente, razon social														7
		$linea[] = $cab["moneda"]; // moneda (PEN,USD,...)														8
		$linea[] = $cab["tnota"]; // codigo tipo nota															9
		$linea[] = $motivo; // motivo o sustento																10
		$linea[] = $cab["tdoc_ref"]; // tipo doc que modifica													11
		$linea[] = $serie_ref.'-'.$numero_ref; // nro doc que modifica											12
		$linea[] = number_format($sumaIgv,2,".",""); // sum tributos (igv)										13
		$linea[] = number_format($totalValorVenta,2,".",""); // total valor venta								14
		$linea[] = number_format($totalPrecioVenta,2,".",""); // total precio venta								15
		$linea[] = number_format($totalDescuento,2,'.',''); // total descuento									16
		$linea[] = number_format($sumaOtrosCargos,2,'.',''); // suma otros cargos								17
		$linea[] = number_format($totalAnticipos,2,'.',''); // total anticipos									18
		$linea[] = number_format($importeTotal,2,".",""); // importe total venta								19
		$linea[] = "2.1"; // version UBL																		20
		$linea[] = "2.0"; // custom documento																	21
		
		// guardamos el archivo cabecera
		$file = $this->save_file($cab["tdoc"], $serie, $numero, "NOT", implode("|", $linea));
		
		// armamos el archivo detalle
		$this->make_file_det($cab["tdoc"], $serie, $numero, $det);
		
		// creamos archivo leyenda
		$this->make_file_ley($cab["tdoc"], $serie, $numero, $cab["total"]);
		
		$res["tipo_doc"] = $cab["tdoc"];
		$res["tip_docu"] = $cab["tdoc"];
		$res["fec_carg"] = date("d/m/Y");
		$res["ind_situ"] = "01";
		$res["serie"] = $serie;
		$res["numero"] = $numero;
		$res["num_docu"] = $serie."-".$numero;
		$res["archivo"] = basename($file, ".NOT");
		$res["ruta"] = dirname($file);
		$res["tip_docu_cliente"] = $tdoc;
		$res["num_docu_cliente"] = $ruc_dni;
		$res["razon_cliente"] = $razon;
		$res["gravado"] = $totalGra;
		$res["inafecto"] = $totalIna;
		$res["exonerado"] = $totalExo;
		$res["suma_igv"] = $sumaIgv;
		$res["suma_descuento"] = 0;
		$res["descuento_global"] = 0;
		$res["gratuito"] = 0;
		$res["valor_venta"] = $totalValorVenta;
		return $res;
	}
}

/* End of file JFacturacion.php */