<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH."service/client.php";

class Jfacturacion_base {
	
	protected $ci = NULL;
	protected $ruc = NULL;
	
	protected $pad_length = 8;
	
	public $path = NULL;
	protected $url_webservice = NULL;
	
	protected $tipo_operacion_venta = "01"; // Anexo 8 Catalogo 17, venta interna
	public $grupo_igv_default = "EXO";
	public $tipo_igv_default = "20"; // Anexo 8 Catalogo 7, exonerado - operacion onerosa
	public $tipo_igv_oferta = "21"; // Anexo 8 Catalogo 7, exonerado - transferencia gratuita
	protected $porcentaje_igv = 18;
	
	protected $create_files = TRUE;
	
	protected $config = array(
		"venta" => array("model"=>"venta.venta", "pkey"=>"idventa")
		,"notacredito" => array("model"=>"venta.notacredito", "pkey"=>"idnotacredito")
		,"notadebito" => array("model"=>"venta.notadebito", "pkey"=>"idnotadebito")
		,"documento_baja" => array("model"=>"venta.documento_baja", "pkey"=>"iddocumento_baja")
		,"resumen_diario" => array("model"=>"venta.resumen_diario", "pkey"=>"idresumen_diario")
		,"guiaremision" => array("model"=>"almacen.guia_remision", "pkey"=>"idguia_remision")
	);
	
	/**
	 * Constructor, obtemos el codeigniter 
	 */
	public function __construct() {
		$this->init();
	}
	
	private function init() {
		$this->ci =& get_instance();
		$this->path = APPPATH."files";
		
		$this->url_webservice = $this->ci->get_param("url_webservice");
		
		$igv = $this->ci->get_param("porcentaje_igv");
		if( ! empty($igv))
			$this->porcentaje_igv = $igv;
		
		$this->load_ruc();
	}
	
	public function register($tabla, $codtabla, $idsucursal) {
		if( ! $this->is_ref($tabla))
			return false;
		
		$this->ci->load_model("venta.facturacion");
		$this->ci->facturacion->text_uppercase(false);
		
		$this->load_ruc($idsucursal);
		
		$this->ci->facturacion->set("idreferencia", $codtabla);
		$this->ci->facturacion->set("referencia", $tabla);
		$this->ci->facturacion->set("idsucursal", $idsucursal);
		$this->ci->facturacion->set("fec_carg", date("d/m/Y"));
		$this->ci->facturacion->set("estado", "A");
		$this->ci->facturacion->set("fecha", date("Y-m-d"));
		$this->ci->facturacion->set("fecha_registro", date("Y-m-d H:i:s"));
		$this->ci->facturacion->set("actualizado", 0);
		$this->ci->facturacion->set("num_ruc", $this->ruc);
		$this->ci->facturacion->insert();
	}
	
	public function load_ruc($idsucursal=FALSE) {
		if($this->ruc != NULL)
			return;
		
		if( ! isset($this->ci->facturacion))
			return;
		
		if($idsucursal === FALSE)
			$idsucursal = $this->ci->facturacion->get("idsucursal");
		
		$sql = "select ruc from seguridad.empresa 
			where idempresa in (
				select idempresa from seguridad.sucursal where idsucursal=?
			)";
		$query = $this->ci->db->query($sql, array($idsucursal));
		if($query->num_rows() > 0)
			$this->ruc = $query->row()->ruc;
	}
	
	public function save_file($tipo_doc, $serie, $numero, $ext, $content) {
		$real_path = $this->path."/".$this->ruc;
		if( ! file_exists($real_path) && $this->create_files === true)
			mkdir($real_path, 0777, true);
		
		$real_path .= "/".$tipo_doc;
		if( ! file_exists($real_path) && $this->create_files === true)
			mkdir($real_path, 0777, true);
		
		$real_path .= "/".$serie;
		if( ! file_exists($real_path) && $this->create_files === true)
			mkdir($real_path, 0777, true);
		
		$file_name = $real_path."/".$this->ruc."-{$tipo_doc}-{$serie}-{$numero}.{$ext}";
		
		if($this->create_files === true) {
			$file = fopen($file_name, "w+");
			fwrite($file, $content);
			fclose($file);
		}
		
		return $file_name;
	}
	
	public function get_data_venta($id) {
		// cabecera
		$sql = "select coalesce(t.codtipo_operacion, '".$this->tipo_operacion_venta."') as top, 
			d.codsunat as tdoc, t.serie, t.correlativo as numero, t.fecha_venta as fecha, c.dni, c.ruc, 
			c.nombres||coalesce(' '||c.apellidos,'') as razon, m.abreviatura as moneda, t.igv, t.subtotal, 
			t.subtotal+t.igv as total, t.idcliente, c.direccion_principal as direccion, t.estado
			from venta.venta t
			join venta.tipo_documento d on d.idtipodocumento=t.idtipodocumento
			join venta.cliente c on c.idcliente=t.idcliente
			join general.moneda m on m.idmoneda=t.idmoneda
			where t.idventa=?";
		$query = $this->ci->db->query($sql, array($id));
		$res["cab"] = $query->row_array();
		
		$eqs = ($res["cab"]["estado"] == "A") ? "=" : "<>";
		
		// detalle
		$sql = "select u.codsunat as unidmed, t.cantidad, p.codigo_producto as codproducto, t.descripcion as producto, 
			case when t.oferta='S' then 0.00 else t.precio end as valor_unit, 
			case when t.oferta='S' then 0.00 else 0.00 end as sum_dscto, 
			case when t.oferta='S' then 0.00 else t.precio*t.igv*t.cantidad end as sum_igv,
			case when t.oferta='S' then 0.00 else t.precio*(1+t.igv) end as precio_venta, 
			case when t.oferta='S' then 0.00 else t.precio*t.cantidad end as valor_venta,
			case when t.codtipo_igv is not null then t.codtipo_igv else
				case when t.oferta='S' then '".$this->tipo_igv_oferta."'::text else 
					case when coalesce(t.igv,0)>0 then '10'::text else '".$this->tipo_igv_default."'::text end
				end
			end as tipo_igv
			,t.oferta, t.precio as pu_real, array_to_string(array_agg(s.serie), ', '::text) AS serie,
			coalesce(t.codgrupo_igv, '".$this->grupo_igv_default."') as codgrupo_igv
			from venta.detalle_venta t 
			join compra.producto p on p.idproducto=t.idproducto
			join compra.unidad u on u.idunidad=t.idunidad
			left join venta.detalle_venta_serie s on s.iddetalle_venta=t.iddetalle_venta and s.idventa=t.idventa
			where t.idventa=? and t.estado{$eqs}'A' 
			group by u.codsunat, t.cantidad, p.codigo_producto, t.descripcion, t.oferta, t.precio, t.igv, 
			t.iddetalle_venta, t.codtipo_igv, t.codgrupo_igv
			order by t.iddetalle_venta";
		$query = $this->ci->db->query($sql, array($id));
		$res["det"] = $query->result_array();
		
		return $res;
	}
	
	public function get_data_nota_credito($id) {
		// cabecera
		$sql = "select t.idtipo_notacredito as top, d.codsunat as tdoc, t.serie, t.numero, 
			t.fecha, c.dni, c.ruc, c.nombres||coalesce(' '||c.apellidos,'') as razon, 
			m.abreviatura as moneda, t.igv, t.subtotal, t.subtotal+t.igv as total, 
			t.descripcion as motivo, d2.codsunat as tdoc_ref, t.serie_ref, t.numero_ref, 
			t.idcliente, c.direccion_principal as direccion, t.estado
			from venta.notacredito t
			join venta.tipo_documento d on d.idtipodocumento=t.idtipodocumento
			join venta.cliente c on c.idcliente=t.idcliente
			join general.moneda m on m.idmoneda=t.idmoneda
			join venta.tipo_documento d2 on d2.idtipodocumento=t.iddocumento_ref
			where t.idnotacredito=?";
			
		$query = $this->ci->db->query($sql, array($id));
		$res["cab"] = $query->row_array();
		
		$eqs = ($res["cab"]["estado"] == "A") ? "=" : "<>";
		
		// detalle
		$sql = "select u.codsunat as unidmed, t.cantidad, p.codigo_producto as codproducto, 
			t.descripcion as producto, t.precio as valor_unit, 0.00 as sum_dscto, 
			t.cantidad*(1+t.igv) as sum_igv, t.precio*(1+t.igv) as precio_venta, 
			t.precio*t.cantidad as valor_venta, t.codtipo_igv as tipo_igv, 'N'::text as oferta, 
			t.precio as pu_real, t.serie, t.codgrupo_igv
			from venta.detalle_notacredito t 
			join compra.unidad u on u.idunidad=t.idunidad
			join compra.producto p on p.idproducto=t.idproducto
			where t.idnotacredito=? and t.estado{$eqs}'A' 
			order by iddetalle_notacredito";
			
		$query = $this->ci->db->query($sql, array($id));
		$res["det"] = $query->result_array();
		
		return $res;
	}
	
	public function get_data_nota_debito($id) {
		// cabecera
		$sql = "select t.idtipo_notadebito as top, d.codsunat as tdoc, t.serie, t.numero, 
			t.fecha, c.dni, c.ruc, c.nombres||coalesce(' '||c.apellidos,'') as razon, 
			m.abreviatura as moneda, t.igv, t.subtotal, t.subtotal+t.igv as total, 
			t.descripcion as motivo, d2.codsunat as tdoc_ref, t.serie_ref, t.numero_ref, 
			t.idcliente, c.direccion_principal as direccion, t.estado
			from venta.notadebito t
			join venta.tipo_documento d on d.idtipodocumento=t.idtipodocumento
			join venta.cliente c on c.idcliente=t.idcliente
			join general.moneda m on m.idmoneda=t.idmoneda
			join venta.tipo_documento d2 on d2.idtipodocumento=t.iddocumento_ref
			where t.idnotadebito=?";
			
		$query = $this->ci->db->query($sql, array($id));
		$res["cab"] = $query->row_array();
		
		$eqs = ($res["cab"]["estado"] == "A") ? "=" : "<>";
		
		// detalle
		$sql = "select u.codsunat as unidmed, t.cantidad, p.codigo_producto as codproducto, 
			t.descripcion as producto, t.precio as valor_unit, 0.00 as sum_dscto, 
			t.cantidad*(1+t.igv) as sum_igv, t.precio*(1+t.igv) as precio_venta, 
			t.precio*t.cantidad as valor_venta, t.codtipo_igv as tipo_igv, 'N'::text as oferta, 
			t.precio as pu_real, t.serie, t.codgrupo_igv
			from venta.detalle_notadebito t 
			join compra.unidad u on u.idunidad=t.idunidad
			join compra.producto p on p.idproducto=t.idproducto
			where t.idnotadebito=? and t.estado{$eqs}'A' 
			order by iddetalle_notadebito";
			
		$query = $this->ci->db->query($sql, array($id));
		$res["det"] = $query->result_array();
		
		return $res;
	}
	
	public function get_data_baja($id) {
		// cabecera
		$sql = "select d.correlativo, f.fec_gene, d.fecha, f.tip_docu, f.num_docu, d.motivo, f.serie, f.numero
			from venta.documento_baja d
			join venta.facturacion f on f.idreferencia=d.idreferencia and f.referencia=d.referencia
			where d.iddocumento_baja = ?";
			
		$query = $this->ci->db->query($sql, array($id));
		$res["cab"] = $query->row_array();
		$res["det"] = false;
		
		return $res;
	}
	
	public function get_data_resumen($id) {
		// cabecera
		$sql = "select * from venta.resumen_diario where idresumen_diario=?";
		$query = $this->ci->db->query($sql, array($id));
		$res["cab"] = $query->row_array();
		
		// detalle resumen
		$sql = "select b.fecha, b.tip_docu, b.num_docu, b.tip_docu_cliente, 
			b.num_docu_cliente, b.gravado, b.exonerado, b.inafecto, b.gratuito, b.suma_igv, 
			b.gravado + b.exonerado + b.inafecto + b.suma_igv - b.suma_descuento - b.descuento_global as total,
			d.tip_docu_modifica, d.serie_docu_modifica, d.nro_docu_modifica, d.estado_docu
			from venta.detalle_resumen_diario d
			join venta.facturacion b on b.idreferencia=d.idreferencia and b.referencia=d.referencia
			where d.estado='A' and d.idresumen_diario=?";
		$query = $this->ci->db->query($sql, array($id));
		$res["det"] = $query->result_array();
		
		return $res;
	}
	
	public function get_data_guia_remision($id) {
		// cabecera
		$sql = "select g.*, '09'::text as tdoc, t.descripcion as motivo_traslado
			from almacen.guia_remision g
			join almacen.motivo_traslado t on t.codmotivo_traslado=g.codmotivo_traslado
			where g.codguia_remision=?";
		$this->ci->db->query($sql, array($id));
		$res["cab"] = $this->ci->db->getArray("assoc");
		
		$eqs = ($res["cab"]["estado"] == "I") ? "=" : "<>";
		
		// detalle resumen
		$sql = "select d.codproducto, d.prod_descripcion as producto, u.codsunat as unidmed, d.cantidad, null::double precision as peso
			from almacen.detalle_guiaremision d
			join general.unidadmedida u on u.codunidadmedida=d.codunidadmedida
			where d.estado{$eqs}'I' and d.codguia_remision=?";
		$this->ci->db->query($sql, array($id));
		$res["det"] = $this->ci->db->getAll("assoc");
		
		return $res;
	}
	
	public function make_file_ley($tdoc, $serie, $numero, $total) {
		$str = "1002|TRANSFERENCIA GRATUITA";
		if($total > 0) {
			if( ! isset($this->ci->numeroletra))
				$this->ci->load->library('numeroletra');
			$str = "1000|".trim($this->ci->numeroletra->convertir(number_format($total,2,'.',''), true));
		}
		$this->save_file($tdoc, $serie, $numero, "LEY", $str);
	}
	
	public function make_file_det($tipo_doc, $serie, $numero, $data) {
		$content = "";
		if( ! empty($data)) {
			foreach($data as $i=>$row) {
				$producto = trim($row["producto"]);
				if( ! empty($row["serie"])) {
					$txtSeries = trim($row["serie"]);
					$txtSeries = str_replace("|", ",", $txtSeries);
					$producto .= " S/N: ".$txtSeries;
				}
				if(strlen($producto) > 250) {
					$producto = substr($producto, 0, 250);
				}
				$producto = str_replace("|","/",$producto);
				$producto = str_replace("\\","/",$producto);
				
				$linea = array();
				$linea[] = $row["unidmed"]; // cod unidad medida (NIU)											1
				$linea[] = $row["cantidad"]; // cantidad de unid x item											2
				$linea[] = $row["codproducto"]; // codigo producto												3
				// $linea[] = ""; // codigo producto																3
				$linea[] = ""; // codigo producto sunat															4
				$linea[] = $producto; // descripcion item														5
				$linea[] = $row["valor_unit"]; // valor unitario x item											6
				$linea[] = number_format($row["sum_dscto"],2,".",""); // descuento x item						7
				$linea[] = number_format($row["sum_igv"],2,".",""); // monto igv x item							8
				$linea[] = $row["tipo_igv"]; // afectacion al igv (catalogo 7)									9
				$linea[] = "0.00"; // monto isc x item															10
				$linea[] = "01"; // tipo sistema isc (catalogo 8, Sistema al valor)								11
				// $linea[] = number_format($row["precio_venta"],2,'.',''); // precio venta unitario x item		12*
				$linea[] = $row["precio_venta"]; // precio venta unitario x item								12
				$linea[] = number_format($row["valor_venta"],2,".",""); // valor venta x item					13
				
				$content .= implode("|", $linea);
				$content .= "\r\n";
			}
		}
		
		// guardamos el archivo detalle
		$this->save_file($tipo_doc, $serie, $numero, "DET", $content);
	}
	
	public function make_file_venta($cab, $det) {
		// datos para el archivo
		$serie = $cab["serie"];
		if(is_numeric($serie)) {
			$serie = intval($serie);
			$serie = str_pad($serie, 4, "0", STR_PAD_LEFT);
		}
		$numero = $cab["numero"];
		if(is_numeric($numero)) {
			$numero = intval($numero);
			$numero = str_pad($numero, $this->pad_length, "0", STR_PAD_LEFT);
		}
		
		// datos del contenido del archivo cabecera
		/* $tdoc = $ruc_dni = "-";
		$razon = trim($cab["razon"]);
		if($cab["tdoc"] == "01") { //
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
			$razon = trim($cab["razon"]);
		}
		else if(!empty($cab["idcliente"]) && $cab["idcliente"] != 0 && !empty($cab["dni"])) {
			$tdoc = 1;
			$ruc_dni = $cab["dni"];
			$razon = trim($cab["razon"]);
		}
		
		if($tdoc == "-" && $ruc_dni == "-") { // fix, no deberia ser asi, consutar en SUNAT
			$tdoc = 1;
			$ruc_dni = "00000000";
		} */
		
		$tdoc = $ruc_dni = "-";
		$razon = trim($cab["razon"]);
		
		if($cab["tdoc"] == "01") { // modifica una 
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
		}
		else if( ! empty($cab["dni"])) {
			$tdoc = 1;
			$ruc_dni = $cab["dni"];
		}
		else if( ! empty($cab["ruc"])) {
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
		}
		else {
			$tdoc = ($cab["tdoc"] == "01") ? 6 : 1;
			$ruc_dni = ($tdoc == 6) ? str_repeat("0", 11) : str_repeat("0", 8);
			$razon = "VARIOS";
		}
		
		if(strlen($razon) > 100)
			$razon = substr($razon, 0, 100);
		$razon = str_replace("&", "&amp;", $razon);
		
		// obtenemos el total descuento desde los items
		$strAde = "";
		$totalDscto = $totalGra = $totalIna = $totalExo = $sumaIgv = $totalOferta = 0;
		if( ! empty($det)) {
			foreach($det as $i=>$row) {
				if($row["codgrupo_igv"] == "GRA")
					$totalGra += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "EXO")
					$totalExo += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "INA")
					$totalIna += $row["valor_venta"];
				
				$sumaIgv += $row["sum_igv"];
				$totalDscto += $row["sum_dscto"];
				
				if($row["oferta"] == "S") {
					$strAde .= number_format($row["pu_real"], 2, '.', '')."|-\r\n";
					$totalOferta += $row["pu_real"] * $row["cantidad"];
				}
				else
					$strAde .= "0.00|-\r\n";
			}
		}
		// $importeTotal = $cab["total"];
		$importeTotal = $totalGra + $totalIna + $totalExo + $sumaIgv;
		
		$linea = array();
		$linea[] = $cab["top"]; // tipo operacion (Anexo 8 Catalogo 17, venta interna)							1
		$linea[] = $cab["fecha"]; // fecha emision																2
		$linea[] = ""; // codigo domicilio fiscal o de local anexo del emisor(N3)								3
		$linea[] = $tdoc; // tipo doc cliente (DNI o RUC)														4
		$linea[] = $ruc_dni; // numero doc cliente (numero DNI o RUC)											5
		$linea[] = $razon; // nombre cliente, razon social														6
		$linea[] = $cab["moneda"]; // moneda (PEN,USD,...)														7
		$linea[] = "0.00"; // descuentos globales																8
		$linea[] = "0.00"; // sum otros cargos																	9
		$linea[] = number_format($totalDscto,2,'.',''); // total descuentos										10
		$linea[] = number_format($totalGra,2,'.',''); // total valor venta - operaciones gravadas				11
		$linea[] = number_format($totalIna,2,".",""); // total valor venta - operaciones inafectas				12
		$linea[] = number_format($totalExo,2,'.',''); // total valor venta - operaciones exoneradas				13
		$linea[] = number_format($sumaIgv,2,".",""); // sum igv													14
		$linea[] = "0.00"; // sum isc (impuesto selectivo al consumo)											15
		$linea[] = "0.00"; // sum otros tributos																16
		$linea[] = number_format($importeTotal,2,".",""); // importe total venta								17
		
		// guardamos el archivo cabecera
		$file = $this->save_file($cab["tdoc"], $serie, $numero, "CAB", implode("|", $linea));
		
		// armamos el archivo detalle
		$this->make_file_det($cab["tdoc"], $serie, $numero, $det);
		
		// verificamos si la venta ha tenido ofertas
		if($totalOferta > 0 && $strAde != "") {
			// creamos los archivos adicionales de cabecera y detalles
			$strAca = "|0.00|0.00|0.00|".number_format($totalOferta, 2, ".", "")."|0.00|||||||".date("Y-m-d");
			$this->save_file($cab["tdoc"], $serie, $numero, "ACA", $strAca);
			
			// creamos el archivo adicionales de detalle
			$this->save_file($cab["tdoc"], $serie, $numero, "ADE", $strAde);
		}
		
		// creamos archivo leyenda
		$this->make_file_ley($cab["tdoc"], $serie, $numero, $importeTotal);
		
		$res["tipo_doc"] = $cab["tdoc"];
		$res["tip_docu"] = $cab["tdoc"];
		$res["fec_carg"] = date("d/m/Y");
		$res["ind_situ"] = "01";
		$res["serie"] = $serie;
		$res["numero"] = $numero;
		$res["num_docu"] = $serie."-".$numero;
		$res["archivo"] = basename($file, ".CAB");
		$res["ruta"] = dirname($file);
		$res["tip_docu_cliente"] = $tdoc;
		$res["num_docu_cliente"] = $ruc_dni;
		$res["razon_cliente"] = $razon;
		$res["direccion_cliente"] = $cab["direccion"];
		$res["gravado"] = $totalGra;
		$res["inafecto"] = $totalIna;
		$res["exonerado"] = $totalExo;
		$res["suma_igv"] = $sumaIgv;
		$res["suma_descuento"] = $totalDscto;
		$res["descuento_global"] = 0;
		$res["gratuito"] = $totalOferta;
		
		return $res;
	}
	
	public function make_file_nota($cab, $det) {
		// datos para el archivo
		$serie = $cab["serie"];
		if(is_numeric($serie)) {
			$serie = intval($serie);
			$serie = str_pad($serie, 4, "0", STR_PAD_LEFT);
		}
		$numero = $cab["numero"];
		if(is_numeric($numero)) {
			$numero = intval($numero);
			$numero = str_pad($numero, $this->pad_length, "0", STR_PAD_LEFT);
		}
		
		// datos del contenido del archivo cabecera
		$serie_ref = $cab["serie_ref"];
		if(is_numeric($serie_ref)) {
			$serie_ref = intval($serie_ref);
			$serie_ref = str_pad($serie_ref, 4, "0", STR_PAD_LEFT);
		}
		$numero_ref = $cab["numero_ref"];
		if(is_numeric($numero_ref)) {
			$numero_ref = intval($numero_ref);
			$numero_ref = str_pad($numero_ref, 8, "0", STR_PAD_LEFT);
		}
		
		// datos del contenido del archivo cabecera
		/* $tdoc = $ruc_dni = "-";
		$razon = trim($cab["razon"]);
		if($cab["tdoc_ref"] == "01") { // modifica una 
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
			$razon = trim($cab["razon"]);
		}
		else if(!empty($cab["idcliente"]) && $cab["idcliente"] != 0 && !empty($cab["dni"])) {
			$tdoc = 1;
			$ruc_dni = $cab["dni"];
			$razon = trim($cab["razon"]);
		}
		
		if($tdoc == "-" && $ruc_dni == "-") { // fix, no deberia ser asi, consutar en SUNAT
			$tdoc = 1;
			$ruc_dni = "00000000";
		} */
		
		$tdoc = $ruc_dni = "-";
		$razon = trim($cab["razon"]);
		
		if($cab["tdoc_ref"] == "01") { // modifica una 
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
		}
		else if( ! empty($cab["dni"])) {
			$tdoc = 1;
			$ruc_dni = $cab["dni"];
		}
		else if( ! empty($cab["ruc"])) {
			$tdoc = 6;
			$ruc_dni = $cab["ruc"];
		}
		else {
			$tdoc = ($cab["tdoc_ref"] == "01") ? 6 : 1;
			$ruc_dni = ($tdoc == 6) ? str_repeat("0", 11) : str_repeat("0", 8);
			$razon = "VARIOS";
		}
		
		if(strlen($razon) > 100)
			$razon = substr($razon, 0, 100);
		$razon = str_replace("&", "&amp;", $razon);
		
		$motivo = $cab["motivo"];
		if(strlen($motivo) > 250)
			$motivo = substr($motivo, 0, 250);
		$motivo = str_replace("|","/",$motivo);
		
		// obtenemos el total descuento desde los items
		$totalGra = $totalIna = $totalExo = $sumaIgv = 0;
		if( ! empty($det)) {
			foreach($det as $i=>$row) {
				if($row["codgrupo_igv"] == "GRA")
					$totalGra += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "EXO")
					$totalExo += $row["valor_venta"];
				else if($row["codgrupo_igv"] == "INA")
					$totalIna += $row["valor_venta"];
				
				$sumaIgv += $row["sum_igv"];
			}
		}
		// $importeTotal = $cab["total"];
		$importeTotal = $totalGra + $totalIna + $totalExo + $sumaIgv;
		
		$linea = array();
		$linea[] = $cab["fecha"]; // fecha emision																1
		$linea[] = $cab["top"]; // tipo nota (rev catalog 9 - 10)												2
		$linea[] = $motivo; // motivo o sustento																3
		$linea[] = $cab["tdoc_ref"]; // tipo doc que modifica													4
		$linea[] = $serie_ref.'-'.$numero_ref; // nro doc que modifica											5
		$linea[] = $tdoc; // tipo doc cliente (DNI o RUC)														6
		$linea[] = $ruc_dni; // numero doc cliente (numero DNI o RUC)											7
		$linea[] = $razon; // nombre cliente, razon social														8
		$linea[] = $cab["moneda"]; // moneda (PEN,USD,...)														9
		$linea[] = "0.00"; // sum otros cargos																	10
		$linea[] = number_format($totalGra,2,'.',''); // total valor venta - operaciones gravadas				11
		$linea[] = number_format($totalIna,2,".",""); // total valor venta - operaciones inafectas				12
		$linea[] = number_format($totalExo,2,'.',''); // total valor venta - operaciones exoneradas				13
		$linea[] = number_format($sumaIgv,2,".",""); // sum igv													14
		$linea[] = "0.00"; // sum isc (impuesto selectivo al consumo)											15
		$linea[] = "0.00"; // sum otros tributos																16
		$linea[] = number_format($importeTotal,2,".",""); // importe total venta								17
		
		// guardamos el archivo cabecera
		$file = $this->save_file($cab["tdoc"], $serie, $numero, "NOT", implode("|", $linea));
		
		// armamos el archivo detalle
		$this->make_file_det($cab["tdoc"], $serie, $numero, $det);
		
		// creamos archivo leyenda
		$this->make_file_ley($cab["tdoc"], $serie, $numero, $cab["total"]);
		
		$res["tipo_doc"] = $cab["tdoc"];
		$res["tip_docu"] = $cab["tdoc"];
		$res["fec_carg"] = date("d/m/Y");
		$res["ind_situ"] = "01";
		$res["serie"] = $serie;
		$res["numero"] = $numero;
		$res["num_docu"] = $serie."-".$numero;
		$res["archivo"] = basename($file, ".NOT");
		$res["ruta"] = dirname($file);
		$res["tip_docu_cliente"] = $tdoc;
		$res["num_docu_cliente"] = $ruc_dni;
		$res["razon_cliente"] = $razon;
		$res["direccion_cliente"] = $cab["direccion"];
		$res["gravado"] = $totalGra;
		$res["inafecto"] = $totalIna;
		$res["exonerado"] = $totalExo;
		$res["suma_igv"] = $sumaIgv;
		$res["suma_descuento"] = 0;
		$res["descuento_global"] = 0;
		$res["gratuito"] = 0;
		return $res;
	}
	
	public function make_file_baja($cab, $det) {
		// datos para el archivo
		$tdoc = "RA";
		$serie = str_replace("-", "", $cab["fecha"]);
		$numero = str_pad($cab["correlativo"], 3, "0", STR_PAD_LEFT);
		
		// datos del contenido del archivo cabecera
		$fec_gen = fecha_en($cab["fec_gene"]); // var: dd/mm/yyyy hh:ii:ss
		
		$motivo = $cab["motivo"];
		if(strlen($motivo) > 100)
			$motivo = substr($motivo, 0, 100);
		
		$linea = array();
		$linea[] = $fec_gen; // fecha generacion del doc de baja				1
		$linea[] = $cab["fecha"]; // fecha comunicacion							2
		$linea[] = $cab["tip_docu"]; // tipo doc baja							3
		$linea[] = $cab["num_docu"]; // num doc baja							4
		$linea[] = $motivo; // motivo baja										5
		
		// guardamos el archivo cabecera
		$file = $this->save_file($tdoc, $serie, $numero, "CBA", implode("|", $linea));
		
		$res["tipo_doc"] = $tdoc;
		$res["tip_docu"] = $tdoc;
		$res["fec_carg"] = date("d/m/Y");
		$res["ind_situ"] = "01";
		$res["serie"] = $serie;
		$res["numero"] = $numero;
		$res["archivo"] = basename($file, ".CBA");
		$res["ruta"] = dirname($file);
		return $res;
	}
	
	public function make_file_resumen($cab, $det) {
		// datos para el archivo
		$tdoc = "RC";
		$serie = str_replace("-", "", $cab["fecha"]);
		$numero = str_pad($cab["correlativo"], 3, "0", STR_PAD_LEFT);
		
		$content = "";
		if( ! empty($det)) {
			foreach($det as $row) {
				$linea = array();
				$linea[] = $row["fecha"]; // fecha generacion documento											1
				$linea[] = $cab["fecha"]; // fecha generacion resumen											2
				$linea[] = $row["tip_docu"]; // tipo documento													3
				$linea[] = $row["num_docu"]; // numero documento (xxxx-12345678)								4
				$linea[] = $row["tip_docu_cliente"]; // tipo doc cliente (DNI, RUC)								5
				$linea[] = $row["num_docu_cliente"]; // nro doc cliente											6
				$linea[] = "PEN"; // moneda (solo acepta SOLES)													7
				$linea[] = number_format($row["gravado"],2,".",""); // total gravado							8
				$linea[] = number_format($row["exonerado"],2,".",""); // total exonerado						9
				$linea[] = number_format($row["inafecto"],2,".",""); // total inafecto							10
				$linea[] = number_format($row["gratuito"],2,".",""); // total gratuito							11
				$linea[] = "0.00"; // sumatoria otros cargos													12
				$linea[] = "0.00"; // total ISC																	13
				$linea[] = number_format($row["suma_igv"],2,".",""); // total IGV								14
				$linea[] = "0.00"; // total otros tributos														15
				$linea[] = number_format($row["total"],2,".",""); // importe total venta						16
				$linea[] = $row["tip_docu_modifica"]; // tipo documento modifica								17
				$linea[] = $row["serie_docu_modifica"]; // serie documento modifica								18
				$linea[] = $row["nro_docu_modifica"]; // numero documento modifica								19
				$linea[] = ""; // regimen percepcion															20
				$linea[] = ""; // porcentaje percepcion														21
				$linea[] = ""; // base imponible percepcion													22
				$linea[] = ""; // monto percepcion															23
				$linea[] = ""; // monto total cobrar (incluye percepcion)									24
				$linea[] = $row["estado_docu"]; // estado (1:nuevo, 2:edit, 3:anular)							25
				
				$content .= implode("|", $linea);
				$content .= "\r\n";
			}
		}
		
		// guardamos el archivo detalle
		$file = $this->save_file($tdoc, $serie, $numero, "RDI", $content);
		
		$res["tipo_doc"] = $tdoc;
		$res["tip_docu"] = $tdoc;
		$res["fec_carg"] = date("d/m/Y");
		$res["ind_situ"] = "01";
		$res["serie"] = $serie;
		$res["numero"] = $numero;
		$res["num_docu"] = $serie."-".$numero;
		$res["archivo"] = basename($file, ".RDI");
		$res["ruta"] = dirname($file);
		return $res;
	}
	
	public function make_file_guia_remision($cab, $det) {
		$ext = "GRR";
		
		$serie = $cab["serie"];
		if(is_numeric($serie)) {
			$serie = intval($serie);
			$serie = str_pad($serie, 4, "0", STR_PAD_LEFT);
		}
		$numero = $cab["numero"];
		if(is_numeric($numero)) {
			$numero = intval($numero);
			$numero = str_pad($numero, $this->pad_length, "0", STR_PAD_LEFT);
		}
		
		$file = $this->save_file($cab["tdoc"], $serie, $numero, $ext, "");
		
		$res["tipo_doc"] = $cab["tdoc"];
		$res["tip_docu"] = $cab["tdoc"];
		$res["fec_carg"] = date("d/m/Y");
		$res["ind_situ"] = "01";
		$res["serie"] = $serie;
		$res["numero"] = $numero;
		$res["num_docu"] = $serie."-".$numero;
		$res["archivo"] = basename($file, ".{$ext}");
		$res["ruta"] = dirname($file);
		return $res;
	}
	
	public function is_ref($ref) {
		return array_key_exists($ref, $this->config);
	}
	
	public function get_ref_model($ref=false, $key="model") {
		if($ref === false && isset($this->ci->facturacion))
			$ref = $this->ci->facturacion->get("referencia");
		if($this->is_ref($ref))
			return $this->config[$ref][$key];
		return false;
	}
	
	public function get_ref_pkey($ref=false) {
		return $this->get_ref_model($ref, "pkey");
	}
	
	public function crear_files() {
		if( ! isset($this->ci->facturacion))
			return false;
		
		$this->load_ruc();
		
		$ref = $this->ci->facturacion->get("referencia");
		$id = $this->ci->facturacion->get("idreferencia");
		$res = false;
		
		if($ref == "venta") {
			$data = $this->get_data_venta($id);
			$res = $this->make_file_venta($data["cab"], $data["det"]);
		}
		else if($ref == "notacredito") {
			$data = $this->get_data_nota_credito($id);
			$res = $this->make_file_nota($data["cab"], $data["det"]);
		}
		else if($ref == "notadebito") {
			$data = $this->get_data_nota_debito($id);
			$res = $this->make_file_nota($data["cab"], $data["det"]);
		}
		else if($ref == "documento_baja") {
			$data = $this->get_data_baja($id);
			$res = $this->make_file_baja($data["cab"], $data["det"]);
		}
		else if($ref == "resumen_diario") {
			$data = $this->get_data_resumen($id);
			$res = $this->make_file_resumen($data["cab"], $data["det"]);
		}
		else if($ref == "guiaremision") {
			$data = $this->get_data_guia_remision($id);
			$res = $this->make_file_guia_remision($data["cab"], $data["det"]);
		}
		
		if( ! empty($res)) {
			$this->ci->facturacion->set($res);
			$this->ci->facturacion->update();
		}
		
		return $this->ci->facturacion->get_fields();
	}
	
	public function enviar_files() {
		if( ! isset($this->ci->facturacion))
			return false;
		
		$data = $this->ci->facturacion->get_fields();
		
		$file = $data["archivo"];
		if($data["tipo_doc"] == "RA") {
			$ext = "CBA";
		}
		else if($data["tipo_doc"] == "RC") { // resumen diario
			$ext = "RDI";
		}
		else if($data["tipo_doc"] == "07" || $data["tipo_doc"] == "08") { // nota debito o credito
			$ext = "NOT";
		}
		else {
			$ext = "CAB";
		}
		
		$temp = $this->path."/temp".date("His").".zip";
		
		// creamos el archivo zip
		$zip_file = new ZipArchive();
		$zip_file->open($temp, ZipArchive::CREATE);
		
		// adjuntamos los archivos al zip
		if(file_exists($data["ruta"]."/{$file}.{$ext}"))
			$zip_file->addFile($data["ruta"]."/{$file}.{$ext}", "{$file}.{$ext}");
		if(file_exists($data["ruta"]."/{$file}.DET"))
			$zip_file->addFile($data["ruta"]."/{$file}.DET", "{$file}.DET");
		if(file_exists($data["ruta"]."/{$file}.LEY"))
			$zip_file->addFile($data["ruta"]."/{$file}.LEY", "{$file}.LEY");
		
		if($ext == "CAB") {
			if(file_exists($data["ruta"]."/{$file}.ACA"))
				$zip_file->addFile($data["ruta"]."/{$file}.ACA", "{$file}.ACA");
			if(file_exists($data["ruta"]."/{$file}.ADE"))
				$zip_file->addFile($data["ruta"]."/{$file}.ADE", "{$file}.ADE");
		}
		
		$zip_file->close();
		
		// obtenemos el contenido del zip
		$zip_content = base64_encode(file_get_contents($temp));
		
		// eliminamos el archivo temporal
		unlink($temp);
		
		// parametros para el envio del zip al facturador
		$param["tipo_doc"] = $data["tipo_doc"];
		$param["archivo"] = $data["archivo"];
		$param["contenido_zip"] = $zip_content;
		$param["fecha"] = date("d/m/Y");
		
		// invocamos el servicio web
		$res = call($this->url_webservice, "add_file", $param);
		if($res !== false && $res != "failed") {
			$arr = json_decode($res, true);
			if($arr != null) {
				$this->ci->facturacion->set($arr);
				$this->ci->facturacion->update();
			}
		}
		
		return $this->ci->facturacion->get_fields();
	}
	
	public function load($ref, $id) {
		if( ! $this->is_ref($ref))
			return false;
		
		if( ! isset($this->ci->facturacion))
			$this->ci->load_model("venta.facturacion");
		
		$this->ci->facturacion->text_uppercase(false);
		$this->ci->facturacion->find(array("idreferencia"=>$id, "referencia"=>$ref));
		return $this->ci->facturacion->get_fields();
	}
	
	public function update($d) {
		if(isset($this->ci->facturacion)) {
			if($this->ci->facturacion->get("idreferencia") == $d["idreferencia"] && $this->ci->facturacion->get("referencia") == $d["referencia"]) {
				$this->ci->facturacion->set($d);
				$this->ci->facturacion->update();
			}
		}
	}
	
	public function crear_comprobante($ref=false, $id=false) {
		if($ref !== false && $id !== false) {
			if( ! $this->is_ref($ref))
				return false;
			
			$this->load($ref, $id);
		}
		else if( ! isset($this->ci->facturacion)) {
			return false;
		}
		
		// parametros de la funcion
		$param["hddNumRuc"] = $this->ci->facturacion->get("num_ruc");
		$param["hddTipDoc"] = $this->ci->facturacion->get("tip_docu");
		$param["hddNumDoc"] = $this->ci->facturacion->get("num_docu");
		$param["hddNomArc"] = $this->ci->facturacion->get("nom_arch");
		$param["hddEstArc"] = $this->ci->facturacion->get("ind_situ");
		
		// invocamos el servicio web
		$res = call($this->url_webservice, "build_comprobante", $param);
		if($res !== false && $res != "failed") {
			$arr = json_decode($res, true);
			if($arr != null) {
				$this->ci->facturacion->set($arr);
				$this->ci->facturacion->update();
			}
		}
		
		return $this->ci->facturacion->get_fields();
	}
	
	public function get_estado($ref=false, $id=false) {
		if($ref !== false && $id !== false) {
			if( ! $this->is_ref($ref))
				return false;
			
			$this->load($ref, $id);
		}
		else if( ! isset($this->ci->facturacion)) {
			return false;
		}
		
		$res = call($this->url_webservice, "get_estado", array("hddNomArc"=>$this->ci->facturacion->get("nom_arch")));
		if($res !== false && $res != "failed") {
			$arr = json_decode($res, true);
			if($arr != null) {
				$this->ci->facturacion->set($arr);
				$this->ci->facturacion->update();
			}
		}
		
		return $this->ci->facturacion->get_fields();
	}
	
	public function enviar_comprobante($ref=false, $id=false) {
		if($ref !== false && $id !== false) {
			if( ! $this->is_ref($ref))
				return false;
			
			$this->load($ref, $id);
		}
		else if( ! isset($this->ci->facturacion)) {
			return false;
		}
		
		// parametros de la funcion
		$param["hddNumRuc"] = $this->ci->facturacion->get("num_ruc");
		$param["hddTipDoc"] = $this->ci->facturacion->get("tip_docu");
		$param["hddNumDoc"] = $this->ci->facturacion->get("num_docu");
		$param["hddNomArc"] = $this->ci->facturacion->get("nom_arch");
		$param["hddEstArc"] = $this->ci->facturacion->get("ind_situ");
		
		// invocamos el servicio web
		$res = call($this->url_webservice, "send_comprobante", $param);
		if($res !== false && $res != "failed") {
			$arr = json_decode($res, true);
			if($arr != null) {
				$this->ci->facturacion->set($arr);
				$this->ci->facturacion->update();
			}
		}
		
		return $this->ci->facturacion->get_fields();
	}
	
	public function delete($nom_arch) {
		// invocamos el servicio web
		return call($this->url_webservice, "delete", array("hddNomArc"=>$nom_arch));
	}
	
	public function get_xml($nom_arch) {
		// invocamos el servicio web
		return call($this->url_webservice, "get_xml", array("hddNomArc"=>$nom_arch));
	}
	
	public function restablecer($nom_arch) {
		// invocamos el servicio web
		return call($this->url_webservice, "restore_default", array("hddNomArc"=>$nom_arch));
	}
	
	protected function get_nom_arch() {
		$d = $this->ci->facturacion->get_fields();
		if( ! empty($d["nom_arch"]))
			return $d["nom_arch"];
		return $d["num_ruc"]."-".$d["tipo_doc"]."-".$d["serie"]."-".$d["numero"];
	}
}

/* End of file JFacturacion.php */