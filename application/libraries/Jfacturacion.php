<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/libraries/Jfacturacion_sunat.php";
require_once APPPATH."/libraries/composer/gconstants.php";
require_once APPPATH."/libraries/composer/gfunctions.php";

class Jfacturacion extends Jfacturacion_sunat {
	protected $create_files = false;
	
	//@override
	public function enviar_files() {
		if( ! isset($this->ci->facturacion))
			return false;
		return $this->ci->facturacion->get_fields();
	}
	
	//@override
	public function crear_comprobante($ref=false, $id=false) {
		if($ref !== false && $id !== false) {
			if( ! $this->is_ref($ref))
				return false;
			
			$this->load($ref, $id);
		}
		else if( ! isset($this->ci->facturacion)) {
			return false;
		}
		
		$data = $this->ci->facturacion->get_fields();
		$datos_send = $this->get_format_data($data);
		// print_r($datos_send);exit;
		// parametros para el envio al servicio web
		$param["ruc"] = $this->ruc;
		$param["datos"] = json_encode($datos_send);
		$param["nom_arch"] = $this->get_nom_arch();
		
		$method = "buildFactura";
		if($data["tipo_doc"] == "RA")
			$method = "buildBaja";
		else if($data["tipo_doc"] == "RC")
			$method = "buildResumen";
		else if($data["tipo_doc"] == "07" || $data["tipo_doc"] == "08")
			$method = "buildNota";
		else if($data["tipo_doc"] == "09")
			$method = "buildGuia";
		
		// invocamos el servicio web
		$res = call_user_func_array($method, array_values($param));
		
		if($res !== false && $res != "failed") {
			$arr = json_decode($res, true);
			if($arr != null) {
				if($arr["code"] == "OK") {
					$this->ci->facturacion->set($arr["data"]);
					$this->ci->facturacion->update();
				}
			}
		}
		
		return $this->ci->facturacion->get_fields();
	}
	
	//@override
	public function enviar_comprobante($ref=false, $id=false) {
		if($ref !== false && $id !== false) {
			if( ! $this->is_ref($ref))
				return false;
			
			$this->load($ref, $id);
		}
		else if( ! isset($this->ci->facturacion)) {
			return false;
		}
		
		// generamos el pdf si aun no se ha generado
		$pdfContent = $this->ci->facturacion->get("pdf_content");
		if(empty($pdfContent)) {
			// $_REQUEST["referencia"] = $this->ci->facturacion->referencia;
			// $_REQUEST["idreferencia"] = $this->ci->facturacion->idreferencia;
			
			// ob_start();
			// include_once CONTROLLER_DIR.'/FacturacionController.class.php';
			// $obj = new FacturacionController();
			// $obj->imprimir();
			// ob_end_clean();
			
			// $this->ci->facturacion->find();
		}
		
		$param["ruc"] = $this->ci->facturacion->get("num_ruc");
		$param["datos"] = json_encode($this->ci->facturacion->get_fields());
		$param["nom_arch"] = $this->get_nom_arch();
		
		if(FE_SEND_CDP_ONLINE === true)
			$res = call($this->url_webservice, "sendXml", $param);
		else
			$res = call_user_func_array("sendXml", array_values($param));
		
		if($res !== false && $res != "failed") {
			$arr = json_decode($res, true);
			if($arr != null) {
				if($arr["code"] == "OK") {
					$this->ci->facturacion->set($arr["data"]);
					$this->ci->facturacion->update();
				}
			}
		}
		
		return $this->ci->facturacion->get_fields();
	}
	
	//@override
	public function get_estado($ref=false, $id=false) {
		if($ref !== false && $id !== false) {
			if( ! $this->is_ref($ref))
				return false;
			
			$this->load($ref, $id);
		}
		else if( ! isset($this->ci->facturacion)) {
			return false;
		}
		
		if(FE_SEND_CDP_ONLINE === true)
			$res = call($this->url_webservice, "get_estado", array("hddNomArc"=>$this->get_nom_arch()));
		else
			$res = call_user_func_array("get_estado", array($this->get_nom_arch()));
		
		if($res !== false && $res != "failed") {
			$arr = json_decode($res, true);
			if($arr != null) {
				if($arr["code"] == "OK") {
					$this->ci->facturacion->set($arr["data"]);
					$this->ci->facturacion->update();
				}
			}
		}
		
		return $this->ci->facturacion->get_fields();
	}
	
	//@override
	public function restablecer($nom_arch) {
		$sql = "UPDATE venta.facturacion 
			SET ind_situ='01', fec_gene='-', fec_envi='-', des_obse='-', resumen_value=null, 
			xml_content=null, pdf_content=null, imagen_qr=null, html_content=null
			WHERE nom_arch=?";
		$this->ci->db->query($sql, array($nom_arch));
		
		if(FE_SEND_CDP_ONLINE === true) {
			$res = call($this->url_webservice, "restore", array("nom_arch"=>$nom_arch));
			if($res !== false && $res != "failed") {
				$arr = json_decode($res, true);
				if($arr != null) {
					if($arr["code"] == "OK") {
						$this->ci->facturacion->set($arr["data"]);
						$this->ci->facturacion->update();
					}
				}
			}
		}
		
		return true;
	}
	
	protected function get_data_all() {
		$ref = $this->ci->facturacion->get("referencia");
		$id = $this->ci->facturacion->get("idreferencia");
		
		if($ref == "venta") {
			return $this->get_data_venta($id);
		}
		else if($ref == "notacredito") {
			return $this->get_data_nota_credito($id);
		}
		else if($ref == "notadebito") {
			return $this->get_data_nota_debito($id);
		}
		else if($ref == "documento_baja") {
			return $this->get_data_baja($id);
		}
		else if($ref == "resumen_diario") {
			return $this->get_data_resumen($id);
		}
		else if($ref == "guiaremision") {
			return $this->get_data_guia_remision($id);
		}
	}
	
	protected function get_format_data($data) {
		$rs = $this->get_data_all();
		
		if($data["tipo_doc"] == "RA") { // comunicacion de baja
			return $this->json_comunicacion_baja($rs, $data);
		}
		else if($data["tipo_doc"] == "RC") { // resumen diario
			return $this->json_resumen_diario($rs, $data);
		}
		else if($data["tipo_doc"] == "07" || $data["tipo_doc"] == "08") { // nota debito o credito
			return $this->json_nota($rs, $data);
		}
		else if($data["tipo_doc"] == "09") { // guia remision remitente
			return $this->json_guia($rs, $data);
		}
		
		return $this->json_factura($rs, $data); // se trata de una factura o boleta
	}
	
	protected function json_comunicacion_baja($rs, $data) {
		$datos = array(
			"correlativo" => str_pad($rs["cab"]["correlativo"], 3, "0", STR_PAD_LEFT)
			,"fecGeneracion" => fecha_en($rs["cab"]["fec_gene"]) // Fecha de generacion de los documentos a dar baja
			,"fecComunicacion" => $rs["cab"]["fecha"] // Fecha de generacion de la comunicacion
			,"details" => array(
				array(
					"tipoDoc" => $rs["cab"]["tip_docu"]
					,"serie" => $rs["cab"]["serie"]
					,"correlativo" => $rs["cab"]["numero"]
					,"desMotivoBaja" => $rs["cab"]["motivo"]
				)
			)
		);
		return $datos;
	}
	
	protected function json_resumen_diario($rs, $data) {
		$items = array();
		foreach($rs["det"] as $row) {
			$obj = array(
				"tipoDoc" => $row["tip_docu"]
				,"serieNro" => $row["num_docu"] // numero documento (xxxx-12345678)
				,"clienteTipo" => $row["tip_docu_cliente"]
				,"clienteNro" => $row["num_docu_cliente"]
				,"estado" => $row["estado_docu"] // (1:nuevo, 2:edit, 3:anular) - Estado del item (catalog: 19)
				,"total" => $row["total"]
			);
			
			if(floatval($row["gravado"]) > 0)
				$obj["mtoOperGravadas"] = $row["gravado"];
			if(floatval($row["inafecto"]) > 0)
				$obj["mtoOperInafectas"] = $row["inafecto"];
			if(floatval($row["exonerado"]) > 0)
				$obj["mtoOperExoneradas"] = $row["exonerado"];
			if(floatval($row["gratuito"]) > 0)
				$obj["mtoOperGratuitas"] = $row["gratuito"];
			if(floatval($row["suma_igv"]) > 0)
				$obj["mtoIGV"] = $row["suma_igv"];
			
			if($row["tip_docu"] == "07" || $row["tip_docu"] == "08") { // nota credito o debito
				$obj["docReferencia"] = array(
					"tipoDoc"=>$row["tip_docu_modifica"]
					,"nroDoc"=>$row["serie_docu_modifica"]."-".$row["nro_docu_modifica"]
				);
			}
			$items[] = $obj;
		}
		$datos = array(
			"correlativo" => str_pad($rs["cab"]["correlativo"], 3, "0", STR_PAD_LEFT)
			,"fecGeneracion" => $rs["det"][0]["fecha"] // Fecha de generacion de los documentos a enviar en el resumen
			,"fecResumen" => $rs["cab"]["fecha"] // Fecha de generacion del resumen
			,"details" => $items
		);
		return $datos;
	}
	
	protected function json_details($arr) {
		$items = array();
		foreach($arr as $row) {
			$oferta = ($row["oferta"] == "A");
			
			$d = array(
				"unidad" => $row["unidmed"]
				,"cantidad" => $row["cantidad"]
				,"codProducto" => $row["codproducto"]
				,"descripcion" => trim($row["producto"])
				,"mtoValorUnitario" => $oferta ? 0 : $row["valor_unit"] // Monto del valor unitario (PrecioUnitario SIN IGV), cero en op. gratuitas
				,"mtoBaseIgv" => $row["valor_venta"]
				,"porcentajeIgv" => $this->porcentaje_igv
				,"igv" => $row["sum_igv"]
				,"tipAfeIgv" => $row["tipo_igv"]
				,"totalImpuestos" => $row["sum_igv"]
				,"mtoPrecioUnitario" => $oferta ? 0 : $row["precio_venta"] // Precio de venta unitario por item, cero en op. gratuitas
				,"mtoValorVenta" => $oferta ? 0 : $row["valor_venta"] // Valor de venta por item. (Total), cero en op. gratuitas
			);
			
			if($oferta)
				$d["mtoValorGratuito"] = $row["valor_unit"]; // Valor referencial unitario por item en operaciones no onerosas (gratuita)
			
			$items[] = $d;
		}
		return $items;
	}
	
	protected function json_legends($total) {
		$codeLey = "1002";
		$descLey = "TRANSFERENCIA GRATUITA";
		if($total > 0) {
			if( ! isset($this->ci->numeroletra))
				$this->ci->load->library('numeroletra');
			$codeLey = "1000";
			$descLey = trim($this->ci->numeroletra->convertir(number_format($total,2,'.',''), true));
		}
		return array("code"=>$codeLey, "value"=>$descLey);
	}
	
	protected function json_base($rs, $data) {
		$gravado = floatval($data["gravado"]);
		$inafecto = floatval($data["inafecto"]);
		$exonerado = floatval($data["exonerado"]);
		$suma_igv = floatval($data["suma_igv"]);
		
		$valorVenta = $gravado + $inafecto + $exonerado;
		
		$totalImpuestos = $suma_igv;
		$totalVenta = $valorVenta + $totalImpuestos;
		
		$totalDescuentos = 0;
		if(floatval($data["suma_descuento"]) > 0)
			$totalDescuentos += floatval($data["suma_descuento"]);
		if(floatval($data["descuento_global"]) > 0)
			$totalDescuentos += floatval($data["descuento_global"]);
		
		$datos = array(
			"tipoDoc" => $data["tipo_doc"]
			,"serie" => $data["serie"]
			,"correlativo" => $data["numero"]
			,"fechaEmision" => $rs["cab"]["fecha"]." ".$rs["cab"]["hora"]
			,"client" => array(
				"tipoDoc" => $data["tip_docu_cliente"]
				,"numDoc" => $data["num_docu_cliente"]
				,"rznSocial" => trim($rs["cab"]["razon"])
			)
			,"tipoMoneda" => $rs["cab"]["moneda"]
			,"mtoIGV" => $suma_igv
			,"totalImpuestos" => $totalImpuestos
			,"mtoImpVenta" => $totalVenta
			,"legends" => array($this->json_legends($totalVenta))
			,"details" => $this->json_details($rs["det"])
		);
		
		if($gravado > 0)
			$datos["mtoOperGravadas"] = $gravado;
		
		if($inafecto > 0)
			$datos["mtoOperInafectas"] = $inafecto;
		
		if($exonerado > 0)
			$datos["mtoOperExoneradas"] = $exonerado;
		
		if($totalDescuentos > 0)
			$datos["mtoDescuentos"] = $totalDescuentos; // sumatoria de los descuentos de cada linea + descuento global
		
		// if(floatval($data["gratuito"]) > 0)
			// $datos["mtoOperGratuitas"] = $data["gratuito"]; // updated 2019-09-10 15:52
		
		if( ! empty($rs["cab"]["direccion"]))
			$datos["client"]["address"] = array("direccion" => trim($rs["cab"]["direccion"]));
		
		return $datos;
	}
	
	protected function json_factura($rs, $data) {
		$valorVenta = floatval($data["gravado"]) + floatval($data["inafecto"]) + floatval($data["exonerado"]);
		
		$datos = $this->json_base($rs, $data);
		
		// invoice
		$datos["tipoOperacion"] = $rs["cab"]["top"]; //Catalogo 51
		$datos["valorVenta"] = empty($data["valor_venta"]) ? $valorVenta : $data["valor_venta"];
		
		if(floatval($data["descuento_global"]) > 0) {
			$dsctoGlobal = floatval($data["descuento_global"]);
			$valorVenta += $dsctoGlobal;
			$factor = $dsctoGlobal / $valorVenta;
			
			$datos["descuentos"] = array(
				"codTipo" => "02" // Descuentos globales que afectan la base imponible del IGV/IVAP
				,"montoBase" => $valorVenta
				,"factor" => $factor
				,"monto" => $data["descuento_global"]
			);
		}
		
		return $datos;
	}
	
	protected function json_guia($rs, $data) {
		$items = array();
		foreach($rs["det"] as $row) {
			$items[] = array(
				"codigo" => $row["codproducto"] // (opcional)
				,"descripcion" => $row["producto"]
				,"unidad" => $row["unidmed"]
				,"cantidad" => $row["cantidad"]
			);
		}
		
		$datos = array(
			"tipoDoc" => $data["tipo_doc"]
			,"serie" => $data["serie"]
			,"correlativo" => $data["numero"]
			,"observacion" => $rs["cab"]["observacion"]
			,"fechaEmision" => $rs["cab"]["fecha_registro"]
			,"destinatario" => array(
				"tipoDoc" => strlen($rs["cab"]["ruc_dni"]) == 11 ? 6 : 1 //(ruc|dni)
				,"numDoc" => $rs["cab"]["ruc_dni"]
				,"rznSocial" => $rs["cab"]["destinatario"]
			)
			,"envio" => array(
				"codTraslado" => $rs["cab"]["codmotivo_traslado"] // motivo traslado, catalogo 20
				,"desTraslado" => $rs["cab"]["motivo_traslado"] // descripcion motivo del traslado (opcional)
				// ,indTransbordo: false // Indicador de Transbordo Programado (opcional)
				,"modTraslado" => $rs["cab"]["codmodalidad_transporte"] // modalidad traslado, catalogo 18
				,"fecTraslado" => $rs["cab"]["fecha_traslado"]
				,"transportista" => array()
				,"partida" => array(
					"ubigueo" => $rs["cab"]["codubigeo_partida"]
					,"direccion" => $rs["cab"]["punto_partida"]
				)
				,"llegada" => array(
					"ubigueo" => $rs["cab"]["codubigeo_llegada"]
					,"direccion" => $rs["cab"]["punto_llegada"]
				)
			)
			,"details" => $items
		);
		if( ! empty($rs["cab"]["peso_total"])) {
			$datos["envio"]["pesoTotal"] = $rs["cab"]["peso_total"]; // kilos
			$datos["envio"]["undPesoTotal"] = "KGM"; // kilos
		}
		if( ! empty($rs["cab"]["bultos"]))
			$datos["envio"]["numBultos"] = $rs["cab"]["bultos"];
		
		if( ! empty($rs["cab"]["ruc_transporte"])) {
			$datos["envio"]["transportista"]["tipoDoc"] = strlen($rs["cab"]["ruc_transporte"]) == 11 ? 6 : 1; //(obligatorio si modalidad es Publico)
			$datos["envio"]["transportista"]["numDoc"] = $rs["cab"]["ruc_transporte"]; // (obligatorio si modalidad es Publico)
		}
		if( ! empty($rs["cab"]["transporte"]))
			$datos["envio"]["transportista"]["rznSocial"] = $rs["cab"]["transporte"]; // (obligatorio si modalidad es Publico)
		
		if( ! empty($rs["cab"]["marca_nroplaca"]))
			$datos["envio"]["transportista"]["placa"] = $rs["cab"]["marca_nroplaca"]; // (obligatorio si modalidad es Privado)
		if( ! empty($rs["cab"]["dni_chofer"])) {
			$datos["envio"]["transportista"]["choferTipoDoc"] = strlen($rs["cab"]["dni_chofer"]) == 11 ? 6 : 1; //(obligatorio si modalidad es Privado)
			$datos["envio"]["transportista"]["choferDoc"] = $rs["cab"]["dni_chofer"]; // (obligatorio si modalidad es Privado)
		}
		
		return $datos;
	}
	
	protected function json_nota($rs, $data) {
		$datos = $this->json_base($rs, $data);	
		
		// Greenter\Model\Sale\Note
		$datos["codMotivo"] = $rs["cab"]["tnota"];
		$datos["desMotivo"] = $rs["cab"]["tnota_desc"];
		$datos["tipDocAfectado"] = $rs["cab"]["tdoc_ref"];
		$datos["numDocfectado"] = $rs["cab"]["serie_ref"]."-".$rs["cab"]["numero_ref"];
		// $datos["mtoOperGratuitas"] = $data["gratuito"];
		
		return $datos;
	}
}

/* End of file JFacturacion.php */