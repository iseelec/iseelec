<?php 

include_once APPPATH."libraries/phpmailer/class.phpmailer.php";

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jmail {
	
	public $host = NULL;
	public $port = NULL;
	public $secure = NULL;
	public $username = NULL;
	public $password = NULL;
	
	public $from = NULL;
	public $from_name = NULL;
	public $subject = NULL;
	public $message = NULL;
	
	private $address = array();
	private $files = array();
	private $ci = NULL;
	private $mailer = NULL;
	
	public function __construct() {
		$this->_init();
	}
	
	private function _init() {
		$this->ci =& get_instance();
		
		$sql = "SELECT * FROM seguridad.mailer WHERE estado='A' and idusuario=?";
		$query = $this->ci->db->query($sql, array($this->ci->get_var_session("idusuario")));
		if($query->num_rows() > 0) {
			$array = $query->row_array();
			
			$this->host = $array["host"];
			$this->port = $array["puerto"];
			$this->secure = $array["tipo"];
			$this->username = $array["mail"];
			$this->password = $array["clave"];
		}
		
		$this->from = $this->username;
		$this->from_name = $this->ci->get_var_session("nombres")." ".$this->ci->get_var_session("appat");
	}
	
	public function eliminar($email = FALSE) {
		if($email !== FALSE) {
			if(count($this->address) > 0) {
				foreach($this->address as $k=>$v) {
					if($v == $email) {
						unset($this->address[$k]);
						return;
					}
				}
			}
		}
		
		$this->address = array();
	}
	
	public function clear_all() {
		if($this->mailer != NULL) {
			$this->mailer->clearAddresses();
			$this->mailer->clearAttachments();
		}
		$this->address = array();
		$this->files = array();
	}
	
	public function asunto($text) {
		$this->subject = $text;
	}
	
	public function mensaje($text) {
		$this->message = $text;
	}
	
	public function correo($email, $name="") {
		$this->address[] = array("email"=>$email, "name"=>$name);
	}
	
	public function archivo($file, $name) {
		$this->files[] = array("file"=>$file, "name"=>$name);
	}
	
	private function _default() {
		if($this->mailer == NULL) {
			$this->mailer = new PHPMailer();
			$this->mailer->isSMTP();
			$this->mailer->SMTPAuth = true;
			// $this->mailer->SMTPDebug = 4;
			$this->mailer->Timeout = 3600;
			$this->mailer->IsHTML(true);
			$this->mailer->CharSet = "UTF-8";
		}
		
		$this->mailer->SMTPSecure = $this->secure;
		$this->mailer->Host = $this->host;
		$this->mailer->Port = $this->port;
		$this->mailer->Username = $this->username;
		$this->mailer->Password = $this->password;
	}
	
	public function send() {
		$this->_default();
		
		$this->mailer->setFrom($this->from, $this->from_name);
		
		if(count($this->address) > 0) {
			foreach($this->address as $v) {
				$this->mailer->addAddress($v['email'], $v['name']);
			}
		}
		
		if(count($this->files) > 0) {
			foreach($this->files as $v) {
				if(@is_file($v["file"]))
					$this->mailer->addAttachment($v["file"], $v["name"]);
				else
					$this->mailer->addStringAttachment($v["file"], $v["name"]);
			}
		}
		
		$this->mailer->Subject = $this->subject;
		$this->mailer->msgHTML($this->message);
		
		if( ! $this->mailer->send()) {
			return false;
		}
		
		$this->clear_all();
		
		return true;
	}
	
	public function error() {
		if($this->mailer == NULL)
			return "";
		return $this->mailer->ErrorInfo;
	}
}

/* End of file Mail.php */