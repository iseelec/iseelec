<?php
// Required PHP version >= 5.6

use Greenter\Ws\Services\SunatEndpoints;

require_once __DIR__ . "/vendor/autoload.php";
require_once __DIR__ . "/gconstants.php";
require_once __DIR__ . "/str.class.php";

/**
 * Retornar los objetos greenter que tienen propiedades en comun
 * Address, SaleDetail, Legend, VoidedDetail, SummaryDetail
 */
function getCommonObj(array $datos, $tipoObj, $withConstruct=false, array $paramConstruct = array()) {
	$obj = false;
	if($withConstruct === true)
		$obj = getCommonObjConstruct($paramConstruct, $tipoObj);
	else if($tipoObj == "address")
		$obj = new Greenter\Model\Company\Address();
	else if($tipoObj == "saleDetail")
		$obj = new Greenter\Model\Sale\SaleDetail();
	else if($tipoObj == "legend")
		$obj = new Greenter\Model\Sale\Legend();
	else if($tipoObj == "voidedDetail")
		$obj = new Greenter\Model\Voided\VoidedDetail();
	else if($tipoObj == "summaryDetail")
		$obj = new Greenter\Model\Summary\SummaryDetail();
	else if($tipoObj == "client")
		$obj = new Greenter\Model\Client\Client();
	else if($tipoObj == "company")
		$obj = new Greenter\Model\Company\Company();
	else if($tipoObj == "invoice")
		$obj = new Greenter\Model\Sale\Invoice();
	else if($tipoObj == "voided")
		$obj = new Greenter\Model\Voided\Voided();
	else if($tipoObj == "summary")
		$obj = new Greenter\Model\Summary\Summary();
	else if($tipoObj == "note")
		$obj = new Greenter\Model\Sale\Note();
	else if($tipoObj == "despatch")
		$obj = new Greenter\Model\Despatch\Despatch();
	else if($tipoObj == "shipment")
		$obj = new Greenter\Model\Despatch\Shipment();
	else if($tipoObj == "transportist")
		$obj = new Greenter\Model\Despatch\Transportist();
	else if($tipoObj == "despatchDetail")
		$obj = new Greenter\Model\Despatch\DespatchDetail();
	
	if($obj !== false) {
		if( ! empty($datos)) {
			foreach($datos as $prop=>$value) {
				$method = "set" . \Str::studly($prop); // input:foo_bar, output:FooBar
				if(method_exists($obj, $method)) {
					if(\Str::startsWith($method, "setFec")) // es un campo de fecha
						$obj->{$method}(new DateTime($value));
					else
						$obj->{$method}($value);
				}
			}
		}
	}
	return $obj;
}
function getCommonObjConstruct(array $datos, $tipoObj) {
	if($tipoObj == "direction")
		return new Greenter\Model\Despatch\Direction(...$datos);
	return false;
}

function getClient(array $datos, $type="client") {
	$address = array();
	if(array_key_exists("address", $datos)) {
		$address = $datos["address"];
		unset($datos["address"]);
	}
	
	$obj = getCommonObj($datos, $type);
	if( ! empty($address) && method_exists($obj, "setAddress"))
		$obj->setAddress(getCommonObj($address, "address"));
	
	return $obj;
}

function getCompany($datos) {
	$inAddress = array("ubigueo","codigo_pais","departamento","provincia","distrito","urbanizacion","direccion","cod_local");
	$newDatos = array("address"=>array());
	
	foreach($datos as $prop=>$value) {
		if(in_array($prop, $inAddress))
			$newDatos["address"][$prop] = $value;
		else
			$newDatos[$prop] = $value;
	}
	
	return getClient($newDatos, "company");
}

/**
 * Obtenemos datos desde la bd
 */
function getDatosEmpresa($ruc) {
	$ci =& get_instance();
	$query = $ci->db->get_where("seguridad.empresa", array("ruc"=>$ruc, "estado"=>"A"));
	if($query->num_rows() > 0) {
		$datos = $query->row_array();
		$datos["razon_social"] = $datos["descripcion"];
		$datos["telephone"] = $datos["telefono"];
		return $datos;
	}
	return false;
}
function getDatosBandeja($nom_arch) {
	$datos = array(
		"num_ruc"=>""
		,"tip_docu"=>""
		,"num_docu"=>""
		,"fec_carg"=>date("d/m/Y")
		,"fec_gene"=>"-"
		,"fec_envi"=>"-"
		,"des_obse"=>"-"
		,"nom_arch"=>""
		,"ind_situ"=>"01"
		,"tip_arch"=>"TXT"
		,"firm_digital"=>"-"
		,"xml_content"=>""
		,"resumen_value"=>"" // valor resumen o codigo hash
		,"resumen_firma"=>""
	);
	
	list($r, $t, $s, $c) = explode("-", $nom_arch);
	
	$ci =& get_instance();
	$query = $ci->db->get_where("venta.facturacion", array("num_ruc"=>$r, "tipo_doc"=>$t, "serie"=>$s, "numero"=>$c, "estado"=>"A"));
	if($query->num_rows() > 0)
		return array_intersect_key($query->row_array(), $datos);
	return $datos;
}

/**
 * Diferentes validaciones, segun doc a generar
 */
function isValidData(array $obj) {
	if(empty($obj["tipoDoc"]))
		return "Falta el tipo documento del comprobante";
	if(empty($obj["serie"]))
		return "Falta la serie del comprobante";
	if(empty($obj["correlativo"]))
		return "Falta el correlativo del comprobante";
	if(empty($obj["details"]))
		return "Debe enviar los item del comprobante";
	if(strlen($obj["serie"]) != 4)
		return "La serie del comprobante debe tener 4 caracteres";
	if(strlen($obj["tipoMoneda"]) != 3)
		return "El tipo de moneda debe tener 3 caracteres. Consultar ISO 4217, codigo de divisas";
	
	if($obj["tipoDoc"] == TIPO_DOC_FACTURA) {
		if( ! \Str::startsWith($obj["serie"], "F"))
			return "La serie del comprobante debe empezar con la letra F";
		if($obj["client"]["tipoDoc"] != "6")
			return "El tipo documento del cliente debe ser RUC";
		if(strlen($obj["client"]["numDoc"]) != 11)
			return "El RUC del cliente debe tener 11 caracteres";
	}
	if($obj["tipoDoc"] == TIPO_DOC_BOLETA) {
		if( ! \Str::startsWith($obj["serie"], "B"))
			return "La serie del comprobante debe empezar con la letra B";
	}
	return true;
}
function isValidDataBaja(array $obj) {
	foreach($obj["details"] as $val) {
		if($val["tipoDoc"] != TIPO_DOC_FACTURA && $val["tipoDoc"] != TIPO_DOC_NOTA_CREDITO && $val["tipoDoc"] != TIPO_DOC_NOTA_DEBITO)
			return "Solo las facturas y sus notas pueden incluirse en la comunicacion de baja";
		if($val["tipoDoc"] == TIPO_DOC_NOTA_CREDITO || $val["tipoDoc"] == TIPO_DOC_NOTA_DEBITO) {
			if( ! \Str::startsWith($val["serie"], "F"))
				return "La comunicacion de baja debe contener notas que modifiquen solo facturas";
		}
	}
	return true;
}
function isValidDataResumen(array $obj) {
	foreach($obj["details"] as $val) {
		if($val["tipoDoc"] != TIPO_DOC_BOLETA && $val["tipoDoc"] != TIPO_DOC_NOTA_CREDITO && $val["tipoDoc"] != TIPO_DOC_NOTA_DEBITO)
			return "El resumen diario debe contener solo boletas o sus notas";
		if($val["tipoDoc"] == TIPO_DOC_NOTA_CREDITO || $val["tipoDoc"] == TIPO_DOC_NOTA_DEBITO) {
			if($val["docReferencia"]["tipoDoc"] != TIPO_DOC_BOLETA)
				return "El resumen diario debe contener notas que modifiquen solo boletas";
		}
	}
	return true;
}
function isValidDataNota(array $obj) {
	if(empty($obj["tipoDoc"]))
		return "Falta el tipo documento de la nota";
	if(empty($obj["serie"]))
		return "Falta la serie de la nota";
	if(empty($obj["correlativo"]))
		return "Falta el correlativo de la nota";
	if(empty($obj["details"]))
		return "Debe enviar el detalle de los item de la nota";
	if(strlen($obj["serie"]) != 4)
		return "La serie de la nota debe tener 4 caracteres";
	if(strlen($obj["tipoMoneda"]) != 3)
		return "El tipo de moneda debe tener 3 caracteres. Consultar ISO 4217, codigo de divisas";
	if($obj["tipoDoc"] != TIPO_DOC_NOTA_CREDITO && $obj["tipoDoc"] != TIPO_DOC_NOTA_DEBITO)
		return "El tipo documento debe ser ".TIPO_DOC_NOTA_CREDITO." si es nota de credito o ".TIPO_DOC_NOTA_DEBITO." si es debito";
	if( ! \Str::startsWith($obj["serie"], "F") &&  ! \Str::startsWith($obj["serie"], "B"))
		return "La serie de la nota debe empezar con F para el caso de facturas o con B para boletas";
	if($obj["tipDocAfectado"] != TIPO_DOC_FACTURA && $obj["tipDocAfectado"] != TIPO_DOC_BOLETA)
		return "Las notas deben modificar solo facturas o boletas";
	
	if($obj["tipDocAfectado"] == TIPO_DOC_FACTURA) {
		if( ! \Str::startsWith($obj["numDocfectado"], "F"))
			return "La serie del comprobante ha modificar debe empezar con la letra F";
		if( ! \Str::startsWith($obj["serie"], "F"))
			return "La serie de la nota debe empezar con la letra F";
		if($obj["client"]["tipoDoc"] != "6")
			return "El tipo documento del cliente debe ser RUC";
		if(strlen($obj["client"]["numDoc"]) != 11)
			return "El RUC del cliente debe tener 11 caracteres";
	}
	if($obj["tipDocAfectado"] == TIPO_DOC_BOLETA) {
		if( ! \Str::startsWith($obj["numDocfectado"], "B"))
			return "La serie del comprobante ha modificar debe empezar con la letra B";
		if( ! \Str::startsWith($obj["serie"], "B"))
			return "La serie de la nota debe empezar con la letra B";
	}
	return true;
}
function isValidDataGuia(array $obj) {
	if($obj["tipoDoc"] != TIPO_DOC_GUIA_REMISION)
		return "El tipo documento debe ser 09 para la guia de remision remitente";
	if( ! \Str::startsWith($obj["serie"], "T"))
		return "La serie de la guia de remision debe empezar con la letra T";
	if(empty($obj["correlativo"]))
		return "Falta el correlativo de la guia";
	if(empty($obj["details"]))
		return "Debe enviar los item de la guia";
	if(strlen($obj["serie"]) != 4)
		return "La serie de la guia debe tener 4 caracteres";
	return true;
}
function hasCertificado($datos) {
	if( ! file_exists(UPLOAD_DIR . '/certificado/' . $datos["ruc"] . '.pem'))
		return "No existe el certificado digital";
	
	if(empty($datos["usuario_sol"]) || empty($datos["clave_sol"]))
		return "Registre el usuario y la clave sol";
	
	return true;
}

/***
 * Estableciendo algunos datos por default
 */
function setDefaultData(array &$obj) {
	$defaults = array(
		"ublVersion" => "2.1"
		,"fechaEmision" => date("Y-m-d")
		,"tipoMoneda" => "PEN"
		,"tipoOperacion" => "0101" // venta interna - catalogo 51
		,"client" => array("tipoDoc"=>"1", "numDoc"=>"00000000", "rznSocial"=>"VARIOS")
	);
	
	foreach($defaults as $prop=>$value) {
		if(empty($obj[$prop]))
			$obj[$prop] = $value;
	}
	
	if($obj["ublVersion"] != "2.0" && $obj["ublVersion"] != "2.1")
		$obj["ublVersion"] = "2.1";
}

/**
 * Otras funciones
 */
function getSee($datos, $tipo_doc = false) {
	$produccion = ($datos["send_to_produccion"] == "S");
	$ws_url = false;
	
	if($tipo_doc !== false) {
		if($tipo_doc == TIPO_DOC_GUIA_REMISION)
			$ws_url = $produccion ? Greenter\Ws\Services\SunatEndpoints::GUIA_PRODUCCION : Greenter\Ws\Services\SunatEndpoints::GUIA_BETA;
	}
	
	if($ws_url === false)
		$ws_url = $produccion ? Greenter\Ws\Services\SunatEndpoints::FE_PRODUCCION : Greenter\Ws\Services\SunatEndpoints::FE_BETA;
	
	$see = new Greenter\See();
	$see->setService($ws_url);
	$see->setCertificate(file_get_contents(UPLOAD_DIR . '/certificado/' . $datos["ruc"] . '.pem'));
	$see->setCredentials($datos["ruc"] . $datos["usuario_sol"], $datos["clave_sol"]);
	$see->setCachePath(FACTURACION_DIR);
	return $see;
}
function error($msg, $nom_arch="0") {
	// $res = getDatosBandeja($nom_arch);
	// $res["ind_situ"] = "06";
	$res["des_obse"] = $msg;
	return json_encode(array("code"=>"OK", "data"=>$res));
}
function success($invoice, $xml, $nom_arch) {
	list($ruc, $tip_docu, $serie, $correlativo) = explode("-", $invoice->getName());
	
	$res = getDatosBandeja($nom_arch);
	$res["num_ruc"] = $ruc;
	$res["tip_docu"] = $tip_docu;
	$res["num_docu"] = $serie . "-" . $correlativo;
	$res["fec_gene"] = date("d/m/Y H:i:s");
	$res["des_obse"] = "Xml Generado";
	$res["nom_arch"] = $invoice->getName();
	$res["ind_situ"] = "02";
	$res["xml_content"] = base64_encode($xml);
	$res["resumen_value"] = (new \Greenter\Report\XmlUtils())->getHashSign($xml);
	return json_encode(array("code"=>"OK", "data"=>$res));
}

/**
 * Funciones para generar el xml de los comprobantes
 */
function buildXml($ruc, $datos, $nom_arch, $tipo="invoice") {
	$datosEmpresa = getDatosEmpresa($ruc);
	if($datosEmpresa === false)
		return error("El RUC del emisor no se encuentra registrado", $nom_arch);
	
	$bool = hasCertificado($datosEmpresa);
	if($bool !== true)
		return error($bool, $nom_arch);
	
	$datosInvoice = json_decode($datos, true);
	
	setDefaultData($datosInvoice);
	
	$bool = ($tipo == "invoice") ? isValidData($datosInvoice) : isValidDataNota($datosInvoice);
	if($bool !== true)
		return error($bool, $nom_arch);
	
	$datosClient = $datosInvoice["client"];
	$datosDetails = $datosInvoice["details"];
	$datosLegends = $datosInvoice["legends"];
	
	unset($datosInvoice["client"]);
	unset($datosInvoice["details"]);
	unset($datosInvoice["legends"]);
	
	$invoice = getCommonObj($datosInvoice, $tipo);
	$invoice->setCompany(getCompany($datosEmpresa)); // asignamos la empresa
	$invoice->setClient(getClient($datosClient)); // asignamos el cliente
	
	// detalle
	$details = array();
	foreach($datosDetails as $value) {
		$details[] = getCommonObj($value, "saleDetail");
	}
	$invoice->setDetails($details);
	
	// leyendas
	$leyends = array();
	foreach($datosLegends as $value) {
		$leyends[] = getCommonObj($value, "legend");
	}
	$invoice->setLegends($leyends);
	
	$see = getSee($datosEmpresa); // bool false para hacer pruebas
	$xml = $see->getXmlSigned($invoice); // generar el xml firmado
	
	return success($invoice, $xml, $nom_arch);
}
function buildFactura($ruc, $datos, $nom_arch) {
	return buildXml($ruc, $datos, $nom_arch, "invoice");
}
function buildNota($ruc, $datos, $nom_arch) {
	return buildXml($ruc, $datos, $nom_arch, "note");
}
function buildBaja($ruc, $datos, $nom_arch) {
	$datosEmpresa = getDatosEmpresa($ruc);
	if($datosEmpresa === false)
		return error("El RUC del emisor no se encuentra registrado", $nom_arch);
	
	$bool = hasCertificado($datosEmpresa);
	if($bool !== true)
		return error($bool, $nom_arch);
	
	$datosInvoice = json_decode($datos, true);
	
	$bool = isValidDataBaja($datosInvoice);
	if($bool !== true)
		return error($bool, $nom_arch);
	
	$datosDetails = $datosInvoice["details"];
	unset($datosInvoice["details"]);
	
	$invoice = getCommonObj($datosInvoice, "voided");
	$invoice->setCompany(getCompany($datosEmpresa));
	
	// detalle
	$details = array();
	foreach($datosDetails as $value) {
		$details[] = getCommonObj($value, "voidedDetail");
	}
	$invoice->setDetails($details);
	
	$see = getSee($datosEmpresa); // bool false para hacer pruebas
	$xml = $see->getXmlSigned($invoice); // generar el xml firmado
	
	return success($invoice, $xml, $nom_arch);
}
function buildResumen($ruc, $datos, $nom_arch) {
	$datosEmpresa = getDatosEmpresa($ruc);
	if($datosEmpresa === false)
		return error("El RUC del emisor no se encuentra registrado", $nom_arch);
	
	$bool = hasCertificado($datosEmpresa);
	if($bool !== true)
		return error($bool, $nom_arch);
	
	$datosInvoice = json_decode($datos, true);
	
	$bool = isValidDataResumen($datosInvoice);
	if($bool !== true)
		return error($bool, $nom_arch);
	
	$datosDetails = $datosInvoice["details"];
	unset($datosInvoice["details"]);
	
	$invoice = getCommonObj($datosInvoice, "summary");
	$invoice->setCompany(getCompany($datosEmpresa));
	
	// detalle
	$details = array();
	foreach($datosDetails as $value) {
		$details[] = getCommonObj($value, "summaryDetail");
	}
	$invoice->setDetails($details);
	
	$see = getSee($datosEmpresa); // bool false para hacer pruebas
	$xml = $see->getXmlSigned($invoice); // generar el xml firmado
	
	return success($invoice, $xml, $nom_arch);
}
function buildGuia($ruc, $datos, $nom_arch) {
	$datosEmpresa = getDatosEmpresa($ruc);
	if($datosEmpresa === false)
		return error("El RUC del emisor no se encuentra registrado", $nom_arch);
	
	$bool = hasCertificado($datosEmpresa);
	if($bool !== true)
		return error($bool, $nom_arch);
	
	$datosInvoice = json_decode($datos, true);
	
	$bool = isValidDataGuia($datosInvoice);
	if($bool !== true)
		return error($bool, $nom_arch);
	
	$datosClient = $datosInvoice["destinatario"];
	$datosEnvio = $datosInvoice["envio"];
	$datosDetails = $datosInvoice["details"];
	
	unset($datosInvoice["destinatario"]);
	unset($datosInvoice["details"]);
	unset($datosInvoice["envio"]);
	
	$invoice = getCommonObj($datosInvoice, "despatch");
	$invoice->setCompany(getCompany($datosEmpresa)); // asignamos la empresa
	$invoice->setDestinatario(getClient($datosClient)); // asignamos el destinatario
	
	// datos del envio
	$datosTransportista = $datosEnvio["transportista"];
	$datosPartida = $datosEnvio["partida"];
	$datosLlegada = $datosEnvio["llegada"];
	
	unset($datosEnvio["transportista"]);
	unset($datosEnvio["partida"]);
	unset($datosEnvio["llegada"]);
	
	$shipment = getCommonObj($datosEnvio, "shipment");
	$shipment->setTransportista(getCommonObj($datosTransportista, "transportist"));
	$shipment->setPartida(getCommonObj(array(), "direction", true, array($datosPartida["ubigueo"], $datosPartida["direccion"])));
	$shipment->setLlegada(getCommonObj(array(), "direction", true, array($datosLlegada["ubigueo"], $datosLlegada["direccion"])));
	
	$invoice->setEnvio($shipment);
	
	// detalle
	$details = array();
	foreach($datosDetails as $value) {
		$details[] = getCommonObj($value, "despatchDetail");
	}
	$invoice->setDetails($details);
	
	$see = getSee($datosEmpresa, TIPO_DOC_GUIA_REMISION);
	$xml = $see->getXmlSigned($invoice); // generar el xml firmado
	
	return success($invoice, $xml, $nom_arch);
}

/**
 * Del 0100 al 1999 Excepciones, documento no registrado corregir y enviar
 * Del 2000 al 3999 Errores que generan rechazo, documento registrado y anulado generar uno nuevo
 * Del 4000 en adelante Observaciones, documento registrado como valido
 */
function getSituacion($res, $tipoDoc, $see=null) {
	$des_obse = "";
	$ind_situ = "04";
	
	if($res->isSuccess()) {
		if($tipoDoc == TIPO_DOC_BAJA || $tipoDoc == TIPO_DOC_RESUMEN) { // comunicacion de baja
			$ticket = $res->getTicket();
			$des_obse .= "Ticket: {$ticket} | ";
			$res = $see->getStatus($ticket);
			if( ! $res->isSuccess()) {
				$error = $res->getError();
				$des_obse .= $error->getCode() . " - " . $error->getMessage();
				return array("des_obse"=>$des_obse, "ind_situ"=>$ind_situ);
			}
		}
		
		$cdr = $res->getCdrResponse(); // Greenter\Model\Response\BillResult - Greenter\Model\Response\CdrResponse
		
		$code = intval($cdr->getCode());
		if($cdr->isAccepted()) // code===0 || code>=4000
			$ind_situ = $code === 0 ? "03" : "04";
		else
			$ind_situ = ($code >= 2000 && $code <= 3999) ? "05" : "06";
		
		$des_obse .= $cdr->getCode() . " - " . $cdr->getDescription();
		if(count($cdr->getNotes()) > 0) {
			$des_obse .= " - ";
			foreach($cdr->getNotes() as $note) {
				$des_obse .= $note . "|";
			}
		}
	}
	else {
		$error = $res->getError();
		return array("des_obse" => $error->getCode() . " - " . $error->getMessage());
	}
	
	return array("des_obse"=>$des_obse, "ind_situ"=>$ind_situ);
}

/**
 * Funcion para enviar el comprobante a SUNAT
 */
function sendXml($ruc, $datos, $nom_arch) {
	$datosEmpresa = getDatosEmpresa($ruc);
	if($datosEmpresa === false)
		return error("El RUC del emisor no se encuentra registrado", $nom_arch);
	
	$bool = hasCertificado($datosEmpresa);
	if($bool !== true)
		return error($bool, $nom_arch);
	
	// $bandeja = json_decode($datos, true);
	$bandeja = getDatosBandeja($nom_arch);
	if($bandeja["tip_docu"] == "" || $bandeja["xml_content"] == "" || $bandeja["nom_arch"] == "")
		return error("El comprobante no se encuentra registrado", $nom_arch);
	
	$class_name = Greenter\Model\Sale\Invoice::class;
	if($bandeja["tip_docu"] == TIPO_DOC_BAJA)
		$class_name = Greenter\Model\Voided\Voided::class;
	else if($bandeja["tip_docu"] == TIPO_DOC_RESUMEN)
		$class_name = Greenter\Model\Summary\Summary::class;
	else if($bandeja["tip_docu"] == TIPO_DOC_NOTA_CREDITO || $bandeja["tip_docu"] == TIPO_DOC_NOTA_DEBITO)
		$class_name = Greenter\Model\Sale\Note::class;
	else if($bandeja["tip_docu"] == TIPO_DOC_GUIA_REMISION)
		$class_name = Greenter\Model\Despatch\Despatch::class;
	
	$see = getSee($datosEmpresa, $bandeja["tip_docu"]);
	$res = $see->sendXml($class_name, $bandeja["nom_arch"], base64_decode($bandeja["xml_content"])); // enviar el xml
	
	// return json_encode($res);
	
	$estado = getSituacion($res, $bandeja["tip_docu"], $see);
	$estado["fec_envi"] = date("d/m/Y H:i:s");
	
	// return json_encode(array("code"=>"OK", "data"=>array_merge($bandeja, $estado)));
	return json_encode(array("code"=>"OK", "data"=>$estado));
}

/**
 * Funcion para obtener el cdr del comprobante 
 */
function getCdr($nom_arch) {
	$bandeja = getDatosBandeja($nom_arch);
	if($bandeja["tip_docu"] == "" || $bandeja["num_ruc"] == "" || $bandeja["nom_arch"] == "")
		return error("El comprobante no existe en el sistema", $nom_arch);
	
	$datosEmpresa = getDatosEmpresa($bandeja["num_ruc"]);
	if($datosEmpresa === false)
		return error("El RUC del emisor no se encuentra registrado", $nom_arch);
	
	$produccion = ($datosEmpresa["send_to_produccion"] == "S");
	
	$ws_url = Greenter\Ws\Services\SunatEndpoints::FE_CONSULTA_CDR.'?wsdl';
	
	$ws = new Greenter\Ws\Services\SoapClient($ws_url);
    $ws->setCredentials($datosEmpresa["ruc"] . $datosEmpresa["usuario_sol"], $datosEmpresa["clave_sol"]);

    $service = new Greenter\Ws\Services\ConsultCdrService();
    $service->setClient($ws);
	
	$args = explode("-", $bandeja["nom_arch"]);
	
	$res = $service->getStatusCdr(...$args);
	
	$estado = getSituacion($res, $bandeja["tip_docu"], getSee($datosEmpresa, $bandeja["tip_docu"]));
	
	// return json_encode(array("code"=>"OK", "data"=>array_merge($bandeja, $estado)));
	return json_encode(array("code"=>"OK", "data"=>$estado));
}
function get_estado($nom_arch) {
	return getCdr($nom_arch);
}
