<?php 
defined("FACTURACION_DIR") OR define("FACTURACION_DIR", APPPATH."files");
defined("UPLOAD_DIR") OR define("UPLOAD_DIR", APPPATH);
defined("FE_SEND_CDP_ONLINE") OR define("FE_SEND_CDP_ONLINE", true); // enviar xml a sunat mediante webservice

define("TIPO_DOC_FACTURA", "01");
define("TIPO_DOC_BOLETA", "03");
define("TIPO_DOC_NOTA_CREDITO", "07");
define("TIPO_DOC_NOTA_DEBITO", "08");
define("TIPO_DOC_BAJA", "RA");
define("TIPO_DOC_RESUMEN", "RC");
define("TIPO_DOC_GUIA_REMISION", "09");
