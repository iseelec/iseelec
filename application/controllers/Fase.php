<?php

include_once "Controller.php";

class Fase extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Mantenimiento de Fases o Etapas");
		//$this->set_subtitle("Lista de Fases o Etapas");
		//$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		// $this->load_library('combobox');
		// $this->combobox->setAttr(array("id"=>"idusuario","name"=>"idusuario","class"=>"form-control","required"=>""));
		// $this->db->select('idusuario,nombres')->order_by("nombres", "asc");
		// $query = $this->db->where("estado","A")->get("seguridad.usuario");
			// $this->combobox->addItem("");
		// $this->combobox->addItem($query->result_array());
		// $data['usuarioes'] = $this->combobox->getAllItems();
		// if( isset($data["proyecto"]["idusuario"]) ) {
			// $this->combobox->setSelectedOption($data["proyecto"]["idusuario"]);
		// }
		// $data['usuario'] = $this->combobox->getObject();
		$data["controller"] = $this->controller;
		$this->css('plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox');
		$this->css("plugins/datapicker/datepicker3");
		$this->css('plugins/iCheck/custom');
		
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		$this->js('plugins/iCheck/icheck.min');
		$this->js('form/'.$this->controller.'/index');
		
		
		// $data["icons"] = $this->get_icons();
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		$this->load_model($this->controller);
		$this->load->library('datatables');

		$this->datatables->setModel($this->fase);

		$this->datatables->where('estado', '=', 'A');//

		$this->datatables->setColumns(array('descripcion','fecha_inicio','fecha_fin'));

		// columnas de la tabla, si no se envia este parametro, se muestra el
		// nombre de la columna de la tabla de la bd
		$columnasName = array(
			'Descripci&oacute;n'
			,'Fecha Inicio'
			,'Fecha Fin'
			// array('Descripci&oacute;n', '95%') // ancho de la columna
		);
		
		// generamos la tabla y el script para el dataTables
		$table = $this->datatables->createTable($columnasName);

		// $table = $this->datatables->createTable();
		$script = "<script>".$this->datatables->createScript()."</script>";

		// agregamos los css para el dataTables
		$this->css('plugins/dataTables/dataTables.bootstrap');
		$this->css('plugins/dataTables/dataTables.responsive');
		$this->css('plugins/dataTables/dataTables.tableTools.min');

		// agregamos los scripts para el dataTables
		$this->js('plugins/dataTables/jquery.dataTables');
		$this->js('plugins/dataTables/dataTables.bootstrap');
		$this->js('plugins/dataTables/dataTables.responsive');
		$this->js('plugins/dataTables/dataTables.tableTools.min');
		$this->js($script, false);
		// echo $script;exit;
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Fases o Etapas de Proyecto");
		$this->set_subtitle("");
		$this->load->library('combobox');
		
		// combo presentacion
		$this->combobox->setAttr("id","idusuario");
		$this->combobox->setAttr("name","idusuario");
		$this->combobox->setAttr("class","form-control");
		$this->combobox->setAttr("required","");
		$this->db->select('idusuario,nombres');
		$query = $this->db->where("estado","A")->order_by("nombres")->get("seguridad.usuario");
		$this->combobox->addItem("","Seleccione...");
		$this->combobox->addItem($query->result_array());
		
		$data['usuario'] = $this->combobox->getObject();
		
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model($this->controller);
		$data = $this->fase->find($id);
		
		$this->set_title("Modificar Fase");
		$this->set_subtitle("");
		$this->load->library('combobox');
		
		// combo presentacion
		$this->combobox->setAttr("id","idusuario");
		$this->combobox->setAttr("name","idusuario");
		$this->combobox->setAttr("class","form-control");
		$this->combobox->setAttr("required","");
		$this->db->select('idusuario,nombres');
		$query = $this->db->where("estado","A")->order_by("nombres")->get("seguridad.usuario");
		$this->combobox->addItem("","Seleccione...");
		$this->combobox->addItem($query->result_array());
		$this->combobox->setSelectedOption($data["idusuario"]);
		
		$data['usuario'] = $this->combobox->getObject();
		
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model($this->controller);
		
		$this->fase->text_uppercase(false);

		$fields = $this->input->post();
		

		// $defaultLogo = "default_logo.png";
		// if(empty($fields["logo"]))
			// $fields["logo"] = $defaultLogo;
		
		// $fields['controller']=$this->controller;
		//$fields['accion']=__FUNCTION__;
		
		
		// if( ! empty($fields["idfase"])) {
			// $data = $this->fase->find($fields["idfase"]);
		// }
		
		$fields['estado'] = "A";
		// $fields['logo'] = imagen_upload('logo','./app/img/fase/','default_logo.png',true);
		
		if(empty($fields["idfase"])) {
			$this->fase->insert($fields);
		}
		else {
			$this->fase->update($fields);
		}
		
		// $this->response($this->fase->get_fields());
		$this->response($fields);
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model($this->controller);
		// cambiamos de estado
		$fields['idfase'] = $id;
		$fields['estado'] = "I";

		$fields['controller']=$this->controller;
		$fields['accion']=__FUNCTION__;

		$this->fase->update($fields);
		
		$this->response($fields);
	}
}
?>