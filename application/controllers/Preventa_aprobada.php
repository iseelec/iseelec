<?php

include_once "Controller.php";

class Preventa_aprobada extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Lista de pedidos de cotización");
		// $this->set_subtitle("Lista de pedidos de venta");
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		// $this->js('form/'.$this->controller.'/index');
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		
		$this->load->library('combobox');
		
		///////////////////////////////////////////////////// combo tipo venta
		$query = $this->db->select('idtipoventa, descripcion')
			->where("estado", "A")->where("mostrar_en_cotizacion", "S")
			->order_by("descripcion", "asc")->get("venta.tipo_venta");
		
		$this->combobox->setAttr("id", "idtipoventa");
		$this->combobox->setAttr("name", "idtipoventa");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->setAttr("required", "");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["preventa"]["idtipoventa"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idtipoventa"]);
		}
		$data["tipoventa"] = $this->combobox->getObject();
		
		///////////////////////////////////////////////////// forma de pago
		$query = $this->db->select('idforma_pago_cotizacion, descripcion')
			->where("estado", "A")
			->order_by("idforma_pago_cotizacion", "asc")->get("venta.forma_pago_cotizacion");
		$this->combobox->init();
		$this->combobox->setAttr("id", "idforma_pago_cotizacion");
		$this->combobox->setAttr("name", "idforma_pago_cotizacion");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->setAttr("required", "");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["preventa"]["idforma_pago_cotizacion"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idforma_pago_cotizacion"]);
		}
		$data["forma_pago"] = $this->combobox->getObject();


		
////////////////////////////////////////////////////// combo Modalidad de Venta
		

	//////////////////////////////////////////////////////////
   

$query = $this->db->select('idmodalidad, modalidad')
			->where("estado", "A")
			->order_by("modalidad", "asc")->get("venta.modalidad");
		$this->combobox->init();
		$this->combobox->setAttr("id", "idmodalidad");
		$this->combobox->setAttr("name", "idmodalidad");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->setAttr("required", "");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["preventa"]["idmodalidad"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idmodalidad"]);
		}
		$data["modalidad"] = $this->combobox->getObject();


/*$query = $this->db->where("estado", "A")
			->order_by("modalidad", "asc")->get("venta.modalidad");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idmodalidad","name"=>"idmodalidad","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem($query->result_array(), array("idmodalidad","modalidad"));
		
		if( isset($data["preventa"]["idmodalidad"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idmodalidad"]);
		}
		$data["modalidad"] = $this->combobox->getObject();*/


////////////////////////////////////////////////////// combo Rampa
		$query = $this->db->where("estado", "A")
			->order_by("descripcion", "asc")->get("venta.rampa");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idrampa","name"=>"idrampa","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem("0"," ");
		$this->combobox->addItem($query->result_array(), array("idrampa","descripcion"));
		
		if( isset($data["preventa"]["idrampa"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idrampa"]);
		}
		$data["rampa"] = $this->combobox->getObject();


////////////////////////////////////////////////////// combo Mecanico
		$query = $this->db->where("estado", "A")->where("baja","N")->order_by("nombre", "asc")->get("seguridad.mecanico_vista");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idmecanico","name"=>"idmecanico","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem("0"," ");
		$this->combobox->addItem($query->result_array(), array("idmecanico","nombre"));
		
		if( isset($data["preventa"]["idmecanico"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idmecanico"]);
		}
		$data["mecanico_vista"] = $this->combobox->getObject();


		
		////////////////////////////////////////////////////// combo tipodocumento
		$query = $this->db->where("estado", "A")->where("mostrar_en_cotizacion", "S")
			->order_by("descripcion", "asc")->get("venta.tipo_documento");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idtipodocumento","name"=>"idtipodocumento","class"=>"form-control input-xs","required"=>""));
		
		$this->combobox->addItem($query->result_array(), array("idtipodocumento","descripcion","codsunat","facturacion_electronica","ruc_obligatorio","dni_obligatorio"));
		
		if( isset($data["preventa"]["idtipodocumento"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idtipodocumento"]);
		}
		$data["tipodocumento"] = $this->combobox->getObject();
		
		//////////////////////////////////////////////////////////// combo sede
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"serie","name"=>"serie","class"=>"form-control input-xs","required"=>""));
		if( isset($data["preventa"]["idtipodocumento"]) ) {
			$sql = "SELECT serie, serie 
				FROM venta.serie_documento 
				WHERE idtipodocumento=? and idsucursal=?";
			$query = $this->db->query($sql, array($data["preventa"]["idtipodocumento"], $this->get_var_session("idsucursal")));
			$this->combobox->addItem($query->result_array());
		}
		if( isset($data["preventa"]["serie"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["serie"]);
		}
		$data["comboserie"] = $this->combobox->getObject();
		
		//////////////////////////////////////////////////////// combo almacen
		$query = $this->db->select('idalmacen, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))
			->order_by("idalmacen", "desc")->get("almacen.almacen");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idalmacen","name"=>"idalmacen","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem($query->result_array());
		if( isset($data["preventa"]["idalmacen"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idalmacen"]);
		}
		$data["almacen"] = $this->combobox->getObject();
		
		//////////////////////////////////////////////////////// combo moneda
		$query = $this->db->select('idmoneda, descripcion')->where("estado", "A")
			->order_by("idmoneda", "asc")->get("general.moneda");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idmoneda","name"=>"idmoneda","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem($query->result_array());
		if( isset($data["preventa"]["idmoneda"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idmoneda"]);
		}
		$data["moneda"] = $this->combobox->getObject();
		
		///////////////////////////////////////////////////// combo tipo operacion (sunat)
		$query = $this->db->select('codtipo_operacion, descripcion')
			->order_by("codtipo_operacion", "asc")->get("general.tipo_operacion");
		
		$this->combobox->init();
		$this->combobox->setAttr("id", "codtipo_operacion");
		$this->combobox->setAttr("name", "codtipo_operacion");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["preventa"]["codtipo_operacion"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["codtipo_operacion"]);
		}
		$data["tipo_operacion"] = $this->combobox->getObject();
		
		//////////////////////////////////////// combos temporales facturacion /////////////////////////////
		$query = $this->db->order_by("orden", "asc")->get("general.grupo_igv");
		$this->combobox->removeAllItems();
		$this->combobox->setAttr("id", "grupo_igv_temp");
		$this->combobox->setAttr("name", "grupo_igv_temp");
		$this->combobox->addItem($query->result_array(), array("codgrupo_igv","decripcion","tipo_igv_default","tipo_igv_oferta","igv"));
		$data["combo_grupo_igv"] = $this->combobox->getObject();
		
		$sql = "select codtipo_igv, codtipo_igv||': '||descripcion as descripcion from general.tipo_igv order by 1";
		$query = $this->db->query($sql);
		$this->combobox->removeAllItems();
		$this->combobox->setAttr("id", "tipo_igv_temp");
		$this->combobox->setAttr("name", "tipo_igv_temp");
		$this->combobox->addItem($query->result_array());
		$data["combo_tipo_igv"] = $this->combobox->getObject();
		
		$data["default_igv"] = $this->get_param("default_igv");
		
		//////////////////////////////////////// combos secciones /////////////////////////////
		//$query = $this->db->order_by("idseccion", "asc")->get("compra.seccion");
		// $query = $this->db->order_by("descripcion", "asc")->get("compra.seccion");
		// $this->combobox->removeAllItems();
		// $this->combobox->setAttr("id", "seccion_temp");
		// $this->combobox->setAttr("name", "seccion_temp");
		// $this->combobox->addItem($query->result_array(), array("idseccion","decripcion","abreviatura","estado"));
		// $data["combo_seccion"] = $this->combobox->getObject();
		
			///////////////////////////////////////////////////// combo tipo venta
		$query = $this->db->select('idseccion, descripcion')
			->where("estado", "A")
			->order_by("descripcion", "asc")->get("compra.seccion");
		$this->combobox->removeAllItems();
		$this->combobox->setAttr("id", "idseccion");
		$this->combobox->setAttr("name", "idseccion");
		$this->combobox->setAttr("class", "form-control input-xs");
		//$this->combobox->setAttr("required", "");
		$this->combobox->addItem($query->result_array());
		
		$data["combo_seccion"] = $this->combobox->getObject();
		///////////////////////////////////////////////////////// combo vendedor
		// $idperfil = 4; // id del perfil vendedor, tal vez deberia ser contante
		$idperfil = $this->get_param("idtipovendedor"); // id del perfil vendedor, tal vez deberia ser constante
		$this->load_model("usuario");
		$datos = $this->usuario->get_vendedor($this->get_var_session("idsucursal"), $idperfil);
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idvendedor","name"=>"idvendedor","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem($datos);
		if( isset($data["preventa"]["idvendedor"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idvendedor"]);
		}
		else {
			$this->combobox->setSelectedOption($this->get_var_session("idusuario"));
		}
		$data["vendedor"] = $this->combobox->getObject();

		$data["controller"] = $this->controller;
		
		$igv = $this->get_param("igv");
		if(!is_numeric($igv)) {
			$igv = 18;
		}
		$data["valor_igv"] = $igv;
		$data["validar_ruc"] = $this->get_param("validar_ruc");
		
		$nueva_venta = "true";
		if( isset($data["preventa"]["idpreventa"]) ) {
			$nueva_venta = "false";
		}
		$this->js("<script>var _es_nuevo_ = $nueva_venta;</script>", false);
		
		if( isset($data["detalle"]) ) {
			$this->js("<script>var data_detalle = ".json_encode($data["detalle"]).";</script>", false);
		}
		$this->css('plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox');
		$this->css("plugins/datapicker/datepicker3");
		$this->css('plugins/iCheck/custom');
		
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		$this->js('plugins/iCheck/icheck.min');
		$this->js('form/'.$this->controller.'/form');
		
		
		$this->load_controller("cliente");

		$data["form_cliente"] = $this->cliente_controller->form(null, "cli_", true);
		
		$this->load_controller("producto");
		//$data["form_producto"] = $this->producto_controller->form(null, "", true);
		$data["form_producto"] = $this->producto_controller->form(null, "", true);
		$data["form_producto_seccion"] = $this->producto_controller->form_seccion_medida(null, "", true);


		$this->js('form/cliente/modal');
		
		$fc = $this->get_param("fixed_venta");
		if(!is_numeric($fc)) {
			$fc = 2;
		}
		$this->js("<script>var _fixed_venta = $fc;</script>", false);
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	public function filtros_grilla($pendiente) {
		$this->load_library("combobox");
		
		$html = '<div class="row">';
		
		// div y combobox recepcionado
		$this->combobox->setAttr("filter", "pendiente");
		$this->combobox->setAttr("class", "form-control");
		$this->combobox->addItem("S", "PENDIENTE");
		$this->combobox->addItem("N", "ATENDIDO");
		
		$html .= '<div class="col-sm-4"><div class="form-group">';
		$html .= '<label class="control-label">Atendido</label>';
		$html .= $this->combobox->getObject();
		$html .= '</div></div>';
		
		$html .= '</div>';
		
		$this->set_filter($html);
	}

	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		$this->load_model("venta.preventa_view");
		
		$this->load->library('datatables');
		
		$this->datatables->setModel($this->preventa_view);
		$this->datatables->setIndexColumn("idpreventa");
		
		$pendiente = 'S';
		
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('aprobado', '!=', 'N');
		$this->datatables->where('pendiente', '=', $pendiente);
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		
		$this->datatables->setColumns(array('idpreventa','fecha_caducidad','cliente','tipoventa',
			'moneda_abreviatura','total','tipodocumento'));
			// 'moneda_abreviatura','total','tipodocumento','serie'));
		
		$columnasName = array(
			array('Id','5%')
			,array('Fecha Caducidad','18%')
			,array('Cliente','28%')
			,array('Tipo','12%')
			,array('Moneda','8%')
			,array('Total','8%')
			// ,array('Vendedor','15')
			,array('CdP','16%')
			// ,array('Serie','8%')
		);
		
		$table = $this->datatables->createTable($columnasName);
		$script = "<script>".$this->datatables->createScript()."</script>";
		$this->js($script, false);
		
		$formMail = $this->form_mail();
		
		$this->filtros_grilla($pendiente);
		
		return $table.$formMail;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Preventa_aprobada");
		$this->set_subtitle("");
		
		$this->set_content($this->form());
		$this->index("content_empty");
//		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model("venta.preventa_view");
		$this->preventa_view->set_column_pk("idpreventa");
		$data["preventa"] = $this->preventa_view->find($id);
		
		$this->load_model("detalle_preventa");
		$data["detalle"] = $this->detalle_preventa->get_items($id, $data["preventa"]["idsucursal"]);
		
		// echo "<pre>";var_dump($data);
		$this->set_title("Modificar Preventa_aprobada");
		$this->set_subtitle("");
		$this->set_content($this->form($data));
		$this->index("content_empty");
		// $this->index("content");
	}
	
	/**
	 * Metodo para guardar un registro
	 */	
	public function guardar() {
		$this->load_model("venta.preventa");
		$this->load_model("venta.detalle_preventa");
		
		$fields = $this->input->post();
		$fields['idsucursal'] = $this->get_var_session("idsucursal");
		$fields['idusuario'] = $this->get_var_session("idusuario");
		$fields['idmodalidad'] = 1;
		//$tipocambio = ($fields["idmoneda"] == 1) ? 1 : floatval($fields["cambio_moneda"]);

		//$fields['fecha_caducidad'] = date("Y-m-d H:i:s");
		$fields['fecha'] = date("Y-m-d H:i:s");
		$fields['estado'] = "A";
		$fields["pendiente"] = "S";
		$fields['aprobado'] = "N";
		$fields['enviado_email'] = "N";

		if(empty($fields["n_modificacion"]))
			$fields["n_modificacion"] = 0;
		// if(($fields["descuento"])=0.00)
			// $fields["filtro_desc"] = 'SI';
		if(empty($fields["descuento"]))
			$fields["descuento"] = 0;
		if(empty($fields["igv"]))
			$fields["igv"] = 0;
		if(empty($fields["idcliente"]))
			$fields["idcliente"] = 0;
		if(empty($fields["idrampa"]))
			$fields["idrampa"] = 0;
		if(empty($fields["idmecanico"]))
			$fields["idmecanico"] = 0;		
        // if(empty($fields["idmodalidad"]))
			// $fields["idmodalidad"] = 0;
		if(empty($fields["idvendedor"]))
			$fields["idvendedor"] = $this->get_var_session("idusuario");
		// print_r($fields);return;
		
		// verificamos datos necesarios segun el tipo comprobante
		//$valid = $this->is_valid_doc($fields["idtipodocumento"], $fields["serie"], $fields["idcliente"]);
			/*$valid = $this->is_valid_doc($fields["idtipodocumento"], $fields["idcliente"]);*/
		/*if($valid !== true) {
			$this->exception($valid);
			return;
		}*/
		
		$this->db->trans_start(); // inciamos transaccion
		
		if(empty($fields["idpreventa"])) {
			$idpreventa = $this->preventa->insert($fields);
		} else {
			$this->preventa->update($fields);
			$idpreventa = $this->preventa->get("idpreventa");
			
			// eliminamos el detalle de la preventa
			$this->detalle_preventa->delete(array("idpreventa"=>$idpreventa));
		}
		
		$this->detalle_preventa->set("idpreventa", $idpreventa);
		$this->detalle_preventa->set("estado", "A");
		foreach($fields["deta_idproducto"] as $key=>$val) {
			$this->detalle_preventa->set("iddetalle_preventa", ($key+1));
			$this->detalle_preventa->set("idproducto", $val);
			$this->detalle_preventa->set("idseccion", $fields["deta_seccion"][$key]);
			$this->detalle_preventa->set("idunidad", $fields["deta_idunidad"][$key]);
			$this->detalle_preventa->set("cantidad", $fields["deta_cantidad"][$key]);
			$this->detalle_preventa->set("precio", $fields["deta_precio_mercado"][$key]);
			$this->detalle_preventa->set("costo", $fields["deta_precio_compra"][$key]);
			$this->detalle_preventa->set("idalmacen", $fields["deta_idalmacen"][$key]);
			//$this->detalle_preventa->set("serie", $fields["deta_series"][$key]);
			$this->detalle_preventa->set("oferta", $fields["deta_oferta"][$key]);
			$this->detalle_preventa->set("codgrupo_igv", "GRA");
			$this->detalle_preventa->set("codtipo_igv", "10");
			$this->detalle_preventa->set("descripcion_cli", $fields["deta_descripcion_cli"][$key]);
			$this->detalle_preventa->set("unidad", $fields["deta_unidad"][$key]);
			$this->detalle_preventa->set("descuento", $fields["deta_descuento"][$key]);
			$this->detalle_preventa->set("margen", $fields["deta_margen"][$key]);
			$this->detalle_preventa->insert(null, false);
		}
		
		$this->db->trans_complete(); // finalizamos transaccion
		
		$this->response($this->preventa->get_fields());
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model("venta.preventa");
		
		$fields['idpreventa'] = $id;
		$fields['estado'] = "I";
		$this->preventa->update($fields);
		
		$this->response($fields);
	}
	//bloquear preventa
	public function bloquear_preventa($id){
		$this->load_model("venta.preventa");
		//$this->load_model($this->controller);
		$fields['idpreventa'] = $id;
		$fields['aprobado'] = "B";
		$this->preventa->update($fields);
		
		$this->response($fields);
	}
	//desbloquear preventa, para que aparezca en cotizacion
	public function desbloquear_preventa($id){
		$this->load_model("venta.preventa");
		//$this->load_model($this->controller);
		$fields['idpreventa'] = $id;
		$fields['aprobado'] = "N";
		$this->preventa->update($fields);
		
		$this->response($fields);
	}
	//finalizar preventa
	public function finalizar_preventa($id){
		$this->load_model("venta.preventa");
		//$this->load_model($this->controller);
		$fields['idpreventa'] = $id;
		$fields['aprobado'] = "F";
		$this->preventa->update($fields);
		
		$this->response($fields);
	}
		public function aprobar_preventa() {
		$post = $this->input->post();
		if(!empty($post["idpreventa"])) {
			$this->load_model("venta.preventa");
			$this->preventa->update($post);
		}
		$this->response($post);
	}
	public function grilla_popup() {
		$this->load_model("venta.preventa_view");
		$this->load->library('datatables');
		
		$this->datatables->setModel($this->preventa_view);
		$this->datatables->setIndexColumn("idpreventa");
		
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('pendiente', '=', 'S');
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		
		$this->datatables->setPopup(true);
		$this->datatables->setColumns(array('idpreventa'
											,'fecha_caducidad'
											,'cliente'
											,'td_documento'
											,'moneda_abreviatura'
											,'modalidad'
											,'rampa'
											,'mecanico_nombre'
											,'vendedor_nombre'
											,'total'
									));

		$this->datatables->order_by('fecha_caducidad', 'desc');
		$this->datatables->setCallback('callbackPreventa_aprobada');
		$table = $this->datatables->createTable(array(array('Id','10')
													,array('Fecha Caducidad','10')
													,array('Cliente','55')
													,array('Doc','4')
													,array('Moneda','2')
													,array('Modalidad','10')
													,array('Rampa','2')
													,array('Mecanico','2')
													,array('Vendedor','2')
													,array('Total','10')
												));
		$script = "<script>".$this->datatables->createScript("", false)."</script>";
		
		$this->response($script.$table);
	}
	
	public function get_detalle($id) {
		$this->load_model("detalle_preventa");
		$res = $this->detalle_preventa->get_items($id, $this->get_var_session("idsucursal"));
		$this->response($res);
	}
	
	// public function getCotizaciones($idpreventa) {
		// $model = new Generic('credito', 'venta');
		
		// $id = intval($idpreventa);
		
		// $sql = "SELECT
				// pv.idpreventa
				// ,COALESCE(l.nombres,'')||COALESCE(l.apellidos,'') cliente
				// ,l.direccion_principal direccion
				// ,COALESCE(l.dni,l.ruc,'') dni
				// ,COALESCE(l.ruc,l.dni,'') ruc
				// ,((SELECT (array_agg(telefono)) FROM venta.cliente_telefono cf WHERE cf.idcliente=pv.idcliente)) telefono
				// ,pv.referencia referencia
				// ,l.observacion observacion
				// FROM venta.preventa pv
				// JOIN venta.cliente l ON l.idcliente=pv.idcliente
				// WHERE pv.idpreventa=$id;";

		// $q=$this->db->query($sql);
		// $arrCotizacion = $q->row_array();
		/////prueba uno- lista de productos de la cotización
		
						// $sqlpv = "SELECT
						// pv.idpreventa,
						// dpv.idproducto,
						// p.imagen_producto,
						// p.descripcion AS producto,
						// ma.descripcion AS marca,
						// mo.descripcion AS modelo,
						// p.precio_mercado,
						// dpv.cantidad,
						// dpv.cantidad * p.precio_mercado AS importe
						// FROM
                        // venta.preventa AS pv
						// INNER JOIN venta.detalle_preventa AS dpv ON pv.idpreventa = dpv.idpreventa
						// INNER JOIN compra.producto AS p ON p.idproducto = dpv.idproducto
						// INNER JOIN general.marca AS ma ON ma.idmarca = p.idmarca
						// INNER JOIN general.modelo AS mo ON mo.idmodelo = p.idmodelo
						// WHERE pv.idpreventa =$id;";
						
						// $q=$this->db->query($sqlpv);
					// $arrPcot = $q->result_array();
		
		//obtenemos la lista de letras del credito
		// $sql = "SELECT l.idletra
				// ,l.nro_letra letra
				// ,l.descripcion tipo_letra
				// ,to_char(l.fecha_vencimiento, 'DD/MM/YYYY') fecha_vencimiento
				// ,l.estado
				// ,l.monto_letra monto
				// ,(case when l.pagado = 'S' then coalesce(a.moras,0.00) else l.mora + coalesce(a.moras,0) end) as moras
				// ,coalesce(a.pago + a.moras,0) as pagos
				// ,coalesce(t.descripcion, '') as tipopago_credito
				// ,coalesce(to_char(l.fecha_cancelado, 'DD/MM/YYYY'), '') as fecha_amortizacion
				// ,a.moras moras_real
				// ,l.descuento
				// ,((monto_letra+(case when l.pagado = 'S' then coalesce(a.moras,0.00) else l.mora + coalesce(a.moras,0) end))-(coalesce(a.pago + a.moras,0))-l.descuento) saldito
				// ,coalesce(a2.serie, '') as serie
				// ,coalesce(a2.numero, null) as nro_recibo
				// ,td.abreviatura||'-'||coalesce(a2.serie||'-', '')||coalesce(a2.numero, null) recibo
				// ,l.pagado
				// FROM credito.letra l
				// LEFT JOIN venta.tipopago t ON t.idtipopago=l.idtipo_pago
				// LEFT JOIN (select idletra, sum(mora) as moras, sum(monto) as pago, max(idamortizacion) as idamrzt
					// from credito.amortizacion where estado<>'I' and idcredito='$id' group by idletra) a ON a.idletra = l.idletra
				// LEFT JOIN credito.amortizacion a2 on a2.idamortizacion=a.idamrzt and a2.idletra=l.idletra and a2.idcredito='$id'
				// LEFT JOIN venta.tipo_documento td on td.idtipodocumento=a2.idtipodocumento
				// WHERE l.idcredito='$id'
				// ORDER BY nro_letra;";
				
 


		
		// $q=$this->db->query($sql);
		// $rs = $q->result_array();
		
		// $arrLetras = array();
		
		// if(!empty($rs)) {
			// foreach($rs as $arr) {
				// $arrLetras[] = $arr;
			// }
		// }
		
		// obtenemos las amortizaciones de las letras, solo los que se realizaron en mas de uno
		// $sql = "SELECT a.idletra
				// ,to_char(fecha_pago,'DD/MM/YYYY') as fecha_amortizacion
				// ,a.monto
				// ,a.mora as moras
				// ,a.monto+a.mora as total
				// ,serie
				// ,numero as nrorecibo
				// ,coalesce(t.descripcion,td.descripcion,'-') as tipopago_credito
				// ,td.abreviatura||'-'||coalesce(a.serie||'-', '')||coalesce(a.numero, null) as recibo
				// FROM credito.amortizacion a
				// JOIN venta.tipo_documento td ON td.idtipodocumento=a.idtipodocumento
				// LEFT JOIN venta.tipopago t ON t.idtipopago=a.idtipo_pago
				// WHERE a.estado<>'I' AND a.idcredito=$id 
				// order by a.idletra, a.idamortizacion;";

		// $q = $this->db->query($sql);
		// $rs = $q->result_array();
		
		// $arrAmrtz = array();
		// $arrPago = array();
		
		// if(!empty($rs)) {
			// $temp = array();
			// foreach($rs as $arr) {
				// if(!array_key_exists($arr['idletra'], $arrAmrtz)) {
					// $arrAmrtz[$arr['idletra']] = array();
					// $arrPago[$arr['idletra']] = array();
				// }
				
				// $arrAmrtz[$arr['idletra']][] = $arr;
				// $arrPago[$arr['idletra']][] = $arr['total'];
			// }
		// }
		
		// obtenemos los datos de la venta
		// $sql = "SELECT 
				// COALESCE(l.nombres,'')||COALESCE(l.apellidos,'') cliente
				// ,COALESCE(l.dni,l.ruc,'') dni
				// ,l.direccion_principal direccion
				// ,((SELECT (array_agg(telefono)) FROM venta.cliente_telefono cf WHERE cf.idcliente=l.idcliente)) telefono
				// ,e.nombres vendedor
				// ,t.abreviatura||' '||v.serie||'-'||v.correlativo comprobante
				// ,p.descripcion producto
				// ,m.descripcion marca
				// ,((SELECT (array_agg(serie)) FROM venta.detalle_venta_serie dvs WHERE dvs.idventa=v.idventa)) serie
				// FROM venta.detalle_venta d
				// JOIN venta.venta v ON v.idventa=d.idventa
				// JOIN compra.producto p ON p.idproducto=d.idproducto
				// JOIN general.marca m ON m.idmarca=p.idmarca
				// JOIN venta.tipo_documento t ON t.idtipodocumento=v.idtipodocumento
				// JOIN venta.cliente l ON l.idcliente=v.idcliente
				// JOIN seguridad.usuario e ON e.idusuario=v.idvendedor
				// WHERE v.idventa='{$arrCredito['idventa']}';";
		
		// $q=$this->db->query($sql);
		// $arrVenta = $q->result_array();
		
		// $response = array('cotizacion'=>$arrCotizacion, 'lista_prod'=>$arrPcot);
		/////$response = array('credito'=>$arrCredito, 'letras'=>$arrLetras, 'amortizaciones'=>$arrAmrtz, 'pagos'=>$arrPago, 'venta'=>$arrVenta);
		
		// return $response;
	// }
	public function getLetrasPagos($id) {
		// $model = new Generic('credito', 'venta');
		
		//$id = intval($idpreventa);
		
		$sql = "SELECT
				pv.idpreventa
				,COALESCE(l.nombres,'')||COALESCE(l.apellidos,'') cliente
				,l.direccion_principal direccion
				,COALESCE(l.dni,l.ruc,'') dni
				,COALESCE(l.ruc,l.dni,'') ruc
				,((SELECT (array_agg(telefono)) FROM venta.cliente_telefono cf WHERE cf.idcliente=pv.idcliente)) telefono
				,pv.referencia referencia
				,pv.garantia_ser garantia_ser
				,pv.garantia_equi garantia_equi
				,pv.condiciones_ser condiciones_ser
				,l.observacion observacion
				FROM venta.preventa pv
				JOIN venta.cliente l ON l.idcliente=pv.idcliente
				WHERE pv.idpreventa=$id;";
		$q=$this->db->query($sql);
		$arrCredito = $q->row_array();
		
		// obtenemos la lista de letras del credito
		$sql = "SELECT
						pv.idpreventa,
						dpv.idproducto,
						p.imagen_producto as imagen,
						p.descripcion_detallada AS producto,
						ma.descripcion AS marca,
						mo.descripcion AS modelo,
						p.precio_mercado as precio_u,
						dpv.cantidad as cantidad,
						dpv.cantidad * p.precio_mercado AS importe
						FROM
                        venta.preventa AS pv
						INNER JOIN venta.detalle_preventa AS dpv ON pv.idpreventa = dpv.idpreventa
						INNER JOIN compra.producto AS p ON p.idproducto = dpv.idproducto
						INNER JOIN general.marca AS ma ON ma.idmarca = p.idmarca
						INNER JOIN general.modelo AS mo ON mo.idmodelo = p.idmodelo
						WHERE pv.idpreventa =$id;";
		
		$q=$this->db->query($sql);
		$rs = $q->result_array();
		
		$arrLetras = array();
		
		if(!empty($rs)) {
			foreach($rs as $arr) {
				$arrLetras[] = $arr;
			}
		}
		
		// obtenemos las amortizaciones de las letras, solo los que se realizaron en mas de uno
		// $sql = "SELECT a.idletra
				// ,to_char(fecha_pago,'DD/MM/YYYY') as fecha_amortizacion
				// ,a.monto
				// ,a.mora as moras
				// ,a.monto+a.mora as total
				// ,serie
				// ,numero as nrorecibo
				// ,coalesce(t.descripcion,td.descripcion,'-') as tipopago_credito
				// ,td.abreviatura||'-'||coalesce(a.serie||'-', '')||coalesce(a.numero, null) as recibo
				// FROM credito.amortizacion a
				// JOIN venta.tipo_documento td ON td.idtipodocumento=a.idtipodocumento
				// LEFT JOIN venta.tipopago t ON t.idtipopago=a.idtipo_pago
				// WHERE a.estado<>'I' AND a.idcredito=$id 
				// order by a.idletra, a.idamortizacion;";

		// $q = $this->db->query($sql);
		// $rs = $q->result_array();
		
		// $arrAmrtz = array();
		// $arrPago = array();
		
		// if(!empty($rs)) {
			// $temp = array();
			// foreach($rs as $arr) {
				// if(!array_key_exists($arr['idletra'], $arrAmrtz)) {
					// $arrAmrtz[$arr['idletra']] = array();
					// $arrPago[$arr['idletra']] = array();
				// }
				
				// $arrAmrtz[$arr['idletra']][] = $arr;
				// $arrPago[$arr['idletra']][] = $arr['total'];
			// }
		// }
		
		////obtenemos los datos de la venta
		// $sql = "SELECT 
				// COALESCE(l.nombres,'')||COALESCE(l.apellidos,'') cliente
				// ,COALESCE(l.dni,l.ruc,'') dni
				// ,l.direccion_principal direccion
				// ,((SELECT (array_agg(telefono)) FROM venta.cliente_telefono cf WHERE cf.idcliente=l.idcliente)) telefono
				// ,e.nombres vendedor
				// ,t.abreviatura||' '||v.serie||'-'||v.correlativo comprobante
				// ,p.descripcion producto
				// ,m.descripcion marca
				// ,((SELECT (array_agg(serie)) FROM venta.detalle_venta_serie dvs WHERE dvs.idventa=v.idventa)) serie
				// FROM venta.detalle_venta d
				// JOIN venta.venta v ON v.idventa=d.idventa
				// JOIN compra.producto p ON p.idproducto=d.idproducto
				// JOIN general.marca m ON m.idmarca=p.idmarca
				// JOIN venta.tipo_documento t ON t.idtipodocumento=v.idtipodocumento
				// JOIN venta.cliente l ON l.idcliente=v.idcliente
				// JOIN seguridad.usuario e ON e.idusuario=v.idvendedor
				// WHERE v.idventa='{$arrCredito['idventa']}';";
		
		// $q=$this->db->query($sql);
		// $arrVenta = $q->result_array();
		
		$response = array('credito'=>$arrCredito, 'letras'=>$arrLetras);
		// $response = array('credito'=>$arrCredito, 'letras'=>$arrLetras, 'amortizaciones'=>$arrAmrtz, 'pagos'=>$arrPago, 'venta'=>$arrVenta);
		
		return $response;
	}
	
	public function cotizacionrrrr($idpreventa) {
			$this->load->library("pdf");
		//$this->pdf->SetLogo(FCPATH."app/img/empresa/".$this->empresa->get("logo"));
		$this->load_model(array("venta.preventa", "seguridad.sucursal", "seguridad.empresa", "venta.cliente", "seguridad.usuario", "venta.forma_pago_cotizacion", "compra.seccion"));
		$this->preventa->find($idpreventa);
		$this->sucursal->find($this->preventa->get("idsucursal"));
		$this->empresa->find($this->sucursal->get("idsucursal"));
		$this->cliente->find($this->preventa->get("idcliente"));
		$this->usuario->find($this->preventa->get("idusuario"));
		$this->seccion->find($this->preventa->get("idseccion"));
		$this->forma_pago_cotizacion->find($this->preventa->get("idforma_pago_cotizacion"));
		$whit_prov=182;
		 // echo "<pre>";
			// print_r($seccion);exit;
		//$cotiza = $res['cotizacion'];
		//$this->load->library("pdf");
		
		$this->pdf->SetLogo(FCPATH."app/img/empresa/".$this->empresa->get("logo"));
		//$this->pdf->SetFont('Arial','',9);
		//$this->pdf->SetTitle(utf8_decode("COTIZACIÓN N° ".$this->empresa->get("descripcion")), null, null, true);
		//$this->pdf->SetTitle(utf8_decode("COTIZACIÓN N° ".$this->preventa->get("idpreventa")), null, null, true);
		//$this->pdf->SetTitle("REGISTRO DE INVENTARIO PERMANENTE EN UNIDADES FISICAS", 11, "");
		$this->pdf->AliasNbPages(); // para el conteo de paginas
		$this->pdf->SetLeftMargin(4);

		$this->pdf->AddPage('','a4');
		$this->pdf->SetFont('Arial','',9);
		
			$this->pdf->SetY(10); /* Inicio */
			$this->pdf->SetFont('Arial','B',9);
			$this->pdf->Cell(136,3,$this->empresa->get("descripcion"),0,0,'R');
			$this->pdf->SetFont('Arial','B',7);
			$this->pdf->MultiCell(65,3,utf8_decode('                                                                                     VENTA, INSTALACIÓN Y MANTENIMIENTO DEL SISTEMA DE ALARMA CONTRA ROBO, DETECCIÓN DE INCENDIO, CONTROL DE ACCESO, CIRCUITO CERRADO DE TV (CCTV), CABLEADO ESTRUCTURADO, RED HUMEDA.                                                      '),1,'J');
			$this->pdf->SetY(12); /* Set 20 Eje Y */
			$this->pdf->SetFont('Arial','B',9);
			$this->pdf->Cell(62,7,"RUC: ".$this->empresa->get("ruc"),0,1,'R');
			$this->pdf->SetXY(36,18); /* Set 20 Eje XY */
			$this->pdf->SetFont('Arial','',8);
			$this->pdf->MultiCell(100,3,utf8_decode("DIRECCIÓN: ".$this->empresa->get("direccion")),0,'L');
			$this->pdf->SetXY(18,80); /* Set 20 Eje XY h*/
			$this->pdf->SetFont('Arial','',7);
			$this->pdf->MultiCell(60,3,substr(utf8_decode("Referencia:  ".$this->preventa->get("referencia")),0,144),0,'L');
			$this->pdf->SetXY(80,80); /* Set 20 Eje XY h*/
			$this->pdf->MultiCell(30,4,utf8_decode("Fecha del Presupuesto: ".$this->preventa->get("fecha")),0,'L');
			$this->pdf->SetXY(112,80); /* Set 20 Eje XY h*/
			$this->pdf->MultiCell(40,3,utf8_decode("Comercial: ".$this->usuario->get("nombres")." ".$this->usuario->get("appat")." ".$this->usuario->get("apmat")),0,'L');
			$this->pdf->SetXY(160,80); /* Set 20 Eje XY h*/
			$this->pdf->MultiCell(30,3,utf8_decode("Plazo de Pago: ".$this->forma_pago_cotizacion->get("descripcion")),0,'L');
			$this->pdf->SetXY(16,78); /* Set 20 Eje XY h*/
			$this->pdf->MultiCell(175,14.5,'',1,'L');
		
		$this->pdf->Ln();
			$this->pdf->SetXY(16,99); 
			$this->pdf->SetFont('Courier','B',9);
			$this->pdf->MultiCell(8,6,'No',1,'L');
			$this->pdf->SetXY(24,99); 
			$this->pdf->MultiCell(24,3,'IMAGEN REFERENCIAL',1,'L');
			$this->pdf->SetXY(48,99); 
			$this->pdf->MultiCell(50,6,utf8_decode('DESCRIPCIÓN'),1,'C');
			$this->pdf->SetXY(98,99);
			$this->pdf->MultiCell(20,6,'MARCA',1,'L');
			$this->pdf->SetXY(118,99);
			$this->pdf->MultiCell(20,6,'MODELO',1,'L');
			$this->pdf->SetXY(138,99);
			$this->pdf->MultiCell(18,6,'CANTIDAD',1,'L');
			$this->pdf->SetXY(156,99);
			$this->pdf->MultiCell(18,3,'PRECIO UNITARIO',1,'L');
			$this->pdf->SetXY(174,99);
			$this->pdf->MultiCell(18,6,'IMPORTE',1,'C');
	
			$this->pdf->SetFont('Courier','',8.3);
			if(!empty($letras)) {
		
				//$res = $this->getCotizaciones($_REQUEST['idpreventa']);
				//$lista_prod = $res['lista_prod'];
				// $totalCuota = $totalMora = $totalTotal = $totalPago = $totalSaldo = $totaldct = 0;
				$seccion = array();
				// $Totalizado_montopagado=$Totalizado_interespagado=$Totalizado_descuentopagado=0;
				// $Totalizado_saldoxpagar=$Totalizado_interesxpagar=$Totalizado_descuentoxpagar=0;
				// $letras_pagadas = array();
				$this->pdf->SetDrawColor(204, 204, 204);
				foreach($seccion as $val) {
					// $total = floatval($val['monto']) + floatval($val['moras']);
					// $pago = floatval($val['pagos']);
					// $dct = floatval($val['descuento']);
					
					// $arrLetras[ $val['idletra'] ] = array('letra'=>$val['letra'], 'estado'=>$val['pagado']);
					
					// $saldo = $val['saldito'];
					
					// $totalCuota += $val['monto'];
					// $totalMora += $val['moras'];
					// $totalTotal += $total;
					// $totalPago += $pago;
					// $totalSaldo += $saldo;
					// $totaldct += $dct;

					$this->pdf->Ln();
					$this->pdf->MultiCell(30,3,utf8_decode("Plazo de Pago: ".$this->seccion->get("descripcion")),0,'L');

				}
				
				
			}
		$this->pdf->Output();
	}
	
	
	public function cotizacion_old($idpreventa){
				$this->load_model(array("venta.preventa", "seguridad.sucursal", "seguridad.empresa", "venta.cliente", "seguridad.usuario", "venta.forma_pago_cotizacion", "compra.seccion"));

			$res = $this->getLetrasPagos($idpreventa);
			$letras = $res['letras'];

			
			$this->load->library("pdf");
		//$this->pdf->SetLogo(FCPATH."app/img/empresa/".$this->empresa->get("logo"));
		$this->preventa->find($idpreventa);
		$this->sucursal->find($this->preventa->get("idsucursal"));
		$this->empresa->find($this->sucursal->get("idsucursal"));
		$this->cliente->find($this->preventa->get("idcliente"));
		$this->usuario->find($this->preventa->get("idusuario"));
		$this->seccion->find($this->preventa->get("idseccion"));
		$this->forma_pago_cotizacion->find($this->preventa->get("idforma_pago_cotizacion"));
		$whit_prov=182;
		 // echo "<pre>";
			// print_r($seccion);exit;
		//$cotiza = $res['cotizacion'];
		//$this->load->library("pdf");

$this->pdf->SetLogo(FCPATH."app/img/empresa/".$this->empresa->get("logo"));
		//$md = $this->pdf->("&amp;#1575;&amp;#1610;&amp;#1604;&amp;#1575;&amp;#1578; &amp;#1601;&amp;#1610;&amp;#1605;&amp;#1575; &amp;#1575;&amp;#1610;&amp;#1604;&amp;#1575;&amp;#1578; &amp;#1601;&amp;#1610;&amp;#1605;&amp;#1575;");
		//$mpdf->SetTitle($md);
					//$this->pdf->SetTitle(136,3,$this->empresa->get("descripcion"),0,0,'R');
								//	$this->pdf->SetTitle($md);
									// $this->pdf->SetTitle(utf8_decode($this->empresa->get("descripcion")), 11, null, true);


		//$this->pdf->SetFont('Arial','',9);
		//$this->pdf->SetTitle(utf8_decode("COTIZACIÓN N° ".$this->empresa->get("descripcion")), null, null, true);
		//$this->pdf->SetTitle(utf8_decode("COTIZACIÓN N° ".$this->preventa->get("idpreventa")), null, null, true);
		//$this->pdf->SetTitle("REGISTRO DE INVENTARIO PERMANENTE EN UNIDADES FISICAS", 11, "");
		
			$this->pdf->AliasNbPages(); // para el conteo de paginas
			$this->pdf->SetLeftMargin(4);

			$this->pdf->AddPage('','a4');
			$this->pdf->SetFont('Arial','',9);
		//	
			$this->pdf->SetY(10); /* Inicio */
			$this->pdf->SetFont('Arial','B',9);
			$this->pdf->Cell(136,3,$this->empresa->get("descripcion"),0,0,'R');
			$this->pdf->SetFont('Arial','B',7);
			$this->pdf->MultiCell(65,3,str_pad(utf8_decode('VENTA, INSTALACIÓN Y MANTENIMIENTO DEL SISTEMA DE ALARMA CONTRA ROBO, DETECCIÓN DE INCENDIO, CONTROL DE ACCESO, CIRCUITO CERRADO DE TV (CCTV), CABLEADO ESTRUCTURADO, RED HUMEDA.'),360," ",STR_PAD_BOTH),1,'J');
			$this->pdf->SetY(12); /* Set 20 Eje Y */
			$this->pdf->SetFont('Arial','B',9);
			$this->pdf->Cell(62,7,"RUC: ".$this->empresa->get("ruc"),0,1,'R');
			$this->pdf->SetXY(36,18); /* Set 20 Eje XY */
			$this->pdf->SetFont('Arial','',8);
			$this->pdf->MultiCell(100,3,utf8_decode("DIRECCIÓN: ".$this->empresa->get("direccion")),0,'L');
			$this->pdf->SetXY(18,80); /* Set 20 Eje XY h*/
			$this->pdf->SetFont('Arial','',7);
			$this->pdf->MultiCell(60,3,substr(utf8_decode("Referencia:  ".$this->preventa->get("referencia")),0,144),0,'L');
			$this->pdf->SetXY(80,80); /* Set 20 Eje XY h*/
			$this->pdf->MultiCell(30,4,utf8_decode("Fecha del Presupuesto: ".$this->preventa->get("fecha")),0,'L');
			$this->pdf->SetXY(112,80); /* Set 20 Eje XY h*/
			$this->pdf->MultiCell(40,3,utf8_decode("Comercial: ".$this->usuario->get("nombres")." ".$this->usuario->get("appat")." ".$this->usuario->get("apmat")),0,'L');
			$this->pdf->SetXY(160,80); /* Set 20 Eje XY h*/
			$this->pdf->MultiCell(30,3,utf8_decode("Plazo de Pago: ".$this->forma_pago_cotizacion->get("descripcion")),0,'L');
			$this->pdf->SetXY(16,78); /* Set 20 Eje XY h*/
			$this->pdf->MultiCell(175,14.5,'',1,'L');
		
		
		$this->pdf->Ln();
		$this->pdf->SetFillColor(200,220,255);
			$this->pdf->SetXY(16,99); 
			$this->pdf->SetFont('Courier','B',9);
			$this->pdf->MultiCell(8,6,'No',1,'L');
			$this->pdf->SetXY(24,99); 
			$this->pdf->MultiCell(24,3,'IMAGEN REFERENCIAL',1,'L');
			$this->pdf->SetXY(48,99); 
			$this->pdf->MultiCell(50,6,utf8_decode('DESCRIPCIÓN'),1,'C');
			$this->pdf->SetXY(98,99);
			$this->pdf->MultiCell(20,6,'MARCA',1,'L');
			$this->pdf->SetXY(118,99);
			$this->pdf->MultiCell(20,6,'MODELO',1,'L');
			$this->pdf->SetXY(138,99);
			$this->pdf->MultiCell(18,6,'CANTIDAD',1,'L');
			$this->pdf->SetXY(156,99);
			$this->pdf->MultiCell(18,3,'PRECIO UNITARIO',1,'L');
			$this->pdf->SetXY(174,99);
			$this->pdf->MultiCell(18,6,'IMPORTE',1,'C');
			$this->pdf->SetFont('Arial','B',10);
			{
				$des = "Descripción Detallada";
			}
			$this->pdf->SetFont('Courier','',8.3);
			if(!empty($letras)) {
				$totalCuota = $totalMora = $totalTotal = $totalPago = $totalSaldo = $totaldct = 0;
				$arrLetras = array();
				$Totalizado_montopagado=$Totalizado_interespagado=$Totalizado_descuentopagado=0;
				$Totalizado_saldoxpagar=$Totalizado_interesxpagar=$Totalizado_descuentoxpagar=0;
				$letras_pagadas = array();
				$this->pdf->SetDrawColor(204, 204, 204);
						$this->pdf->SetLeftMargin(16);

				foreach($letras as $key => $val) {
					// $total = floatval($val['monto']) + floatval($val['moras']);
					// $pago = floatval($val['pagos']);
					// $dct = floatval($val['descuento']);
					
					// $arrLetras[ $val['idletra'] ] = array('letra'=>$val['letra'], 'estado'=>$val['pagado']);
					
					// $saldo = $val['saldito'];
					// $totalCuota += $val['monto'];
					// $totalMora += $val['moras'];
					// $totalTotal += $total;
					// $totalPago += $pago;
					// $totalSaldo += $saldo;
					// $totaldct += $dct;

							//$this->pdf->Ln(-0.2);
								$this->pdf->Cell(8,20,$key+1,1,0,'C');
								//$this->pdf->Image("leon.jpg" , 10 ,8, 35 , 38 , "JPG" );
								//		$this->pdf->SetLogo->Cell(FCPATH."app/img/empresa/".$this->empresa->get("logo"));

								//$this->pdf->Cell(24,22,FCPATH."app/img/producto/".$val["imagen"],1,0,'L');
								$this->pdf->Cell(24,20,"imagen",1,0,'L');
								//$this->pdf->Image(FCPATH."app/img/producto/".$val["imagen"], 80 ,22, 35 , 38,'png');
								$this->pdf->MultiCell(144,3,str_pad(utf8_decode($des.": ".$val["producto"]),380," ",STR_PAD_BOTH),1,'C');
								//$this->pdf->Ln();
								$this->pdf->SetX(48);
								$this->pdf->Cell(30,5,"Marca: ".$val["marca"],1,0,'L');
								$this->pdf->Cell(30,5,"Modelo: ".$val["modelo"],1,0,'L');
								$this->pdf->Cell(28,5,"Cantidad: ".$val["cantidad"],1,0,'L');
								$this->pdf->Cell(28,5,"Precio: ".$val["precio_u"],1,0,'L');
								$this->pdf->Cell(28,5,"Importe: ".$val["importe"],1,0,'L');
					$this->pdf->Ln();
					// if(!empty($val["fecha_amortizacion"])){
						// $Totalizado_montopagado   = $Totalizado_montopagado + $val["monto"];
						// $Totalizado_interespagado = $Totalizado_interespagado + $val["moras"];
						// $Totalizado_descuentopagado = $Totalizado_descuentopagado + $val["descuento"];
					// }else{
						// $Totalizado_interesxpagar = $Totalizado_interesxpagar + $val["moras"];
						// $letras_pagadas[] = array($val["letra"]);
						// $Totalizado_saldoxpagar = $Totalizado_saldoxpagar + $val["monto"];
					// }
				}
				
				
				 $this->pdf->Ln();
				
				// $this->pdf->SetFont('Courier','B',8.3);
				// $this->pdf->Cell(84,5,'TOTALES S/.',0,0,'L');
				// $this->pdf->Cell(18,5,number_format($totalCuota, 2, '.', ''),1,0,'R');
				// $this->pdf->Cell(16,5,number_format($totalMora, 2, '.', ''),1,0,'R');
				// $this->pdf->Cell(18,5,number_format($totalTotal, 2, '.', ''),1,0,'R');
				// $this->pdf->Cell(14,5,number_format($totaldct, 2, '.', ''),1,0,'R');
				// $this->pdf->Cell(18,5,number_format($totalPago, 2, '.', ''),1,0,'R');
				// $this->pdf->Cell(18,5,number_format($totalSaldo, 2, '.', ''),1,0,'R');
				// $this->pdf->Cell(20,5,'',0,0,'L');
				
				// if(count($amortizaciones)) {
					// $has_title = false;
					// foreach($amortizaciones as $codletra => $arr) {
						// $continuar = (count($arr) > 1 && $arrLetras[$codletra]['estado'] == 'S');
						// if(!$continuar) {
							// $continuar = ($arrLetras[$codletra]['estado'] == 'N');
						// }
						
						// if($continuar) {
							// if(!$has_title) {
								// $this->pdf->SetDrawColor(0, 0, 0);
								// $this->pdf->Ln();$this->pdf->Ln();$this->pdf->Ln();$this->pdf->Ln();
								// $this->pdf->SetFont('Courier','B',15);
								// $this->pdf->Cell(205,5,'AMORTIZACIONES',0,0,'L');
								// $this->pdf->Ln();
								// $this->pdf->SetFont('Courier','B',10);
								// $this->pdf->Cell(10,5,'No',1,0,'L');
								// $this->pdf->Cell(20,5,'TIPO',1,0,'L');
								// $this->pdf->Cell(20,5,'LETRA',1,0,'L');
								// $this->pdf->Cell(30,5,'FECHA PAGO',1,0,'L');
								// $this->pdf->Cell(40,5,'TIPO PAGO',1,0,'L');
								// $this->pdf->Cell(20,5,'CUOTA',1,0,'L');
								// $this->pdf->Cell(20,5,'MORAS',1,0,'L');
								// $this->pdf->Cell(20,5,'TOTAL',1,0,'L');
								// $this->pdf->Cell(25,5,'RECIBO',1,0,'L');
								// $has_title = true;
								// $this->pdf->SetFont('Courier','',8.3);
								// $this->pdf->SetDrawColor(204, 204, 204);
							// }
							
							// foreach($arr as $key => $amrtz) {
								// $this->pdf->Ln();
								// $this->pdf->Cell(10,5,($key+1),1,0,'L');
								// $this->pdf->Cell(20,5,'AMRTZ',1,0,'L');
								// $this->pdf->Cell(20,5,$arrLetras[$codletra]['letra'],1,0,'L');
								// $this->pdf->Cell(30,5,$amrtz["fecha_amortizacion"],1,0,'L');
								// $this->pdf->Cell(40,5,$amrtz["tipopago_credito"],1,0,'L');
								// $this->pdf->Cell(20,5,$amrtz["monto"],1,0,'R');
								// $this->pdf->Cell(20,5,$amrtz["moras"],1,0,'R');
								// $this->pdf->Cell(20,5,$amrtz["total"],1,0,'R');
								// $this->pdf->Cell(25,5,$amrtz["recibo"],1,0,'R');

								// if($letras_pagadas[0][0]!=$arrLetras[$codletra]['letra']){
									// $Totalizado_saldoxpagar = $Totalizado_saldoxpagar;
								// }else{
									// $Totalizado_interesxpagar = $Totalizado_interesxpagar - $amrtz["moras"];
									// $Totalizado_interespagado = $Totalizado_interespagado + $amrtz["moras"];
									// $Totalizado_montopagado = $Totalizado_montopagado + $amrtz["monto"];
									// $Totalizado_saldoxpagar = $Totalizado_saldoxpagar - $amrtz["monto"];
								// }
							// }
						// }
					// }
				// }
				
				// if(isset($_REQUEST['resumen']) && !empty($_REQUEST['resumen'])){
					// $this->pdf->SetFont('Courier','B',13);
					// $this->pdf->Ln(20);
					// $this->pdf->Cell(10,5,'',0,0,'L');
					// $this->pdf->Cell(30,5,'',0,0,'L');
					// $this->pdf->Cell(30,5,'MONTO',1,0,'C');
					// $this->pdf->Cell(30,5,'INTERES',1,0,'C');
					// $this->pdf->Cell(30,5,'DESCUENTO',1,0,'C');
					// $this->pdf->Cell(40,5,'TOTAL',1,1,'C');
					
					// $this->pdf->SetFont('Courier','B',10);
					// $this->pdf->Cell(10,5,'',0,0,'L');
					// $this->pdf->Cell(30,5,'Monto Pagado',1,0,'L');
					// $this->pdf->SetFont('Courier','',10);
					// $this->pdf->Cell(30,5,number_format($Totalizado_montopagado,2),1,0,'R');
					// $this->pdf->Cell(30,5,number_format($Totalizado_interespagado,2),1,0,'R');
					// $this->pdf->Cell(30,5,number_format($Totalizado_descuentopagado,2),1,0,'R');
					// $this->pdf->Cell(40,5,number_format(($Totalizado_montopagado+$Totalizado_interespagado - $Totalizado_descuentopagado),2),1,1,'R');
					
					// $this->pdf->SetFont('Courier','B',10);
					// $this->pdf->Cell(10,5,'',0,0,'L');
					// $this->pdf->Cell(30,5,'Saldo x pagar',1,0,'L');
					// $this->pdf->SetFont('Courier','',10);
					// $this->pdf->Cell(30,5,number_format($Totalizado_saldoxpagar,2),1,0,'R');
					// $this->pdf->Cell(30,5,number_format($Totalizado_interesxpagar,2),1,0,'R');
					// $this->pdf->Cell(30,5,number_format($Totalizado_descuentoxpagar,2),1,0,'R');
					// $this->pdf->Cell(40,5,number_format(($Totalizado_saldoxpagar+$Totalizado_interesxpagar - $Totalizado_descuentoxpagar),2),1,1,'R');
				// }
			}
			
			$this->pdf->Output();
		// }else{
			// echo "No se encontró el credito que esta buscando :'( ";
		// }
				
				
			// if(!empty($letras)) {
				
				// $totalCuota = $totalMora = $totalTotal = $totalPago = $totalSaldo = $totaldct = 0;
				 // $arrLetras = array();
				// $Totalizado_montopagado=$Totalizado_interespagado=$Totalizado_descuentopagado=0;
				// $Totalizado_saldoxpagar=$Totalizado_interesxpagar=$Totalizado_descuentoxpagar=0;
				// $letras_pagadas = array();
				// $this->pdf->SetDrawColor(204, 204, 204);
				
				// foreach($letras as $key => $val) {
								// $this->pdf->Ln();
								// $this->pdf->Cell(8,5,$key+1,1,1,'L');
								// $this->pdf->Cell(24,5,$val["imagen"],1,1,'L');
								// $this->pdf->Cell(50,5,utf8_decode($val["producto"]),1,1,'L');
								// $this->pdf->Cell(20,5,$val["marca"],1,1,'L');
								// $this->pdf->Cell(20,5,$val["modelo"],1,1,'L');
								// $this->pdf->Cell(18,5,$val["cantidad"],1,1,'L');
								// $this->pdf->Cell(18,5,$val["precio_u"],1,1,'L');
								// $this->pdf->Cell(18,5,$val["importe"],1,1,'L');
					
					// }

			 
		
			// }
		// $this->pdf->Output();
	}
	
	protected function getDataToPrint($idpreventa) {
		$sql = "select p.idpreventa, p.idcliente, p.fecha, p.subtotal, p.igv, p.subtotal+p.igv as total,
			p.garantia_equi, p.garantia_ser, p.condiciones_ser, p.descuento,
			p.referencia, t.descripcion as tipopago, m.descripcion as desmoneda, m.simbolo as moneda, c.nombres||coalesce(' '||c.apellidos,'') as cliente,
			us.nombres||coalesce(' '||us.appat,'')||coalesce(' '||us.apmat,'') as vendedor,
			c.dni, c.ruc, c.direccion_principal as direccion, p.n_modificacion
			from venta.preventa p
			join venta.forma_pago_cotizacion t on t.idforma_pago_cotizacion=p.idforma_pago_cotizacion
			join general.moneda m on m.idmoneda=p.idmoneda
			join venta.cliente c on c.idcliente=p.idcliente
			join seguridad.usuario AS us ON us.idusuario = p.idusuario
			where p.idpreventa=?";
		$query = $this->db->query($sql, array($idpreventa));
		return $query->row_array();
	}
	
	protected function getItemToPrint($idpreventa) {
		$sql = "SELECT dv.iddetalle_preventa, dv.idproducto, dv.idseccion, p.descripcion_detallada as producto, dv.descripcion_cli, 
			dv.idunidad, dv.descuento, dv.cantidad, coalesce(dv.precio,0.00) as precio, dv.oferta, s.descripcion as seccion, 
			--u.descripcion as unidad, dv.cantidad*coalesce(dv.precio,0.00) as importe, p.imagen_producto as imagen,
			u.descripcion as unidad, dv.cantidad*coalesce(dv.precio,0.00) - ((dv.descuento *(dv.cantidad*coalesce(dv.precio,0.00)))/100) as importe, p.imagen_producto as imagen,
			mo.descripcion as modelo, ma.descripcion as marca
			FROM venta.detalle_preventa dv
			JOIN compra.producto p ON p.idproducto = dv.idproducto
			JOIN compra.unidad u ON u.idunidad = dv.idunidad
			JOIN compra.seccion s ON s.idseccion = dv.idseccion
			JOIN general.modelo as mo ON mo.idmodelo = p.idmodelo
			JOIN general.marca as ma ON ma.idmarca = p.idmarca
			WHERE dv.estado = 'A' AND dv.idpreventa = ?
			ORDER BY idseccion, iddetalle_preventa";
		$query = $this->db->query($sql, array($idpreventa));
		$rs = $query->result_array();
		
		$res = array();
		foreach($rs as $row) {
			// validamos las imagen
			if( ! empty($row["imagen"])) {
				if(file_exists(FCPATH."app/img/producto/".$row["imagen"]))
					$row["imagen"] = base64_encode(file_get_contents(FCPATH."app/img/producto/".$row["imagen"]));
				else
					$row["imagen"] = null;
			}
			
			if( ! array_key_exists($row["idseccion"], $res))
				$res[$row["idseccion"]] = array("seccion"=>$row["seccion"], "total"=>0, "items"=>array());
			
			$res[$row["idseccion"]]["total"] += floatval($row["importe"]);
			$res[$row["idseccion"]]["items"][] = $row;
		}
		
		return $res;
	}
	
	public function cotizacion($idpreventa, $buildPdf=false, $returnContent=false){
	    $this->unlimit();
	    $this->load->library('Numeroletra');
		$this->load_model(array("seguridad.empresa","seguridad.sucursal"));
		
		$this->empresa->find($this->get_var_session("idempresa"));
		$this->sucursal->find($this->get_var_session("idsucursal"));
		
		$datos["empresa"] = $this->empresa->get_fields();
		$datos["sucursal"] = $this->sucursal->get_fields();
		$datos["cotizacion"] = $this->getDataToPrint($idpreventa);
		$datos["conceptos"] = $this->getItemToPrint($idpreventa);
		$datos["print"] = false;
		$datos["close"] = false;
		if( ! empty($datos["empresa"]["logo"])) {
			if(file_exists(FCPATH."app/img/empresa/".$datos["empresa"]["logo"]))
				$datos["logo"] = base64_encode(file_get_contents(FCPATH."app/img/empresa/".$datos["empresa"]["logo"]));
		}
		
		$buildPdf=true;
		if($buildPdf) {
			$datos["buildPdf"] = true;
			$html = $this->load->view("preventa/cotizacion", $datos, true);
			
			include_once APPPATH.'/libraries/Mpdf_print.php';
			$size = array(297,210);
			$mpdf = new Mpdf_print('c', $size, '', '', 10, 10);
			@$mpdf->WriteHTML($html);
			// $mpdf->AutoPrint();
			// echo $mpdf->Output("tmp.pdf",'S');exit;
			if($returnContent)
				return @$mpdf->Output("Cotizacion - ".$datos["cotizacion"]['idpreventa'].".pdf",'S');
			
			$mpdf->Output("Cotizacion - ".$datos["cotizacion"]['idpreventa'].".pdf",'I');
			return;
		}
		
		echo $this->load->view("preventa/cotizacion", $datos, true);
	}
	
	public function sendmail() {
		$post = $this->input->post();
		// print_r($post);exit;
		
		// $correos = explode(",", preg_replace("/\s+/", "", $post["correos"]));
		$correos = explode(";", preg_replace("/\s+/", "", $post["destinatario"]));
		foreach($correos as $correo) {
			if( ! is_email($correo)) {
				$this->exception("El correo electr&oacute;nico {$correo} no es v&aacute;lido");
				return;
			}
		}
		
		$this->unlimit();
		
		$this->load_model(array("seguridad.empresa", "venta.preventa", "seguridad.usuario", "venta.cliente", "seguridad.perfil"));
		
		$empresa = $this->empresa->find($this->get_var_session("idempresa"));
		$usuario = $this->usuario->find($this->get_var_session("idusuario"));
		$perfil = $this->perfil->find($this->get_var_session("idperfil"));
		$cotizacion = $this->preventa->find($post["idpreventa"]);
		//$cliente = $this->preventa->find->$this->cliente->find($post["idcliente"]);
			
		//$cliente = $this->getDataToPrint->find($post["idpreventa"]);
		//$cliente = $this->getDataToPrint($idpreventa);

		$datos["nom_tipodoc"] = "COTIZACION";
		//$datos["cliente"] = $cliente["cliente"];
		$datos["subtotal"] = $cotizacion["subtotal"];
		$datos["igv"] = $cotizacion["igv"];
		$datos["total"] = $cotizacion["subtotal"] + $cotizacion["igv"];
		$datos["num_docu"] = $cotizacion["idpreventa"];
		$datos["saludo"] = $cotizacion["saludo_email"];
		$datos["usuario"]= $usuario["nombres"].' '.$usuario["appat"].' '.$usuario["apmat"];
		$datos["email"]= $usuario["email"];
		$datos["telefono"]= $usuario["telefono"];
		$datos["perfil"]= $perfil["descripcion"];
		//$datos["cliente"]= $cliente["nombres"];
		$datos["empresa"] = $empresa["descripcion"];
		
		if( ! empty($empresa["logo"])) {
			if(file_exists(FCPATH."app/img/empresa/".$empresa["logo"]))
				$datos["logo"] = "data:image/png;base64, " . base64_encode(file_get_contents(FCPATH."app/img/empresa/".$empresa["logo"]));
		}
		
		$asunto = empty($post["asunto"]) ? $datos["nom_tipodoc"]." ".$datos["num_docu"] : $post["asunto"];
		$mensaje = empty($post["mensaje"]) ? $this->load->view('preventa/mail', $datos, true) : $post["mensaje"];
		
		$this->load_library("Jmail");
		$this->jmail->asunto($asunto);
		$this->jmail->mensaje($mensaje);
		
		foreach($correos as $correo) {
			$this->jmail->correo($correo, $correo);
		}
		
		$pdfContentFile = $this->cotizacion($post["idpreventa"], true, true);
		
		$this->jmail->archivo($pdfContentFile, $datos["nom_tipodoc"]." ".$datos["num_docu"].".pdf");
		
		if($this->jmail->send())
			$this->response(true);
		else
			$this->response($this->jmail->error());
	}
	
	protected function form_mail() {
		//$post = $this->input->post();
		// print_r($post);exit;
		$this->load_model(array("seguridad.empresa", "venta.preventa", "seguridad.usuario", "venta.cliente", "seguridad.perfil"));
		
		$empresa = $this->empresa->find($this->get_var_session("idempresa"));
		$usuario = $this->usuario->find($this->get_var_session("idusuario"));
		$perfil = $this->perfil->find($this->get_var_session("idperfil"));
		 //$cotizacion = $this->preventa->find($post["id"]);
		
		$datos["nom_tipodoc"] = "COTIZACION";
		 // $datos["subtotal"] = $cotizacion["subtotal"];
		// $datos["igv"] = $cotizacion["igv"];
		// $datos["total"] = $cotizacion["subtotal"] + $cotizacion["igv"];
		// $datos["num_docu"] = $cotizacion["idpreventa"];
		// $datos["saludo"] = $cotizacion["saludo_email"];
		$datos["usuario"]= $usuario["nombres"].' '.$usuario["appat"].' '.$usuario["apmat"];
		$datos["email"]= $usuario["email"];
		$datos["telefono"]= $usuario["telefono"];
		$datos["perfil"]= $perfil["descripcion"];
		$datos["empresa"] = $empresa["descripcion"];
		
		if( ! empty($empresa["logo"])) {
			if(file_exists(FCPATH."app/img/empresa/".$empresa["logo"]))
				$datos["logo"] = "data:image/png;base64, " . base64_encode(file_get_contents(FCPATH."app/img/empresa/".$empresa["logo"]));
		}

		$params = array(
			"title" => "Enviar cotizaci&oacute;n"
			,"width" => "modal-lg"
			,"buttons" => array(
				'<button id="btnSendMail" type="button" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Enviar</button>'
				,'<button id="btnCancelMail" type="button" class="btn btn-white btn-sm"><i class="fa fa-times"></i> Cancelar</button>'
			)
		);
		
		$this->css('plugins/summernote/summernote');
		$this->css('plugins/summernote/summernote-bs3');
		$this->js("plugins/summernote/summernote.min");
		
		// return $this->get_modal($this->load->view($this->controller."/form_mail", false, true), $params, "dialogMail");
		return $this->get_modal($this->load->view($this->controller."/form_mail", $datos, true), $params, "dialogMail");
	}
}
