<?php

include_once "Controller.php";

class Ajustes extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("FORMULARIO MAESTRO DE CONFIGURACIONES PRINCIPALES");
		//$this->set_subtitle("Lista de taman&ntilde;os / tallas");
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null, $prefix = "", $modal = false) {
		if(!is_array($data)) {
			$data = array();
		}
		$data["controller"] = $this->controller;
		$data["controller"] = $this->controller;
		$data["prefix"] = $prefix;
		$data["modal"] = $modal;
		$data["terminos"] = $this->get_param("123456");
		//modals
		// formulario ALMACEN
		$this->load_controller("almacen");
		$data["form_almacen"] = $this->almacen_controller->form(null, "alm_", true);
		$this->js('form/almacen/modal');
		//::::::::::::fin almacen :::::::::::
		$this->js('form/ajustes/index', true, true, $data);
		if($modal === true) {
			$this->js('form/ajustes/modal', true, true, $data);
			return $this->modal($this->load->view($this->controller."/form", $data, true));
		}
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		return null;
	}
	public function index($tpl = "") {
		$data = array(
			"menu_title" => $this->menu_title
			,"menu_subtitle" => $this->menu_subtitle
			,"content" => $this->form()
			,"with_tabs" => $this->with_tabs
		);
		
		if($this->show_path) {
			$data['path'] = $this->get_path();
		}
		
		$str = $this->load->view("content", $data, true);
		$this->show($str);
	}
	// public function grilla() {
		///////////// cargamos el modelo y la libreria
		// $this->load_model("general.ajustes");
		// $this->load->library('datatables');
		
		///////////indicamos el modelo al datatables
		// $this->datatables->setModel($this->ajustes);
		
		////////// filtros adicionales para la tabla de la bd (perfil en este caso)
		// $this->datatables->where('estado', '=', 'A');
		
		///////////// indicamos las columnas a mostrar de la tabla de la bd
		// $this->datatables->setColumns(array('descripcion'));
		
		////////// columnas de la tabla, si no se envia este parametro, se muestra el 
		////////// nombre de la columna de la tabla de la bd
		// $columnasName = array(
			// 'Descripci&oacute;n'
		// );

		////////////// generamos la tabla y el script para el dataTables
		// $table = $this->datatables->createTable($columnasName);
		////// $table = $this->datatables->createTable();
		// $script = "<script>".$this->datatables->createScript()."</script>";
		// $this->js($script, false);
		
		// return $table;
	// }
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar ajustes");
		$this->set_subtitle("");
		$this->set_content($this->form());
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model("general.ajustes");
		$data = $this->ajustes->find($id);
		
		$this->set_title("Modificar ajustes");
		$this->set_subtitle("");
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model("general.ajustes");
		
		$fields = $this->input->post();
		$fields['estado'] = "A";
		if(empty($fields["idajustes"])) {
			if($this->ajustes->exists(array("descripcion"=>$fields["descripcion"])) == false) {
				$this->ajustes->insert($fields);
			}
			else {
				$this->exception("El ajustes ".$fields["descripcion"]." ya existe");
			}
		}
		else {
			$this->ajustes->update($fields);
		}
		
		$this->response($this->ajustes->get_fields());
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model("general.ajustes");
		
		// cambiamos de estado
		$fields['idajustes'] = $id;
		$fields['estado'] = "I";
		$this->ajustes->update($fields);
		
		$this->response($fields);
	}
	
	public function options() {
		$query = $this->db->where("estado", "A")
			->order_by("descripcion", "asc")
			->get("general.ajustes");
		
		$html = '';
		if($query->num_rows() > 0) {
			$fo = "true";
			if($this->input->post("first_option") == "false") {
				$fo = "false";
			}
			if($fo == "true") {
				$html .= '<option value=""></option>';
			}
			foreach($query->result() as $row) {
				$html .= '<option value="'.$row->idajustes.'">'.$row->descripcion.'</option>';
			}
		}
		
		$this->response($html);
	}
	
	public function autocomplete() {
		$txt = trim($this->input->post("startsWith"));
		$txt = "%".preg_replace('/\s+/', '%', $txt)."%";
		
		$sql = "SELECT idajustes, descripcion
			FROM general.ajustes
			WHERE estado='A' and (descripcion ILIKE ?)
			ORDER BY descripcion
			LIMIT ?";
		$query = $this->db->query($sql, array($txt, $this->input->post("maxRows")));
		$this->response($query->result_array());
	}
	
	public function modal($form) {
		$params = array(
			"title" => "Registrar ".$this->controller
			,"width" => "modal-sm"
			,"buttons" => array()
		);
		
		return $this->get_modal($form, $params, "modal-".$this->controller);
	}
	
	/**
	 * Metodo para obtener los datos del producto
	 */
	public function Get_terminos() {
		//$idsucursal = $this->get_var_session("idsucursal");
		
		$sql = "SELECT p.idparam, p.valor, p.descripcion, p.tipo
				FROM seguridad.param as p
				WHERE p.idparam = '123456'";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		$this->response($data);
	}
	
}
?>