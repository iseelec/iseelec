<?php

include_once "Controller.php";

class Kardex extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Kardex de Almacen");
		$this->set_subtitle("Movimientos de los productos");
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		// $this->js('form/'.$this->controller.'/index');
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		
	}
	
	
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		$data = array();				
		$this->load->library('combobox');
						
		// combo AÑO
		$this->combobox->init(); // un nuevo combo
		$anio = date("Y");
		$year = array();
		$t=0;
		for($y=2010;$y<=$anio;$y++){ 
			$year[$t] = array($y);
			$t++;
		}		
		$this->combobox->setAttr("id", "anio");
		$this->combobox->setAttr("name", "anio");
		$this->combobox->setAttr("class", "form-control");
		$this->combobox->addItem($year);		
		if( isset($anio) ) {
			$this->combobox->setSelectedOption($anio);
		}
		$data["annio"] = $this->combobox->getObject();
		
		
		//combo periodo
		$periodo = date("m");
		$t=0;
		for($i=0;$i<12;$i++){
			$mes = str_pad($i+1,2,'0', STR_PAD_LEFT);			
			$year[$t] = array($mes);
			$t++;
		}
					  
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr("id", "periodo");
		$this->combobox->setAttr("name", "periodo");
		$this->combobox->setAttr("class", "form-control");
		$this->combobox->addItem($year);		
		if( isset($periodo) ) {
			$this->combobox->setSelectedOption($periodo);
		}
		$data["periodo"] = $this->combobox->getObject();

		// combo almacen inicio
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idalmacen_i"
				,"name"=>"idalmacen_i"
				,"class"=>"form-control"
			)
		);
		$query = $this->db->select('idalmacen, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))->get("almacen.almacen");
		$this->combobox->addItem($query->result_array());
		if( isset($data["compra"]["idalmacen"]) ) {
			$this->combobox->setSelectedOption($data["compra"]["idalmacen"]);
		}
		$data["almacen_i"] = $this->combobox->getObject();
		
		// combo almacen fin
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idalmacen_f"
				,"name"=>"idalmacen_f"
				,"class"=>"form-control"
			)
		);
		$query = $this->db->select('idalmacen, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))->get("almacen.almacen");
		$this->combobox->addItem($query->result_array());
		if( isset($data["compra"]["idalmacen"]) ) {
			$this->combobox->setSelectedOption($data["compra"]["idalmacen"]);
		}
		$data["almacen_f"] = $this->combobox->getObject();
					
		$data["controller"] = $this->controller;		

		
	
			
		$this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
		$this->css("plugins/datapicker/datepicker3");
		// $this->css('plugins/iCheck/custom');
		$this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		// $this->js('plugins/iCheck/icheck.min');
		$this->js('form/'.$this->controller.'/form');
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	public function reporte_kardex() {
		
			
		// $id = $this->input->post('id');
		// $idcompra = $this->input->post("idcompra");

		// echo "ddddddddgg_".$id;
		
		/*$lsCol = explode('_', $datos);
		$idproveedor = $lsCol[0];
		$idproducto = $lsCol[1];
		$anio = $lsCol[3];
		$periodo = $lsCol[4];
		$idalmacen_i = $lsCol[5];
		// $fecha = $lsCol[6];
		// $idalmacen_f = $lsCol[7];
		// $opc_tipo = $lsCol[8];
		
		
		// $post = $this->input->get_fields();q
		echo $anio;
		echo $periodo;
		echo $idalmacen_i;*/
				
		
		$this->load_model(array("almacen.kardex", "seguridad.sucursal", "seguridad.empresa"));
		
			
		$this->load->library("pdf");		
		// $this->pdf->SetLogo(FCPATH."app/img/empresa/".$this->empresa->get("logo"));
		$this->pdf->SetTitle(utf8_decode("HISTORIAL DE MOVIMIENTOS DE STOCK DE PRODUCTOS"), 11, null, true);
		$this->pdf->AliasNbPages(); // para el conteo de paginas
		$this->pdf->AddPage("L");

		
		$sql2 = "SELECT k.correlativo, k.idproducto, p.descripcion as producto, u.abreviatura as u_medida, a.idalmacen, 
				a.descripcion as almacen, t.tipo_movimiento, t.tipo, t.descripcion desc_tipo, k.tabla, k.fecha_emision, k.hora, k.observacion, 
				k.idreferencia, k.tipo_docu, k.serie, k.numero, k.cantidad, k.costo_unit_s, k.importe_s
				FROM almacen.kardex k
				INNER JOIN compra.producto p ON p.idproducto = k.idproducto
				INNER JOIN compra.unidad u ON u.idunidad = k.idunidad
				INNER JOIN almacen.almacen a ON a.idalmacen = k.idalmacen
				INNER JOIN almacen.tipo_movimiento t ON t.tipo_movimiento = k.tipo_movimiento
				WHERE k.estado= 'A'
				ORDER BY k.idproducto, a.idalmacen, k.fecha_emision, k.hora";
		$query2 = $this->db->query($sql2);
		
		//DETALLE
		$this->pdf->SetFont('Arial','',7);	
		$this->pdf->SetLeftMargin(4);		
		$a = 6;
		
		$this->pdf->SetXY(4,25);
		$this->pdf->Cell(16,$a,'FECHA',1,0,'C');
		$this->pdf->Cell(15,$a,'HORA',1,0,'C');
		$this->pdf->Cell(50,$a,'TIPO MOVIMIENTO',1,0,'C');
		$this->pdf->Cell(65,$a,'OBSERVACIONES',1,0,'C');
		$this->pdf->Cell(17,$a,'Nro. KARDEX',1,0,'C');
		$this->pdf->Cell(16,$a,'ID. REFER.',1,0,'C');
		$this->pdf->Cell(10,$a,'TIPO',1,0,'C');
		$this->pdf->Cell(12,$a,'SERIE',1,0,'C');
		$this->pdf->Cell(15,$a,'NUMERO',1,0,'C');
		$this->pdf->Cell(18,$a,'ENTRADAS',1,0,'C');
		$this->pdf->Cell(18,$a,'SALIDAS',1,0,'C');
		$this->pdf->Cell(20,$a,'SALDO FINAL',1,0,'C');
		$this->pdf->Cell(13,$a,'U.M.',1,0,'C');
		
		$this->pdf->Ln();
		// $this->pdf->SetX(8);
		
		$item = $dsit = $alma = $dsal = '';
		// $this->SetFillColor(255,0,0);
		// $this->SetTextColor(0);
		// $this->SetDrawColor(128,0,0);
		// $this->SetLineWidth(.3);
		$suma_cant = 0;
		
		foreach ($query2->result() as $row){
			
			if($item != $row->idproducto){
				if($item!=''){
					$this->pdf->SetFont('','B',7);
					$this->pdf->Cell(216,5,'TOTALES',0,0,'R');
					$this->pdf->Cell(18,5,number_format($suma_cant_ent,2,'.',''),'LRBT',0,'R');
					$this->pdf->Cell(18,5,number_format($suma_cant_sal,2,'.',''),'LRBT',0,'R');
					$this->pdf->Cell(20,5,number_format($suma_cant,2,'.',''),'LRBT',0,'R');
					$this->pdf->Cell(13,5,'',0,0,'R');
					$this->pdf->Ln();
				}
				$item = $row->idproducto;
				$dsit = $row->producto;
		  
				$this->pdf->Ln();
				$this->pdf->SetFont('','B',7);
				$this->pdf->Cell(273,5,'ITEM : '.strtoupper($item.' '.$dsit),'',0,'L');
				$this->pdf->Ln();
		  
				$alma = $row->idalmacen;
				$dsal = $row->almacen;
				$suma_cant = $suma_cant_ent = $suma_cant_sal = 0;
				// $this->pdf->Ln();
				$this->pdf->Cell(285,5,'Almacen : '.$alma.' '.$dsal,1,1);
			}else{
				$item = $row->idproducto;
				$dsit = $row->producto;
		  
				if($alma != $row->idalmacen){
					if($alma != ''){
						$this->pdf->SetFont('','B',7);
						$this->pdf->Cell(201,5,'TOTALES',0,0,'R');
						$this->pdf->Cell(20,5,$suma_cant_ent,'LRBT',0,'R');
						$this->pdf->Cell(20,5,$suma_cant_sal,'LRBT',0,'R');
						$this->pdf->Cell(20,5,$suma_cant,'LRBT',0,'R');
						$this->pdf->Cell(12,5,'',0,0,'R');
						$this->pdf->Ln();
					}
					$alma = $row->idalmacen;
					$dsal = $row->almacen;
				
					$suma_cant = $suma_cant_ent = $suma_cant_sal = 0;
					$this->pdf->Ln();
					$this->pdf->SetFont('','B',7);
					$this->pdf->Cell(273,5,'Almacen : '.$alma.' '.$dsal,1,1);
				}else{
					$alma = $row->idalmacen;
					$dsal = $row->almacen;
				}
			}
			$this->pdf->SetFont('','',7);
			$this->pdf->Cell(16,5,$row->fecha_emision,'LRBT',0,'C');
			$this->pdf->Cell(15,5,$row->hora,'LRBT',0,'C');
			$this->pdf->Cell(50,5,$row->desc_tipo,'LRBT',0,'L');
			$this->pdf->SetFont('','',6);
			$this->pdf->Cell(65,5,substr($row->observacion,0,45),'LRBT',0,'L');
			$this->pdf->SetFont('','',7);
			$this->pdf->Cell(17,5,$row->correlativo,'LRBT',0,'C');
			$this->pdf->Cell(16,5,$row->idreferencia,'LRBT',0,'C');
			$this->pdf->Cell(10,5,$row->tipo_docu,'LRBT',0,'C');
			$this->pdf->Cell(12,5,$row->serie,'LRBT',0,'C');
			$this->pdf->Cell(15,5,$row->numero,'LRBT',0,'L');
		
			if($row->tipo == 'ENT'){
				$this->pdf->Cell(18,5,number_format($row->cantidad,2,'.',''),'LRBT',0,'R');
				$this->pdf->Cell(18,5,'','LRBT',0,'L');
				$varsum = (float) $row->cantidad;
				$suma_cant_ent = $suma_cant_ent + $varsum;		  
				$suma_cant = $suma_cant + $varsum;
			}elseif($row->tipo == 'SAL'){
				$this->pdf->Cell(18,5,'','LRBT',0,'L');
				$this->pdf->Cell(18,5,$row->cantidad,'LRBT',0,'R');
				$varsum = (float) $row->cantidad;
				$suma_cant_sal = $suma_cant_sal + $varsum;
				$suma_cant = $suma_cant - $varsum;
			}
			$this->pdf->Cell(20,5,number_format($suma_cant,4,'.',''),'LRBT',0,'R');
			$this->pdf->Cell(13,5,$row->u_medida,'LRBT',0,'C');
			$this->pdf->Ln();
		}
		
		if ($query2->num_rows() > 0){
			$this->pdf->SetFont('','B');
			$this->pdf->Cell(216,5,'TOTALES',0,0,'R');
			$this->pdf->Cell(18,5,number_format($suma_cant_ent,2,'.',''),'LRBT',0,'R');
			$this->pdf->Cell(18,5,number_format($suma_cant_sal,2,'.',''),'LRBT',0,'R');
			$this->pdf->Cell(20,5,number_format($suma_cant,2,'.',''),'LRBT',0,'R');
			$this->pdf->Cell(13,5,'',0,0,'R');
			$this->pdf->Ln();
		}
		$this->pdf->Output();
	}
}
?>