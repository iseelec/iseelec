<?php

include_once "Controller.php";

class Compra extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Mantenimiento de Compra");
		$this->set_subtitle("Lista de compras");
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		// $this->js('form/'.$this->controller.'/index');
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		
		$this->load->library('combobox');
		
		// combo tipo compra
		$this->combobox->setAttr("id", "idtipoventa");
		$this->combobox->setAttr("name", "idtipoventa");
		$this->combobox->setAttr("class", "form-control");
		$this->combobox->setAttr("required", "");
		
		$this->db->select('idtipoventa, descripcion');
		$query = $this->db->where("estado", "A")->where("mostrar_en_compra", "S")->get("venta.tipo_venta");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["compra"]["idtipoventa"]) ) {
			$this->combobox->setSelectedOption($data["compra"]["idtipoventa"]);
		}
		
		$data["tipocompra"] = $this->combobox->getObject();
		
		// combo tipodocumento
		$this->combobox->init(); // un nuevo combo
		
		$this->combobox->setAttr(
			array(
				"id"=>"idtipodocumento"
				,"name"=>"idtipodocumento"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$this->db->select('idtipodocumento, descripcion');
		$query = $this->db->where("estado", "A")->where("mostrar_en_compra", "S")->get("venta.tipo_documento");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["compra"]["idtipodocumento"]) ) {
			$this->combobox->setSelectedOption($data["compra"]["idtipodocumento"]);
		}
		
		$data["tipodocumento"] = $this->combobox->getObject();
		
		// combo almacen
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idalmacen"
				,"name"=>"idalmacen"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$query = $this->db->select('idalmacen, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))->get("almacen.almacen");
		$this->combobox->addItem($query->result_array());
		if( isset($data["compra"]["idalmacen"]) ) {
			$this->combobox->setSelectedOption($data["compra"]["idalmacen"]);
		}
		$data["almacen"] = $this->combobox->getObject();
		
		// combo moneda
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idmoneda"
				,"name"=>"idmoneda"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$query = $this->db->select('idmoneda, descripcion')->where("estado", "A")->order_by("idmoneda", "asc")->get("general.moneda");
		$this->combobox->addItem($query->result_array());
		if( isset($data["compra"]["idmoneda"]) ) {
			$this->combobox->setSelectedOption($data["compra"]["idmoneda"]);
		}
		$data["moneda"] = $this->combobox->getObject();
		
		
		// combo tipo pago
		// $this->combobox->init();
		// $this->combobox->setAttr("id", "idtipopago");
		// $this->combobox->setAttr("name", "idtipopago");
		// $this->combobox->setAttr("class", "form-control");
		// $this->combobox->setAttr("required", "");
		// $this->db->select('idtipopago, descripcion');
		// $query = $this->db->where("estado", "A")->where("mostrar_en_venta", "S")->get("venta.tipopago");
		// $this->combobox->addItem($query->result_array());
		// if( isset($data["venta"]["idtipopago"]) ) {
			// $this->combobox->setSelectedOption($data["venta"]["idtipopago"]);
		// }
		// $data["tipopago"] = $this->combobox->getObject();

		// combo concepto Movimiento
		// $this->combobox->init(); // un nuevo combo
		// $this->combobox->setAttr(
			// array(
				// "id"=>"idconceptomovimiento"
				// ,"name"=>"idconceptomovimiento"
				// ,"class"=>"form-control"
				// ,"required"=>""
			// )
		// );
		// $query = $this->db->select('idconceptomovimiento, descripcion')->where("estado", "A")->where("ver_compra", "S")->get("caja.conceptomovimiento");
		// $this->combobox->addItem($query->result_array());
		// if( isset($data["idconceptomovimiento"]) ) {
			// $this->combobox->setSelectedOption($data["idconceptomovimiento"]);
		// }
		// $data["movimiento"] = $this->combobox->getObject();
		// combo concepto Movimiento

		// combo cuentasbancarias
		// $this->combobox->init(); // un nuevo combo
		// $this->combobox->setAttr(
			// array(
				// "id"=>"idcuentas_bancarias"
				// ,"name"=>"idcuentas_bancarias"
				// ,"class"=>"form-control"
				// ,"required"=>""
				// ,"type-name"=>"idcuentas_bancarias"
			// )
		// );
		// $query = $this->db->select('idcuentas_bancarias, cuenta')->where("estado", "A")->where("idsucursal", $this->get_var_session("idsucursal"))->get("general.view_cuentas_bancarias");
		// $this->combobox->addItem("");
		// $this->combobox->addItem($query->result_array());
		// if( isset($data["idcuentas_bancarias"]) ) {
			// $this->combobox->setSelectedOption($data["idcuentas_bancarias"]);
		// }
		// $data["cuentas_bancarias"] = $this->combobox->getObject();
		// combo cuentasbancarias

		// combo tarjeta
		// $this->combobox->init(); // un nuevo combo
		// $this->combobox->setAttr(
			// array(
				// "id"=>"idtarjeta"
				// ,"name"=>"idtarjeta"
				// ,"class"=>"form-control"
				// ,"required"=>""
				// ,"type-name"=>"idtarjeta"
			// )
		// );
		// $query = $this->db->select('idtarjeta, descripcion')->where("estado", "A")->get("general.tarjeta");
		// $this->combobox->addItem($query->result_array());
		// if( isset($data["idtarjeta"]) ) {
			// $this->combobox->setSelectedOption($data["idtarjeta"]);
		// }
		// $data["tarjeta"] = $this->combobox->getObject();
		// combo tarjeta
		
		$data["controller"] = $this->controller;
		
		$igv = $this->get_param("igv");
		if(!is_numeric($igv)) {
			$igv = 18;
		}
		$data["valor_igv"] = $igv;
		
		$es_nuevo = "true";
		if( isset($data["compra"]["idcompra"]) ) {
			$es_nuevo = "false";
		}
		$this->js("<script>var _es_nuevo_".$this->controller."_ = $es_nuevo;</script>", false);
		
		$this->load_controller("producto");
		// $this->producto_controller->load = $this->load;
		// $this->producto_controller->db = $this->db;
		// $this->producto_controller->session = $this->session;
		// $this->producto_controller->combobox = $this->combobox;
		
		$data["form_producto"] = $this->producto_controller->form(null, "", true);
		$data["form_producto_unidad"] = $this->producto_controller->form_unidad_medida(null, "", true);
		$data["modal_pago"] = $this->get_form_pago("compra", true);
		
		$fc = $this->get_param("fixed_compra");
		if(!is_numeric($fc)) {
			$fc = 2;
		}
		$this->js("<script>var _fixed_compra = $fc;</script>", false);
		
		$es_nuevo = "true";
		if( isset($data["compra"]["idcompra"]) ) {
			$es_nuevo = "false";
		}
		$this->js("<script>var _es_nuevo_".$this->controller."_ = $es_nuevo;</script>", false);
		
		if( isset($data["detalle"]) ) {
			$this->js("<script>var data_detalle = ".json_encode($data["detalle"]).";</script>", false);
		}
		
		$this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
		$this->css("plugins/datapicker/datepicker3");
		// $this->css('plugins/iCheck/custom');
		$this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		// $this->js('plugins/iCheck/icheck.min');
		$this->js('form/'.$this->controller.'/form');
		$this->js('form/producto/modal');
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		// $this->load_model($this->controller);
		$this->load_model("compra.compra_view");
		$this->load->library('datatables');
		
		$this->datatables->setModel($this->compra_view);
		
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		
		$this->datatables->setColumns(array('fecha_compra','tipo_documento',
			'nrodocumento','proveedor','tipoventa','moneda','subtotal','igv','total'));
		
		$this->datatables->setCallback("verificarRecepcionados");
		
		$columnasName = array('F.Compra','Documento','Nro.Documento','Proveedor',
			'Tipo compra','Moneda','Subtotal','IGV','Total');

		$table = $this->datatables->createTable($columnasName);
		$script = "<script>".$this->datatables->createScript()."</script>";
		
		$this->css('plugins/dataTables/dataTables.bootstrap');
		$this->css('plugins/dataTables/dataTables.responsive');
		$this->css('plugins/dataTables/dataTables.tableTools.min');
		
		$this->js('plugins/dataTables/jquery.dataTables');
		$this->js('plugins/dataTables/dataTables.bootstrap');
		$this->js('plugins/dataTables/dataTables.responsive');
		$this->js('plugins/dataTables/dataTables.tableTools.min');
		$this->js($script, false);
		
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Compra");
		$this->set_subtitle("");
		
		$this->set_content($this->form());
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model($this->controller);
		$data["compra"] = $this->compra->find($id);
		$data["total"] = $data["compra"]["subtotal"]+$data["compra"]["igv"];
		
		$this->load_model("detalle_compra");
		$data["detalle"] = $this->detalle_compra->get_items($id);
		
		$this->load_model("proveedor");
		$data["proveedor"] = $this->proveedor->find($data["compra"]["idproveedor"]);
		
		$this->set_title("Modificar Compra");
		$this->set_subtitle("");
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model($this->controller);
		
		$fields = $this->input->post();
		// echo $fields["fecha_compra"];exit;
		
		$fields['idsucursal'] = $this->get_var_session("idsucursal");
		$fields['idusuario'] = $this->get_var_session("idusuario");
		$fields['fecha_registro'] = date("Y-m-d");
		$fields['recepcionado'] = (!empty($fields['recepcionado'])) ? "S" : "N";
		$fields['afecta_caja'] = (!empty($fields['afecta_caja'])) ? "S" : "N";
		$fields['estado'] = "A";
		if(empty($fields["descuento"]))
			$fields["descuento"] = 0;
		if(empty($fields["gastos"]))
			$fields["gastos"] = 0;
		if(empty($fields["flete"]))
			$fields["flete"] = 0;
		if(empty($fields["igv"]))
			$fields["igv"] = 0;
		if(empty($fields["fecha_compra"]))
			$fields["fecha_compra"] = date("Y-m-d");

		$esNuevaCompra=true;

		//DATOS PARA KARDEX-->INICIO
		$status = 'A';
		if($fields['recepcionado']=='S'){
			$status = 'C';
		}
		$precio_unit_venta_s = number_format(0,4, '.', '');
		$precio_unit_venta_d = number_format(0,4, '.', '');
		//--->FIN
		
		
		$this->db->trans_start(); // inciamos transaccion
		
		if(empty($fields["idcompra"])) {
			$idcompra = $this->compra->insert($fields);

			$this->load_model("detalle_compra");
			$this->load_model("detalle_compra_serie");
			$this->load_model("detalle_almacen");
			$this->load_model("detalle_almacen_serie");
			
			// detalle compra
			$data1["idcompra"] = $idcompra;
			$data1["estado"] = 'A';
			$data1["recepcionado"] = $fields["recepcionado"];
			
			if($fields['recepcionado'] == "S") {
				// detalle almacen
				$data2["idalmacen"] = $fields["idalmacen"];
				$data2["tipo"] = "E";
				$data2["tipo_number"] = 1;
				$data2["fecha"] = date("Y-m-d");
				$data2["tabla"] = "C";
				$data2["idtabla"] = $idcompra;
				$data2["estado"] = "A";
				$data2["despachado"] = "N";
			}
			
			foreach($fields["deta_idproducto"] as $key=>$val) {
				$data1["idproducto"] = $val;
				$data1["idunidad"] = $fields["deta_idunidad"][$key];
				$data1["cantidad"] = intval($fields["deta_cantidad"][$key]);
				$data1["precio"] = floatval($fields["deta_precio"][$key]);
				$data1["igv"] = floatval($fields["deta_igv"][$key]);
				$data1["flete"] = floatval($fields["deta_flete"][$key]);
				$data1["gastos"] = floatval($fields["deta_gastos"][$key]);
				$data1["costo"] = floatval($fields["deta_costo"][$key]);
				$this->detalle_compra->insert($data1);
				
				if(!empty($fields["deta_series"][$key])) {
					$arr = explode(",", $fields["deta_series"][$key]);
					foreach($arr as $serie) {
						$data1["serie"] = $serie;
						$this->detalle_compra_serie->insert($data1, false);
					}
				}
				
				if($fields['recepcionado'] == "S") {
					$data2["idproducto"] = $val;
					$data2["idunidad"] = $fields["deta_idunidad"][$key];
					$data2["cantidad"] = $fields["deta_cantidad"][$key];
					$data2["precio_costo"] = $fields["deta_costo"][$key];
					$this->detalle_almacen->insert($data2);
				}
				
				if(!empty($fields["deta_series"][$key]) && $fields['recepcionado'] == "S") {
					$arr = explode(",", $fields["deta_series"][$key]);
					foreach($arr as $serie) {
						$data2["serie"] = $serie;
						$this->detalle_almacen_serie->insert($data1, false);
					}
				}
				
				
				//INGRESO A RECEPCION Y A KARDEX
				if($fields['serie'] !='' && $fields['nrodocumento']!='' ){
					// $tipo_movimiento = 1;
					$fields2['idcompra'] = $idcompra;
					$fields2['idproducto'] = $val;
					$fields2['idalmacen']= $fields["idalmacen"];
					$fields2['cant_recepcionada']= $data1["cantidad"];
					$fields2['tipo_docu'] = $fields['idtipodocumento'];
					$fields2['serie'] = $fields['serie'];
					$fields2['numero'] = $fields['nrodocumento'];					
					$fields2['observacion'] = "RECEPCION AUTOMATICA DE OC NRO. ".$idcompra;					
					$fields2['estado'] = $status;					
					$fields2['fecha'] = date("Y-m-d");
					$fields2['hora'] = date("H:i:s");
					$fields2['idusuario'] = $this->get_var_session("idusuario");					
											
					//Preparamos para hacer el ingreso de la recepcion al kardex
					// tipo_movimiento=1 es para  compras
					// $sql = "SELECT* FROM almacen.tipo_movimiento WHERE tipo_movimiento=$tipo_movimiento";
					// $query = $this->db->query($sql);
					// $query = $query->result_array();
											
					// $tip_movimiento = $query[0]['tipo_movimiento'];//tipo movimiento como ingresra al karde 1 -> compras
					// $correlativo = $query[0]['correlativo']; // correlativo, este numero se vera refleado en el kardex
					// $fields2['correlativo'] = $correlativo;
					$correlativo = $this->get_next_correlativo("comp"); // correlativo, este numero se vera refleado en el kardex
					$fields2['correlativo'] = $correlativo;
						
					$this->load_model("recepcion");
					$idrecepcion = $this->recepcion->insert($fields2);
						
					// registramos movimiento kardex
					if($fields['recepcionado']=='S'){//INGRESO A ALMACEN	
					
						$precio = $data1["precio"];
						$igv = $data1["igv"];
						$flete = $data1["flete"];
						$gastos = $data1["gastos"];
						
						$tipo_cambio= $fields["cambio_moneda"];						
						if(empty($tipo_cambio)) {$tipo_cambio = 1;}
												
						if($fields['idmoneda']==2){//'USD'
							$costo_unit_d = $precio+$igv+$flete+$gastos;
							$costo_unit_s = $costo_unit_d*$tipo_cambio;		
							
							$costo_unit_d = number_format($costo_unit_d,4, '.', '');				
							$costo_unit_s = number_format($costo_unit_s,4, '.', '');				
													
						}else if($fields['idmoneda']==1){//'PEN'
							$costo_unit_s = $precio+$igv+$flete+$gastos;
							$costo_unit_d = number_format($costo_unit_s/$tipo_cambio,4, '.', '');
							
							$costo_unit_d = number_format($costo_unit_d,4, '.', '');				
							$costo_unit_s = number_format($costo_unit_s,4, '.', '');
						}						
						$importe_s = number_format($costo_unit_s*$data1["cantidad"],4, '.', '');
						$importe_d = number_format($costo_unit_d*$data1["cantidad"],4, '.', '');
					
						$this->load_kardex("kardex","libreria_kardex");
						$this->libreria_kardex->correlativo = $correlativo;
						$this->libreria_kardex->ingreso(true);
						$this->libreria_kardex->idproducto = $val;			
						$this->libreria_kardex->idalmacen = $fields['idalmacen'];
						$this->libreria_kardex->idunidad = $data2["idunidad"];
						$this->libreria_kardex->cantidad = number_format($data1["cantidad"],2, '.', ''); 						
						$this->libreria_kardex->precio_unit_venta_s = $precio_unit_venta_s;
						$this->libreria_kardex->precio_unit_venta_d = $precio_unit_venta_d;						
						$this->libreria_kardex->costo_unit_s = $costo_unit_s;
						$this->libreria_kardex->costo_unit_d = $costo_unit_d;				
						$this->libreria_kardex->importe_s = $importe_s;
						$this->libreria_kardex->importe_d = $importe_d;
						$this->libreria_kardex->idreferencia = $idcompra;
						$this->libreria_kardex->idtercero = $fields['idproveedor'];
						$this->libreria_kardex->tipo_docu = $fields["idtipodocumento"];
						$this->libreria_kardex->serie = $fields['serie'];
						$this->libreria_kardex->numero = $fields['nrodocumento'];
						$this->libreria_kardex->observacion = '';
						$this->libreria_kardex->estado = "C";
						$this->libreria_kardex->referencia('COMP');					
						$this->libreria_kardex->run();								
					}
				}
			}

			if(  $fields["afecto_caja"] == 'S' && ($fields["idtipoventa"] == 1 || $fields["idtipoventa"] == 3) ) {//AQUI DEBERIA SER 1
					$fields['total'] = ($fields['total']) * (-1);
					if($esNuevaCompra) {
						$idconceptomovimiento = $fields['idconceptomovimiento'];
						$this->load_controller("caja");
						$this->caja_controller->idusuario = $fields['idusuario'];
						$this->caja_controller->idsucursal = $this->get_var_session("idsucursal");
						$this->caja_controller->db = $this->db;
						$this->caja_controller->egresoCaja($idconceptomovimiento
							, $fields['total']
							, 'COMPRA AL CONTADO'
							, $fields['proveedor']
							, $this->controller
							, $idcompra
							, $fields['idmoneda']
							, $fields["cambio_moneda"]
							, $fields["idtipodocumento"]
							, $fields['idproveedor'] 
							, $fields["serie"]
							, $fields["nrodocumento"] 
							, $this->get_var_session("idsucursal")
							, $fields["idtipopago"] );
					}

					$this->db->query("UPDATE venta.movimiento_tarjeta SET estado='I' WHERE idsucursal='{$fields["idsucursal"]}' AND idoperacion='{$idcompra}' AND tabla='".strtoupper($this->controller)."' ");
					$this->db->query("UPDATE venta.movimiento_deposito SET estado='I' WHERE idsucursal='{$fields["idsucursal"]}' AND idoperacion='{$idcompra}' AND tabla='".strtoupper($this->controller)."' ");
					if (isset($fields['idtarjeta'])) {// PAGO TARJETA
						$this->load_model("movimiento_tarjeta");
						$data_mov = $this->movimiento_tarjeta->find($fields['idsucursal'], $fields["idtarjeta"], $idcompra);

						$data2["idsucursal"]  = $this->get_var_session("idsucursal");
						$data2["idusuario"]   = $this->get_var_session("idusuario");
						$data2["idoperacion"] = $idcompra;
						$data2["tabla"]       = $this->controller;
						$data2["idtarjeta"]   = $fields["idtarjeta"];
						$data2["nro_operacion"]   = $fields["nro_operacion"];
						$data2["nro_tarjeta"]   = $fields["nro_tarjeta"];
						$data2["importe"]   	= $fields["importe"];
						$data2['estado'] 		= "A";

						if(empty($data_mov)) {
							$data2['hora'] 			= date("H:i:s");
							$data2['fecha'] 		= date("Y-m-d");
							$this->movimiento_tarjeta->insert($data2,false);
						}else{
							$this->movimiento_tarjeta->update($data2);
						}

					}else if( isset($fields['idcuentas_bancarias']) ){// DEPOSITO
						$this->load_model("movimiento_deposito");
						$data_mov = $this->movimiento_deposito->find($fields["idsucursal"], $fields["idcuentas_bancarias"], $idcompra);
						
						$data2["idsucursal"]  = $this->get_var_session("idsucursal");
						$data2["idusuario"]   = $this->get_var_session("idusuario");
						$data2["idoperacion"] = $idcompra;
						$data2["tabla"]       = $this->controller;
						$data2["idcuentas_bancarias"]   = $fields["idcuentas_bancarias"];
						$data2["nro_operacion"]   = $fields["nro_operacion"];
						$data2["importe"]   	= $fields["importe"];
						$data2["fecha_deposito"]   = $fields["fecha_deposito"];
						$data2['estado'] 		= "A";
						if(empty($data_mov)) {
							$data2['hora'] 			= date("H:i:s");
							$data2['fecha'] 		= date("Y-m-d");
							$this->movimiento_deposito->insert($data2,false);
						}else{
							$this->movimiento_deposito->update($data2);
						}
					}
			}
		}
		else {
			$this->compra->update($fields);
		}
		
		$this->db->trans_complete(); // finalizamos transaccion
		
		if ($this->db->trans_status() === FALSE) {
			// generate an error... or use the log_message() function to log your error
		}
		
		$this->response($fields);
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model($this->controller);
		
		$fields['idperfil'] = $id;
		$fields['estado'] = "0";
		$this->perfil->update($fields);
		
		$this->response($fields);
	}
	
	
	public function autocomplete() {
		$txt = $this->input->post("startsWith").'%';
		
		$sql = "SELECT c.idcompra, p.nombre, p.ruc 
			FROM compra.compra AS c, compra.proveedor as p 
			WHERE c.idproveedor = p.idproveedor and c.estado='A'
			GROUP BY c.idcompra,p.nombre, p.ruc 
			ORDER BY c.idcompra";
			
		$query = $this->db->query($sql, array($txt, $txt, $this->input->post("maxRows")));
		$this->response($query->result_array());
	}
	
	public function select_detalle() {
		$data = array();
		$txt = $this->input->post("startsWith").'%';
		$idcompra = $this->input->post("idcompra");
		
		// $sql = "SELECT p.idproducto, p.descripcion, u.abreviatura, d.cantidad, coalesce(sum(r.cant_recepcionada),0) AS cant_recepcionada
		$sql = "SELECT p.idproducto, p.descripcion, u.abreviatura, d.cantidad
				FROM compra.producto as p
				INNER JOIN compra.unidad as u ON p.idunidad=u.idunidad				
				INNER JOIN compra.detalle_compra as d ON p.idproducto=d.idproducto
				INNER JOIN compra.compra as c ON d.idcompra=c.idcompra
				WHERE d.idcompra=$idcompra";
				// LEFT JOIN almacen.recepcion r ON (c.idcompra=r.idcompra AND r.idproducto=p.idproducto)				
				
		$query = $this->db->query($sql, array($txt, $txt, $this->input->post("maxRows")));
		$data['lsProdctos_compras'] = $query->result_array();
		$this->response(json_encode($data));
	}
}
?>