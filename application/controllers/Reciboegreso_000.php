<?php

include_once "Controller.php";

class Reciboegreso extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Mantenimiento de Recibo Egreso");
		$this->set_subtitle("Lista de Recibo Egreso");
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->css('plugins/iCheck/custom');
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		
		// $data["idtipodocumento"] = '4';
		$data["idtipodocumento"] = $this->get_param("idrecibo_egreso");
		$this->load->library('combobox');
		
		// combo moneda
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idmoneda"
				,"name"=>"idmoneda"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$query = $this->db->select('idmoneda, descripcion')->where("estado", "A")->get("general.moneda");
		$this->combobox->addItem($query->result_array());
		if( isset($data["idmoneda"]) ) {
			$this->combobox->setSelectedOption($data["idmoneda"]);
		}
		$data["moneda"] = $this->combobox->getObject();

		// combo tipo Recibo
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idtipo_recibo"
				,"name"=>"idtipo_recibo"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$query = $this->db->select('idtipo_recibo, descripcion')->where("estado", "A")->where("tipo", "E")->where("mostrar_en_recibo", "S")->get("credito.tipo_recibo");
		$this->combobox->addItem("");
		$this->combobox->addItem($query->result_array());
		if( isset($data["idtipo_recibo"]) ) {
			$this->combobox->setSelectedOption($data["idtipo_recibo"]);
		}
		$data["tipo_recibo"] = $this->combobox->getObject();
		// combo tipo Recibo
		
		// combo tipopago
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idtipopago"
				,"name"=>"idtipopago"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$query = $this->db->select('idtipopago, descripcion')->where("estado", "A")->where("mostrar_en_reciboegreso", "S")->get("venta.tipopago");
		$this->combobox->addItem($query->result_array());
		if( isset($data["idtipopago"]) ) {
			$this->combobox->setSelectedOption($data["idtipopago"]);
		}
		$data["tipopago"] = $this->combobox->getObject();
		// combo tipopago

		// combo tipopago_MODAL
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idtipopago_modal"
				,"name"=>"idtipopago_modal"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$query = $this->db->select('idtipopago, descripcion')->where("estado", "A")->where("mostrar_en_reciboingreso", "S")->get("venta.tipopago");
		$this->combobox->addItem($query->result_array());
		if( isset($data["idtipopago"]) ) {
			$this->combobox->setSelectedOption($data["idtipopago"]);
		}
		$data["tipopago_modal"] = $this->combobox->getObject();
		// combo tipopago_MODAL
		
		// combo tipo documento
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idtipodocumento_ref"
				,"name"=>"idtipodocumento_ref"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$query = $this->db->select('idtipodocumento, descripcion')->where("estado", "A")->where("mostrar_en_recibo", "S")->get("venta.tipo_documento");
		$this->combobox->addItem("");
		$this->combobox->addItem($query->result_array());
		if( isset($data["idtipodocumento_ref"]) ) {
			$this->combobox->setSelectedOption($data["idtipodocumento_ref"]);
		}
		$data["tidocumento"] = $this->combobox->getObject();
		
		// combo concepto Movimiento
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idconceptomovimiento"
				,"name"=>"idconceptomovimiento"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		// $query = $this->db->select('idconceptomovimiento, descripcion')->where("estado", "A")->where("mostrar_en_recibo", "S")->get("caja.conceptomovimiento");
		// $query = $this->db->select('idconceptomovimiento, descripcion')->where("estado", "A")->get("caja.conceptomovimiento");
		$query = $this->db->select('idconceptomovimiento, descripcion')->where("estado", "A")->where("ver_reciboegreso", "S")->get("caja.conceptomovimiento");
		// $this->combobox->addItem("");
		$this->combobox->addItem($query->result_array());
		if( isset($data["idconceptomovimiento"]) ) {
			$this->combobox->setSelectedOption($data["idconceptomovimiento"]);
		}
		$data["movimiento"] = $this->combobox->getObject();

		// combo cuentasbancarias
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idcuentas_bancarias"
				,"name"=>"idcuentas_bancarias"
				,"class"=>"form-control"
				,"required"=>""
				,"type-name"=>"idcuentas_bancarias"
			)
		);
		$query = $this->db->select('idcuentas_bancarias, cuenta')->where("estado", "A")->where("idsucursal", $this->get_var_session("idsucursal"))->get("general.view_cuentas_bancarias");
		$this->combobox->addItem("");
		$this->combobox->addItem($query->result_array());
		if( isset($data["idcuentas_bancarias"]) ) {
			$this->combobox->setSelectedOption($data["idcuentas_bancarias"]);
		}
		$data["cuentas_bancarias"] = $this->combobox->getObject();
		// combo cuentasbancarias

		// combo tarjeta
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idtarjeta"
				,"name"=>"idtarjeta"
				,"class"=>"form-control"
				,"required"=>""
				,"type-name"=>"idtarjeta"
			)
		);
		$query = $this->db->select('idtarjeta, descripcion')->where("estado", "A")->get("general.tarjeta");
		//$this->combobox->addItem("");
		$this->combobox->addItem($query->result_array());
		if( isset($data["idtarjeta"]) ) {
			$this->combobox->setSelectedOption($data["idtarjeta"]);
		}
		//echo $this->combobox->getObject();
		$data["tarjeta"] = $this->combobox->getObject();
		// combo tarjeta
		
		
		$this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		$this->js('plugins/iCheck/icheck.min');
		$this->js("<script>$(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });</script>", false);
		$this->css('plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox');//PARA CLIENTE
		$this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
		$this->css("plugins/datapicker/datepicker3");
		$this->css('plugins/iCheck/custom');
		
		// formulario CLIENTE
		$this->load_controller("cliente");
		// $this->cliente_controller->load = $this->load;
		// $this->cliente_controller->db = $this->db;
		// $this->cliente_controller->session = $this->session;
		// $this->cliente_controller->combobox = $this->combobox;
		$data["form_cliente"] = $this->cliente_controller->form(null, "cli_", true);
		$this->js('form/cliente/modal');
		
		// formulario EMPLEADO
		$this->load_controller("usuario");
		// $this->usuario_controller->load = $this->load;
		// $this->usuario_controller->db = $this->db;
		// $this->usuario_controller->session = $this->session;
		// $this->usuario_controller->combobox = $this->combobox;
		$data["form_usuario"] = $this->usuario_controller->form(null, "usu_", true);
		$this->js('form/usuario/modal');


		$this->js('form/'.$this->controller.'/form');
		// $this->js('form/cliente/modal');
		
		$data["modal_pago"] = $this->get_form_pago("reciboegreso", false);
		$data["controller"] = $this->controller;
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		// cargamos el modelo y la libreria
		// $this->load_model($this->controller);
		$this->load_model("reciboegreso_view");
		$this->load->library('datatables');
		
		// indicamos el modelo al datatables
		$this->datatables->setModel($this->reciboegreso_view);
		$this->datatables->setIndexColumn("idreciboegreso");
		// filtros adicionales para la tabla de la bd (perfil en este caso)
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		
		// indicamos las columnas a mostrar de la tabla de la bd
		$this->datatables->setColumns(array('recibo','referencia','concepto','fecha','monto'));
		
		// columnas de la tabla, si no se envia este parametro, se muestra el 
		// nombre de la columna de la tabla de la bd
		$columnasName = array(
			array('Recibo','9%')
			,array('Referencia','40%')
			,array('Concepto','32%')
			,array('Fecha','10%')
			,array('Monto','10%')
			// array('Descripci&oacute;n', '95%') // ancho de la columna
		);

		$this->datatables->setCallback('callbackRE');

		// generamos la tabla y el script para el dataTables
		$table = $this->datatables->createTable($columnasName);
		// $table = $this->datatables->createTable();
		$script = "<script>".$this->datatables->createScript()."</script>";
		
		// agregamos los css para el dataTables
		// $this->css('plugins/dataTables/dataTables.bootstrap');
		// $this->css('plugins/dataTables/dataTables.responsive');
		// $this->css('plugins/dataTables/dataTables.tableTools.min');
		
		// agregamos los scripts para el dataTables
		// $this->js('plugins/dataTables/jquery.dataTables');
		// $this->js('plugins/dataTables/dataTables.bootstrap');
		// $this->js('plugins/dataTables/dataTables.responsive');
		// $this->js('plugins/dataTables/dataTables.tableTools.min');
		$this->js($script, false);
		
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Recibo Egreso");
		$this->set_subtitle("");
		$this->set_content($this->form());
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		//$this->load_model($this->controller);
		$this->load_model('reciboegreso_view');
		$data = $this->reciboegreso_view->find(array("idreciboegreso"=>$id));
		// print_r($data);
		$this->set_title("Modificar Recibo Egreso");
		$this->set_subtitle("");
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model($this->controller);
		$this->load_model("tipo_documento");
		$esNuevRecibo = false;
		
		$fields = $this->input->post();
		
		$fields['idcliente']=$fields['idpersona'];//AUDIT
		
		$fields['estado'] = "A";
		$fields['idsucursal'] = $this->get_var_session("idsucursal");
		if(empty($fields['idtipodocumento_ref']))
			$fields['idtipodocumento_ref'] = null;
		
		$this->db->trans_start(); // inciamos transaccion

		if(empty($fields["idreciboegreso"])) {
			$fields['idusuario'] = $this->get_var_session("idusuario");
			$fields['fecha'] = date("Y-m-d");
			$fields['hora'] = date("H:i:s");
			$idreciboegreso = $this->reciboegreso->insert($fields);
			
			$esNuevRecibo = true;
			
		}else {
			$idreciboegreso = $fields["idreciboegreso"];
			$this->reciboegreso->update($fields);
		}
		
		// if ($fields["idtipopago"] == 3) {//SI ES EFECTIVO
			$fields["idtipodocumento"] = $idtipodocumento = '4';//(EN LA BASE 3= RECIBO EGRESO)
			if($esNuevRecibo) {

				$datostipodoc = $this->tipo_documento->find($fields["idtipodocumento"]);
				if($datostipodoc["genera_correlativo"] == 'S') {
					$this->load_model("serie_documento");
					$datos_serie = $this->serie_documento->find(array("idsucursal"=>$fields["idsucursal"], 
						"idtipodocumento"=>$fields["idtipodocumento"], "serie"=>$fields["serie"]));
					$datos_serie["correlativo"] = $datos_serie["correlativo"] + 1;
					$this->serie_documento->update($datos_serie);
				}

				$fields['monto'] = ($fields['monto']) * (-1);

				// $this->load_controller("caja");
				// $this->caja_controller->idusuario = $fields['idusuario'];
				// $this->caja_controller->idsucursal = $this->get_var_session("idsucursal");
				// $this->caja_controller->db = $this->db;
				// $this->caja_controller->egresoCaja($fields['idconceptomovimiento']
					// , $fields['monto']
					// , strtoupper($fields['concepto'])
					// , $fields['cliente']
					// , $this->controller
					// , $idreciboegreso
					// , $fields['idmoneda']
					// , $fields["tipocambio"]
					// , $idtipodocumento
					// , $fields['idpersona'] 
					// , $fields["serie"]
					// , $fields["numero"] 
					// , $this->get_var_session("idsucursal")
					// , $fields["idtipopago"] );

			}else{
				$fields["tabla"] = "reciboegreso";
				$fields["idoperacion"] = $idreciboegreso;
				
				// cargamos el controlador
				$this->load_controller("caja");
				// $this->caja_controller->db = $this->db;
				
				// cargamos la libreria
				$this->load->library('pay');
				$this->pay->set_controller($this->caja_controller);
				$this->pay->set_data($fields); // revisar metodo para verificar los datos necesarios
				$this->pay->remove();
			}
			
			if( $fields["afecta_caja"] == 'S' ) {
				// datos necesarios para la libreria pay
				$fields["descripcion"] = strtoupper($fields['concepto']);
				$fields["referencia"] = $fields['cliente'];
				$fields["tabla"] = "reciboegreso";
				$fields["idoperacion"] = $idreciboegreso;
				$fields["numero"] = $fields["numero"];
				
				if(!isset($this->caja_controller)) {
					$this->load_controller("caja");
					// $this->caja_controller->db = $this->db;
				}
				if(!isset($this->pay)) {
					$this->load->library('pay');
				}
				$this->pay->set_controller($this->caja_controller);
				$this->pay->set_data($fields);
				$this->pay->entrada(false); // false si es salida, default true
				$this->pay->process();
			}

			// $this->db->query("UPDATE venta.movimiento_tarjeta SET estado='I' WHERE idsucursal='{$fields["idsucursal"]}' AND idoperacion='{$idreciboegreso}' AND tabla='".strtoupper($this->controller)."' ");
			// $this->db->query("UPDATE venta.movimiento_deposito SET estado='I' WHERE idsucursal='{$fields["idsucursal"]}' AND idoperacion='{$idreciboegreso}' AND tabla='".strtoupper($this->controller)."' ");
			// if (isset($fields['idtarjeta'])) {// PAGO TARJETA
				// $this->load_model("movimiento_tarjeta");
				// $data_mov = $this->movimiento_tarjeta->find($fields['idsucursal'], $fields["idtarjeta"], $idreciboegreso);

				// $data2["idsucursal"]  = $this->get_var_session("idsucursal");
				// $data2["idusuario"]   = $this->get_var_session("idusuario");
				// $data2["idoperacion"] = $idreciboegreso;
				// $data2["tabla"]       = $this->controller;
				// $data2["idtarjeta"]   = $fields["idtarjeta"];
				// $data2["nro_operacion"]   = $fields["nro_operacion"];
				// $data2["nro_tarjeta"]   = $fields["nro_tarjeta"];
				// $data2["importe"]   	= $fields["importe"];
				// $data2['estado'] 		= "A";

				// if(empty($data_mov)) {
					// $data2['hora'] 			= date("H:i:s");
					// $data2['fecha'] 		= date("Y-m-d");
					// $this->movimiento_tarjeta->insert($data2,false);
				// }else{
					// $this->movimiento_tarjeta->update($data2);
				// }

			// }else if( isset($fields['idcuentas_bancarias']) ){// DEPOSITO
				// $this->load_model("movimiento_deposito");
				// $data_mov = $this->movimiento_deposito->find($fields["idsucursal"], $fields["idcuentas_bancarias"], $idreciboegreso);
				
				// $data2["idsucursal"]  = $this->get_var_session("idsucursal");
				// $data2["idusuario"]   = $this->get_var_session("idusuario");
				// $data2["idoperacion"] = $idreciboegreso;
				// $data2["tabla"]       = $this->controller;
				// $data2["idcuentas_bancarias"]   = $fields["idcuentas_bancarias"];
				// $data2["nro_operacion"]   = $fields["nro_operacion"];
				// $data2["importe"]   	= $fields["importe"];
				// $data2["fecha_deposito"]   = $fields["fecha_deposito"];
				// $data2['estado'] 		= "A";
				// if(empty($data_mov)) {
					// $data2['hora'] 			= date("H:i:s");
					// $data2['fecha'] 		= date("Y-m-d");
					// $this->movimiento_deposito->insert($data2,false);
				// }else{
					// $this->movimiento_deposito->update($data2);
				// }
			// }
		// }
		$this->db->trans_complete(); // finalizamos transaccion
		$this->response($this->reciboegreso->get_fields());
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model($this->controller);
		
		$this->db->trans_start();
		
		// cambiamos de estado
		$fields['idreciboegreso'] = $id;
		$fields['estado'] = "I";
		$this->reciboegreso->update($fields);
		
		// si la caja esta abierta, eliminamos el registro nomas, 
		// de lo contrario que hagan un recibo de egreso, si afecta caja
		$this->load_library('pay');
		$this->pay->remove_if_open("reciboegreso", $id, $this->get_var_session('idsucursal'));
		
		$this->db->trans_complete();
		
		$this->response($fields);
	}
}
?>