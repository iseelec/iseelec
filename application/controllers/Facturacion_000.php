<?php

include_once "Controller.php";

class Facturacion extends Controller {
	
	private $ListaSituacion = null;
	private $TipoComprobante = null;
	
	public function __construct() {
		parent::__construct();
		
		$this->ListaSituacion = array(
			array("id"=>"01","nombre"=>"Por Generar XML"),
			array("id"=>"02","nombre"=>"XML Generado"),
			array("id"=>"03","nombre"=>"Enviado y Aceptado SUNAT"),
			array("id"=>"04","nombre"=>"Enviado y Aceptado SUNAT con Obs."),
			array("id"=>"05","nombre"=>"Enviado y Anulado por SUNAT"),
			array("id"=>"06","nombre"=>"Con Errores"),
			array("id"=>"07","nombre"=>"Por Validar XML"),
			array("id"=>"08","nombre"=>"Enviado a SUNAT Por Procesar"),
			array("id"=>"09","nombre"=>"Enviado a SUNAT Procesando"),
			array("id"=>"10","nombre"=>"Rechazado por SUNAT")
		);
		
		$this->TipoComprobante = array(
			array("id"=>"01","nombre"=>"Factura","abrev"=>"F"),
			array("id"=>"03","nombre"=>"Boleta de Venta","abrev"=>"BV"),
			array("id"=>"07","nombre"=>"Nota de Credito","abrev"=>"NC"),
			array("id"=>"08","nombre"=>"Nota de Debito","abrev"=>"ND"),
			array("id"=>"RA","nombre"=>"Comunicación de Baja","abrev"=>"CB")
		);
	}
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Consulta de productos");
		$this->set_subtitle("Consultar Productos");
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null, $prefix = "", $modal = false) {
		$data["controller"] = $this->controller;
		
		$this->load->library('combobox');
		
		// combo tipo documentos
		$sql = "select idtipodocumento,descripcion from venta.tipo_documento 
			where estado='A' and facturacion_electronica='S'";
		$query = $this->db->query($sql);
		$this->combobox->setAttr("id","idtipodocumento");
		$this->combobox->setAttr("name","idtipodocumento");
		$this->combobox->setAttr("class","form-control input-xs");
		$this->combobox->setStyle("width","120px");
		$this->combobox->addItem("", "TODOS");
		$this->combobox->addItem($query->result_array());
		$this->combobox->addItem("RA", "COMUNICACION DE BAJA");
		$data['tipodocumento'] = $this->combobox->getObject();
		
		// combo sucursal
		$sql = "select idsucursal,descripcion from seguridad.sucursal where estado='A'";
		$query = $this->db->query($sql);
		$this->combobox->removeItems(1);
		$this->combobox->setAttr("id","idsucursal");
		$this->combobox->setAttr("name","idsucursal");
		$this->combobox->setAttr("class","form-control input-xs");
		// $this->combobox->addItem("", "TODOS");
		$this->combobox->addItem($query->result_array());
		$data['sucursal'] = $this->combobox->getObject();
		
		// combo anios
		$aniof = intval(date("Y"));
		$anioi = $aniof - 10;
		$this->combobox->removeStyle("width");
		$this->combobox->removeItems(0);
		$this->combobox->setAttr("id", "anio");
		$this->combobox->setAttr("name", "anio");
		for($i=$aniof; $i >= $anioi; $i--) {
			$this->combobox->addItem($i, $i);
		}
		$this->combobox->setSelectedOption(date("Y"));
		$data['anios'] = $this->combobox->getObject();
		
		// combo meses
		$this->combobox->removeItems(0);
		$this->combobox->setAttr("id", "mes");
		$this->combobox->setAttr("name", "mes");
		$meses = getMonthsName();
		foreach($meses as $k=>$v) {
			if($k == 0) {
				continue;
			}
			$this->combobox->addItem($k, strtoupper($v));
		}
		$this->combobox->setSelectedOption(intval(date("m")));
		$data['meses'] = $this->combobox->getObject(true);
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		return null;
	}
	
	public function index($tpl = "") {
		$data = array(
			"menu_title" => $this->menu_title
			,"menu_subtitle" => $this->menu_subtitle
			,"content" => $this->form()
			,"with_tabs" => $this->with_tabs
		);
		
		if($this->show_path) {
			$data['path'] = $this->get_path();
		}
		
		$str = $this->load->view("content_empty", $data, true);
		$this->show($str);
	}
	
	public function getSituacion($var) {
		foreach($this->ListaSituacion as $array) {
			if($array["id"] == $var)
				return $array["nombre"];
		}
		
		return "-";
	}
	
	public function getComprobante($var, $key="nombre") {
		foreach($this->TipoComprobante as $array) {
			if($array["id"] == $var)
				return $array[$key];
		}
		
		return "-";
	}
	
	private function _queryNotaCredito($order_by = TRUE) {
		$post = $this->input->post();
		
		$sql = "select f.num_ruc, f.tip_docu, f.num_docu, to_char(r.fecha,'DD/MM/YYYY') as fec_emis, f.fec_carg, 
			f.fec_gene, f.fec_envi, f.ind_situ, f.des_obse, f.referencia, f.idreferencia, r.fecha as fecha_real
			,'-'::text as doc_ref
			from venta.facturacion f
			join venta.notacredito r on r.idnotacredito=f.idreferencia and f.referencia='notacredito'
			where extract(year from r.fecha) = " . intval($post["anio"]) . "
			and extract(month from r.fecha) = " . intval($post["mes"]);
		
		if( ! empty($post["idtipodocumento"]))
			$sql .= " and r.idtipodocumento = " . intval($post["idtipodocumento"]);
		
		if( ! empty($post["idsucursal"]))
			$sql .= " and r.idsucursal = " . intval($post["idsucursal"]);
		
		if($order_by) {
			$sql .= " order by fecha_real desc, idreferencia desc";
		}
		
		return $sql;
	}
	
	private function _queryNotaDebito($order_by = TRUE) {
		$post = $this->input->post();
		
		$sql = "select f.num_ruc, f.tip_docu, f.num_docu, to_char(r.fecha,'DD/MM/YYYY') as fec_emis, f.fec_carg, 
			f.fec_gene, f.fec_envi, f.ind_situ, f.des_obse, f.referencia, f.idreferencia, r.fecha as fecha_real
			,'-'::text as doc_ref
			from venta.facturacion f
			join venta.notadebito r on r.idnotadebito=f.idreferencia and f.referencia='notadebito'
			where extract(year from r.fecha) = " . intval($post["anio"]) . "
			and extract(month from r.fecha) = " . intval($post["mes"]);
		
		if( ! empty($post["idtipodocumento"]))
			$sql .= " and r.idtipodocumento = " . intval($post["idtipodocumento"]);
		
		if( ! empty($post["idsucursal"]))
			$sql .= " and r.idsucursal = " . intval($post["idsucursal"]);
		
		if($order_by) {
			$sql .= " order by fecha_real desc, idreferencia desc";
		}
		
		return $sql;
	}
	
	private function _queryVenta($order_by = TRUE) {
		$post = $this->input->post();
		
		$sql = "select f.num_ruc, f.tip_docu, f.num_docu, to_char(r.fecha_venta,'DD/MM/YYYY') as fec_emis, f.fec_carg, 
			f.fec_gene, f.fec_envi, f.ind_situ, f.des_obse, f.referencia, f.idreferencia, r.fecha_venta as fecha_real
			,'-'::text as doc_ref
			from venta.facturacion f
			join venta.venta r on r.idventa=f.idreferencia and f.referencia='venta'
			where extract(year from r.fecha_venta) = " . intval($post["anio"]) . "
			and extract(month from r.fecha_venta) = " . intval($post["mes"]);
		
		if( ! empty($post["idtipodocumento"]))
			$sql .= " and r.idtipodocumento = " . intval($post["idtipodocumento"]);
		
		if( ! empty($post["idsucursal"]))
			$sql .= " and r.idsucursal = " . intval($post["idsucursal"]);
		
		if($order_by) {
			$sql .= " order by fecha_real desc, idreferencia desc";
		}
		
		return $sql;
	}
	
	private function _queryBaja($order_by = TRUE) {
		$post = $this->input->post();
		
		$sql = "select f.num_ruc, f.tip_docu, f.num_docu, to_char(r.fecha,'DD/MM/YYYY') as fec_emis, f.fec_carg, 
			f.fec_gene, f.fec_envi, f.ind_situ, f.des_obse, f.referencia, f.idreferencia, r.fecha as fecha_real,
			r.tip_docu||'-'||r.num_docu as doc_ref
			from venta.facturacion f
			join venta.documento_baja r on r.iddocumento_baja=f.idreferencia::integer and f.referencia='documento_baja'
			where extract(year from r.fecha) = " . intval($post["anio"]) . "
			and extract(month from r.fecha) = " . intval($post["mes"]);
		
		if( ! empty($post["idsucursal"])) {
			$sql .= " and r.idsucursal = " . intval($post["idsucursal"]);
		}
		
		if($order_by) {
			$sql .= " order by fecha_real desc, idreferencia desc";
		}
		
		return $sql;
	}
	
	public function getData() {
		$post = $this->input->post();
		
		if( ! empty($post["idtipodocumento"])) {
			if($post["idtipodocumento"] != "RA") {
				$sql = "select * from venta.tipo_documento where idtipodocumento=?";
				$query = $this->db->query($sql, array($post["idtipodocumento"]));
				$row = $query->row_array();
				
				$sql = '';
				if(stripos($row["descripcion"], "credito") !== FALSE) {
					// nota de credito
					$sql = $this->_queryNotaCredito();
				}
				else if(stripos($row["descripcion"], "debito") !== FALSE) {
					// nota de debito
					// $sql = $this->_queryNotaDebito();
				}
				else if(stripos($row["descripcion"], "baja") !== FALSE) {
					// comunicacion baja
					$sql = $this->_queryBaja();
				}
				else {
					// boleta o factura
					$sql = $this->_queryVenta();
				}
			}
			else {
				$sql = $this->_queryBaja();
			}
			
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		// todos los documentos
		$sql = $this->_queryVenta(false);
		$sql .= " union ".$this->_queryNotaCredito(false);
		// $sql .= " union ".$this->_queryNotaDebito(false);
		$sql .= " union ".$this->_queryBaja(false);
		$sql .= " order by fecha_real";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_records() {
		$arr = $this->getData();
		
		if(empty($arr)) {
			$this->response('<tr class="empty-rs"><td colspan="9"><i>No se han encontrado resultados segun los criterios de b&uacute;squeda.</i></td></tr>');
			return;
		}
		
		$html = '';
		
		foreach($arr as $row) {
			$doc_ref = $row["doc_ref"];
			if($doc_ref != "-") {
				$ref = explode("-", $doc_ref);
				$doc_ref = $this->getComprobante($ref[0], "abrev").". ".$ref[1]."-".intval($ref[2]);
			}
			
			$html .= '<tr data-idref="'.$row["idreferencia"].'" data-ref="'.$row["referencia"].'" data-tdoc="'.$row["tip_docu"].'">';
			$html .= '<td>'.$row["num_ruc"].'</td>';
			$html .= '<td>'.$this->getComprobante($row["tip_docu"]).'</td>';
			$html .= '<td>'.$row["num_docu"].'</td>';
			$html .= '<td>'.$doc_ref.'</td>';
			$html .= '<td>'.$row["fec_carg"].'</td>';
			$html .= '<td>'.$row["fec_gene"].'</td>';
			$html .= '<td>'.$row["fec_envi"].'</td>';
			$html .= '<td>'.$this->getSituacion($row["ind_situ"]).'</td>';
			$html .= '<td>'.$row["des_obse"].'</td>';
			$html .= '</tr>';
		}
		
		$this->response($html);
	}
	
	public function update() {
		$this->load_model("venta.facturacion");
		$this->load->library('jfacturacion');
		
		$this->facturacion->find($this->input->post());
		
		$res = $this->jfacturacion->get_estado($this->facturacion->get("nom_arch"));
		if($res != "ok" && $res != "failed") {
			$datos = json_decode($res, true);
			$this->facturacion->set($datos);
			$this->facturacion->text_uppercase(false);
			$this->facturacion->update(null);
		}
		
		$this->response($this->facturacion->get_fields());
	}
	
	public function generar() {
		$this->load_model("venta.facturacion");
		$this->load->library('jfacturacion');
		
		$datos_fact = $this->facturacion->find($this->input->post());
		
		$estado = true;
		
		if($datos_fact["referencia"] == "venta") {
			$this->load_model("venta.venta");
			$v = $this->venta->find($datos_fact["idreferencia"]);
			$t = $v["subtotal"] + $v["igv"] - $v["descuento"];
			$estado = $this->is_valid_doc($v["idtipodocumento"],$v["serie"],$v["idcliente"],$t,$v["idmoneda"]);
			// var_dump($estado);
		}
		else if($datos_fact["referencia"] == "notacredito" || $datos_fact["referencia"] == "notadebito") {
			$this->load_model("venta.notacredito");
			$v = $this->notacredito->find($datos_fact["idreferencia"]);
			$t = $v["subtotal"] + $v["igv"];
			$estado = $this->is_valid_doc_nota($v["idtipodocumento"],$v["serie"],$v["iddocumento_ref"],$v["serie_ref"],$v["idcliente"],$t,$v["idmoneda"]);
		}
		
		if($estado !== true) {
			$this->exception($estado);
			return;
		}
		
		if(empty($datos_fact["ind_situ"])) {
			$datos_fact["ind_situ"] = "01";
		}
		// var_dump($datos_fact["ind_situ"]);
		if(in_array($datos_fact["ind_situ"], array("01","06","07","10"))) {
			$datos = $this->jfacturacion->crear_files($this->facturacion->get("referencia"), $this->facturacion->get("idreferencia"));
			// print_r($datos);
			if($datos !== FALSE) {
				$res = $this->jfacturacion->enviar_files($datos);
				// print_r($res);
				// return;
				if($res !== false) {
					if($res != "failed" && $res != "ok") {
						$arr = json_decode($res, true);
						$datos = array_merge($datos, $arr);
					}
					
					$this->facturacion->set($datos);
					$this->facturacion->text_uppercase(false);
					$this->facturacion->update(null);
				}
			}
			
			// se trata de un archivo (registro) nuevo o con error, creamos el comprobante
			$res = $this->jfacturacion->crear_comprobante($this->facturacion->get("referencia"), $this->facturacion->get("idreferencia"));
			
			if($res !== FALSE) {
				if($res != "failed" && $res != "ok") {
					$datos = json_decode($res, true);
					$this->facturacion->set($datos);
					$this->facturacion->text_uppercase(false);
					$this->facturacion->update(null);
				}
			}
		}
		
		$this->response($this->facturacion->get_fields());
	}
	
	public function send() {
		$this->load_model("venta.facturacion");
		
		$datos_fact = $this->facturacion->find($this->input->post());
		
		if($datos_fact["ind_situ"] == "02" || $datos_fact["ind_situ"] == "10") {
			$this->load->library('jfacturacion');
			
			$res = $this->jfacturacion->enviar_comprobante($datos_fact["referencia"], $datos_fact["idreferencia"]);
			// print_r($res);return;
			if($res !== FALSE) {
				if($res != "failed" && $res != "ok") {
					$datos = json_decode($res, true);
					$this->facturacion->set($datos);
					$this->facturacion->text_uppercase(false);
					$this->facturacion->update(null);
				}
			}
		}
		
		$this->response($this->facturacion->get_fields());
	}
	
	public function baja() {
		$this->unlimit();
		
		$this->load_model("venta.facturacion");
		
		$datos_fact = $this->facturacion->find($this->input->post());
		
		if($datos_fact["tip_docu"] == "RA") {
			$this->exception("Debe seleccionar el comprobante que desea dar de baja");
			return;
		}
		
		if( ! in_array($datos_fact["ind_situ"], array("03","04","05"))) {
			$this->exception("El comprobante aun no se envia a SUNAT, no hay razon para dar de baja.");
			return;
		}
		
		$sql = "SELECT * FROM venta.documento_baja WHERE idreferencia=? AND referencia=?";
		$query = $this->db->query($sql, array($datos_fact["idreferencia"], $datos_fact["referencia"]));
		if($query->num_rows() > 0) {
			$this->exception("La ".$this->getComprobante($datos_fact["tip_docu"])." ".$datos_fact["num_docu"]." ya se ha comunicado de baja.");
			return;
		}
		
		$current_date = date("Y-m-d");
		
		// obtenemos el maximo correlativo del dia
		$sql = "select max(correlativo) as corr from venta.documento_baja where fecha=?";
		$query = $this->db->query($sql, array($current_date));
		$correlativo = intval($query->row()->corr) + 1;
		
		// ingresamos nuevo registro
		$this->load_model("venta.documento_baja");
		$this->documento_baja->set($datos_fact);
		$this->documento_baja->set("idsucursal", $this->get_var_session("idsucursal"));
		$this->documento_baja->set("idusuario", $this->get_var_session("idusuario"));
		$this->documento_baja->set("fecha", $current_date);
		$this->documento_baja->set("correlativo", $correlativo);
		$this->documento_baja->set("motivo", $this->input->post("motivo"));
		$this->documento_baja->text_uppercase(false);
		$id = $this->documento_baja->insert(null);
		
		// enviamos el archivo a la carpeta DATA del facturador
		$this->send_to_facturador("documento_baja", $id, $this->get_var_session("idsucursal"));
		
		// verificamos el estado para generar el comprobante xml
		$datos_fact = $this->facturacion->find(array("idreferencia"=>$id, "referencia"=>"documento_baja"));
		if(in_array($datos_fact["ind_situ"], array("01","06","07","10"))) {
			$res = $this->jfacturacion->crear_comprobante("documento_baja", $id);
			if($res !== FALSE) {
				if($res != "failed" && $res != "ok") {
					$datos = json_decode($res, true);
					$this->facturacion->set($datos);
					$this->facturacion->text_uppercase(false);
					$this->facturacion->update(null);
				}
			}
		}
		
		/* // enviamos sunat el comprobante
		$datos_fact = $this->facturacion->find(array("idreferencia"=>$id, "referencia"=>"documento_baja"));
		if(in_array($datos_fact["ind_situ"], array("02","10"))) {
			$res = $this->jfacturacion->enviar_comprobante("documento_baja", $id);
			if($res !== FALSE) {
				if($res != "failed" && $res != "ok") {
					$datos = json_decode($res, true);
					$this->facturacion->set($datos);
					$this->facturacion->text_uppercase(false);
					$this->facturacion->update(null);
				}
			}
		} */
		
		$this->response($this->facturacion->get_fields());
	}
}
?>