<?php

include_once "Controller.php";
class Preventa extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Lista de pedidos de cotización");
		// $this->set_subtitle("Lista de pedidos de venta");
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		// $this->js('form/'.$this->controller.'/index');
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		
		$this->load->library('combobox');
		
		///////////////////////////////////////////////////// combo tipo venta
		$query = $this->db->select('idtipoventa, descripcion')
			->where("estado", "A")->where("mostrar_en_cotizacion", "S")
			->order_by("descripcion", "asc")->get("venta.tipo_venta");
		
		$this->combobox->setAttr("id", "idtipoventa");
		$this->combobox->setAttr("name", "idtipoventa");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->setAttr("required", "");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["preventa"]["idtipoventa"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idtipoventa"]);
		}
		$data["tipoventa"] = $this->combobox->getObject();
		
		///////////////////////////////////////////////////// forma de pago
		$query = $this->db->select('idforma_pago_cotizacion, descripcion')
			->where("estado", "A")
			->order_by("idforma_pago_cotizacion", "asc")->get("venta.forma_pago_cotizacion");
		$this->combobox->init();
		$this->combobox->setAttr("id", "idforma_pago_cotizacion");
		$this->combobox->setAttr("name", "idforma_pago_cotizacion");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->setAttr("required", "");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["preventa"]["idforma_pago_cotizacion"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idforma_pago_cotizacion"]);
		}
		$data["forma_pago"] = $this->combobox->getObject();


		
////////////////////////////////////////////////////// combo Modalidad de Venta
		

	//////////////////////////////////////////////////////////
   

$query = $this->db->select('idmodalidad, modalidad')
			->where("estado", "A")
			->order_by("modalidad", "asc")->get("venta.modalidad");
		$this->combobox->init();
		$this->combobox->setAttr("id", "idmodalidad");
		$this->combobox->setAttr("name", "idmodalidad");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->setAttr("required", "");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["preventa"]["idmodalidad"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idmodalidad"]);
		}
		$data["modalidad"] = $this->combobox->getObject();


/*$query = $this->db->where("estado", "A")
			->order_by("modalidad", "asc")->get("venta.modalidad");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idmodalidad","name"=>"idmodalidad","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem($query->result_array(), array("idmodalidad","modalidad"));
		
		if( isset($data["preventa"]["idmodalidad"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idmodalidad"]);
		}
		$data["modalidad"] = $this->combobox->getObject();*/


////////////////////////////////////////////////////// combo Rampa
		$query = $this->db->where("estado", "A")
			->order_by("descripcion", "asc")->get("venta.rampa");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idrampa","name"=>"idrampa","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem("0"," ");
		$this->combobox->addItem($query->result_array(), array("idrampa","descripcion"));
		
		if( isset($data["preventa"]["idrampa"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idrampa"]);
		}
		$data["rampa"] = $this->combobox->getObject();


////////////////////////////////////////////////////// combo Mecanico
		$query = $this->db->where("estado", "A")->where("baja","N")->order_by("nombre", "asc")->get("seguridad.mecanico_vista");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idmecanico","name"=>"idmecanico","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem("0"," ");
		$this->combobox->addItem($query->result_array(), array("idmecanico","nombre"));
		
		if( isset($data["preventa"]["idmecanico"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idmecanico"]);
		}
		$data["mecanico_vista"] = $this->combobox->getObject();


		
		////////////////////////////////////////////////////// combo tipodocumento
		$query = $this->db->where("estado", "A")->where("mostrar_en_cotizacion", "S")
			->order_by("descripcion", "asc")->get("venta.tipo_documento");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idtipodocumento","name"=>"idtipodocumento","class"=>"form-control input-xs","required"=>""));
		
		$this->combobox->addItem($query->result_array(), array("idtipodocumento","descripcion","codsunat","facturacion_electronica","ruc_obligatorio","dni_obligatorio"));
		
		if( isset($data["preventa"]["idtipodocumento"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idtipodocumento"]);
		}
		$data["tipodocumento"] = $this->combobox->getObject();
		
		//////////////////////////////////////////////////////////// combo sede
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"serie","name"=>"serie","class"=>"form-control input-xs","required"=>""));
		if( isset($data["preventa"]["idtipodocumento"]) ) {
			$sql = "SELECT serie, serie 
				FROM venta.serie_documento 
				WHERE idtipodocumento=? and idsucursal=?";
			$query = $this->db->query($sql, array($data["preventa"]["idtipodocumento"], $this->get_var_session("idsucursal")));
			$this->combobox->addItem($query->result_array());
		}
		if( isset($data["preventa"]["serie"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["serie"]);
		}
		$data["comboserie"] = $this->combobox->getObject();
		
		//////////////////////////////////////////////////////// combo almacen
		$query = $this->db->select('idalmacen, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))
			->order_by("idalmacen", "desc")->get("almacen.almacen");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idalmacen","name"=>"idalmacen","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem($query->result_array());
		if( isset($data["preventa"]["idalmacen"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idalmacen"]);
		}
		$data["almacen"] = $this->combobox->getObject();
		
		//////////////////////////////////////////////////////// combo moneda
		$query = $this->db->select('idmoneda, descripcion')->where("estado", "A")
			->order_by("idmoneda", "asc")->get("general.moneda");
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idmoneda","name"=>"idmoneda","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem($query->result_array());
		if( isset($data["preventa"]["idmoneda"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idmoneda"]);
		}
		$data["moneda"] = $this->combobox->getObject();
		
		///////////////////////////////////////////////////// combo tipo operacion (sunat)
		$query = $this->db->select('codtipo_operacion, descripcion')
			->order_by("codtipo_operacion", "asc")->get("general.tipo_operacion");
		
		$this->combobox->init();
		$this->combobox->setAttr("id", "codtipo_operacion");
		$this->combobox->setAttr("name", "codtipo_operacion");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["preventa"]["codtipo_operacion"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["codtipo_operacion"]);
		}
		$data["tipo_operacion"] = $this->combobox->getObject();
		
		//////////////////////////////////////// combos temporales facturacion /////////////////////////////
		$query = $this->db->order_by("orden", "asc")->get("general.grupo_igv");
		$this->combobox->removeAllItems();
		$this->combobox->setAttr("id", "grupo_igv_temp");
		$this->combobox->setAttr("name", "grupo_igv_temp");
		$this->combobox->addItem($query->result_array(), array("codgrupo_igv","decripcion","tipo_igv_default","tipo_igv_oferta","igv"));
		$data["combo_grupo_igv"] = $this->combobox->getObject();
		
		$sql = "select codtipo_igv, codtipo_igv||': '||descripcion as descripcion from general.tipo_igv order by 1";
		$query = $this->db->query($sql);
		$this->combobox->removeAllItems();
		$this->combobox->setAttr("id", "tipo_igv_temp");
		$this->combobox->setAttr("name", "tipo_igv_temp");
		$this->combobox->addItem($query->result_array());
		$data["combo_tipo_igv"] = $this->combobox->getObject();
		
		$data["default_igv"] = $this->get_param("default_igv");
		
		//////////////////////////////////////// combos secciones /////////////////////////////
		//$query = $this->db->order_by("idseccion", "asc")->get("compra.seccion");
		// $query = $this->db->order_by("descripcion", "asc")->get("compra.seccion");
		// $this->combobox->removeAllItems();
		// $this->combobox->setAttr("id", "seccion_temp");
		// $this->combobox->setAttr("name", "seccion_temp");
		// $this->combobox->addItem($query->result_array(), array("idseccion","decripcion","abreviatura","estado"));
		// $data["combo_seccion"] = $this->combobox->getObject();
		
			///////////////////////////////////////////////////// combo tipo venta
		$query = $this->db->select('idseccion, descripcion')
			->where("estado", "A")
			->order_by("descripcion", "asc")->get("compra.seccion");
		$this->combobox->removeAllItems();
		$this->combobox->setAttr("id", "idseccion");
		$this->combobox->setAttr("name", "idseccion");
		$this->combobox->setAttr("class", "form-control input-xs");
		//$this->combobox->setAttr("required", "");
		$this->combobox->addItem($query->result_array());
		
		$data["combo_seccion"] = $this->combobox->getObject();
		///////////////////////////////////////////////////////// combo vendedor
		// $idperfil = 4; // id del perfil vendedor, tal vez deberia ser contante
		$idperfil = $this->get_param("idtipovendedor"); // id del perfil vendedor, tal vez deberia ser constante
		$this->load_model("usuario");
		$datos = $this->usuario->get_vendedor($this->get_var_session("idsucursal"), $idperfil);
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idvendedor","name"=>"idvendedor","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem($datos);
		if( isset($data["preventa"]["idvendedor"]) ) {
			$this->combobox->setSelectedOption($data["preventa"]["idvendedor"]);
		}
		else {
			$this->combobox->setSelectedOption($this->get_var_session("idusuario"));
		}
		$data["vendedor"] = $this->combobox->getObject();

		$data["controller"] = $this->controller;
		
		$igv = $this->get_param("igv");
		if(!is_numeric($igv)) {
			$igv = 18;
		}
		$data["valor_igv"] = $igv;
		$data["validar_ruc"] = $this->get_param("validar_ruc");
		
		$nueva_venta = "true";
		if( isset($data["preventa"]["idpreventa"]) ) {
			$nueva_venta = "false";
		}
		$this->js("<script>var _es_nuevo_ = $nueva_venta;</script>", false);
		
		if( isset($data["detalle"]) ) {
			$this->js("<script>var data_detalle = ".json_encode($data["detalle"]).";</script>", false);
		}
		$this->css('plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox');
		$this->css("plugins/datapicker/datepicker3");
		$this->css('plugins/iCheck/custom');
		
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		$this->js('plugins/iCheck/icheck.min');
		$this->js('form/'.$this->controller.'/form');
		
		
		$this->load_controller("cliente");
		$data["form_cliente"] = $this->cliente_controller->form(null, "cli_", true);
		
		$this->load_controller("producto");
		//$data["form_producto"] = $this->producto_controller->form(null, "", true);
		$data["form_producto"] = $this->producto_controller->form(null, "", true);
		$data["form_producto_seccion"] = $this->producto_controller->form_seccion_medida(null, "", true);


		$this->js('form/cliente/modal');
		
		$fc = $this->get_param("fixed_venta");
		if(!is_numeric($fc)) {
			$fc = 2;
		}
		$this->js("<script>var _fixed_venta = $fc;</script>", false);
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	public function filtros_grilla($pendiente) {
		$this->load_library("combobox");
		$this->combobox->setAttr("class", "form-control");
		$this->combobox->addItem("", "TODOS");
		
		$html = '<div class="row">';
		// div y combobox tipo venta
// div y combobox cancelado
		
		// div y combobox recepcionado
		$this->combobox->setAttr("filter", "pendiente");
		$this->combobox->removeItems(1);
//		$this->combobox->setAttr("class", "form-control");
		$this->combobox->addItem("S", "PENDIENTE");
		$this->combobox->addItem("N", "ATENDIDO");
		
		$html .= '<div class="col-sm-4"><div class="form-group">';
		$html .= '<label class="control-label">Atendido</label>';
		$html .= $this->combobox->getObject();
		$html .= '</div></div>';
		
		$html .= '</div>';
		
		$this->set_filter($html);
	}

	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		$this->load_model("venta.preventa_view");
		
		$this->load->library('datatables');
		
		$this->datatables->setModel($this->preventa_view);
		$this->datatables->setIndexColumn("idpreventa");
		
		$pendiente = 'S';
		
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('pendiente', '=', $pendiente);
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		
		$this->datatables->setColumns(array('idpreventa','fecha_caducidad','cliente','tipoventa',
			'moneda_abreviatura','total','tipodocumento'));
			// 'moneda_abreviatura','total','tipodocumento','serie'));
		
		$columnasName = array(
			array('Id','5%')
			,array('Fecha Caducidad','18%')
			,array('Cliente','28%')
			,array('Tipo','12%')
			,array('Moneda','8%')
			,array('Total','8%')
			// ,array('Vendedor','15')
			,array('CdP','16%')
			// ,array('Serie','8%')
		);
		
		$table = $this->datatables->createTable($columnasName);
		$script = "<script>".$this->datatables->createScript()."</script>";
		$this->js($script, false);
		
		$this->filtros_grilla($pendiente);
		
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Preventa");
		$this->set_subtitle("");
		
		$this->set_content($this->form());
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model("venta.preventa_view");
		$this->preventa_view->set_column_pk("idpreventa");
		$data["preventa"] = $this->preventa_view->find($id);
		
		$this->load_model("detalle_preventa");
		$data["detalle"] = $this->detalle_preventa->get_items($id, $data["preventa"]["idsucursal"]);
		
		// echo "<pre>";var_dump($data);
		$this->set_title("Modificar Preventa");
		$this->set_subtitle("");
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	/**
	 * Metodo para guardar un registro
	 */	
	public function guardar() {
		$this->load_model("venta.preventa");
		$this->load_model("venta.detalle_preventa");
		
		$fields = $this->input->post();
		$fields['idsucursal'] = $this->get_var_session("idsucursal");
		$fields['idusuario'] = $this->get_var_session("idusuario");
		$fields['idmodalidad'] = 1;
		//$fields['fecha_caducidad'] = date("Y-m-d H:i:s");
		$fields['fecha'] = date("Y-m-d H:i:s");
		$fields['estado'] = "A";
		$fields["pendiente"] = "S";
		$fields['aprobado'] = "N";
		$fields['enviado_email'] = "N";

		if(empty($fields["descuento"]))
			$fields["descuento"] = 0;
		if(empty($fields["igv"]))
			$fields["igv"] = 0;
		if(empty($fields["idcliente"]))
			$fields["idcliente"] = 0;
		if(empty($fields["idrampa"]))
			$fields["idrampa"] = 0;
		if(empty($fields["idmecanico"]))
			$fields["idmecanico"] = 0;		
        // if(empty($fields["idmodalidad"]))
			// $fields["idmodalidad"] = 0;
		if(empty($fields["idvendedor"]))
			$fields["idvendedor"] = $this->get_var_session("idusuario");
		// print_r($fields);return;
		$this->db->trans_start(); // inciamos transaccion
		
		if(empty($fields["idpreventa"])) {
			$idpreventa = $this->preventa->insert($fields);
		} else {
			$this->preventa->update($fields);
			$idpreventa = $this->preventa->get("idpreventa");
			
			// eliminamos el detalle de la preventa
			$this->detalle_preventa->delete(array("idpreventa"=>$idpreventa));
		}
		
		$this->detalle_preventa->set("idpreventa", $idpreventa);
		$this->detalle_preventa->set("estado", "A");
		foreach($fields["deta_idproducto"] as $key=>$val) {
			$this->detalle_preventa->set("iddetalle_preventa", ($key+1));
			$this->detalle_preventa->set("idproducto", $val);
			$this->detalle_preventa->set("idseccion", $fields["deta_seccion"][$key]);
			$this->detalle_preventa->set("idunidad", $fields["deta_idunidad"][$key]);
			$this->detalle_preventa->set("cantidad", $fields["deta_cantidad"][$key]);
			$this->detalle_preventa->set("precio", $fields["deta_precio"][$key]);
			$this->detalle_preventa->set("idalmacen", $fields["deta_idalmacen"][$key]);
			$this->detalle_preventa->set("serie", $fields["deta_series"][$key]);
			$this->detalle_preventa->set("oferta", $fields["deta_oferta"][$key]);
			$this->detalle_preventa->set("codgrupo_igv", $fields["deta_grupo_igv"][$key]);
			$this->detalle_preventa->set("codtipo_igv", $fields["deta_tipo_igv"][$key]);
			$this->detalle_preventa->insert(null, false);
		}
		
		$this->db->trans_complete(); // finalizamos transaccion
		
		$this->response($this->preventa->get_fields());
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model("venta.preventa");
		
		$fields['idpreventa'] = $id;
		$fields['estado'] = "I";
		$this->preventa->update($fields);
		
		$this->response($fields);
	}
	public function aprobar_preventa($id){
		//$this->load_model("venta.preventa");
		$this->load_model($this->controller);
		$fields['idpreventa'] = $id;
		$fields['aprobado'] = "S";
		$this->preventa->update($fields);
		
		$this->response($fields);
	}
	public function grilla_popup() {
		$this->load_model("venta.preventa_view");
		$this->load->library('datatables');
		
		$this->datatables->setModel($this->preventa_view);
		$this->datatables->setIndexColumn("idpreventa");
		
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('pendiente', '=', 'S');
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		
		$this->datatables->setPopup(true);
		$this->datatables->setColumns(array('idpreventa'
											,'fecha_caducidad'
											,'cliente'
											,'td_documento'
											,'moneda_abreviatura'
											,'modalidad'
											,'rampa'
											,'mecanico_nombre'
											,'vendedor_nombre'
											,'total'
									));

		$this->datatables->order_by('fecha_caducidad', 'desc');
		$this->datatables->setCallback('callbackPreventa');
		$table = $this->datatables->createTable(array(array('Id','10')
													,array('Fecha Caducidad','10')
													,array('Cliente','55')
													,array('Doc','4')
													,array('Moneda','2')
													,array('Modalidad','10')
													,array('Rampa','2')
													,array('Mecanico','2')
													,array('Vendedor','2')
													,array('Total','10')
												));
		$script = "<script>".$this->datatables->createScript("", false)."</script>";
		
		$this->response($script.$table);
	}
	
	public function get_detalle($id) {
		$this->load_model("detalle_preventa");
		$res = $this->detalle_preventa->get_items($id, $this->get_var_session("idsucursal"));
		$this->response($res);
	}
	
protected function getDataToPrint($idpreventa) {
		$sql = "select p.idpreventa, p.idcliente, p.fecha, p.subtotal, p.igv, p.subtotal+p.igv as total,
			p.referencia, t.descripcion as tipopago, m.simbolo as moneda, c.nombres||coalesce(' '||c.apellidos,'') as cliente,
			c.dni, c.ruc, c.direccion_principal as direccion
			from venta.preventa p
			join venta.forma_pago_cotizacion t on t.idforma_pago_cotizacion=p.idforma_pago_cotizacion
			join general.moneda m on m.idmoneda=p.idmoneda
			join venta.cliente c on c.idcliente=p.idcliente
			where p.idpreventa=?";
		$query = $this->db->query($sql, array($idpreventa));
		return $query->row_array();
	}
	
	protected function getItemToPrint($idpreventa) {
		$sql = "SELECT dv.iddetalle_preventa, dv.idproducto, dv.idseccion, p.descripcion_detallada as producto, 
			dv.idunidad, dv.cantidad, coalesce(dv.precio,0.00) as precio, dv.oferta, s.descripcion as seccion, 
			u.descripcion as unidad, dv.cantidad*coalesce(dv.precio,0.00) as importe, p.imagen_producto as imagen,
			mo.descripcion as modelo, ma.descripcion as marca
			FROM venta.detalle_preventa dv
			JOIN compra.producto p ON p.idproducto = dv.idproducto
			JOIN compra.unidad u ON u.idunidad = dv.idunidad
			JOIN compra.seccion s ON s.idseccion = dv.idseccion
			JOIN general.modelo as mo ON mo.idmodelo = p.idmodelo
			JOIN general.marca as ma ON ma.idmarca = p.idmarca
			WHERE dv.estado = 'A' AND dv.idpreventa = ?
			ORDER BY idseccion, iddetalle_preventa";
		$query = $this->db->query($sql, array($idpreventa));
		$rs = $query->result_array();
		
		$res = array();
		foreach($rs as $row) {
			// validamos las imagen
			if( ! empty($row["imagen"])) {
				if(file_exists(FCPATH."app/img/producto/".$row["imagen"]))
					$row["imagen"] = base64_encode(file_get_contents(FCPATH."app/img/producto/".$row["imagen"]));
				else
					$row["imagen"] = null;
			}
			
			if( ! array_key_exists($row["idseccion"], $res))
				$res[$row["idseccion"]] = array("seccion"=>$row["seccion"], "total"=>0, "items"=>array());
			
			$res[$row["idseccion"]]["total"] += floatval($row["importe"]);
			$res[$row["idseccion"]]["items"][] = $row;
		}
		
		return $res;
	}
	
	public function cotizacion($idpreventa, $buildPdf=false, $returnContent=false){
<<<<<<< HEAD
	    $this->unlimit();
	    
=======
		$this->unlimit();
		
>>>>>>> e3dd8fe64d3f49f1acb33204538101714cf1408b
		$this->load_model(array("seguridad.empresa","seguridad.sucursal"));
		
		$this->empresa->find($this->get_var_session("idempresa"));
		$this->sucursal->find($this->get_var_session("idsucursal"));
		
		$datos["empresa"] = $this->empresa->get_fields();
		$datos["sucursal"] = $this->sucursal->get_fields();
		$datos["cotizacion"] = $this->getDataToPrint($idpreventa);
		$datos["conceptos"] = $this->getItemToPrint($idpreventa);
		$datos["print"] = false;
		$datos["close"] = false;
		
		if( ! empty($datos["empresa"]["logo"])) {
			if(file_exists(FCPATH."app/img/empresa/".$datos["empresa"]["logo"]))
				$datos["logo"] = base64_encode(file_get_contents(FCPATH."app/img/empresa/".$datos["empresa"]["logo"]));
		}
		
		$buildPdf=true;
		if($buildPdf) {
			$datos["buildPdf"] = true;
			$html = $this->load->view("preventa/cotizacion", $datos, true);
			
			include_once APPPATH.'/libraries/Mpdf_print.php';
			$size = array(210,297);
			$mpdf = new Mpdf_print('c', $size, '', '', 10, 10);
			@$mpdf->WriteHTML($html);
			// $mpdf->AutoPrint();
			// echo $mpdf->Output("tmp.pdf",'S');exit;
			if($returnContent)
				return @$mpdf->Output("tmp.pdf",'S');
			
			$mpdf->Output("tmp.pdf",'I');
			return;
		}
		
		echo $this->load->view("preventa/cotizacion", $datos, true);
	}
	
	public function sendmail() {
		$post = $this->input->post();
		
		$correos = explode(",", preg_replace("/\s+/", "", $post["correos"]));
		foreach($correos as $correo) {
			if( ! is_email($correo)) {
				$this->exception("El correo electr&oacute;nico {$correo} no es v&aacute;lido");
				return;
			}
		}
		
		$this->unlimit();
		
		$this->load_model(array("seguridad.empresa", "venta.preventa"));
		
		$empresa = $this->empresa->find($this->get_var_session("idempresa"));
		$cotizacion = $this->preventa->find($post["idpreventa"]);
		
		$datos["nom_tipodoc"] = "COTIZACION";
		$datos["num_docu"] = $cotizacion["idpreventa"];
		$datos["empresa"] = $empresa["descripcion"];
		
		if( ! empty($empresa["logo"])) {
			if(file_exists(FCPATH."app/img/empresa/".$empresa["logo"]))
				$datos["logo"] = "data:image/png;base64, " . base64_encode(file_get_contents(FCPATH."app/img/empresa/".$empresa["logo"]));
		}
		
		$mensaje = $this->load->view('preventa/mail', $datos, true);
		
		$this->load_library("Jmail");
		$this->jmail->asunto($datos["nom_tipodoc"]." ".$datos["num_docu"]);
		$this->jmail->mensaje($mensaje);
		
		foreach($correos as $correo) {
			$this->jmail->correo($correo, $correo);
		}
		
		$pdfContentFile = $this->cotizacion($post["idpreventa"], true, true);
		
		$this->jmail->archivo($pdfContentFile, $datos["nom_tipodoc"]." ".$datos["num_docu"].".pdf");
		
		if($this->jmail->send())
			$this->response(true);
		else
			$this->response($this->jmail->error());
	}
}
