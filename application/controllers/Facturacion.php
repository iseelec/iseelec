<?php

include_once "Controller.php";

// ini_set("display_errors", 1);
// error_reporting(E_ALL);

class Facturacion extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Comprobantes electr&oacute;nicos");
		$this->set_subtitle("Envio de comprobantes");
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null, $prefix = "", $modal = false) {
		$data["controller"] = $this->controller;
		
		$this->load->library('combobox');
		
		// combo tipo documentos
		$sql = "SELECT tip_docu, tip_docu||': '||descripcion as desc
			FROM general.tipo_comprobante ORDER BY tip_docu";
		$query = $this->db->query($sql);
		$this->combobox->setAttr("id","tip_docu");
		$this->combobox->setAttr("name","tip_docu");
		$this->combobox->setAttr("class","form-control input-xs");
		$this->combobox->setStyle("width","120px");
		$this->combobox->addItem("", "TODOS");
		$this->combobox->addItem($query->result_array());
		$data['tipodocumento'] = $this->combobox->getObject();
		
		// combo situacion
		$sql = "SELECT ind_situ, ind_situ||': '||descripcion as desc 
			FROM general.situacion ORDER BY ind_situ";
		$query = $this->db->query($sql);
		$this->combobox->removeItems(1);
		$this->combobox->setAttr("id","ind_situ");
		$this->combobox->setAttr("name","ind_situ");
		$this->combobox->setAttr("class","form-control input-xs");
		// $this->combobox->addItem("", "TODOS");
		$this->combobox->addItem($query->result_array());
		$data['situacion'] = $this->combobox->getObject();
		
		// combo sucursal
		$sql = "select idsucursal,descripcion from seguridad.sucursal where estado='A'";
		$query = $this->db->query($sql);
		$this->combobox->removeItems(1);
		$this->combobox->setAttr("id","idsucursal");
		$this->combobox->setAttr("name","idsucursal");
		$this->combobox->setAttr("class","form-control input-xs");
		// $this->combobox->addItem("", "TODOS");
		$this->combobox->addItem($query->result_array());
		$data['sucursal'] = $this->combobox->getObject();
		
		$colsTabla = array(
			"tip_docu_desc"=>"Tipo Doc."
			,"num_docu"=>"Numero Doc."
			,"fec_carg"=>"F. Carga"
			,"fec_gene"=>"F. Generaci&oacute;n"
			,"fec_envi"=>"F. Envio"
			,"ind_situ_desc"=>"Situaci&oacute;n"
			,"des_obse"=>"Observaciones"
		);
		$data["cols"] = $colsTabla;
		$this->js("<script>var table_columns = ".json_encode(array_keys($colsTabla)).";</script>", false);
		
		$this->css("plugins/datapicker/datepicker3");
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		return null;
	}
	
	public function index($tpl = "") {
		$data = array(
			"menu_title" => $this->menu_title
			,"menu_subtitle" => $this->menu_subtitle
			,"content" => $this->form()
			,"with_tabs" => $this->with_tabs
		);
		
		if($this->show_path) {
			$data['path'] = $this->get_path();
		}
		
		$str = $this->load->view("content_empty", $data, true);
		$this->show($str);
	}
	
	protected function get_data($post) {
		$w = "";
		if( ! empty($post["fechai"]))
			$w .= " and b.fecha >= '".$post["fechai"]."'";
		if( ! empty($post["fechaf"]))
			$w .= " and b.fecha <= '".$post["fechaf"]."'";
		if( ! empty($post["tip_docu"]))
			$w .= " and b.tip_docu = '".$post["tip_docu"]."'";
		if( ! empty($post["ind_situ"]))
			$w .= " and b.ind_situ = '".$post["ind_situ"]."'";
		if( ! empty($post["serie"]))
			$w .= " and b.serie = '".$post["serie"]."'";
		if( ! empty($post["numero"]))
			$w .= " and b.numero::integer = ".intval($post["numero"]);
		if( ! empty($post["idsucursal"]))
			$w .= " and b.idsucursal = ".intval($post["idsucursal"]);
		if( ! empty($post["q"])) {
			$q = preg_replace("/\s+/", "%", trim($post["q"]));
			$w .= " and b.num_docu ilike '%{$q}%'";
		}
		if( ! empty($post["referencia"]))
			$w .= " and b.referencia = '".$post["referencia"]."'";
		if( ! empty($post["idreferencia"]))
			$w .= " and b.idreferencia = ".intval($post["idreferencia"]);
		
		$sql = "select b.idreferencia, b.referencia, b.tip_docu, b.num_docu, b.nom_arch, b.ind_situ, 
			t.descripcion as tip_docu_desc, b.fec_carg, coalesce(b.fec_gene,'-') as fec_gene, 
			coalesce(b.fec_envi,'-') as fec_envi, s.descripcion as ind_situ_desc, b.des_obse, b.num_ruc
			from venta.facturacion b
			join general.tipo_comprobante t on t.tip_docu = b.tip_docu
			join general.situacion s on s.ind_situ = b.ind_situ
			where b.estado='A' {$w}
			order by b.fecha_registro desc";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_records() {
		$post = $this->input->post();
		$arr = $this->get_data($post);
		$this->response($arr);
	}
	
	protected function formatted_data($datos) {
		$arr = $this->get_data($datos);
		if(count($arr) > 0)
			return $arr[0];
		return false;
	}
	
	public function update() {
		$this->load->library('jfacturacion');
		$d = $this->input->post();
		$this->jfacturacion->load($d["referencia"], $d["idreferencia"]);
		$res = $this->jfacturacion->get_estado();
		if( ! empty($d["udt"])) {
			$this->response($this->formatted_data($res));
			return;
		}
		return $res;
	}
	
	public function generar() {
		$this->unlimit();
		$dfact = $this->update();
		if(empty($dfact["ind_situ"])) {
			$dfact["ind_situ"] = "01";
		}
		
		$this->load_model("general.situacion");
		$this->situacion->find($dfact["ind_situ"]);
		if($this->situacion->get("generar") == "S") {
			$excepc = array("documento_baja", "resumen_diario", "guiaremision");
			
			if( ! in_array($dfact["referencia"], $excepc)) {
				$estado = true;
				if($dfact["referencia"] == "venta") {
					$this->load_model("venta.venta");
					$v = $this->venta->find($dfact["idreferencia"]);
					$t = $v["subtotal"] + $v["igv"] - $v["descuento"];
					$estado = $this->is_valid_doc($v["idtipodocumento"],$v["serie"],$v["idcliente"],$t,$v["idmoneda"]);
				}
				else if($dfact["referencia"] == "notacredito" || $dfact["referencia"] == "notadebito") {
					$this->load_model("venta.notacredito");
					$v = $this->notacredito->find($dfact["idreferencia"]);
					$t = $v["subtotal"] + $v["igv"];
					$estado = $this->is_valid_doc_nota($v["idtipodocumento"],$v["serie"],$v["iddocumento_ref"],$v["serie_ref"],$v["idcliente"],$t,$v["idmoneda"]);
				}
				
				if($estado !== true) {
					$this->exception($estado);
					return;
				}
				
				$this->jfacturacion->crear_files();
				$this->jfacturacion->enviar_files();
				$dfact = $this->jfacturacion->crear_comprobante();
			}
			else {
				$this->jfacturacion->crear_files();
				$this->jfacturacion->enviar_files();
				$dfact = $this->jfacturacion->crear_comprobante();
			}
		}
		
		$this->response($this->formatted_data($dfact));
	}
	
	public function send() {
		$this->unlimit();
		$dfact = $this->update();
		
		$this->load_model("general.situacion");
		$this->situacion->find($dfact["ind_situ"]);
		
		if($this->situacion->get("enviar") == "S") {
			if($dfact["referencia"] != "documento_baja") {
				$dfact = $this->jfacturacion->enviar_comprobante();
			}
			else {
				// si es comunicacion de baja, verificamos que la factura este en situacion enviado para dar de baja
				$sql = "select f.*
					from venta.documento_baja d
					join venta.facturacion f on f.idreferencia=d.idreferencia and f.referencia=d.referencia
					where d.iddocumento_baja=?";
				$query = $this->db->query($sql, array($dfact["idreferencia"]));
				if($query->num_rows() <= 0) {
					$this->exception("No se puede encontrar la factura referenciada en la comunicacion de baja");
					return;
				}
				
				$f = $query->row_array();
				$this->situacion->find($f["ind_situ"]);
				
				if($this->situacion->get("baja") != "S") {
					$this->exception("Primero debe enviar el comprobante ".$f["num_docu"]." a SUNAT y luego enviar la comunicacion de baja");
					return;
				}
				
				$dfact = $this->jfacturacion->enviar_comprobante();
			}
		}
		
		$this->response($this->formatted_data($dfact));
	}
	
	public function download() {
		$this->unlimit();
		
		$this->load->library('jfacturacion');
		$post = $this->input->get();
		$fact = $this->jfacturacion->load($post["referencia"], $post["idreferencia"]);
		if(empty($fact["xml_content"])) {
			$fact = $this->update();
		}
		
		$ext = empty($post["ext"]) ? "zip" : strtolower($post["ext"]);
		$filename = $fact["num_docu"].".".$ext;
		
		$content = "";
		if($ext == "xml")
			$content = base64_decode($fact["xml_content"]);
		else if($ext == "pdf")
			$content = base64_decode($fact["pdf_content"]);
		else if($ext == "zip") {
			$zip_file = new ZipArchive();
			$zip_file->open($this->jfacturacion->path."/".$filename, ZipArchive::CREATE);
			$zip_file->addFromString($fact["num_docu"].".xml", base64_decode($fact["xml_content"]));
			$zip_file->addFromString($fact["num_docu"].".pdf", base64_decode($fact["pdf_content"]));
			$zip_file->close();
			$content = file_get_contents($this->jfacturacion->path."/".$filename);
			unlink($this->jfacturacion->path."/".$filename);
		}
		
		header('Content-Description: File Transfer');
		header('Content-Transfer-Encoding: binary');
		header('Cache-Control: public, must-revalidate, max-age=0');
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Type: application/force-download');
		header('Content-Type: application/octet-stream', false);
		header('Content-Type: application/download', false);
		header('Content-Type: application/'.$ext, false);
		header('Content-disposition: attachment; filename="'.$filename.'"');
		header('Content-Length: '.strlen($content));
		echo $content;
		exit;
	}
	
	public function resumen_diario() {
		$this->unlimit();
		
		$fecha = $this->input->post("fecha");
		
		$sql = "select b.fecha_registro, b.fec_carg, b.tip_docu, b.num_docu, b.tip_docu_cliente, 
			b.num_docu_cliente, b.gravado, b.exonerado, b.inafecto, b.gratuito, b.suma_igv, 
			b.gravado + b.exonerado + b.inafecto + b.suma_igv as total, v.estado,
			b.referencia, b.idreferencia, b.ind_situ, null::character varying(5) as tip_docu_ref, 
			null::character varying(15) as serie_ref, null::character varying(15) as numero_ref
			from venta.facturacion b
			join venta.venta v on v.idventa = b.idreferencia and b.referencia='venta'
			where b.estado='A' and b.tip_docu = '03' 
			and (b.fecha='{$fecha}' or v.fecha_hora_anulacion::date='{$fecha}')
			and (b.ind_situ not in ('03','04','11','12') or v.estado='I')
			union
			select b.fecha_registro, b.fec_carg, b.tip_docu, b.num_docu, b.tip_docu_cliente, 
			b.num_docu_cliente, b.gravado, b.exonerado, b.inafecto, b.gratuito, b.suma_igv, 
			b.gravado + b.exonerado + b.inafecto + b.suma_igv as total, n.estado,
			b.referencia, b.idreferencia, b.ind_situ, 
			bv.tip_docu as tip_docu_ref, bv.serie as serie_ref, bv.numero as numero_ref
			from venta.facturacion b
			join venta.notacredito n on n.idnotacredito = b.idreferencia and b.referencia='notacredito'
			join venta.facturacion bv on bv.idreferencia = n.idventa and bv.referencia='venta' and bv.tip_docu = '03'
			where b.estado='A' and b.tip_docu='07'
			and (b.fecha='{$fecha}' or n.fecha_hora_anulacion::date='{$fecha}')
			and (b.ind_situ not in ('03','04','11','12') or n.estado='I')
			order by fecha_registro";
		
		$query = $this->db->query($sql);
		$arr = $query->result_array();
		
		if( ! empty($arr)) {
			$sql = "select coalesce(max(correlativo),0) as corr 
				from venta.resumen_diario where fecha=? and estado='A'";
			$query = $this->db->query($sql, array(date("Y-m-d")));
			$correlativo = 1;
			if($query->num_rows() > 0)
				$correlativo = intval($query->row()->corr) + 1;
			
			$this->load_model(array("venta.resumen_diario", "venta.detalle_resumen_diario"));
			$this->resumen_diario->text_uppercase(false);
			$this->detalle_resumen_diario->text_uppercase(false);
			
			$this->resumen_diario->set("idsucursal", $this->get_var_session("idsucursal"));
			$this->resumen_diario->set("idusuario", $this->get_var_session("idusuario"));
			$this->resumen_diario->set("fecha", date("Y-m-d"));
			$this->resumen_diario->set("correlativo", $correlativo);
			$this->resumen_diario->set("estado", "A");
			$id = $this->resumen_diario->insert();
			
			foreach($arr as $i=>$row) {
				$estado = ($row["estado"]=="A") ? 1 : 3;
				
				$this->detalle_resumen_diario->set("iddetalle_resumen_diario", (intval($i)+1));
				$this->detalle_resumen_diario->set("idresumen_diario", $id);
				$this->detalle_resumen_diario->set("idreferencia", $row["idreferencia"]);
				$this->detalle_resumen_diario->set("referencia", $row["referencia"]);
				$this->detalle_resumen_diario->set("tip_docu_modifica", $row["tip_docu_ref"]);
				$this->detalle_resumen_diario->set("serie_docu_modifica", $row["serie_ref"]);
				$this->detalle_resumen_diario->set("nro_docu_modifica", $row["numero_ref"]);
				$this->detalle_resumen_diario->set("estado_docu", $estado);
				$this->detalle_resumen_diario->set("estado", "A");
				$this->detalle_resumen_diario->insert();
			}
			
			$this->load->library('jfacturacion');
			$this->jfacturacion->register("resumen_diario", $id, $this->resumen_diario->get("idsucursal"));
			$this->jfacturacion->crear_files();
			$this->jfacturacion->enviar_files();
			$this->jfacturacion->crear_comprobante();
			// $this->jfacturacion->enviar_comprobante();
		}
		else {
			$this->exception("No se han encontrado boletas emitidas pendientes de envio o anulaciones segun la fecha ingresada");
			return;
		}
		
		$this->response(true);
	}
	
	public function restablecer() {
		$this->unlimit();
		
		$dfact = $this->update();
		
		$this->load_model("general.situacion");
		$this->situacion->find($dfact["ind_situ"]);
		
		if($this->situacion->get("restablecer") == "S") {
			$this->jfacturacion->restablecer($dfact["nom_arch"]);
		}
		
		unset($dfact["ind_situ"]);
		
		$this->response($this->formatted_data($dfact));
	}
	
	public function comunicacion_baja($datos, $motivo = "") {
		$tdoc_baja = array("01", "07", "08");
		if( ! in_array($datos["tipo_doc"], $tdoc_baja))
			return false;
		
		if($datos["tipo_doc"] == "07" || $datos["tipo_doc"] == "08") {
			// Verificamos si la nota modifica alguna factura
			$sql = "select b.* from venta.".$datos["referencia"]." n
				join venta.facturacion b on b.idreferencia=n.idventa and b.referencia='venta' and b.estado='A'
				where n.idnotacredito=?";
			$query = $this->db->query($sql, array($datos["idreferencia"]));
			if($query->num_rows() <= 0)
				return false;
			if($query->row()->tipo_doc != "01") // documento no es factura
				return false;
		}
		
		$this->unlimit();
		
		// registramos la comunicacion de baja
		$sql = "select * from venta.documento_baja where estado='A' and idreferencia=? and referencia=?";
		$query = $this->db->query($sql, array($datos["idreferencia"], $datos["referencia"]));
		
		$ci =& get_instance();
		
		if($query->num_rows() > 0) {
			$modelbaja = $query->row_array();
		}
		else {
			$sql = "select coalesce(max(correlativo),0) as corr 
				from venta.documento_baja where fecha=? and estado='A'";
			$query = $this->db->query($sql, array(date("Y-m-d")));
			$correlativo = 1;
			if($query->num_rows() > 0)
				$correlativo = intval($query->row()->corr) + 1;
			
			$ci->load_model("venta.documento_baja");
			$ci->documento_baja->text_uppercase(false);
			
			$ci->documento_baja->set("idsucursal", $this->get_var_session("idsucursal"));
			$ci->documento_baja->set("idusuario", $this->get_var_session("idusuario"));
			$ci->documento_baja->set("fecha", date("Y-m-d"));
			$ci->documento_baja->set("correlativo", $correlativo);
			$ci->documento_baja->set("idreferencia", $datos["idreferencia"]);
			$ci->documento_baja->set("referencia", $datos["referencia"]);
			$ci->documento_baja->set("motivo", $motivo);
			$ci->documento_baja->set("estado", "A");
			$ci->documento_baja->insert();
			$modelbaja = $ci->documento_baja->get_fields();
		}
		
		// ingresamos a la bandeja la comunicacion de baja
		$ci->load->library('jfacturacion');
		$dfact = $ci->jfacturacion->load("documento_baja", $modelbaja["iddocumento_baja"]);
		
		if(empty($dfact["fecha_registro"]) && empty($dfact["idsucursal"])) {
			$ci->jfacturacion->register("documento_baja", $modelbaja["iddocumento_baja"], $modelbaja["idsucursal"]);
			$ci->jfacturacion->crear_files();
			$ci->jfacturacion->enviar_files();
			$dfact = $ci->jfacturacion->crear_comprobante();
		}
		
		$this->response($this->formatted_data($dfact));
	}
}
