<?php

include_once "Controller.php";

class Proyecto extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Mantenimiento de Proyectos");
		// $this->set_subtitle("Lista de Proyectos");
		//$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
				$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null, $prefix = "", $modal=false) {
		if(!is_array($data)) {
			$data = array();
		}
		
		// creamos los combos
		// $this->load->library('combobox','','combobox');
		$this->load_library('combobox');
		
		// combo usuario
		// $this->combobox->setAttr("id",$prefix."idusuario");
		// $this->combobox->setAttr("name","idusuario");
		// $this->combobox->setAttr("class","form-control");
		// $this->combobox->setAttr("required","");
		$this->combobox->setAttr(array("id"=>"idusuario","name"=>"idusuario","class"=>"form-control","required"=>""));
		$this->db->select('idusuario,nombres')->order_by("nombres", "asc");
		$query = $this->db->where("estado","A")->get("seguridad.usuario");
		$this->combobox->addItem("");
		$this->combobox->addItem($query->result_array());
		$data['usuarioes'] = $this->combobox->getAllItems();
		if( isset($data["proyecto"]["idusuario"]) ) {
			$this->combobox->setSelectedOption($data["proyecto"]["idusuario"]);
		}
		$data['usuario'] = $this->combobox->getObject();
	////combo estatus
		$this->combobox->removeAllItems();
		$this->combobox->setAttr("id", "idestatus");
		$this->combobox->setAttr("name", "idestatus");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->addItem(" ", "Selecione..");
		$this->combobox->addItem("T", "Trabajando");
		$this->combobox->addItem("E", "Echo");
		$this->combobox->addItem("S", "Espera Supervición");
		$this->combobox->addItem("A", "Atascado");
		
		$data["combo_estatus"] = $this->combobox->getObject();	
	////combo PRIORIDAD
	//combo prioridad
		$this->combobox->removeAllItems();
		 $this->combobox->setAttr("id", "idprioridad");
		 $this->combobox->setAttr("name", "idprioridad");
		
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->addItem(" ", "Selecione..");
		$this->combobox->addItem("U", "Urgente");
		$this->combobox->addItem("A", "Alta");
		$this->combobox->addItem("M", "Media");
		$this->combobox->addItem("B", "Baja");
		
		$data["combo_prioridad"] = $this->combobox->getObject();

	
	//// combo fases
	$this->combobox->setAttr(array("id"=>"idfase","name"=>"idfase","class"=>"form-control","required"=>""));
		$this->db->select('idfase,descripcion')->order_by("descripcion", "asc");
		$query = $this->db->where("estado","A")->get("proyecto.fase");
		$this->combobox->init();
		$this->combobox->addItem("");
		$this->combobox->addItem($query->result_array());
		$data['fasees'] = $this->combobox->getAllItems();
		if( isset($data["fase"]["idfase"]) ) {
			$this->combobox->setSelectedOption($data["fase"]["idfase"]);
		}
		$data['fases'] = $this->combobox->getObject();
	
		// $data["tipo_precio_temp"] = $this->combobox->getObject();
		$data["tipo_precio_temp"] = '';
		
		$this->db->select('idmoneda,descripcion')->order_by("descripcion", "desc");
		$query = $this->db->where("estado","A")->get("general.moneda");
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"moneda_temp", "name"=>"moneda_temp", "class"=>"form-control input-sm"));
		$this->combobox->addItem($query->result_array());
		$data["moneda_temp"] = $this->combobox->getObject();
		
		if( !isset($data["proyecto"]["nro_codigo_proyecto"]) ) {
			$data["proyecto"]["nro_codigo_proyecto"] = str_pad($this->get_last_value(), 5, "0", STR_PAD_LEFT);
		}
		
		$data["corr_temp"] = $data["proyecto"]["nro_codigo_proyecto"];
		$data["controller"] = $this->controller;
		$data["prefix"] = $prefix;
		$data["modal"] = $modal;
		
		
		
		// formulario usuario
		// $this->load_controller("usuario");
		// $data["form_usuario"] = $this->usuario_controller->form(null, "uni_", true);
		
		$this->load_controller("cliente");
		$data["form_cliente"] = $this->cliente_controller->form(null, "cli_", true);
		$this->load_controller("fase");
		$data["form_fase"] = $this->fase_controller->form(null, "fas_", true);

		$this->js('form/usuario/modal');
		$this->js('form/fase/modal');
		$this->js('form/cliente/modal');
		
		$data["controller"] = $this->controller;
		$this->css('plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox');
		$this->css("plugins/datapicker/datepicker3");
		$this->css('plugins/iCheck/custom');
		
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		$this->js('plugins/iCheck/icheck.min');
		$this->js('form/'.$this->controller.'/index');
		
		// if( isset($data["proyecto"]["idproyecto"]) ) {
			// $this->js("<script>update_combo_usuario_temp();</script>", false);
		// }
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	// public function form_usuario_responsable($data=null, $prefix="", $modal=false) {
		// if(!is_array($data)) {
			// $data = array();
		// }
		
		// $query = $this->db->where("estado", "A")->order_by("descripcion", "asc")->get("seguridad.usuario");
		// $data["usuarioes"] = $query->result_array();
		
		// if(isset($data["proyecto"]["idusuario"])) {
			// $this->load_model("usuario");
			// $data["usuario"] = $this->usuario->find($data["proyecto"]["idusuario"]);
		// }
		// else {
			// $data["usuario"] = $data["usuarioes"];
		// }
		
		// if(isset($data["proyecto"]["idproyecto"])) {
			// $query = $this->db
				// ->select("u.descripcion,u.abreviatura,p.idusuario,p.cantidad_usuario,p.cantidad_usuario_min")
				// ->where("p.idproyecto", $data["proyecto"]["idproyecto"])
				// ->join("proyecto.usuario u", "u.idusuario=p.idusuario")
				// ->order_by("u.descripcion", "asc")->get("proyecto.proyecto_usuario p");
			// $data["proyecto_usuario"] = $query->result_array();
		// }
		
		/////////$data["permiso"] = $this->get_permisos();
		
		// $data["controller"] = $this->controller;
		// $data["prefix"] = $prefix;
		// $data["modal"] = $modal;
		
		/////$this->js('form/'.$this->controller.'/proyecto_usuario');
		
	
		
		/////return $this->load->view($this->controller."/form_proyecto_usuario", $data, true);
	// }
	
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		// cargamos el modelo y la libreria
		$this->load_model("proyecto");
		$this->load->library('datatables');
		
		// indicamos el modelo al datatables
		$this->datatables->setModel($this->proyecto);
		
		// filtros adicionales para la tabla de la bd (perfil en este caso)
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('idempresa', '=', $this->get_var_session("idempresa"));

		
		// indicamos las columnas a mostrar de la tabla de la bd
		$this->datatables->setColumns(array('idproyecto','descripcion_detallada','descripcion'));
		
		// columnas de la tabla, si no se envia este parametro, se muestra el 
		// nombre de la columna de la tabla de la bd
		$columnasName = array(
			'Codigo'
			,'Descripcion detallada'
			,'Descripcion generica'
			// ,'Telefono'
			// array('Descripci&oacute;n', '95%') // ancho de la columna
		);

		// generamos la tabla y el script para el dataTables
		$table = $this->datatables->createTable($columnasName);
		// $table = $this->datatables->createTable();
		$script = "<script>".$this->datatables->createScript()."</script>";
		$this->js($script, false);
		
		$row = $this->get_permisos();
		// if($row->nuevo == 1 || $row->editar == 1 || $row->eliminar == 1) {
		// 	$this->add_button("btn_usuario_responsable", "Asignar usuario responsable");
		// }
		
		return $table;
	}
	
	public function modal_grilla() {
		// cargamos el modelo y la libreria
		$this->load_model("proyecto");
		$this->load->library('datatables');
		
		// indicamos el modelo al datatables
		$this->datatables->setModel($this->proyecto);
		
		// filtros adicionales para la tabla de la bd (perfil en este caso)
		$this->datatables->where('estado', '=', 'A');
		
		// indicamos las columnas a mostrar de la tabla de la bd
		$this->datatables->setColumns(array('descripcion','stock_minimo','precio_proyecto'));
		
		// columnas de la tabla, si no se envia este parametro, se muestra el 
		// nombre de la columna de la tabla de la bd
		$columnasName = array(
			'Proyecto'
			,'Stock'
			,'Precio'
			// ,'Telefono'
			// array('Descripci&oacute;n', '95%') // ancho de la columna
		);

		// generamos la tabla y el script para el dataTables
		$table = $this->datatables->createTable($columnasName);
		// $table = $this->datatables->createTable();
		$script = "<script>".$this->datatables->createScript()."</script>";
		$this->js($script, false);
		
		$row = $this->get_permisos();
		if($row->nuevo == 1 || $row->editar == 1 || $row->eliminar == 1) {
			$this->add_button("btn_usuario_responsable", "Asignar usuario responsable");
		}
		
		return $table;
	}
	
	protected function get_last_value() {
		$query = $this->db->select("last_value")->get("proyecto.proyecto_idproyecto_seq");
		$data = $query->row();
		return ($data->last_value + 1);
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Proyecto");
		$this->set_subtitle("");
		
		$this->set_content($this->form());
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->set_title("Modificar Proyecto");
		$this->set_subtitle("");
	
		$this->load_model(array("proyecto.proyecto", "general.linea", "general.categoria", "general.marca", 
			"general.modelo", "general.color", "proyecto.proyecto_fase", "proyecto.fase", "proyecto.seccion", "proyecto.usuario", 
			"proyecto.proyecto_precio_unitario"));
		
		$idsucursal = $this->get_var_session("idsucursal");
		
		$data["proyecto"] = $this->proyecto->find($id);
		//$data["proyecto_alterno"] = $this->proyecto->find(intval($data["proyecto"]["codigo_alterno"]));
		 //echo '<pre>';print_r($data);echo '</pre>';exit;
		
		$usuario = $this->usuario->find($data["proyecto"]["idusuario"]);
		$data["tr_usuario"] = $this->table_tr_usuarioes($id, $usuario["nombres"]);
		
		$fase = $this->fase->find($data["proyecto"]["idfase"]);
		$data["tr_fases"] = $this->table_tr_fasees($id, $fase["descripcion"]);
		
		$data["tr_tareaf"] = $this->table_tr_tareaf($id);

		// $fase = $this->fase->find($data["proyecto"]["idfase"]);
		// $data["tr_fases"] = $this->table_tr_fasees($id, $fase["descripcion"]);

		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	public function usuario_responsable($id) {
		$this->load_model("proyecto");
		$data["proyecto"] = $this->proyecto->find($id);

		$this->set_title("Asignar usuarios responsables");
		$this->set_subtitle($data["proyecto"]["descripcion"]);

		$this->set_content($this->form_usuario_responsable($data));
		$this->index("content");
	}
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model(array("proyecto.proyecto", "proyecto.proyecto_usuario", "proyecto.proyecto_fase", "proyecto.proyecto_tareaf"));
		$this->proyecto->text_uppercase(false);
		$fields = $this->input->post();
		$fields['estado'] = "A";
		// $fields['fecha_inicio'] = date("d/m/Y");
		// $fields['fecha_fin'] = date("d/m/Y");
		$fields['idempresa'] = $this->get_var_session("idempresa");
		$fields['idsucursal'] = $this->get_var_session("idsucursal");
		if(empty($fields["idcliente"]))
			$fields["idcliente"] = 0;
				
		$this->db->trans_start();
		
		// guardamos el proyecto
		if(empty($fields["idproyecto"])) {
			if($this->proyecto->exists(array("descripcion"=>$fields["descripcion"])) == false) {
				//$this->proyecto->insert($fields);
				 $id = $this->proyecto->insert($fields);
				 $fields["idproyecto"] = $id;
			}
			else {
				$this->exception("El proyecto ".$fields["descripcion"]." ya existe");
			}
		}
		else {
			$this->proyecto->update($fields);
		}
		// eliminamos los responsables asignadas anteriormente
		$this->db->delete("proyecto.proyecto_usuario", array("idproyecto"=>$fields["idproyecto"]));
		// asignamos los responsables del py
		if(!empty($fields["proy_usuario"])) {
			$param["idproyecto"] = $fields["idproyecto"];
			foreach($fields["proy_usuario"] as $k=>$idusuario) {
					$param["idusuario"] = $idusuario;
					$param["dias_usuario"] = floatval($fields["proy_dias_usuario"][$k]);
					$param["sueldo_usuario"] = floatval($fields["proy_sueldo_usuario"][$k]);
					$param["viatico_usuario"] = floatval($fields["proy_viatico_usuario"][$k]);
					$param["estado"] = "A";
					$this->proyecto_usuario->insert($param, false);
			}
		}
		// eliminamos las fases asignadas anteriormente
		$this->db->delete("proyecto.proyecto_fase", array("idproyecto"=>$fields["idproyecto"]));
		//// asignamos las fases de responsable
		if(!empty($fields["proy_fase"])) {
			$param["idproyecto"] = $fields["idproyecto"];
			foreach($fields["proy_fase"] as $k=>$idfase) {
				///////if(!empty($fields["proy_dias_usuario"][$k]) && !empty($fields["proy_sueldo_usuario"][$k])) {
					$param["idfase"] = $idfase;
					$param["fecha_inicio"] = $fields["deta_fecha_i"][$k];
					$param["fecha_fin"] = $fields["deta_fecha_f"][$k];
					$param["idusuario"] = $fields["deta_usuario"][$k];
					$param["estado"] = "A";
					$this->proyecto_fase->insert($param, false);
				}
		}
		// eliminamos las tareas asignadas anteriormente
		$this->db->delete("proyecto.proyecto_tareaf", array("idproyecto"=>$fields["idproyecto"]));
		//// asignamos las tarea de responsable
		if(!empty($fields["deta_idfase"])) {
			$param["idproyecto"] = $fields["idproyecto"];
			foreach($fields["deta_idfase"] as $k=>$idfase) {
				///////if(!empty($fields["proy_dias_usuario"][$k]) && !empty($fields["proy_sueldo_usuario"][$k])) {
					$param["idfase"] = $idfase;
					$param["idtarea"] = $idfase + 1;
					$param["descripcion_fase"] = $fields["deta_descripcion_fase"][$k];
					$param["idusuario"] = $fields["deta_usuariot"][$k];
					$param["estatus"] = $fields["deta_estatus"][$k];
					$param["tiempo_dias"] = floatval($fields["deta_tiempo_dias"][$k]);
					$param["fecha_vencimiento"] = $fields["deta_fecha_vencimiento"][$k];
					$param["prioridad"] = $fields["deta_prioridad"][$k];
					$param["fecha_registro"] = date("d/m/Y");
					$param["estado"] = "A";
					$this->proyecto_tareaf->insert($param, false);
				}
		}
		$this->db->trans_complete();
		$this->response($fields);
	}
	
	public function guardar_usuario() {
		$post = $this->input->post();
		// eliminamos todas las usuarioes existentes
		$this->db->where("idproyecto", $post["idproyecto"]);
		$this->db->delete("proyecto.proyecto_usuario");
		
		// insertamos las usuarioes
		$this->load_model("proyecto_usuario");
		$param["idproyecto"] = $post["idproyecto"];
		
		if(!empty($post["idusuario"])) {
			foreach($post["idusuario"] as $k=>$v) {
				$param["idusuario"] = $v;
				$param["dias_usuario"] = $post["dias_usuario"][$k];
				$param["sueldo_usuario"] = $post["sueldo_usuario"][$k];
				$param["viatico_usuario"] = $post["viatico_usuario"][$k];
				
				$this->proyecto_usuario->insert($param, false);
			}
		}
		
		// guardamos la usuario minima
		$this->load_model("proyecto");
		$data = $this->proyecto->find($param["idproyecto"]);
		
		$param["idusuario"] = $data["idusuario"];
		$param["dias_usuario"] = 0;
		$param["sueldo_usuario"] = 0.00;
		$param["viatico_usuario"] = 0.00;
		
		$this->proyecto_usuario->save($param, false);
		
		$this->response($this->proyecto_usuario->get_fields());
	}
	
	public function guardar_fase() {
		$post = $this->input->post();
		
		// eliminamos todas las usuarioes existentes
		$this->db->where("idproyecto", $post["idproyecto"]);
		$this->db->delete("proyecto.proyecto_fase");
		
		// insertamos las fases
		$this->load_model("proyecto_fase");
		$param["idproyecto"] = $post["idproyecto"];
		
		if(!empty($post["idfase"])) {
			foreach($post["idfase"] as $k=>$v) {
				$param["idfase"] = $v;
				$param["fecha_inicio"] = $post["fecha_inicio"][$k];
				$param["fecha_fin"] = $post["fecha_fin"][$k];
				$param["idusuario"] = $post["idusuario"][$k];
				$param["estado"] = 'A';
				$this->proyecto_fase->insert($param, false);
			}
		}
		
		// guardamos la fase minima
		$this->load_model("proyecto");
		$data = $this->proyecto->find($param["idproyecto"]);
		$param["idfase"] = $data["idfase"];
		$param["fecha_inicio"] = date("Y-m-d");
		$param["fecha_fin"] = date("Y-m-d");
		$param["idusuario"] = $data["idusuario"];
		$param["estado"] = 'A';
		$this->proyecto_fase->save($param, false);
		$this->response($this->proyecto_fase->get_fields());
	}
/// metodo para guardar las tareas por fase
public function guardar_tareaf() {
		$post = $this->input->post();
		
		// eliminamos todas las tareas existentes
		$this->db->where("idproyecto", $post["idproyecto"]);
		$this->db->delete("proyecto.proyecto_tareaf");
		
		// insertamos las fases
		$this->load_model("proyecto_tareaf");
		$param["idproyecto"] = $post["idproyecto"];
		
		if(!empty($post["idfase"])) {
			foreach($post["idfase"] as $k=>$v) {
				$param["idfase"] = $v;
				$param["idtarea"] = $k + 1;
				$param["descripcion_fase"] = $post["descripcion_fase"][$k];
				$param["idusuario"] = $post["deta_usuario"][$k];
				$param["estatus"] = $post["deta_estatus"][$k];
				$param["tiempo_dias"] = $post["tiempo_dias"][$k];
				$param["fecha_vencimiento"] = $post["fecha_vencimiento"][$k];
				$param["prioridad"] = $post["deta_prioridad"][$k];
				$param["fecha_registro"] = date("d/m/Y");
				$param["estado"] = "A";	
				$this->proyecto_tareaf->insert($param, false);
		
	
	}
		}
		
		// guardamos la fase minima
		// $this->load_model("proyecto");
		// $data = $this->proyecto->find($param["idproyecto"]);
		// $param["idfase"] = $data["idfase"];
		// $param["idtarea"] = $data["idfase"];
		// $param["fecha_vencimiento"] = date("Y-m-d");
		// $param["fecha_fin"] = date("Y-m-d");
		//$param["idusuario"] = $data["idusuario"];
		//$param["estado"] = 'A';
		// $this->proyecto_tareaf->save($param, false);
		// $this->response($this->proyecto_tareaf->get_fields());
	}
//echo '<pre>';print_r($param);echo '</pre>';exit;
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model("proyecto");
		
		// cambiamos de estado
		$fields['idproyecto'] = $id;
		$fields['estado'] = "I";
		$this->proyecto->update($fields);
		
		$this->response($fields);
	}
	
	/**
	 * Metodo para obtener los datos del proyecto
	 */
	public function get($id) {
		$idsucursal = $this->get_var_session("idsucursal");
		
		$sql = "SELECT p.*, pu.precio_proyecto, pu.precio_venta
			FROM proyecto.proyecto p
			LEFT JOIN proyecto.proyecto_precio_unitario pu 
			on pu.idproyecto=p.idproyecto and pu.idsucursal={$idsucursal}
			WHERE p.idproyecto = ?";
		
		$query = $this->db->query($sql, array($id));
		
		$data = $query->row_array();
		if(empty($data["precio_venta"])) {
			// obtenemos el precio de venta
			$precios = $this->get_real_precio_venta($data["idproyecto"], true);
			$data["precio_venta"] = array_shift($precios);
		}
		
		$this->response($data);
	}
	
	public function get_usuarioes($id, $idusuario = FALSE) {
		$this->load_model("proyecto");
		$this->response($this->proyecto->usuarioes($id, $idusuario));
	}

	public function get_fasees($id, $idfase = FALSE) {
		$this->load_model("proyecto");
		$this->response($this->proyecto->fasees($id, $idfase));
	}
	
	public function get_all($id, $return=false) {
		$this->load_model(array("usuario", "proyecto", "fase"));
		$data["proyecto"] = $this->proyecto->find($id);
		$data["proyecto_usuario"] = $this->usuario->find($data["proyecto"]["idusuario"]);
		$data["usuarioes"] = $this->proyecto->usuarioes($id);
		$data["proyecto_fase"] = $this->fase->find($data["proyecto_fase"]["idfase"]);
		$data["fasees"] = $this->proyecto->fasees($id);
		if($return) {
			return $data;
		}
		$this->response($data);
	}

	

	
	public function autocomplete_descripcion() {
		$txt = trim($this->input->post("startsWith"));
		$txt = "%".preg_replace('/\s+/', '%', $txt)."%";
		
		$sql = "SELECT distinct descripcion FROM proyecto.proyecto
			WHERE estado='A' and descripcion ILIKE ?
			ORDER BY descripcion LIMIT ?";
		$query = $this->db->query($sql, array($txt, $this->input->post("maxRows")));
		$this->response($query->result_array());
	}
	
	public function table_tr_usuarioes($idproyecto, $udesc) {
		$rs = $this->proyecto->usuarioes($idproyecto);
		$html = '';
		if(!empty($rs)) {
			foreach($rs as $val) {
				$html .= '<tr index="'.$val["idusuario"].'">';
				$html .= '<td><input type="hidden" name="proy_usuario[]" class="proy_usuario" value="'.$val["idusuario"].'">'.$val["nombres"].'</td>';
				$html .= '<td>Dias <strong> de trabajo: </strong></td>';
				$html .= '<td><input type="text" name="proy_dias_usuario[]" class="proy_dias_usuario form-control input-xs" value="'.$val["dias_usuario"].'"></td>';
				$html .= '<td>Sueldo <strong>x Día:</strong></td>';
				$html .= '<td><input type="text" name="proy_sueldo_usuario[]" class="proy_sueldo_usuario form-control input-xs" value="'.$val["sueldo_usuario"].'"></td>';
				$html .= '<td>Viaticos <strong>x Día:</strong></td>';
				$html .= '<td><input type="text" name="proy_viatico_usuario[]" class="proy_viatico_usuario form-control input-xs" value="'.$val["viatico_usuario"].'"></td>';
				$html .= '<td><button class="btn btn-danger btn-xs btn-del-usuario"><i class="fa fa-trash"></i></button></td>';
				$html .= '</tr>';
			}
		}
		return $html;
	}
	
public function table_tr_fasees($idproyecto, $udesc) {
		$rs = $this->proyecto->fasees($idproyecto);
		$html = '';
		if(!empty($rs)) {
			// $this->load->library('combobox', '', 'combo_usuario');
			// $this->combo_usuario->setAttr("name","deta_usuario[]");
			// $this->combo_usuario->setAttr("class","deta_usuario form-control input-xs");
			// $this->combo_usuario->addItem($this->proyecto->fasees($idproyecto));
			
			foreach($rs as $val) {
				// $this->combo_usuario->setSelectedOption($val["nombres"]);
				$html .= '<tr index="'.$val["idfase"].'">';
				$html .= '<td><input type="hidden" name="proy_fase[]" class="proy_fase" value="'.$val["idfase"].'">'.$val["descripcion"].'</td>';
				$html .= '<td>Fecha <strong> inicio: </strong></td>';
				$html .= '<td><input type="text" name="deta_fecha_i[]" class="deta_fecha_i form-control input-xs" value="'.$val["fecha_inicio"].'" readonly></td>';
				$html .= '<td>Fecha <strong>x Fin:</strong></td>';
				$html .= '<td><input type="text" name="deta_fecha_f[]" class="deta_fecha_f form-control input-xs" value="'.$val["fecha_fin"].'" readonly></td>';
				$html .= '<td><strong>Responsable</strong></td>';
				$html .= '<td><input type="text" name="idusuario[]" class="idusuario form-control input-xs" value="'.$val["nombres"].'" readonly></td>';
				// $html .= '<td>'.$this->combo_usuario->getObject().'</td>';
				$html .= '<td><button class="btn btn-danger btn-xs btn-del-usuario"><i class="fa fa-trash"></i></button></td>';
				$html .= '</tr>';
			}
		}
		return $html;
	}
	
	public function table_tr_tareaf($idproyecto) {
		$rs = $this->proyecto->tareasf($idproyecto);
		$html = '';
		if(!empty($rs)) {
			// $this->load->library('combobox', '', 'combo_tipo');
			// $this->combo_tipo->setAttr("name","precio_venta_idtipo_precio[]");
			// $this->combo_tipo->setAttr("class","precio_venta_idtipo_precio form-control input-xs");
			// $query = $this->db->select("idtipo_precio, descripcion")->where("estado", "A")->get("compra.tipo_precio");
			// $this->combo_tipo->addItem($query->result_array());
			
			$this->load->library('combobox', '', 'combo_fase');
			$this->combo_fase->setAttr("name","idfase[]");
			$this->combo_fase->setAttr("class","idfase form-control input-xs");
			$this->combo_fase->addItem($this->proyecto->fasees($idproyecto));
			
			// $this->load->library('combobox', '', 'combo_moneda');
			// $this->combo_moneda->setAttr("name","precio_venta_idmoneda[]");
			// $this->combo_moneda->setAttr("class","precio_venta_idmoneda form-control input-xs");
			// $query = $this->db->select("idmoneda, descripcion")->where("estado", "A")->get("general.moneda");
			// $this->combo_moneda->addItem($query->result_array());
			
			foreach($rs as $val) {
				$this->combo_fase->setSelectedOption($val["fase"]);
				// $this->combo_moneda->setSelectedOption($val["idmoneda"]);
				
				$html .= '<tr>';
				$html .= '<td>'.$this->combo_fase->getObject().'</td>';
				// $html .= '<td>'.$this->combo_moneda->getObject().'</td>';
				$html .= '<td><input type="text" name="deta_descripcion_fase[]" class="deta_descripcion_fase form-control input-xs" value="'.$val["descripcion_fase"].'"></td>';
				$html .= '<td><input type="text" name="deta_usuariot[]" class="deta_usuariot form-control input-xs" value="'.$val["idusuario"].'"></td>';
				$html .= '<td><input type="text" name="deta_estatus[]" class="deta_estatus form-control input-xs" value="'.$val["estatus"].'"></td>';
				$html .= '<td><input type="text" name="deta_tiempo_dias[]" class="deta_tiempo_dias form-control input-xs" value="'.$val["tiempo_dias"].'"></td>';
				$html .= '<td><input type="text" name="deta_fecha_vencimiento[]" class="deta_fecha_vencimiento form-control input-xs" value="'.$val["fecha_vencimiento"].'"></td>';
				$html .= '<td><input type="text" name="deta_prioridad[]" class="deta_prioridad form-control input-xs" value="'.$val["prioridad"].'"></td>';
				$html .= '<td><button class="btn btn-danger btn-xs btn-del-precio-venta"><i class="fa fa-trash"></i></button></td>';
				$html .= '</tr>';
			}
		}
		return $html;
	}
	
	public function get_equivalencia($idproyecto, $idusuario=1) {
		$res = array();
		
		$sql = "select * from proyecto.proyecto_usuario where idproyecto=? and idusuario=?";
		$query = $this->db->query($sql, array($idproyecto, $idusuario));
		if($query->num_rows() > 0)
			$res["usuario"] = $query->row_array();
		
		$sql = "select * from proyecto.proyecto where idproyecto_padre=? and idusuario=?";
		$query = $this->db->query($sql, array($idproyecto, $idusuario));
		if($query->num_rows() > 0)
			$res["proyecto"] = $query->row_array();
		
		$this->response($res);
	}
}
?>