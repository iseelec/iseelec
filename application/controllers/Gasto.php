<?php

include_once "Controller.php";

class Gasto extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Mantenimiento de Gasto");
		$this->set_subtitle("Lista de Gasto");
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		$this->load->library('combobox');
		$data["controller"] = $this->controller;
		//////////////////////////////////////////////////////// combo proyecto
		$query = $this->db->select('idproyecto, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))
			->order_by("descripcion", "asc")->get("proyecto.proyecto");
			

		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idproyecto","name"=>"idproyecto","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem($query->result_array());
		if( isset($data["proyecto"]["idproyecto"]) ) {
			$this->combobox->setSelectedOption($data["proyecto"]["idproyecto"]);
		}
		$data["proyecto"] = $this->combobox->getObject();
		// $data["icons"] = $this->get_icons();
		$this->css("plugins/datapicker/datepicker3");
		$this->css('plugins/iCheck/custom');
		
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		$this->load_model($this->controller);
		$this->load->library('datatables');

		$this->datatables->setModel($this->gasto);

		$this->datatables->where('estado', '=', 'A');//

		$this->datatables->setColumns(array('descripcion','fecha_gasto','gasto','referencia_documento'));

		// columnas de la tabla, si no se envia este parametro, se muestra el
		// nombre de la columna de la tabla de la bd
		$columnasName = array(
			'Desc. de Gasto'
			,'Fecha Gasto'
			,'Gasto S/'
			,'Doc. Referencia'
			// array('Descripci&oacute;n', '95%') // ancho de la columna
		);
		
		// generamos la tabla y el script para el dataTables
		$table = $this->datatables->createTable($columnasName);

		// $table = $this->datatables->createTable();
		$script = "<script>".$this->datatables->createScript()."</script>";

		// agregamos los css para el dataTables
		$this->css('plugins/dataTables/dataTables.bootstrap');
		$this->css('plugins/dataTables/dataTables.responsive');
		$this->css('plugins/dataTables/dataTables.tableTools.min');

		// agregamos los scripts para el dataTables
		$this->js('plugins/dataTables/jquery.dataTables');
		$this->js('plugins/dataTables/dataTables.bootstrap');
		$this->js('plugins/dataTables/dataTables.responsive');
		$this->js('plugins/dataTables/dataTables.tableTools.min');
		$this->js($script, false);
		// echo $script;exit;
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Gasto");
		$this->set_subtitle("");
		$this->set_content($this->form());
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model($this->controller);
		$data = $this->gasto->find($id);
		
		$this->set_title("Modificar Gasto");
		$this->set_subtitle("");
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model($this->controller);
		$this->gasto->text_uppercase(false);
		$fields = $this->input->post();
		// if(empty($fields["ruc"])) {
			// $this->exception("Ingrese el RUC de la gasto");
			// return;
		// }
		// if(strlen($fields["ruc"]) != 11) {
			// $this->exception("El RUC debe tener 11 caracteres");
			// return;
		// }
		$defaultImagen_doc = "default_imagen_doc.png";
		if(empty($fields["imagen_doc"]))
			$fields["imagen_doc"] = $defaultImagen_doc;
		
		$fields['controller']=$this->controller;
		$fields['accion']=__FUNCTION__;
		
		if( ! empty($_FILES["file"])) {
			$pathImagen_doc = upload("file", "gasto", false, 128);
			if($pathImagen_doc !== false)
				$fields["imagen_doc"] = basename($pathImagen_doc);
		}
		
		if( ! empty($fields["idgasto"])) {
			$data = $this->gasto->find($fields["idgasto"]);
			
			$currentImagen_doc = $data["imagen_doc"];
			if( ! empty($currentImagen_doc) && $currentImagen_doc != $defaultImagen_doc && $currentImagen_doc != $fields["imagen_doc"])
				unlink(FCPATH.'app/img/gasto/'.$currentImagen_doc);
		}
		
		$fields['estado'] = "A";
		// $fields['imagen_doc'] = imagen_upload('imagen_doc','./app/img/gasto/','default_imagen_doc.png',true);
		
		if(empty($fields["idgasto"])) {
			$this->gasto->insert($fields);
		}
		else {
			$this->gasto->update($fields);
		}
		
		// $this->response($this->gasto->get_fields());
		$this->response($fields);
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model($this->controller);
		// cambiamos de estado
		$fields['idgasto'] = $id;
		$fields['estado'] = "I";

		$fields['controller']=$this->controller;
		$fields['accion']=__FUNCTION__;

		$this->gasto->update($fields);
		
		$this->response($fields);
	}
}
?>