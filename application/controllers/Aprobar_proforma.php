<?php

include_once "Controller.php";

class Aprobar_proforma extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Aprobar Proforma para las respectivas compras");
		$this->set_subtitle("Lista de proformas aprobadas");
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		// $this->js('form/'.$this->controller.'/index');
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		
		$this->load->library('combobox');
				
		// combo tipodocumento
		$this->combobox->init(); // un nuevo combo		
		$this->combobox->setAttr(
			array(
				"id"=>"idtipodocumento"
				,"name"=>"idtipodocumento"
				,"class"=>"form-control"
			)
		);
		$this->db->select('idtipodocumento, descripcion');
		$query = $this->db->where("estado", "A")->get("venta.tipo_documento");
		$this->combobox->addItem($query->result_array());
		$data["tipodocumento"] = $this->combobox->getObject();
		
		// combo almacen
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr("id", "idalmacen_temp");
		$query = $this->db->select('idalmacen, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))->get("almacen.almacen");
		$this->combobox->addItem($query->result_array());
		$data["almacen"] = $this->combobox->getObject();
		
		// combo proyecto
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr("id", "idproyecto_temp");
		$query = $this->db->select('idproyecto, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))->get("proyecto.proyecto");
		$this->combobox->addItem($query->result_array());
		$data["proyecto"] = $this->combobox->getObject();
	
		
		$data["controller"] = $this->controller;
		
		$this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
		
		$this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		$this->js('form/'.$this->controller.'/form');
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	public function form_view($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		
		$data["controller"] = $this->controller;
		
		$this->js('form/'.$this->controller.'/form_view');
		
		return $this->load->view($this->controller."/form_view", $data, true);
	}
	
	public function filtros_grilla($aprobado) {
		$this->load_library("combobox");
		
		$html = '<div class="row">';
		
		// div y combobox 
		$this->combobox->setAttr("filter", "aprobado");
		$this->combobox->setAttr("class", "form-control");
		$this->combobox->addItem("N", "PENDIENTE");
		$this->combobox->addItem("S", "APROBADO");
		$this->combobox->setSelectedOption($aprobado);
		////borramos combos temporalmente a solicitud del dueño del sistema
		// $html .= '<div class="col-sm-4"><div class="form-group">';
		// $html .= '<label class="control-label">Proformas Aprobadas / Pendientes de aprobar</label>';
		// $html .= $this->combobox->getObject();;
		// $html .= '</div></div>';
		// $html .= '</div>';
		// $this->set_filter($html);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		$this->load_model("almacen.aprobar_proforma_view");
		$this->load->library('datatables');
		
		$this->datatables->setModel($this->aprobar_proforma_view);
		$this->datatables->setIndexColumn("idproforma");
		
		$aprobado = "N";
		
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('aprobado', '=', $aprobado);
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
	
		$this->datatables->setColumns(array('fecha_proforma','documento','proveedor','cantidad','fecha_aprobado_proforma'));
		$this->datatables->order_by("fecha_proforma", "desc");
		$this->datatables->setCallback("format_fecha");
		
		$columnasName = array('Fecha','Compra','Proveedor','Item aprobados','Ultima Aprobación');
		
		$table = $this->datatables->createTable($columnasName);
		$script = "<script>".$this->datatables->createScript()."</script>";
		
		// $this->css('plugins/dataTables/dataTables.bootstrap');
		// $this->css('plugins/dataTables/dataTables.responsive');
		// $this->css('plugins/dataTables/dataTables.tableTools.min');
		
		// $this->js('plugins/dataTables/jquery.dataTables');
		// $this->js('plugins/dataTables/dataTables.bootstrap');
		// $this->js('plugins/dataTables/dataTables.responsive');
		// $this->js('plugins/dataTables/dataTables.tableTools.min');
		
		$this->js($script, false);
		
		$this->filtros_grilla($aprobado);
		
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Aprobar_proforma");
		$this->set_subtitle("");
		
		$this->set_content($this->form());
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model("compra.proforma_view");
		$data["proforma"] = $this->proforma_view->find(array("idproforma"=>$id));
		
		$sql = "select r.idaprobar_proforma, p.descripcion_detallada as producto, py.descripcion as almacen,
			t.abreviatura as tipodocumento, r.serie, r.numero, r.observacion, 
			to_char(r.fecha,'DD/MM/YYYY') as fecha, to_char(r.hora::interval, 'HH12:MI am') as hora,
			u.descripcion as unidad, r.cant_aprobada as cantidad, e.nombres as usuario
			--array_to_string(array_agg(das.serie), '|'::text) AS series
			from almacen.aprobar_proforma r
			join compra.producto p on p.idproducto = r.idproducto
			join proyecto.proyecto py on py.idproyecto = r.idproyecto
			join venta.tipo_documento t on t.idtipodocumento = r.tipo_docu::integer
			join compra.unidad u on u.idunidad = r.idunidad
			join seguridad.usuario e on e.idusuario = r.idusuario
			--left join almacen.detalle_almacen_serie das on das.idaprobar_proforma = r.idaprobar_proforma and das.estado='A'
			--	and das.tabla_ingreso = 'C' and das.idtabla_ingreso = r.idproforma
			where r.estado <> 'I' and r.referencia = 'C' and r.idproforma = ?
			group by r.idaprobar_proforma, p.descripcion_detallada, py.descripcion, t.abreviatura, r.serie, r.numero,
			r.observacion, r.fecha, r.hora, u.descripcion, r.cant_aprobada, e.nombres
			order by r.idaprobar_proforma desc, r.fecha desc";
		$query = $this->db->query($sql, array($id));
		$data["detalle"] = $query->result_array();
		
		$this->set_title("Lista de aprobar_proformas");
		$this->set_subtitle("");
		$this->set_content($this->form_view($data));
		$this->index("content");
	}
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$fields = $this->input->post();
		$fields['idsucursal'] = $this->get_var_session("idsucursal");
		$fields['idusuario'] = $this->get_var_session("idusuario");
		$idproforma = $fields["idproforma"];
		
		// modelos
		$this->load_model($this->controller);
		$this->load_model("proforma");
		$this->load_model("detalle_proforma");
		$this->load_model("detalle_proforma_serie");
		$this->load_model("compra.producto_unidad");
		$this->load_model("detalle_almacen");
		$this->load_model("detalle_almacen_serie");
		$this->load_model("aprobar_proforma");
		$this->load_model("tipo_movi_almacen");
		
		$this->proforma->find(array("idproforma"=>$idproforma, "idsucursal"=>$fields["idsucursal"]));
		
		$this->detalle_almacen->set("tipo", "E");
		$this->detalle_almacen->set("tipo_number", 1);
		$this->detalle_almacen->set("fecha", date("Y-m-d"));
		$this->detalle_almacen->set("tabla", "C");
		$this->detalle_almacen->set("idtabla", $idproforma);
		$this->detalle_almacen->set("estado", "A");
		$this->detalle_almacen->set("idsucursal", $this->proforma->get("idsucursal"));
		
		$this->detalle_almacen_serie->set("fecha_ingreso", date("Y-m-d"));
		$this->detalle_almacen_serie->set("tabla_ingreso", "C");
		$this->detalle_almacen_serie->set("idtabla_ingreso", $idproforma);
		$this->detalle_almacen_serie->set("despachado", "N");
		$this->detalle_almacen_serie->set("estado", "A");
		$this->detalle_almacen_serie->set("idsucursal", $this->proforma->get("idsucursal"));
		
		$this->aprobar_proforma->set("tipo_docu", $fields["idtipodocumento"]);
		$this->aprobar_proforma->set("serie", $fields["serie"]);
		$this->aprobar_proforma->set("numero", $fields["numero"]);
		$this->aprobar_proforma->set("observacion", $fields["observacion"]);
		$this->aprobar_proforma->set("fecha", date("Y-m-d"));
		$this->aprobar_proforma->set("hora", date("H:i:s"));
		$this->aprobar_proforma->set("idusuario", $fields["idusuario"]);
		$this->aprobar_proforma->set("referencia", "C");
		
		$this->tipo_movi_almacen->find($this->get_idtipo_movimiento("compra"));
		$correlativo = intval($this->tipo_movi_almacen->get("correlativo"));
		
		$tipo_movimiento = $this->get_idtipo_movimiento("compra");
		$this->db->trans_start(); // inciamos transaccion
		$arrProductosKardex = array();
		
		// recorremos lista de producto
		foreach($fields["deta_iddetalle"] as $key=>$val) {
			$cantidad = floatval($fields["deta_ingreso"][$key]);
			$pendiente = floatval($fields["deta_pendiente"][$key]);
			
			if($cantidad > $pendiente) {
				$this->exception("La cantidad a ingresar es mayor a la cantidad pendiente 
					de aprobar para el producto ".$fields["deta_producto"][$key]);
				return false;
			}
			
			$this->detalle_proforma->find(array("iddetalle_proforma"=>$val, "idproforma"=>$idproforma));
			
			// insertamos las series del detalle compra
			if($fields["deta_controla_serie"][$key] == "S") {
				if( ! empty($fields["deta_series"][$key])) {
					$this->detalle_proforma_serie->set($this->detalle_proforma->get_fields());
					$arr = explode("|", $fields["deta_series"][$key]);
					foreach($arr as $serie) {
						$this->detalle_proforma_serie->set("serie", $serie);
						$this->detalle_proforma_serie->save(null, false);
					}
				}
			}
			
			// hacemos la aprobar_proforma de todos modos
			$this->aprobar_proforma->set($this->detalle_proforma->get_fields());
			$this->aprobar_proforma->set("idproyecto", $fields["deta_idproyecto"][$key]);
			$this->aprobar_proforma->set("cant_aprobada", $cantidad);
			$this->aprobar_proforma->set("correlativo", $correlativo);
			$this->aprobar_proforma->set("estado", "C");
			$this->aprobar_proforma->insert();
			$correlativo = $correlativo + 1; // nuevo correlativo
			
			// si aprobar_proformaamos la compra directamente, ingresamos el stock y las series al almacen
			if($fields["deta_controla_stock"][$key] == "S") {
				// ingresamos el stock
				$this->detalle_almacen->set($this->detalle_proforma->get_fields());
				$this->detalle_almacen->set("idalmacen", $fields["deta_idalmacen"][$key]);
				$this->detalle_almacen->set("cantidad", $cantidad);
				$this->detalle_almacen->set("precio_costo", $this->detalle_proforma->get("costo"));
				$this->detalle_almacen->set("idaprobar_proforma", $this->aprobar_proforma->get("idaprobar_proforma"));
				$this->detalle_almacen->insert();
				
				$this->producto_unidad->find(array(
					"idproducto"=>$this->detalle_proforma->get("idproducto")
					,"idunidad"=>$this->detalle_proforma->get("idunidad")
				));
				
				// verificamos el ingreso de las series
				if($fields["deta_controla_serie"][$key] == "S") {
					if(empty($fields["deta_series"][$key])) {
						$this->exception("Ingrese las series del producto ".$fields["deta_producto"][$key]);
						return false;
					}
					
					$count_real_serie = intval($this->producto_unidad->get("cantidad_unidad_min")) * $cantidad;
					
					$arr = explode("|", $fields["deta_series"][$key]);
					if(count($arr) != $count_real_serie) {
						$this->exception("Debe ingresar $count_real_serie series para el producto: ".$fields["deta_producto"][$key]);
						return false;
					}
					
					// ingresamos las series al almacen
					$this->detalle_almacen_serie->set($this->detalle_almacen->get_fields());
					foreach($arr as $serie) {
						if( $this->detalle_almacen_serie->exists(array("serie"=>$serie, "despachado"=>"N", "estado"=>"A")) ) {
							$this->exception("La serie $serie del producto ".$fields["deta_producto"][$key]." ya existe.");
							return false;
						}
						$this->detalle_almacen_serie->set("serie", $serie);
						$this->detalle_almacen_serie->insert(null, false);
					}
				}
				
				// almacenmos los datos para el ingreso en kardex
				$temp = $this->aprobar_proforma->get_fields();
				$temp["cantidad"] = $temp["cant_aprobada"];
				$temp["preciocosto"] = $this->detalle_proforma->get("costo") / $this->producto_unidad->get("cantidad_unidad_min");
				$arrProductosKardex[] = $temp;
			}
			
			// actualizamos el campo aprobado del detalle compra
			if($cantidad >= $pendiente) {
				$this->detalle_proforma->set("aprobado", "S");
				$this->detalle_proforma->update();
			}
		} // fin [foreach]
		
		if( ! empty($arrProductosKardex)) {
			// actualizamos el correlativo del tipo movimiento
			$this->tipo_movi_almacen->set("correlativo", $correlativo);
			$this->tipo_movi_almacen->update();
			
			// registramos el movimiento de kardex
			$this->load_library("jkardex");
			
			$this->jkardex->idtipodocumento = $this->aprobar_proforma->get("tipo_docu");
			$this->jkardex->serie = $this->aprobar_proforma->get("serie");
			$this->jkardex->numero = $this->aprobar_proforma->get("numero");
			$this->jkardex->idtercero = $this->compra->get("idproveedor");
			$this->jkardex->idmoneda = $this->compra->get("idmoneda");
			$this->jkardex->tipocambio = $this->compra->get("cambio_moneda");
			$this->jkardex->observacion = $this->aprobar_proforma->get("observacion");
			// $this->jkardex->tipo_movimiento = $tipo_movimiento;
			
			$this->jkardex->referencia("compra", $idproforma, $fields["idsucursal"]);
			$this->jkardex->entrada();
			$this->jkardex->push($arrProductosKardex);
			$this->jkardex->run();
			
			
		}
		
		// verificamos el estado (aprobado) de la compra
		$sql = "SELECT * FROM compra.detalle_proforma
			WHERE idproforma=? AND estado=? AND aprobado=?";
		$query = $this->db->query($sql, array($idproforma, "A", "N"));
		if($query->num_rows() <= 0) {
			$this->proforma->update(array("idproforma"=>$idproforma, "aprobado"=>"S"));
		}
		
		// finalizamos transaccion
		$this->db->trans_complete();
		
		$this->response($this->proforma->get_fields());
	}
	
	/**
	 * Metodo para eliminar un registro segun varios parametros
	 */
	public function eliminar($id) {
		$this->load_model("almacen.aprobar_proforma");
		
		$this->aprobar_proforma->find($id);
		
		$this->db->trans_start(); // inciamos transaccion
		
		//// obtenemos el listado de series del almacen
		// $rs_series = $this->db->where("tabla_ingreso", "C")
			// ->where("idtabla_ingreso", $this->aprobar_proforma->get("idproforma"))
			// ->where("idaprobar_proforma", $this->aprobar_proforma->get("idaprobar_proforma"))
			// ->where("estado !=", "I")
			// ->get("almacen.detalle_almacen_serie");
		
		//// eliminamos el ingreso en detalle_almacen
		// $this->db->where("tabla", "C")->where("idtabla", $this->aprobar_proforma->get("idproforma"))
			// ->where("idaprobar_proforma", $this->aprobar_proforma->get("idaprobar_proforma"))
			// ->update("almacen.detalle_almacen", array("estado"=>"I"));
			
	//	// eliminamos el ingreso de las series en almacen
		// $this->db->where("tabla_ingreso", "C")->where("idtabla_ingreso", $this->aprobar_proforma->get("idproforma"))
			// ->where("idaprobar_proforma", $this->aprobar_proforma->get("idaprobar_proforma"))
			// ->update("almacen.detalle_almacen_serie", array("estado"=>"I"));
			
		//// eliminar del detalle_proforma_serie las series
		// if($rs_series->num_rows() > 0) {
			// foreach($rs_series->result() as $row) {
				// $this->db->where("idproforma", $this->aprobar_proforma->get("idproforma"))
					// ->where("iddetalle_proforma", $this->aprobar_proforma->get("iddetalle_proforma"))
					// ->where("idproducto", $row->idproducto)
					// ->where("serie", $row->serie)
					// ->update("compra.detalle_proforma_serie", array("estado"=>"I"));
			// }
		// }
		
		//// eliminar del kardex
		// $this->db->where("tabla", "compra")->where("idreferencia", $this->aprobar_proforma->get("idproforma"))
			// ->where("correlativo", $this->aprobar_proforma->get("correlativo"))
			// ->update("almacen.kardex", array("estado"=>"I"));
		
		// eliminar la aprobar_proforma
		$this->aprobar_proforma->set("estado", "I");
		$this->aprobar_proforma->update();
		
		// actualizamos el estado del detalle_proforma
		$aprobado = "N";
		$sql = "select dc.cantidad - coalesce(sum(r.cant_aprobada),0) as pendiente
			from compra.detalle_proforma dc
			left join almacen.aprobar_proforma r on r.idproforma = dc.idproforma and r.referencia = 'C' 
				and r.iddetalle_proforma = dc.iddetalle_proforma and r.estado <> 'I'
			where dc.iddetalle_proforma = ?
			group by dc.iddetalle_proforma, dc.cantidad";
		$query = $this->db->query($sql, array($this->aprobar_proforma->get("iddetalle_proforma")));
		if($query->num_rows() > 0) {
			$aprobado =($query->row()->pendiente > 0) ? "N" : "S";
		}
		$this->db->where("iddetalle_proforma", $this->aprobar_proforma->get("iddetalle_proforma"))
			->update("compra.detalle_proforma", array("aprobado"=>$aprobado));
		
		// actualizamos el estado de la proforma
		$sql = "SELECT * FROM compra.detalle_proforma
			WHERE idproforma=? AND estado=? AND aprobado=?";
		$query = $this->db->query($sql, array($this->aprobar_proforma->get("idproforma"), "A", "N"));
		$aprobado = ($query->num_rows() <= 0) ? "S" : "N";
		$this->db->where("idproforma", $this->aprobar_proforma->get("idproforma"))
			->update("compra.proforma", array("aprobado"=>$aprobado));
		
		$this->db->trans_complete(); // finalizamos transaccion
		
		$this->response($this->aprobar_proforma->get_fields());
	}
	
	public function autocomplete() {
		$txt = trim($this->input->post("startsWith"));
		$txt = "%".preg_replace('/\s+/', '%', $txt)."%";
		
		$sql = "SELECT idproforma, documento, proveedor, idtipodocumento, serie, numero
			FROM almacen.aprobar_proforma_view
			WHERE estado='A' and aprobado='N' 
			and idsucursal=".$this->get_var_session("idsucursal")." 
			and (documento ILIKE ? OR proveedor ILIKE ?)
			ORDER BY documento
			LIMIT ?";
		$query = $this->db->query($sql, array($txt, $txt, $this->input->post("maxRows")));
		$this->response($query->result_array());
	}
	
	public function grilla_popup() {
		$this->load_model("almacen.aprobar_proforma_view");
		$this->load->library('datatables');
		
		$this->datatables->setModel($this->aprobar_proforma_view);
		$this->datatables->setIndexColumn("idproforma");
		
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('aprobado', '=', 'N');
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		
		$this->datatables->setColumns(array('fecha_proforma','documento','proveedor','cantidad','fecha_aprobado_proforma'));
		$this->datatables->setPopup(true);
		
		$this->datatables->order_by("fecha_proforma", "desc");
		$this->datatables->setCallback("format_fecha");
		
		$table = $this->datatables->createTable(array('Fecha','Compra','Proveedor','Item aprobados','Ultima Aprobación'));
		$script = "<script>".$this->datatables->createScript("", false)."</script>";
		
		$this->response($script.$table);
	}
	
	public function get_detalle_pendiente($idproforma) {
		$sql = "select dc.iddetalle_proforma, p.descripcion_detallada as producto, u.descripcion as unidad,
			dc.cantidad, coalesce(sum(r.cant_aprobada),0) as cantidad_aprobar_proformaada, 
			dc.cantidad - coalesce(sum(r.cant_aprobada),0) as cantidad_pendiente,
		 	pu.cantidad_unidad_min as cantidad_um, dc.idproyecto
			from compra.detalle_proforma dc
			join compra.producto p on p.idproducto = dc.idproducto
			join compra.unidad u on u.idunidad = dc.idunidad
			join compra.producto_unidad pu on pu.idproducto = dc.idproducto and pu.idunidad = dc.idunidad
			left join almacen.aprobar_proforma r on r.idproforma = dc.idproforma and r.referencia = 'C' 
				and r.iddetalle_proforma = dc.iddetalle_proforma and r.estado <> 'I'
			where dc.estado = 'A' and dc.aprobado = 'N' and dc.idproforma = ?
			group by dc.iddetalle_proforma, p.descripcion_detallada, u.descripcion, dc.cantidad,  
				pu.cantidad_unidad_min, dc.idproyecto
			order by iddetalle_proforma";
		$query = $this->db->query($sql, array($idproforma));
		$this->response($query->result_array());
	}
}
?>