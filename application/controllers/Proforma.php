<?php

include_once "Controller.php";

class Proforma extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Mantenimiento de Proformas");
		$this->set_subtitle("Lista de proformas");
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		// $this->js('form/'.$this->controller.'/index');
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		
		$this->load->library('combobox');
		
		// combo almacen
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idalmacen"
				,"name"=>"idalmacen"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$query = $this->db->select('idalmacen, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))->get("almacen.almacen");
		$this->combobox->addItem($query->result_array());
		if( isset($data["proforma"]["idalmacen"]) ) {
			$this->combobox->setSelectedOption($data["proforma"]["idalmacen"]);
		}
		$data["almacen"] = $this->combobox->getObject();
		
		//combo tipo pedito interno o proforma a proveedor
		// $this->combobox->init();
		// $this->combobox->setAttr(
			// array( "id"=>"idtipo_proforma"
				// ,"name"=>"idtipo_proforma"
				// ,"class"=>"form-control input-sm"
				// ,"required"=>"")
			// );
		// $this->db->select('idtipo_proforma, descripcion');
		// $query = $this->db->where("estado", "A")->get("compra.tipo_proforma");
		// $this->combobox->addItem($query->result_array());
		
		// if( isset($data["proforma"]["idtipo_proforma"]) ) {
			// $this->combobox->setSelectedOption($data["proforma"]["idtipo_proforma"]);
		// }
		// combo tipodocumento
		$this->combobox->init(); // un nuevo combo
		
		$this->combobox->setAttr(
			array(
				"id"=>"idtipodocumento"
				,"name"=>"idtipodocumento"
				,"class"=>"form-control input-sm"
				,"required"=>""
			)
		);
		$this->db->select('idtipodocumento, descripcion');
		$query = $this->db->where("estado", "A")->where("mostrar_en_compra", "S")->get("venta.tipo_documento");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["compra"]["idtipodocumento"]) ) {
			$this->combobox->setSelectedOption($data["compra"]["idtipodocumento"]);
		}
		
		$data["tipodocumento"] = $this->combobox->getObject();
		
		
		// combo proyecto
		$query = $this->db->select('idproyecto, descripcion')
			->where("estado", "A")
			->order_by("descripcion", "asc")->get("proyecto.proyecto");
		$this->combobox->removeAllItems();
		$this->combobox->setAttr("id", "idproyecto");
		$this->combobox->setAttr("name", "idproyecto");
		$this->combobox->setAttr("class", "form-control input-xs");
		//$this->combobox->setAttr("required", "");
		$this->combobox->addItem($query->result_array());
		
		$data["combo_proyecto"] = $this->combobox->getObject();

		//fin combo proyecto
		// $data["tipo_proforma"] = $this->combobox->getObject();
		

		//$this->js('form/proveedor/modal');
		
		$es_nuevo = "true";
		if( isset($data["proforma"]["idproforma"]) ) {
			$es_nuevo = "false";
		}
		$this->js("<script>var _es_nuevo_".$this->controller."_ = $es_nuevo;</script>", false);
		
		if( isset($data["detalle_proforma"]) ) {
			$this->js("<script>var data_detalle = ".json_encode($data["detalle_proforma"]).";</script>", false);
		}
		
		$this->combobox->init(); // un nuevo combo
		
		$data["controller"] = $this->controller;
		
		$this->load_controller("producto");
		$data["form_producto"] = $this->producto_controller->form(null, "prod_", true);
		//$data["form_producto"] = $this->producto_controller->form(null, "", true);
		$data["form_producto_unidad"] = $this->producto_controller->form_unidad_medida(null, "", true);
		// formulario PROVEEDOR
		$this->load_controller("proveedor");
		$data["form_proveedor"] = $this->proveedor_controller->form(null, "prov_", true);
		
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
		$this->css("plugins/datapicker/datepicker3");
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		//$this->js('form/'.$this->controller.'/form');
		//$this->js('form/producto/modal');
		
		$this->js('form/'.$this->controller.'/form', true, true, array(), false);
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	// public function filtros_grilla($aprobado = "N", $atendido = "N") {
	// public function filtros_grilla($aprobado = "N") {
		// $this->load_library("combobox");
		
		// $html = '<div class="row">';
		
		//// div y combobox aprobado
		// $this->combobox->setAttr("filter", "aprobado");
		// $this->combobox->setAttr("class", "form-control");
		// $this->combobox->addItem("S", "APROBADO");
		// $this->combobox->addItem("N", "NO APROBADO");
		// $this->combobox->setSelectedOption($aprobado);
		
		// $html .= '<div class="col-sm-4"><div class="form-group">';
		// $html .= '<label class="control-label">Aprobado</label>';
		// $html .= $this->combobox->getObject();;
		// $html .= '</div></div>';
		

		// $html .= '</div>';
		
		// $this->set_filter($html);
	// }
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		$this->load_model($this->controller);
		$this->load->library('datatables');
		
		//$aprobado = "N";
		//$atendido = "N";
		
		$this->datatables->setModel($this->proforma);
		$this->datatables->where('estado', '=', 'A');
		//$this->datatables->where('aprobado', '=', $aprobado);
		///////$this->datatables->where('atendido', '=', $atendido);
		
		$this->datatables->setColumns(array('idproforma','fecha_proforma','descripcion'));
		$this->datatables->order_by('idproforma', "desc");
		
			$columnasName = array(
			array('Nro','5%')
			,array('Fecha Emision','30%')
			,array('Descripci&oacute;n','65%')
			);
		
		// $columnasName = array(
			// 'Nro'
			// ,'Fecha de Emision'
			// ,'Descripci&oacute;n'
		// );

		$table = $this->datatables->createTable($columnasName);
		$script = "<script>".$this->datatables->createScript()."</script>";
		$this->js($script, false);
		
		/////// $this->filtros_grilla($aprobado, $atendido);
		//$this->filtros_grilla($aprobado);
		
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Proforma");
		$this->set_subtitle("");
		
		$this->set_content($this->form());
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model(array("proforma", "detalle_proforma"));
		$data["proforma"] = $this->proforma->find($id);
		
		$data["detalle_proforma"] = $this->detalle_proforma->get_item_proforma($id);
		
		$this->load_model("proveedor");
		$data["proveedor"] = $this->proveedor->find($data["proforma"]["idproveedor"]);
		
		$this->set_title("Modificar Proforma");
		$this->set_subtitle("");
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	/* public function proforma_detalle($id) {
		$this->load_model("proforma");
		$data["proforma"] = $this->proforma->find($id);

		
		$this->set_title("Detalle del Proforma / ");
		$this->set_subtitle($data["proforma"]["descripcion"]);

		$this->set_content($this->form_proforma_detalle($data,$id));
		$this->index("content");
	} */
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model($this->controller);
		
		$fields = $this->input->post();
		$fields['idsucursal'] = $this->get_var_session("idsucursal");
		$fields['idusuario'] = $this->get_var_session("idusuario");
		//$fields['fecha'] =  date('Y-m-d H:i:s');
		$fields['estado'] = "A";
		$fields['atendido'] = "N";
		$fields['aprobado'] = "N";
		$fields['fecha_registro'] = date("Y-m-d");
		//$fields['aprobado'] = (!empty($fields['recepcionado'])) ? "S" : "N";
		
		$this->db->trans_start(); // inciamos transaccion
		
		if(empty($fields["idproforma"])) {
			$idproforma = $this->proforma->insert($fields);
		}
		else {
			$this->proforma->update($fields);
			$idproforma = $fields["idproforma"];
		}
		
		// detalle proforma
		$this->load_model("detalle_proforma");
		$this->db->query("UPDATE compra.detalle_proforma SET estado=? WHERE idproforma=?", array("I", $idproforma));		
		
		foreach($fields["deta_idproducto"] as $key=>$val) {
			$data = $this->detalle_proforma->find(array("idproforma"=>$idproforma, "idproducto"=>$val, "idunidad"=>$fields["deta_idunidad"][$key], "idproyecto"=>$fields["deta_proyecto"][$key], "estado"=>"I"));
			if($data != null) {
				$data["estado"] = 'A';
				$data["aprobado"] = 'N';
				$data["cantidad"] = floatval($fields["deta_cantidad"][$key]);
				$data['atendido'] = "N";
				$this->detalle_proforma->update($data);
			}
			else {
				$data["idproforma"] = $idproforma;
				$data["idproducto"] = $val;
				$data["estado"] = 'A';
				$data["aprobado"] = 'N';
				$data["idproyecto"] = $fields["deta_proyecto"][$key];
				$data["idunidad"] = $fields["deta_idunidad"][$key];
				$data["cantidad"] = floatval($fields["deta_cantidad"][$key]);
				$data["precio_compra"] = floatval($fields["deta_precio_compra"][$key]);
				$data['atendido'] = "N";
				$this->detalle_proforma->insert($data);
			}
		}
		
		$this->db->trans_complete(); // finalizamos transaccion
		
		$this->response($this->proforma->get_fields());
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model($this->controller);
		
		$fields['idproforma'] = $id;
		$fields['estado'] = "I";
		$this->proforma->update($fields);
		
		$this->response($fields);
	}
	
	public function aprobar_proforma($id){
		$this->load_model($this->controller);
		$fields['idproforma'] = $id;
		$fields['aprobado'] = "S";
		$this->proforma->update($fields);
		$this->response($fields);
	}
	
	public function grilla_popup() {
		$this->load_model($this->controller);
		$this->load->library('datatables');
		
		$this->datatables->setModel($this->proforma);
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('aprobado', '=', 'S');
		$this->datatables->where('atendido', '=', 'N');
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		$this->datatables->setColumns(array('idproforma','fecha','descripcion'));
		$this->datatables->setPopup(true);
		$this->datatables->setSubgrid("cargarDetalle", true);
		
		$table = $this->datatables->createTable(array('Nro','Fecha de Emision','Descripci&oacute;n'));
		$script = "<script>".$this->datatables->createScript("", false)."</script>";
		
		$this->response($script.$table);
	}
	
	public function get_detalle_proforma() {
		$this->load_model("proforma");
		
		$post = $this->input->post();
		
		$params = array("d.idproforma"=>$post["idproforma"], "d.estado"=>'A', "d.atendido"=>'N');
		if(!empty($post["idproducto"])) {
			$params["d.idproducto"] = $post["idproducto"];
		}
		
		$prods = $this->proforma->get_detalle($params);
		$this->response($prods);
	}
}
?>