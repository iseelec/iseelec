<?php

include_once "Controller.php";

class Pedido extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Mantenimiento de Pedidos");
		$this->set_subtitle("Lista de pedidos");
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		// $this->js('form/'.$this->controller.'/index');
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		
		$this->load->library('combobox');
		
		// combo almacen
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idalmacen"
				,"name"=>"idalmacen"
				,"class"=>"form-control"
				,"required"=>""
			)
		);
		$query = $this->db->select('idalmacen, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))->get("almacen.almacen");
		$this->combobox->addItem($query->result_array());
		if( isset($data["pedido"]["idalmacen"]) ) {
			$this->combobox->setSelectedOption($data["pedido"]["idalmacen"]);
		}
		$data["almacen"] = $this->combobox->getObject();
		
		//combo tipo pedito interno o pedido a proveedor
		$this->combobox->init();
		$this->combobox->setAttr(
			array( "id"=>"idtipo_pedido"
				,"name"=>"idtipo_pedido"
				,"class"=>"form-control input-sm"
				,"required"=>"")
			);
		$this->db->select('idtipo_pedido, descripcion');
		$query = $this->db->where("estado", "A")->get("compra.tipo_pedido");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["pedido"]["idtipo_pedido"]) ) {
			$this->combobox->setSelectedOption($data["pedido"]["idtipo_pedido"]);
		}
		
		$data["tipo_pedido"] = $this->combobox->getObject();

		$es_nuevo = "true";
		if( isset($data["pedido"]["idpedido"]) ) {
			$es_nuevo = "false";
		}
		$this->js("<script>var _es_nuevo_".$this->controller."_ = $es_nuevo;</script>", false);
		
		if( isset($data["detalle_pedido"]) ) {
			$this->js("<script>var data_detalle = ".json_encode($data["detalle_pedido"]).";</script>", false);
		}
		
		$this->combobox->init(); // un nuevo combo
		
		$data["controller"] = $this->controller;
		
		$this->load_controller("producto");
		$data["form_producto"] = $this->producto_controller->form(null, "", true);
		$data["form_producto_unidad"] = $this->producto_controller->form_unidad_medida(null, "", true);
		
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
		$this->css("plugins/datapicker/datepicker3");
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		$this->js('form/'.$this->controller.'/form');
		$this->js('form/producto/modal');
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	public function filtros_grilla($aprobado = "N", $atendido = "N") {
		$this->load_library("combobox");
		
		$html = '<div class="row">';
		
		// div y combobox aprobado
		$this->combobox->setAttr("filter", "aprobado");
		$this->combobox->setAttr("class", "form-control");
		$this->combobox->addItem("S", "APROBADO");
		$this->combobox->addItem("N", "NO APROBADO");
		$this->combobox->setSelectedOption($aprobado);
		
		$html .= '<div class="col-sm-4"><div class="form-group">';
		$html .= '<label class="control-label">Aprobado</label>';
		$html .= $this->combobox->getObject();;
		$html .= '</div></div>';
		
		// div y combobox atendido
		$this->combobox->removeItems();
		$this->combobox->setAttr("filter", "atendido");
		$this->combobox->addItem("S", "ATENDIDO");
		$this->combobox->addItem("N", "NO ATENDIDO");
		$this->combobox->setSelectedOption($atendido);
		
		$html .= '<div class="col-sm-4"><div class="form-group">';
		$html .= '<label class="control-label">Atendido</label>';
		$html .= $this->combobox->getObject();;
		$html .= '</div></div>';
		
		$html .= '</div>';
		
		$this->set_filter($html);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		$this->load_model($this->controller);
		$this->load->library('datatables');
		
		$aprobado = "N";
		$atendido = "N";
		
		$this->datatables->setModel($this->pedido);
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('aprobado', '=', $aprobado);
		$this->datatables->where('atendido', '=', $atendido);
		
		$this->datatables->setColumns(array('idpedido','fecha','descripcion'));
		$this->datatables->order_by('idpedido', "desc");
		
		$columnasName = array(
			'Nro'
			,'Fecha de Emision'
			,'Descripci&oacute;n'
		);

		$table = $this->datatables->createTable($columnasName);
		$script = "<script>".$this->datatables->createScript()."</script>";
		$this->js($script, false);
		
		$this->filtros_grilla($aprobado, $atendido);
		
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Pedido");
		$this->set_subtitle("");
		
		$this->set_content($this->form());
		$this->index("content");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model(array("pedido", "detalle_pedido"));
		$data["pedido"] = $this->pedido->find($id);
		
		$data["detalle_pedido"] = $this->detalle_pedido->get_item_pedido($id);
		
		$this->set_title("Modificar Pedido");
		$this->set_subtitle("");
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	/* public function pedido_detalle($id) {
		$this->load_model("pedido");
		$data["pedido"] = $this->pedido->find($id);

		
		$this->set_title("Detalle del Pedido / ");
		$this->set_subtitle($data["pedido"]["descripcion"]);

		$this->set_content($this->form_pedido_detalle($data,$id));
		$this->index("content");
	} */
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model($this->controller);
		
		$fields = $this->input->post();
		$fields['idsucursal'] = $this->get_var_session("idsucursal");
		$fields['idusuario'] = $this->get_var_session("idusuario");
		$fields['fecha'] =  date('Y-m-d H:i:s');
		$fields['estado'] = "A";
		$fields['atendido'] = "N";
		$fields['aprobado'] = "N";
		
		$this->db->trans_start(); // inciamos transaccion
		
		if(empty($fields["idpedido"])) {
			$idpedido = $this->pedido->insert($fields);
		}
		else {
			$this->pedido->update($fields);
			$idpedido = $fields["idpedido"];
		}
		
		// detalle pedido
		$this->load_model("detalle_pedido");
		$this->db->query("UPDATE compra.detalle_pedido SET estado=? WHERE idpedido=?", array("I", $idpedido));		
		
		foreach($fields["deta_idproducto"] as $key=>$val) {
			$data = $this->detalle_pedido->find(array("idpedido"=>$idpedido, "idproducto"=>$val, "idunidad"=>$fields["deta_idunidad"][$key], "estado"=>"I"));
			if($data != null) {
				$data["estado"] = 'A';
				$data["cantidad"] = floatval($fields["deta_cantidad"][$key]);
				$data['atendido'] = "N";
				$this->detalle_pedido->update($data);
			}
			else {
				$data["idpedido"] = $idpedido;
				$data["idproducto"] = $val;
				$data["estado"] = 'A';
				$data["idunidad"] = $fields["deta_idunidad"][$key];
				$data["cantidad"] = floatval($fields["deta_cantidad"][$key]);
				$data['atendido'] = "N";
				$this->detalle_pedido->insert($data);
			}
		}
		
		$this->db->trans_complete(); // finalizamos transaccion
		
		$this->response($this->pedido->get_fields());
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model($this->controller);
		
		$fields['idpedido'] = $id;
		$fields['estado'] = "I";
		$this->pedido->update($fields);
		
		$this->response($fields);
	}
	
	public function aprobar_pedido($id){
		$this->load_model($this->controller);
		$fields['idpedido'] = $id;
		$fields['aprobado'] = "S";
		$this->pedido->update($fields);
		$this->response($fields);
	}
	
	public function grilla_popup() {
		$this->load_model($this->controller);
		$this->load->library('datatables');
		
		$this->datatables->setModel($this->pedido);
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('aprobado', '=', 'S');
		$this->datatables->where('atendido', '=', 'N');
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		$this->datatables->setColumns(array('idpedido','fecha','descripcion'));
		$this->datatables->setPopup(true);
		$this->datatables->setSubgrid("cargarDetalle", true);
		
		$table = $this->datatables->createTable(array('Nro','Fecha de Emision','Descripci&oacute;n'));
		$script = "<script>".$this->datatables->createScript("", false)."</script>";
		
		$this->response($script.$table);
	}
	
	public function get_detalle_pedido() {
		$this->load_model("pedido");
		
		$post = $this->input->post();
		
		$params = array("d.idpedido"=>$post["idpedido"], "d.estado"=>'A', "d.atendido"=>'N');
		if(!empty($post["idproducto"])) {
			$params["d.idproducto"] = $post["idproducto"];
		}
		
		$prods = $this->pedido->get_detalle($params);
		$this->response($prods);
	}
}
?>