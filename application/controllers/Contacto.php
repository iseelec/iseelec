<?php

include_once "Cliente.php";

class Contacto extends Cliente {
	
	public function guardar() {
		$fields = $this->input->post();
		
		$idcliente = parent::guardar();
		
		if(empty($fields["idcliente"]))
			$this->db->query("update venta.cliente set estado='I' where idcliente=?", array($idcliente));
		
		return $this->get_all($idcliente);
	}
}
