<?php

include_once "Controller.php";

class Home extends Controller {

	public function init_controller() {
		$this->set_title("Bienvenido");
		$this->set_subtitle("Haga clic en el modulo que desea ingresar");
		
		if(!$this->with_tabs) {
			if(!$this->in_session("idsistema")) {
				$this->set_subtitle("Haga clic en el sistema que desea ingresar");
				$this->set_content($this->get_sistemas());
			}
		}
		
		$this->session->unset_userdata('menu_p');
		$this->session->unset_userdata('menu_c');
	}
	
	public function index($tpl = "") {
		// print_r($this->get_var_session());exit;
		if(!$this->with_tabs) {
			parent::index();
			return;
		}
		$ver_dash = true;//Variable para hacer pruebas, hasta que se termine de armar todo el dash
		$data["title"] = "Dashboard"; 
		$data["lista"] = $this->ventas(); /*Ventas Total Mes sucursales*/
		$data["credito"] = $this->ventas_credito(); /*Venta Total Mes al credito*/
		$data["compras"] = $this->compras(); /*Compras Total Mes*/
		$data["compras_credito"] = $this->compras_credito(); /*Compras al credito por pagar*/
		$data["reporteventas"] = $this->ventas_total(); /*Reporte*/
		$data["ventas_d"] = $this->ventas_c(); /*Reporte*/
		$data["sucursal"] = $this->sucursal(); /*Reporte Ventas Sucursales*/
		// echo "<pre>";
		// print_r($this->session);
		// exit;
		if(!$ver_dash){
			$data["content"] = "Algun contenido aqui"; //Enviar una vista por el array content
			$str = $this->load->view("content_tab", $data, true); //,trues
		}else{
			$data["content"] = file_get_contents("application/views/content_tab2.php"); //Enviar una vista por el array content
			$str = $this->load->view("content_tab2", $data, true); //,true
		}
		$this->show($str, null, true);
	}
	
	public function end_controller() {
		return null;
	}
	
	public function form() {
		return null;
	}
	
	public function grilla() {
		return null;
	}
	
	public function cambiar_sucursal($idsucursal) {
		$this->db->where("idusuario",$this->get_var_session("idusuario"));
		$this->db->where("idsucursal",$idsucursal);
		$query = $this->db->get("seguridad.acceso_empresa");
		
		if($query->num_rows() > 0) {
			$row = $query->row();
			
			$query1 = $this->db->where("idsucursal",$row->idsucursal)->get("seguridad.sucursal");
			$query2 = $this->db->where("idperfil",$row->idperfil)->get("seguridad.perfil");
			
			$this->session->set_userdata('idsucursal', $row->idsucursal);
			$this->session->set_userdata('es_superusuario', $row->es_superusuario);
			$this->session->set_userdata('sucursal', $query1->row()->descripcion);
			$this->session->set_userdata('idperfil', $row->idperfil);
			$this->session->set_userdata('idempresa', $query1->row()->idempresa);
			if( !empty($query2->row()->descripcion) )
				$this->session->set_userdata('perfil', $query2->row()->descripcion);
			else
				$this->session->set_userdata('perfil', '');
			// die("---");
			
		}
		redirect('home/');
	}
	
	public function get_sistemas() {
		$this->db->select("s.*");
		$this->db->from("seguridad.acceso_sistema a");
		$this->db->join('seguridad.sistema s', 's.idsistema = a.idsistema');
		$this->db->where('a.idsucursal', $this->get_var_session("idsucursal"));
		$this->db->where('s.estado', 'A');
		
		$arr = array();
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$arr = $query->result_array();
		}
		
		return $this->load->view("sistemas", array("sistemas"=>$arr), true);
	}
	
	public function cambiar_sistema($idsistema) {
		$this->db->select("s.*");
		$this->db->from("seguridad.acceso_sistema a");
		$this->db->where("a.idsucursal",$this->get_var_session("idsucursal"));
		$this->db->where("a.idsistema",$idsistema);
		$this->db->join("seguridad.sistema s", "s.idsistema=a.idsistema");
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			$row = $query->row();
			
			$this->session->set_userdata('idsistema', $row->idsistema);
			$this->session->set_userdata('sistema', $row->descripcion);
			
		}
		redirect('home/');
	}
	
	public function seleccion_sistema($redirect = true) {
		// eliminamos variable de session sistema
		$this->session->unset_userdata('idsistema');
		$this->session->unset_userdata('sistema');
		$this->session->unset_userdata('menu_p');
		$this->session->unset_userdata('menu_c');
		
		if($redirect) {
			redirect('home/');
		}
	}
	
	public function seleccion_sucursal() {
		// eliminamos variable de session
		$this->seleccion_sistema(false);
		
		// eliminamos variable de session sucursal
		$this->session->unset_userdata('idsucursal');
		$this->session->unset_userdata('idempresa');
		$this->session->unset_userdata('sucursal');
		$this->session->unset_userdata('idperfil');
		$this->session->unset_userdata('perfil');
		
		redirect('home/');
	}
	
	public function default_values() {
		$this->load_model("seguridad.datos_usuario");
		
		$post = $this->input->post();
		$idsucursal = $this->get_var_session("idsucursal");
		$idusuario = $this->get_var_session("idusuario");
		
		// eliminamos los datos almacenados
		$sql = "DELETE FROM seguridad.datos_usuario WHERE idsucursal=? AND idusuario=?";
		$this->db->query($sql, array($idsucursal, $idusuario));
		
		$this->datos_usuario->set("idsucursal", $idsucursal);
		$this->datos_usuario->set("idusuario", $idusuario);
		
		if( ! empty($post["datos"])) {
			foreach($post["datos"] as $row) {
				if( ! empty($row["valor"])) {
					$this->datos_usuario->set($row);
					$this->datos_usuario->text_uppercase(false);
					$this->datos_usuario->insert(null, false);
				}
			}
		}
		
		$this->response($this->datos_usuario->get_fields());
	}
	
	/*Dashboard*/
	public function ventas() {
		$idempresa = $this->get_var_session("idempresa")?$this->get_var_session("idempresa"):0;
		$sql = "SELECT round((sum(venta.subtotal))::DECIMAL, 2)::TEXT as venta
				FROM venta.venta, seguridad.sucursal
				WHERE sucursal.idsucursal = venta.idsucursal 
				and venta.estado = 'A' 
				and extract(Year from venta.fecha_venta) = extract( 'Year' from now()) 
				and venta.idtipodocumento in (1,2)
				and extract( 'Month' from now()) = extract(MONTH from venta.fecha_venta) 
				and sucursal.idempresa = ".$idempresa.";";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function ventas_credito() {
		$idempresa = $this->get_var_session("idempresa")?$this->get_var_session("idempresa"):0;
		$sql = "SELECT coalesce(sum(subtotal), '0.00') as v_credito FROM venta.venta, seguridad.sucursal 
			where sucursal.idsucursal = venta.idsucursal  
			and venta.idtipodocumento in (1,2) and
			venta.estado = 'A' and venta.con_credito = 'S' and extract(Year from venta.fecha_venta) = 
			extract (Year from Now()) and extract(Month from venta.fecha_venta) =  extract(Month from now()) and sucursal.idempresa = ".$idempresa.";";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function compras() {
		$pago_contado = $this->get_param('idpago_compra_contado')?$this->get_param('idpago_compra_contado'):0;
		/*$sql = "SELECT round((sum(subtotal + igv))::DECIMAL, 2)::TEXT as compras FROM compra.compra where estado = 'A' and extract( 'Day' from now()) = extract(Day from fecha_compra) and extract ('Month' from now()) = extract(Month from fecha_compra) and idforma_pago_compra = 1"; Mes actual*/
		$sql = "SELECT COALESCE(round((sum(subtotal + igv))::DECIMAL, 2)::TEXT,'0.00') as compras FROM compra.compra where estado = 'A' and extract ('Month' from now()) = extract(Month from fecha_compra) and idforma_pago_compra = $pago_contado ;";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function compras_credito() {
		$pago_credito = $this->get_param('idpago_compra_credito')?$this->get_param('idpago_compra_credito'):0;
		/*$sql = "SELECT round((sum(subtotal + igv))::DECIMAL, 2)::TEXT as compras_credito FROM compra.compra where estado = 'A' and extract( 'Day' from now()) = extract(Day from fecha_compra) and extract ('Month' from now()) = extract(Month from fecha_compra) and idforma_pago_compra = 2"; Mes actual*/
		$sql = "SELECT COALESCE(round((sum(subtotal + igv))::DECIMAL, 2)::TEXT,'0.00') as compras_credito FROM compra.compra where estado = 'A' and extract ('Month' from now()) = extract(Month from fecha_compra) and idforma_pago_compra = $pago_credito ;";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function ventas_total() {
		$idempresa = $this->get_var_session("idempresa")?$this->get_var_session("idempresa"):0;		
		$sql="SELECT coalesce(sum(subtotal), '0.00') as total, extract(Day from venta.fecha_venta) as dia FROM venta.venta, seguridad.sucursal 
			where sucursal.idsucursal = venta.idsucursal and venta.estado = 'A' and extract(Year from venta.fecha_venta) = extract (Year from Now()) and extract(Month from venta.fecha_venta) =  extract(Month from now()) 
			and venta.idtipodocumento in (1,2)and sucursal.idempresa = $idempresa
			GROUP BY venta.fecha_venta order by venta.fecha_venta";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function ventas_c() {
		$idempresa = $this->get_var_session("idempresa")?$this->get_var_session("idempresa"):0;
		$sql = "SELECT coalesce(sum(subtotal), '0.00') as ventas_d, extract(Day from venta.fecha_venta) as dia FROM venta.venta, seguridad.sucursal 
			where sucursal.idsucursal = venta.idsucursal and venta.estado = 'A' 
			and venta.idtipodocumento in (1,2)
			and extract(Year from venta.fecha_venta) = extract (Year from Now()) and extract(Month from venta.fecha_venta) =  extract(Month from now()) and sucursal.idempresa = $idempresa and venta.con_credito = 'S' 
			GROUP BY venta.fecha_venta order by venta.fecha_venta";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function sucursal() {
		$idempresa = $this->get_var_session("idempresa")?$this->get_var_session("idempresa"):0;
		$sql = "SELECT sucursal.idsucursal, sucursal.descripcion as nombre, round((sum(subtotal))::DECIMAL, 2)::TEXT as venta, (SELECT round((sum(venta.subtotal))::DECIMAL, 2)::TEXT as venta
			FROM venta.venta, seguridad.sucursal
			WHERE sucursal.idsucursal = venta.idsucursal and venta.estado = 'A' and extract(Year from venta.fecha_venta) = extract( 'Year' from now()) 
			and extract( 'Month' from now()) = extract(MONTH from venta.fecha_venta) and sucursal.idempresa = $idempresa) as total 
			FROM venta.venta, seguridad.sucursal
			WHERE sucursal.idsucursal = venta.idsucursal and venta.estado = 'A' and extract(Year from fecha_venta) = extract( 'Year' from now()) 
			and extract( 'Month' from now()) = extract(MONTH from fecha_venta) and sucursal.idempresa = $idempresa 
			GROUP BY venta.idsucursal, sucursal.idsucursal, nombre
			ORDER BY venta.idsucursal;";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}
?>