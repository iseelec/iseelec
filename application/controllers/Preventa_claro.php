<?php

include_once "Controller.php";

class Preventa_claro extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Preventas de Claro");
		$this->set_subtitle("Lista de preventas");
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form() {
		$data["controller"] = $this->controller;
		
		$this->load->library('combobox');
		
		// combo tipo pago
		$query = $this->db->where("estado", "A")->where("mostrar_en_reciboingreso", "S")->get("venta.tipopago");
		
		$this->combobox->setAttr("id", "idtipopago_temp");
		$this->combobox->setAttr("name", "idtipopago_temp");
		$this->combobox->setAttr("class", "form-control input-xs");
		$this->combobox->addItem($query->result_array(), null, array("idtipopago", "descripcion"));
		$data["tipopago"] = $this->combobox->getObject();
		
		// combo estado de credito
		$query = $this->db->where("estado", "A")->get("credito.estado_credito");
		$this->combobox->init();
		$this->combobox->setAttr(array("name"=>"id_estado_credito", "id"=>"id_estado_credito", "class"=>"form-control"));
		$this->combobox->addItem($query->result_array(), null, array("id_estado_credito", "descripcion"));
		$data["estado_credito"] = $this->combobox->getObject();
		
		// dias del mes
		$dias = $this->get_param("dias_mes");
		if($dias == null) {
			$dias = 30;
		}
		$data["dias_mes"] = $dias;
		
		// valor de la mora
		$mora = $this->get_param("mora");
		if($mora == null) {
			$mora = 0;
		}
		$data["valor_mora"] = $mora;
		
		// id recibo ingreso
		$idrecibo_ingreso = $this->get_param("idrecibo_ingreso");
		if($idrecibo_ingreso == null) {
			throw new Exception("No existe el parametro <b>idrecibo_ingreso</b>");
		}
		
		$data["modal_pago"] = $this->get_form_pago("reciboingreso");
		
		$this->css("plugins/datapicker/datepicker3");
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		$this->js("<script>var _idrecibo_ingreso_ = ".$idrecibo_ingreso.";</script>", false);
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		return null;
	}
	
	public function index($tpl = "") {
		$data = array(
			"menu_title" => $this->menu_title
			,"menu_subtitle" => $this->menu_subtitle
			,"content" => $this->form()
			,"with_tabs" => $this->with_tabs
		);
		
		if($this->show_path) {
			$data['path'] = $this->get_path();
		}
		
		$str = $this->load->view("content_empty", $data, true);
		$this->show($str);
	}
}
?>