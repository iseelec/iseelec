<?php

include_once "Controller.php";

class Cotizacion extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Mantenimiento de Cotizacion");
		$this->set_subtitle("Lista de cotizacions");
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		// $this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		// $this->js('form/'.$this->controller.'/index');
		// $this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		
		$this->load->library('combobox');
		
		// combo tipo cotizacion
		$this->combobox->setAttr("id", "idtipocotizacion");
		$this->combobox->setAttr("name", "idtipocotizacion");
		$this->combobox->setAttr("class", "form-control input-sm");
		$this->combobox->setAttr("required", "");
		
		$this->db->select('idtipocotizacion, descripcion');
		$query = $this->db->where("estado", "A")->where("mostrar_en_cotizacion", "S")->get("cotizacion.tipo_cotizacion");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["cotizacion"]["idtipocotizacion"]) ) {
			$this->combobox->setSelectedOption($data["cotizacion"]["idticotizacion"]);
		}
		
		$data["tipocotizacion"] = $this->combobox->getObject();
		
		// combo tipodocumento
		$this->combobox->init(); // un nuevo combo
		
		$this->combobox->setAttr(
			array(
				"id"=>"idtipodocumento"
				,"name"=>"idtipodocumento"
				,"class"=>"form-control input-sm"
				,"required"=>""
			)
		);
		$this->db->select('idtipodocumento, descripcion');
		$query = $this->db->where("estado", "A")->where("mostrar_en_cotizacion", "S")->get("venta.tipo_documento");
		$this->combobox->addItem($query->result_array());
		
		if( isset($data["cotizacion"]["idtipodocumento"]) ) {
			$this->combobox->setSelectedOption($data["cotizacion"]["idtipodocumento"]);
		}
		
		$data["tipodocumento"] = $this->combobox->getObject();
		
		// combo almacen
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idalmacen"
				,"name"=>"idalmacen"
				,"class"=>"form-control input-sm"
				,"required"=>""
			)
		);
		$query = $this->db->select('idalmacen, descripcion')->where("estado", "A")
			->where("idsucursal", $this->get_var_session("idsucursal"))->get("almacen.almacen");
		$this->combobox->addItem($query->result_array());
		if( isset($data["cotizacion"]["idalmacen"]) ) {
			$this->combobox->setSelectedOption($data["cotizacion"]["idalmacen"]);
		}
		$data["almacen"] = $this->combobox->getObject();
		
		// combo moneda
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idmoneda"
				,"name"=>"idmoneda"
				,"class"=>"form-control input-sm"
				,"required"=>""
			)
		);
		$query = $this->db->select('idmoneda, descripcion')->where("estado", "A")->order_by("idmoneda", "asc")->get("general.moneda");
		$this->combobox->addItem($query->result_array());
		if( isset($data["cotizacion"]["idmoneda"]) ) {
			$this->combobox->setSelectedOption($data["cotizacion"]["idmoneda"]);
		}
		$data["moneda"] = $this->combobox->getObject();

		// combo moneda flete
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idmoneda_flete"
				,"name"=>"idmoneda_flete"
				,"class"=>"form-control input-xs combo_min"
				,"required"=>""
			)
		);
		$query = $this->db->select('idmoneda, abreviatura')->where("estado", "A")->order_by("idmoneda", "asc")->get("general.moneda");
		$this->combobox->addItem($query->result_array());
		if( isset($data["cotizacion"]["idmoneda_flete"]) ) {
			$this->combobox->setSelectedOption($data["cotizacion"]["idmoneda_flete"]);
		}
		$data["moneda_flete"] = $this->combobox->getObject();
		
		// combo moneda gastos
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idmoneda_gastos"
				,"name"=>"idmoneda_gastos"
				,"class"=>"form-control input-xs combo_min"
				,"required"=>""
			)
		);
		$query = $this->db->select('idmoneda, abreviatura')->where("estado", "A")->order_by("idmoneda", "asc")->get("general.moneda");
		$this->combobox->addItem($query->result_array());
		if( isset($data["cotizacion"]["idmoneda_gastos"]) ) {
			$this->combobox->setSelectedOption($data["cotizacion"]["idmoneda_gastos"]);
		}
		$data["moneda_gastos"] = $this->combobox->getObject();
		
		
		// forma pago cotizacion
		$this->combobox->init(); // un nuevo combo
		$this->combobox->setAttr(
			array(
				"id"=>"idforma_pago_cotizacion"
				,"name"=>"idforma_pago_cotizacion"
				,"class"=>"form-control input-sm"
			)
		);
		$query = $this->db->select('idforma_pago_cotizacion, descripcion')->where("estado", "A")->get("cotizacion.forma_pago_cotizacion");
		$this->combobox->addItem($query->result_array());
		if( isset($data["cotizacion"]["idforma_pago_cotizacion"]) ) {
			$this->combobox->setSelectedOption($data["cotizacion"]["idforma_pago_cotizacion"]);
		}
		$data["forma_pago_cotizacion"] = $this->combobox->getObject();
		
		$data["controller"] = $this->controller;
		
		$igv = $this->get_param("igv");
		if(!is_numeric($igv)) {
			$igv = 18;
		}
		$data["valor_igv"] = $igv;
		
		$es_nuevo = "true";
		if( isset($data["cotizacion"]["idcotizacion"]) ) {
			$es_nuevo = "false";
		}
		$this->js("<script>var _es_nuevo_".$this->controller."_ = $es_nuevo;</script>", false);
		
		$this->load_controller("producto");
		// $this->producto_controller->load = $this->load;
		// $this->producto_controller->db = $this->db;
		// $this->producto_controller->session = $this->session;
		// $this->producto_controller->combobox = $this->combobox;
		
		$data["form_producto"] = $this->producto_controller->form(null, "", true);
		$data["form_producto_unidad"] = $this->producto_controller->form_unidad_medida(null, "", true);
		$data["modal_pago"] = $this->get_form_pago("cotizacion", true);
		
		$fc = $this->get_param("fixed_cotizacion");
		if(!is_numeric($fc)) {
			$fc = 2;
		}
		$this->js("<script>var _fixed_cotizacion = $fc;</script>", false);
		
		$es_nuevo = "true";
		if( isset($data["cotizacion"]["idcotizacion"]) ) {
			$es_nuevo = "false";
		}
		$this->js("<script>var _es_nuevo_".$this->controller."_ = $es_nuevo;</script>", false);
		
		if( isset($data["detalle"]) ) {
			$this->js("<script>var data_detalle = ".json_encode($data["detalle"]).";</script>", false);
		}
		
		$this->css('plugins/jQueryUI/jquery-ui-autocomplete.min');
		$this->css("plugins/datapicker/datepicker3");
		// $this->css('plugins/iCheck/custom');
		$this->js('plugins/jquery-ui/jquery-ui-autocomplete.min');
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		// $this->js('plugins/iCheck/icheck.min');
		$this->js('form/'.$this->controller.'/form');
		$this->js('form/producto/modal');
		
		// formulario CLIENTE
		$this->load_controller("cliente");
		$data["form_cliente"] = $this->cliente_controller->form(null, "cli_", true);
		$this->js('form/cliente/modal');
		
		//// formulario PROVEEDOR
		//$this->load_controller("proveedor");
		//// $this->proveedor_controller->load = $this->load;
		//// $this->proveedor_controller->db = $this->db;
		//// $this->proveedor_controller->session = $this->session;
		//// $this->proveedor_controller->combobox = $this->combobox;
		//$data["form_proveedor"] = $this->proveedor_controller->form(null, "prov_", true);

		//$this->js('form/proveedor/modal');
		
		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * filtros de la grilla 
	 */
	public function filtros_grilla() {
		$this->load_library("combobox");
		$this->load_model($this->controller);
				$this->load->library('datatables');


		$html = '<div class="row">';
		
		// div y combobox recepcionado
		$this->combobox->setAttr("filter", "recepcionado");
		$this->combobox->setAttr("class", "form-control");
		$this->combobox->addItem("", "TODOS");
		$this->combobox->addItem("S", "COTIZACIÓN");
		$this->combobox->addItem("N", "PRESUPUESTO ENVIADO");
		
		$html .= '<div class="col-sm-4"><div class="form-group">';
		$html .= '<label class="control-label">Recepcionado</label>';
		$html .= $this->combobox->getObject();
		$html .= '</div></div>';
		
		$html .= '</div>';
		
		$this->set_filter($html);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		//$this->load_model("cotizacion.cotizacion_view");
		$this->load_model($this->controller);
		$this->load->library('datatables');
		
	//	$this->datatables->setModel($this->cotizacion_view);
		$this->datatables->setModel($this->cotizacion);
		$this->datatables->setIndexColumn("idcotizacion");
		
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		
		$this->datatables->setColumns(array('fecha_cotizacion','tipo_documento',
			'nrodocumento','cliente','tipoventa','moneda','subtotal','igv','total'));
		
		$this->datatables->order_by("fecha_cotizacion", "desc"); // desc default
		
		$this->datatables->setCallback("verificarRecepcionados");
		
		$columnasName = array('F.Cotizacion','Documento','Nro.Documento','Cliente',
			'Tipo cotizacion','Moneda','Subtotal','IGV','Total');

		$table = $this->datatables->createTable($columnasName);
		$script = "<script>".$this->datatables->createScript()."</script>";
		
		// $this->css('plugins/dataTables/dataTables.bootstrap');
		// $this->css('plugins/dataTables/dataTables.responsive');
		// $this->css('plugins/dataTables/dataTables.tableTools.min');
		
		// $this->js('plugins/dataTables/jquery.dataTables');
		// $this->js('plugins/dataTables/dataTables.bootstrap');
		// $this->js('plugins/dataTables/dataTables.responsive');
		// $this->js('plugins/dataTables/dataTables.tableTools.min');
		$this->js($script, false);
		
		$this->filtros_grilla();
		
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	public function nuevo() {
		$this->set_title("Registrar Cotizacion");
		$this->set_subtitle("");
		
		$this->set_content($this->form());
		$this->index("content_empty");
	}
	
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->load_model($this->controller);
		$data["cotizacion"] = $this->cotizacion->find($id);
		$data["total"] = $data["cotizacion"]["subtotal"]+$data["cotizacion"]["igv"];
		
		$this->load_model("detalle_cotizacion");
		$data["detalle"] = $this->detalle_cotizacion->get_items($id);
		
		$this->load_model("proveedor");
		$data["proveedor"] = $this->proveedor->find($data["cotizacion"]["idproveedor"]);
		//cliente
		$this->load_model("cliente");
		$data["cliente"] = $this->cliente->find($data["cotizacion"]["idcliente"]);
		
		// verificamos si la cotizacion es editable
		$is_editable = true;
		
		// revisamos si se ha hecho alguna recepcion
		$sql = "SELECT * FROM cotizacion.detalle_cotizacion
			WHERE idcotizacion=? AND estado=? AND recepcionado=? and afecta_stock=?";
		$query = $this->db->query($sql, array($id, "A", "S", "S"));
		if($query->num_rows() > 0) {
			// comprobamos la fecha de registro de la cotizacion
			$arr = explode(" ", $this->cotizacion->get("fecha_registro"));
			$fecha_reg = array_shift($arr);
			if($fecha_reg != date("Y-m-d")) {
				$is_editable = false;
			}
		}
		$data["editable"] = $is_editable;
		
		$this->set_title("Modificar Cotizacion");
		$this->set_subtitle("");
		$this->set_content($this->form($data));
		$this->index("content_empty");
	}
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model($this->controller);
		$this->load_model("general.moneda");
		$fields = $this->input->post();
		
		$fields['idsucursal'] = $this->get_var_session("idsucursal");

		$fields['idusuario'] = $this->get_var_session("idusuario");
		$fields['fecha_registro'] = date("Y-m-d");
		$fields['recepcionado'] = (!empty($fields['recepcionado'])) ? "S" : "N";
		$fields['afecta_caja'] = (!empty($fields['afecta_caja'])) ? "S" : "N";
		$fields['estado'] = "A";
		if(empty($fields["descuento"]))
			$fields["descuento"] = 0;
		if(empty($fields["gastos"]))
			$fields["gastos"] = 0;
		if(empty($fields["flete"]))
			$fields["flete"] = 0;
		if(empty($fields["igv"]))
			$fields["igv"] = 0;
		if(empty($fields["fecha_cotizacion"]))
			$fields["fecha_cotizacion"] = date("Y-m-d");
		if(empty($fields["nro_letras"]))
			$fields["nro_letras"] = 1;
		
		$m_flete = $this->moneda->find($fields['idmoneda_flete']);
		$m_gasto = $this->moneda->find($fields['idmoneda_gastos']);
		
		if(empty($fields["flete_convertido"]))
			$fields["flete_convertido"] = $fields["flete"]*$m_flete['valor_cambio'];
		if(empty($fields["gastos_convertido"]))
			$fields["gastos_convertido"] = $fields["gastos"]*$m_gasto['valor_cambio'];

		$esNuevaCotizacion = (empty($fields["idcotizacion"]) == true);
		
		$status = 'A';
		if($fields['recepcionado'] == 'S'){
			$status = 'C';
		}
		
		$this->db->trans_start(); // inciamos transaccion
		
		if($esNuevaCotizacion) {
			$idcotizacion = $this->cotizacion->insert($fields);
		}
		else {
			$idcotizacion = $fields["idcotizacion"];
			
			// obtenemos los datos anteriores
			$temp = $this->cotizacion->find($idcotizacion);
			
			// verificamos si la cotizacion ha sido al credito, quizas necesitemos hacer alguna validacion
			if($temp["idtipoventa"] == 2) {
				// cotizacion al credito, verificamos si hay algun pago
				$query = $this->db->where("estado", "A")->where("idcotizacion", $idcotizacion)->get("cotizacion.pago_cotizacion");
				if($query->num_rows() > 0) {
					$this->exception("Ya se han realizado pagos de la cotizacion. No se puede modificar.");
					return false;
				}
			}
			
			// actualizamos los datos de la cotizacion
			$this->cotizacion->update($fields);
			
			// eliminamos el ingreso en detalle_almacen
			$this->db->where("tabla", "C")->where("idtabla", $idcotizacion)
				->update("almacen.detalle_almacen", array("estado"=>"I"));
				
			// eliminamos el ingreso de las series en almacen
			$this->db->where("tabla_ingreso", "C")->where("idtabla_ingreso", $idcotizacion)
				->update("almacen.detalle_almacen_serie", array("estado"=>"I"));
				
			// eliminamos el detalle de la cotizacion
			$this->db->where("idcotizacion", $idcotizacion)
				->update("cotizacion.detalle_cotizacion", array("estado"=>"I"));
				
			// eliminamos las series de la cotizacion
			$this->db->where("idcotizacion", $idcotizacion)
				->update("cotizacion.detalle_cotizacion_serie", array("estado"=>"I"));
				
			// eliminamos la recepcion
			$this->db->where("idcotizacion", $idcotizacion)->where("referencia", "C")
				->update("almacen.recepcion", array("estado"=>"I"));
				
			// eliminamos el credito de la cotizacion
			$this->db->where("idcotizacion", $idcotizacion)
				->update("compra.cronograma_pago", array("estado"=>"I"));
		}
		
		// cargamos los modelos
		$this->load_model("detalle_cotizacion");
		$this->load_model("detalle_cotizacion_serie");
		$this->load_model("compra.producto_precio_unitario");
		$this->load_model("compra.producto_precio_cotizacion");
		$this->load_model("compra.producto_unidad");
		
		// llenamos datos por default para los modelos
		$this->detalle_cotizacion->set("idcotizacion", $idcotizacion);
		$this->detalle_cotizacion->set("estado", "A");
		$this->detalle_cotizacion->set("recepcionado", $fields["recepcionado"]);
		$this->detalle_cotizacion->set("idalmacen", $fields["idalmacen"]);
		
		$this->producto_precio_cotizacion->set("idsucursal", $this->cotizacion->get("idsucursal"));
		$this->producto_precio_cotizacion->set("idmoneda", $this->cotizacion->get("idmoneda"));
		
		if($fields['recepcionado'] == "S") {
			// modelos para el almacen
			$this->load_model("detalle_almacen");
			$this->load_model("detalle_almacen_serie");
			$this->load_model("recepcion"); // siempre se registra esta vaina
			$this->load_model("tipo_movi_almacen");
			
			$this->detalle_almacen->set("tipo", "E");
			$this->detalle_almacen->set("tipo_number", 1);
			$this->detalle_almacen->set("fecha", date("Y-m-d"));
			$this->detalle_almacen->set("tabla", "C");
			$this->detalle_almacen->set("idtabla", $idcotizacion);
			$this->detalle_almacen->set("estado", "A");
			$this->detalle_almacen->set("idsucursal", $this->cotizacion->get("idsucursal"));
			
			$this->detalle_almacen_serie->set("fecha_ingreso", date("Y-m-d"));
			$this->detalle_almacen_serie->set("tabla_ingreso", "C");
			$this->detalle_almacen_serie->set("idtabla_ingreso", $idcotizacion);
			$this->detalle_almacen_serie->set("despachado", "N");
			$this->detalle_almacen_serie->set("estado", "A");
			$this->detalle_almacen_serie->set("idsucursal", $this->cotizacion->get("idsucursal"));
			
			$this->recepcion->set("tipo_docu", $this->cotizacion->get("idtipodocumento"));
			$this->recepcion->set("serie", $this->cotizacion->get("serie"));
			$this->recepcion->set("numero", $this->cotizacion->get("numero"));
			$this->recepcion->set("observacion", "RECEPCION AUTOMATICA DE OC NRO. ".$idcotizacion);
			$this->recepcion->set("fecha", date("Y-m-d"));
			$this->recepcion->set("hora", date("H:i:s"));
			$this->recepcion->set("idusuario", $this->cotizacion->get("idusuario"));
			$this->recepcion->set("referencia", "C");
			
			$this->tipo_movi_almacen->find($this->get_idtipo_movimiento("cotizacion"));
			$correlativo = intval($this->tipo_movi_almacen->get("correlativo"));
		}
		
		$arrPedido = array();
		$arrProductosKardex = array(); // datos almacen kardex
		$tipocambio = ($fields["idmoneda"] == 1) ? 1 : floatval($fields["cambio_moneda"]);
		
		// recorremos lista de producto
		foreach($fields["deta_idproducto"] as $key=>$val) {
			// insertamos el detalle cotizacion
			$this->detalle_cotizacion->set("idproducto", $val);
			$this->detalle_cotizacion->set("idunidad", $fields["deta_idunidad"][$key]);
			$this->detalle_cotizacion->set("cantidad", floatval($fields["deta_cantidad"][$key])); // has modificado aqui?, verifica el ingreso de series
			$this->detalle_cotizacion->set("precio", floatval($fields["deta_precio"][$key]));
			$this->detalle_cotizacion->set("igv", floatval($fields["deta_igv"][$key]));
			$this->detalle_cotizacion->set("flete", floatval($fields["deta_flete"][$key]));
			$this->detalle_cotizacion->set("gastos", floatval($fields["deta_gastos"][$key]));
			$this->detalle_cotizacion->set("costo", floatval($fields["deta_costo"][$key]));
			$this->detalle_cotizacion->set("afecta_stock", $fields["deta_controla_stock"][$key]);
			$this->detalle_cotizacion->set("afecta_serie", $fields["deta_controla_serie"][$key]);
			$this->detalle_cotizacion->insert();
			
			// insertamos las series del detalle cotizacion
			if($fields["deta_controla_serie"][$key] == "S") {
				if( ! empty($fields["deta_series"][$key])) {
					$this->detalle_cotizacion_serie->set($this->detalle_cotizacion->get_fields());
					$arr = explode("|", $fields["deta_series"][$key]);
					foreach($arr as $serie) {
						$this->detalle_cotizacion_serie->set("serie", $serie);
						$this->detalle_cotizacion_serie->insert(null, false);
					}
				}
			}
			
			$this->producto_unidad->find(array("idproducto"=>$val, "idunidad"=>$fields["deta_idunidad"][$key]));
			$cantidad_unidad = floatval($this->producto_unidad->get("cantidad_unidad_min"));
			
			// ingresamos el precio unitario de cotizacion del producto
			$datos["idproducto"] = $val;
			$datos["idsucursal"] = $fields["idsucursal"];
			$datos["precio_cotizacion"] = floatval($fields["deta_costo"][$key]) * $tipocambio / $cantidad_unidad;
			$this->producto_precio_unitario->save($datos, false);
			
			// si recepcionamos la cotizacion directamente, ingresamos el stock y las series al almacen
			if($fields['recepcionado'] == "S") {
				// ingresamos a recepcion
				$this->recepcion->set($this->detalle_cotizacion->get_fields());
				$this->recepcion->set("cant_recepcionada", $this->detalle_cotizacion->get("cantidad"));
				$this->recepcion->set("correlativo", $correlativo);
				// $this->recepcion->set("estado", $status);
				$this->recepcion->set("estado", "C");
				$this->recepcion->insert();
				$correlativo = $correlativo + 1; // nuevo correlativo
				
				if($fields["deta_controla_stock"][$key] == "S") {
					// ingresamos el stock en el almacen
					$this->detalle_almacen->set($this->detalle_cotizacion->get_fields());
					$this->detalle_almacen->set("precio_costo", $this->detalle_cotizacion->get("costo"));
					$this->detalle_almacen->set("idrecepcion", $this->recepcion->get("idrecepcion"));
					$this->detalle_almacen->insert();
					
					// verificamos para ingresar las series al almacen
					if($fields["deta_controla_serie"][$key] == "S") {
						if(empty($fields["deta_series"][$key])) {
							$this->exception("Ingrese las series del producto ".$fields["deta_producto"][$key]);
							return false;
						}
						
						$count_real_serie = $cantidad_unidad * floatval($fields["deta_cantidad"][$key]);
						
						$arr = explode("|", $fields["deta_series"][$key]);
						if(count($arr) != $count_real_serie) {
							$this->exception("Debe ingresar $count_real_serie series para el producto: ".$fields["deta_producto"][$key]);
							return false;
						}
						
						// ingresamos las series
						$this->detalle_almacen_serie->set($this->detalle_almacen->get_fields());
						foreach($arr as $serie) {
							if( $this->detalle_almacen_serie->exists(array("serie"=>$serie, "despachado"=>"N", "estado"=>"A")) ) {
								$this->exception("La serie $serie del producto ".$fields["deta_producto"][$key]." ya existe.");
								return false;
							}
							$this->detalle_almacen_serie->set("serie", $serie);
							$this->detalle_almacen_serie->insert(null, false);
						}
					}
					
					$temp = $this->recepcion->get_fields();
					$temp["cantidad"] = $temp["cant_recepcionada"];
					$temp["preciocosto"] = $this->detalle_cotizacion->get("costo") / $cantidad_unidad;
					$arrProductosKardex[] = $temp;
				}
			}
			
			// actualizamos el precio de costo del producto
			$this->producto_precio_compra->set($this->detalle_cotizacion->get_fields());
			$this->producto_precio_compra->set("precio", $this->detalle_cotizacion->get("costo"));
			$this->producto_precio_compra->save(null, false);
			
			// actualizamos el estado del pedido si se ha indicado
			if( ! empty($fields["deta_idpedido"][$key])) {
				$this->db->where("idpedido", $fields["deta_idpedido"][$key])
					->where("idproducto", $val)
					->update("compra.detalle_pedido", array("atendido"=>"S"));
				if( ! in_array($fields["deta_idpedido"][$key], $arrPedido)) {
					$arrPedido[] = $fields["deta_idpedido"][$key];
				}
			}
		} // fin [foreach]
		
		
		if( ! $esNuevaCotizacion) { // si estamos editando la cotizacion
			// eliminamos el ingreso de kardex
			$this->load_library("jkardex");
			$this->jkardex->remove("cotizacion", $idcotizacion, $fields["idsucursal"]);
			
			// eliminamos el pago ingresado
			$this->load->library('pay');
			$this->pay->remove("cotizacion", $idcotizacion, $fields["idsucursal"]);
		}
		
		if($fields['recepcionado'] == "S") {
			if( ! empty($arrProductosKardex)) {
				// actualizamos el correlativo del tipo movimiento
				$this->tipo_movi_almacen->set("correlativo", $correlativo);
				$this->tipo_movi_almacen->update();
				
				// registramos el movimiento de kardex, obtenemos data priquis
				// $sql = "SELECT idproducto, cant_recepcionada as cantidad, idunidad, idalmacen, correlativo
					// FROM almacen.recepcion WHERE idcotizacion = ?";
				// $query = $this->db->query($sql, array($idcotizacion));
				
				if( ! isset($this->jkardex)) {
					// importamos librari
					$this->load_library("jkardex");
				}
				
				$this->jkardex->idtercero = $this->cotizacion->get("idproveedor");
				$this->jkardex->idmoneda = $this->cotizacion->get("idmoneda");
				$this->jkardex->tipocambio = $this->cotizacion->get("cambio_moneda");
				
				$this->jkardex->referencia("cotizacion", $idcotizacion, $fields["idsucursal"]);
				$this->jkardex->entrada();
				// $this->jkardex->calcular_precio_costo();
				// $this->jkardex->push($query->result_array());
				$this->jkardex->push($arrProductosKardex);
				$this->jkardex->run();
			}
		}
		
		if( $fields["afecta_caja"] == 'S' && $fields["idtipoventa"] == 1 ) {
			// datos necesarios para la libreria pay, revisar la clase para mas info sobre las
			// variables necesarias para la clase
			$fields["descripcion"] = "COMPRA AL CONTADO";
			$fields["referencia"] = $fields['proveedor'];
			$fields["tabla"] = "cotizacion";
			$fields["idoperacion"] = $idcotizacion;
			
			if( ! isset($this->caja_controller)) {
				$this->load_controller("caja");
			}
			if( ! isset($this->pay)) {
				$this->load->library('pay');
			}
			$this->pay->set_controller($this->caja_controller);
			$this->pay->set_data($fields);
			$this->pay->entrada(false); // false si es salida, default true
			$this->pay->process();
		}
		else if($fields["idtipoventa"] == 2 && ! empty($fields["idforma_pago_cotizacion"])) {
			// creamos el credito para la cotizacion
			$this->load_model("cotizacion.forma_pago_cotizacion");
			$this->load_model("cotizacion.cronograma_pago");
			
			$this->cronograma_pago->set("idcotizacion", $idcotizacion);
			$this->cronograma_pago->set("idmoneda", $this->cotizacion->get("idmoneda"));
			$this->cronograma_pago->set("cancelado", "N");
			$this->cronograma_pago->set("estado", "A");
			
			$this->forma_pago_cotizacion->find($fields["idforma_pago_cotizacion"]);
			
			$nro_letras = intval($fields["nro_letras"]);
			$monto = floatval($fields["total"]) / $nro_letras;
			$dias = $this->forma_pago_cotizacion->get("nrodias");
			$fecha = new DateTime($this->cotizacion->get("fecha_cotizacion"));
			$i = 0;
			do {
				$i ++;
				$fecha->add(new DateInterval("P".$dias."D"));
				
				$this->cronograma_pago->set("letra", $i);
				$this->cronograma_pago->set("monto_letra", $monto);
				$this->cronograma_pago->set("fecha_vencimiento", $fecha->format("Y-m-d"));
				$this->cronograma_pago->set("saldo", $monto);
				$this->cronograma_pago->insert(null, false);
			}
			while($nro_letras > $i);
		}
		
		// cambiamos de estado a los pedidoss
		if(! empty($arrPedido)) {
			$this->load_model("pedido");
			
			foreach($arrPedido as $idpedido) {
				$sql = "SELECT * FROM cotizacion.detalle_pedido
					WHERE idpedido=? AND estado=? AND atendido=?";
				$query = $this->db->query($sql, array($idpedido, "A", "N"));
				
				if($query->num_rows() <= 0) {
					$this->pedido->update(array("idpedido"=>$idpedido, "atendido"=>"S"));
				}
			}
		}
		
		$this->db->trans_complete(); // finalizamos transaccion
		
		if ($this->db->trans_status() === FALSE) {
			// generate an error... or use the log_message() function to log your error
		}
		
		$this->response($this->cotizacion->get_fields());
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($idcotizacion) {
		$this->load_model($this->controller);
		
		$this->cotizacion->find($idcotizacion);
		
		$this->db->trans_start();
		
		$this->cotizacion->update(array("idcotizacion"=>$idcotizacion, "estado"=>"I"));
		
		// eliminamos la recepcion
		$this->db->where("idcotizacion", $idcotizacion)
			->update("almacen.recepcion", array("estado"=>"I"));
		
		// eliminamos el ingreso en detalle_almacen
		$this->db->where("tabla", "C")->where("idtabla", $idcotizacion)
			->update("almacen.detalle_almacen", array("estado"=>"I"));
			
		// eliminamos el ingreso de las series en almacen
		$this->db->where("tabla_ingreso", "C")->where("idtabla_ingreso", $idcotizacion)
			->update("almacen.detalle_almacen_serie", array("estado"=>"I"));
			
		// eliminamos el detalle de la cotizacion
		$this->db->where("idcotizacion", $idcotizacion)
			->update("cotizacion.detalle_cotizacion", array("estado"=>"I"));
			
		// eliminamos las series de la cotizacion
		$this->db->where("idcotizacion", $idcotizacion)
			->update("cotizacion.detalle_cotizacion_serie", array("estado"=>"I"));
		
		if($this->cotizacion->get("idtipoventa") == 1) { // contado
			// eliminamos el pago ingresado
			$this->load_library('pay');
			$this->pay->remove("cotizacion", $idcotizacion, $this->cotizacion->get("idsucursal"));
		}
		else {
			// eliminamos el credito de la cotizacion
			$this->db->where("idcotizacion", $idcotizacion)
				->update("cotizacion.pago_cotizacion", array("estado"=>"I"));
				
			$this->db->where("idcotizacion", $idcotizacion)
				->update("cotizacion.cronograma_pago", array("estado"=>"I"));
		}
		
		// eliminamos el ingreso de kardex
		$this->load_library("jkardex");
		$this->jkardex->remove("cotizacion", $idcotizacion, $this->cotizacion->get("idsucursal"));
		
		$this->db->trans_complete();
		
		$this->response($this->cotizacion->get_fields());
	}
	
	
	/* public function autocomplete() {
		$txt = $this->input->post("startsWith").'%';
		
		$sql = "SELECT c.idcotizacion, p.nombre, p.ruc 
			FROM cotizacion.cotizacion AS c, cotizacion.proveedor as p 
			WHERE c.idproveedor = p.idproveedor and c.estado='A'
			GROUP BY c.idcotizacion,p.nombre, p.ruc 
			ORDER BY c.idcotizacion";
			
		$query = $this->db->query($sql, array($txt, $txt, $this->input->post("maxRows")));
		$this->response($query->result_array());
	}
	
	public function select_detalle() {
		$data = array();
		$txt = $this->input->post("startsWith").'%';
		$idcotizacion = $this->input->post("idcotizacion");
		
		// $sql = "SELECT p.idproducto, p.descripcion, u.abreviatura, d.cantidad, coalesce(sum(r.cant_recepcionada),0) AS cant_recepcionada
		$sql = "SELECT p.idproducto, p.descripcion, u.abreviatura, d.cantidad
				FROM cotizacion.producto as p
				INNER JOIN cotizacion.unidad as u ON p.idunidad=u.idunidad				
				INNER JOIN cotizacion.detalle_cotizacion as d ON p.idproducto=d.idproducto
				INNER JOIN cotizacion.cotizacion as c ON d.idcotizacion=c.idcotizacion
				WHERE d.idcotizacion=$idcotizacion";
				// LEFT JOIN almacen.recepcion r ON (c.idcotizacion=r.idcotizacion AND r.idproducto=p.idproducto)				
				
		$query = $this->db->query($sql, array($txt, $txt, $this->input->post("maxRows")));
		$data['lsProdctos_cotizacions'] = $query->result_array();
		$this->response(json_encode($data));
	} */
	
	public function grilla_popup() {
		$this->load_model($this->controller);
//		$this->load_model("cotizacion.cotizacion_view");
		$this->load->library('datatables');
		
		//$this->datatables->setModel($this->cotizacion_view);
		$this->datatables->setModel($this->cotizacion);
		$this->datatables->where('estado', '=', 'A');
		$this->datatables->where('idsucursal', '=', $this->get_var_session("idsucursal"));
		
		$this->datatables->setColumns(array('fecha_cotizacion','tipo_documento','nrodocumento','cliente','moneda','total'));
		$this->datatables->setPopup(true);
		
		$table = $this->datatables->createTable(array('Fecha','Tipo Doc.','Nro.Doc.','Cliente','Moneda','Total'));
		$script = "<script>".$this->datatables->createScript("", false)."</script>";
		
		$this->response($script.$table);
	}
	
	public function get_detalle($idcotizacion) {
		$sql = "select dv.iddetalle_cotizacion, p.descripcion_detallada as producto, u.descripcion as unidad,
			dv.cantidad, dv.afecta_stock as controla_stock, dv.afecta_serie as controla_serie, 
			dv.idalmacen, dv.idproducto, dv.precio, dv.idunidad, 
			array_to_string(array_agg(dvs.serie), '|'::text) as serie
			from cotizacion.detalle_cotizacion dv
			join cotizacion.producto p on p.idproducto = dv.idproducto
			join cotizacion.unidad u on u.idunidad = dv.idunidad
			left join cotizacion.detalle_cotizacion_serie dvs on dvs.iddetalle_cotizacion=dv.iddetalle_cotizacion 
				and dvs.idcotizacion=dv.idcotizacion and dvs.idproducto=dv.idproducto and dvs.estado='A'
			where dv.estado = 'A' and dv.idcotizacion = ?
			group by dv.iddetalle_cotizacion, p.descripcion_detallada, u.descripcion, dv.cantidad, dv.afecta_stock, 
				dv.afecta_serie, dv.idalmacen, dv.idproducto, dv.idcotizacion, dv.precio, dv.idunidad
			order by iddetalle_cotizacion";
		$query = $this->db->query($sql, array($idcotizacion));
		$this->response($query->result_array());
	}
}
?>