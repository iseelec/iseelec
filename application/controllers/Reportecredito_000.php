<?php

include_once "Controller.php";

class Reportecredito extends Controller {
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		// $this->set_title("Movimiento de Caja");
		//$this->set_subtitle("Lista de Caja");
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null) {
		if(!is_array($data)) {
			$data = array();
		}
		$this->load->library('combobox');

		$data["controller"] = $this->controller;
		$data["sucursal"] = $this->listsucursal();
		$data['idperfil'] = $this->get_var_session("idperfil");
		
		/*---------------------------------------------------------------------*/
		$this->combobox->init();
		$this->combobox->setAttr("id","idcliente");
		$this->combobox->setAttr("name","idcliente");
		$this->combobox->setAttr("class","chosen-select form-control");
		// $this->combobox->setAttr("required","");
		$this->db->select("idcliente,cliente");
		$query = $this->db->order_by("cliente")->get("venta.cliente_credito_view");
		$this->combobox->addItem("","[TODOS]");
		$this->combobox->addItem($query->result_array());
		
		$data['cliente'] = $this->combobox->getObject();
		/*---------------------------------------------------------------------*/
		
		
		/*---------------------------------------------------------------------*/
		// $idperfil = 4; // id del perfil vendedor, tal vez deberia ser contante
		$idperfil = $this->get_param("idtipovendedor"); // id del perfil vendedor, tal vez deberia ser contante
		$this->load_model("usuario");
		$datos = $this->usuario->get_vendedor($this->get_var_session("idsucursal"), $idperfil);
		
		$this->combobox->init();
		$this->combobox->setAttr(array("id"=>"idvendedor","name"=>"idvendedor","class"=>"form-control input-xs","required"=>""));
		$this->combobox->addItem("","[TODOS]");
		$this->combobox->addItem($datos);
		// if( isset($data["venta"]["idvendedor"]) ) {
			// $this->combobox->setSelectedOption($data["venta"]["idvendedor"]);
		// }
		$data["vendedor"] = $this->combobox->getObject();
		/*---------------------------------------------------------------------*/
		
		
		/*---------------------------------------------------------------------*/
		$this->combobox->init();
		$this->combobox->setAttr("id","idmoneda");
		$this->combobox->setAttr("name","idmoneda");
		$this->combobox->setAttr("class","form-control input-xs");
		$this->db->select('idmoneda,descripcion');
		$query = $this->db->where("estado","A")->order_by("descripcion")->get("general.moneda");
		$this->combobox->addItem("","[TODOS]");
		$this->combobox->addItem($query->result_array());
		
		$data['moneda'] = $this->combobox->getObject();
		/*---------------------------------------------------------------------*/
		
		
		/*---------------------------------------------------------------------*/
		$this->combobox->init();
		$this->combobox->setAttr("id","idtipodocumento");
		$this->combobox->setAttr("name","idtipodocumento");
		$this->combobox->setAttr("class","form-control input-xs");
		$this->db->select('idtipodocumento,descripcion');
		$query = $this->db->where("mostrar_en_venta","S")->where("estado","A")->order_by("descripcion")->get("venta.tipo_documento");
		$this->combobox->addItem("","[TODOS]");
		$this->combobox->addItem($query->result_array());
		
		$data['comprobante'] = $this->combobox->getObject();
		/*---------------------------------------------------------------------*/
		$data["sucursal"] = $this->listsucursal();
		
		$this->css("plugins/datapicker/datepicker3");
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		$this->css("plugins/chosen/chosen");
		$this->js("plugins/chosen/chosen.jquery");

		return $this->load->view($this->controller."/form", $data, true);
	}
	
	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		return $this->form();
	}

	public function listsucursal(){
		$idsucursal = $this->get_var_session("idsucursal");
		$whereAnd = '';
		if ($this->get_var_session("idperfil")!=1) {// SI NO ES ADMINOSTRADOR LA BUSQUEDA SOLO ES POR LA SESION INICIADA
			$whereAnd.= ' AND s.idsucursal='.$idsucursal;
		}
		$sql = "SELECT
				s.idsucursal,s.descripcion, idempresa
				FROM seguridad.sucursal s 
				WHERE s.estado='A' AND idempresa IN (SELECT e.idempresa FROM seguridad.empresa e JOIN seguridad.sucursal ss ON ss.idempresa=e.idempresa WHERE ss.idsucursal=$idsucursal $whereAnd)";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
		
	public function seleccion($datos,$comparar = array()){
		$data = array();
		foreach($datos as $kk=>$vv){
			if(!empty($comparar)){
				$band = false;
				foreach($comparar as $k=>$v){
					if($vv[$k]==$v)
						$band=true;
					else{
						$band=false;
						break;
					}
				}
				if($band){
					$data[]=$vv;					
				}
			}
		}	
		return $data;
	}
	
	public function dataresumido(){
		$sql = "SELECT 
				to_char(cr.fecha_credito, 'DD/MM/YYYY') fecha_creditos
				, c.zona
				,cr.nro_credito credito
				,trim(c.cliente) cliente
				,cr.nro_letras
				,cr.monto_credito AS monto
				,SUM(l.monto_letra) cuota
				,SUM(l.descuento) descuento
				,SUM(l.mora) moras_letras
				,COALESCE(SUM(a.monto),0.00) amortizado
				,COALESCE(sum(a.mora),0.00) AS moras_amortizado 
				,(cr.monto_credito)-(SUM(l.descuento)) total
				,cr.monto_credito - COALESCE(sum(a.mora),0.00) saldo
				FROM credito.credito cr
				JOIN venta.cliente_view c ON c.idcliente=cr.idcliente
				JOIN venta.venta_view v ON v.idcliente=cr.idventa
				JOIN credito.letra l ON l.idcredito=cr.idcredito 
				AND l.estado<>'I' 
				LEFT JOIN credito.amortizacion a ON a.idletra=l.idletra 
				AND a.idcredito=cr.idcredito AND a.estado<>'I'
				WHERE cr.estado<>'I'
				{$this->condicion_resumido()}
				GROUP BY fecha_credito,credito,cliente,nro_letras,monto_credito, zona
				ORDER BY cliente , fecha_credito ASC;
				";
		$query      = $this->db->query($sql);
		// echo $sql;exit;
		$data = $query->result_array();
		return $data;
	}

	public function condicion_resumido($add_where=''){
		$where = "";
		if(!empty($_REQUEST['fechainicio'])){
			if(!empty($_REQUEST['fechafin'])){
				$where.=" AND date(cr.fecha_credito)>='{$_REQUEST['fechainicio']}' AND date(cr.fecha_credito)<='{$_REQUEST['fechafin']}'";
			}else{
				$where.=" AND date(cr.fecha_credito)='{$_REQUEST['fechainicio']}' ";
			}
		}
		
		if(!empty($_REQUEST['idcliente'])){
			$where.=" AND cr.idcliente='{$_REQUEST['idcliente']}' ";
		}
		
		if(!empty($_REQUEST['idtipodocumento'])){
			$where.=" AND idtipodocumento='{$_REQUEST['idtipodocumento']}' ";
		}
		
		if(!empty($_REQUEST['idvendedor'])){
			$where.=" AND idvendedor='{$_REQUEST['idvendedor']}' ";
		}
		
		if(!empty($_REQUEST['idmoneda'])){
			$where.=" AND cr.idmoneda='{$_REQUEST['idmoneda']}' ";
		}
		
		if(!empty($_REQUEST['idsucursal'])){
			$where.=" AND cr.idsucursal='{$_REQUEST['idsucursal']}' ";
		}
		
		$where.=$add_where;
		return $where;
	}

	public function imprimir(){
		// if($_REQUEST['ver']=='R'){
			$this->resumido();
		// }else if($_REQUEST['ver']=='D'){
			// $this->detallado();
		// }
	}
	
	public function resumido(){
		$datos      = $this->dataresumido();
		$whit_fecha=17;
		$whit_zona=30;
		$whit_credt=17;
		$whit_clien=62;
		
		
		$whit_monto=20;
		$whit_desct=12;
		$whit_total=20;
		
		$whit_cuota=20;
		$whit_moras=16;
		
		$whit_saldo=20;


		$cabecera = array('fecha_creditos'=> array('FECHA',$whit_fecha)
							,'zona' => array('ZONA',$whit_zona)
							,'credito' => array('CRED',$whit_credt)
							,'cliente' => array('CLIENTE',$whit_clien)
						);
		
		$cabecera_sub = array('monto'=>array("MONTO",$whit_monto)
								,'descuento'=>array("DESC",$whit_desct)
								,'total'=>array("TOTAL",$whit_total)
								,'cuota'=>array("CUOTA",$whit_cuota)
								,'moras_amortizado'=>array("MORAS",$whit_moras)
								,'saldo'=>array("SALDO",$whit_saldo)
						);
		$this->load->library("pdf");
		$this->load_model(array( "seguridad.empresa","general.marca","general.modelo","general.categoria","venta.cliente_view","venta.tipo_venta","general.moneda","seguridad.sucursal","seguridad.view_usuario","venta.tipo_documento"));
		$this->empresa->find($this->get_var_session("idsucursal"));
		
		$this->pdf->SetLogo(FCPATH."app/img/empresa/".$this->empresa->get("logo"));
		
		if(!empty($_REQUEST['fechainicio'])){
			if(!empty($_REQUEST['fechafin']))
				$this->pdf->SetTitle(utf8_decode("REPORTE CREDITO DE ".fecha_es($_REQUEST['fechainicio']).' A '.fecha_es($_REQUEST['fechainicio'])), 11, null, true);
			else
				$this->pdf->SetTitle(utf8_decode("REPORTE CREDITO DE ".fecha_es($_REQUEST['fechainicio'])), 11, null, true);
		}else
			$this->pdf->SetTitle(utf8_decode("REPORTE CREDITO "), 11, null, true);
			
		$this->pdf->AliasNbPages(); // para el conteo de paginas
		$this->pdf->SetLeftMargin(4);

		$this->pdf->AddPage('L','a4');
		$this->pdf->SetFont('Arial','',9);

		$this->pdf->Cell(65,3,$this->empresa->get("descripcion"),0,0,'L');
		$this->pdf->Cell(106,3,date('d/m/Y'),0,0,'R');
		$this->pdf->Cell(20,3,date('H:i:s'),0,1,'R');
		$this->pdf->Cell(45,3,"RUC: ".$this->empresa->get("ruc"),0,1,'C');
		$this->pdf->Ln(5);
		
		
		$this->pdf->SetFont('Arial','B',10);
		
		if(!empty($_REQUEST['idcliente'])){
			$this->pdf->SetFont('Arial','B',10);
			$this->pdf->Cell(30,3,"CLIENTE",0,0,'L');
			$this->pdf->Cell(5,3,":",0,0,'C');
			$this->pdf->SetFont('Arial','',10);
			$this->cliente_view->find($_REQUEST['idcliente']);
			$this->pdf->Cell(5,3,$this->cliente_view->get("cliente"),0,1,'L');
		}
		
		if(!empty($_REQUEST['idsucursal'])){
			$this->pdf->SetFont('Arial','B',10);
			$this->pdf->Cell(30,3,"SUCURSAL",0,0,'L');
			$this->pdf->Cell(5,3,":",0,0,'C');
			$this->pdf->SetFont('Arial','',10);
			$this->sucursal->find($_REQUEST['idsucursal']);
			$this->pdf->Cell(5,3,$this->sucursal->get("descripcion"),0,1,'L');
		}
		
		if(!empty($_REQUEST['idmoneda'])){
			$this->pdf->SetFont('Arial','B',10);
			$this->pdf->Cell(30,3,"MONEDA",0,0,'L');
			$this->pdf->Cell(5,3,":",0,0,'C');
			$this->pdf->SetFont('Arial','',10);
			$this->moneda->find($_REQUEST['idmoneda']);
			$this->pdf->Cell(5,3,$this->moneda->get("descripcion"),0,1,'L');
		}
		
		if(!empty($_REQUEST['idtipodocumento'])){
			$this->pdf->SetFont('Arial','B',10);
			$this->pdf->Cell(30,3,"COMPROBANTE",0,0,'L');
			$this->pdf->Cell(5,3,":",0,0,'C');
			$this->pdf->SetFont('Arial','',10);
			$this->tipo_documento->find($_REQUEST['idtipodocumento']);
			$this->pdf->Cell(5,3,$this->tipo_documento->get("descripcion"),0,1,'L');
		}
		
		if(!empty($_REQUEST['idvendedor'])){
			$this->pdf->SetFont('Arial','B',10);
			$this->pdf->Cell(30,3,"VENDEDOR",0,0,'L');
			$this->pdf->Cell(5,3,":",0,0,'C');
			$this->pdf->SetFont('Arial','',10);
			$this->view_usuario->find($_REQUEST['idvendedor']);
			$this->pdf->Cell(5,3,$this->view_usuario->get("user_nombres"),0,1,'L');
		}
		
		$this->pdf->Ln();
		$this->pdf->SetFont('Arial','B',8);

		/************************** CABECERA *****************************************/		
		$this->pdf->Cell($whit_fecha,10,'FECHA',1,0,'C');
		$this->pdf->Cell($whit_zona,10,'ZONA',1,0,'C');
		$this->pdf->Cell($whit_credt,10,'CRED',1,0,'C');
		$this->pdf->Cell($whit_clien,10,'CLIENTE',1,0,'C'); //63
		$this->pdf->Cell(52,5,'CREDITO',1,0,'C');
		$this->pdf->Cell(36,5,'AMORTIZADO',1,0,'C');
		$this->pdf->Cell(20,5,'','TRL',1,'C');
		
		// FILA 2

		$this->pdf->Cell(126,5,'',0,0,'C');
		
		$this->pdf->Cell($whit_monto,5,'MONTO',1,0,'C');
		$this->pdf->Cell($whit_desct,5,'DESC',1,0,'C');
		$this->pdf->Cell($whit_total,5,'TOTAL',1,0,'C');
		
		$this->pdf->Cell($whit_cuota,5,'CUOTA',1,0,'C');
		$this->pdf->Cell($whit_moras,5,'MORAS',1,0,'C');
		
		$this->pdf->Cell($whit_saldo,5,'SALDO','RBL',1,'C');
		/************************** CABECERA *****************************************/
		

		/************************** BODY *****************************************/
		$this->pdf->SetFont('Arial','',8);
		$new_cabecera = array_merge($cabecera,$cabecera_sub);
		$width = array($whit_fecha, $whit_zona,$whit_credt, $whit_clien, $whit_monto, $whit_desct, $whit_total, $whit_cuota, $whit_moras, $whit_saldo);
		$cols  = array('fecha_creditos','zona','credito','cliente','monto','descuento','total', 'cuota', 'moras_amortizado', 'saldo');
		$pos   = array("L", "L", "L","L", "R", "R", "R",'R','R','R');
		$fill_ = array(false, false, false,false, false, false, true,false,false,true);
		$totaltotal = $totalmora = $totalcuota = $totalsaldo = 0;

		$this->pdf->SetDrawColor(204, 204, 204);
		foreach ($datos as $key => $val) {
			// foreach ($cabecera as $k => $v) {
				// $this->pdf->Cell($v[1],5,utf8_decode($val[$k]),1,0);
			// }
			
			$this->pdf->setFillColor(249, 249, 249);
			// foreach ($cabecera_sub as $k => $v) {
				// $fill = false;
				// if(in_array($k,array("total","saldo")))
					// $fill = true;
				
				// $this->pdf->Cell($v[1],5,number_format($val[$k],2),1,0,'R',$fill);
			// }
			
			$this->pdf->SetWidths($width);
			$values = array();
			
			foreach($cols as $f){
				$values[] = utf8_decode((($val[$f])));
			}
			
			$fill=false;
			// if(in_array($key,array("total","saldo")))
				// $fill = true;
			
			$this->pdf->Row($values, $pos, "Y", "Y",$fill_);
			
			$total = $val['total'] - $val["descuento"];
			$amortizado = $val['amortizado'];
			$saldo = $total - $amortizado;
			$saldo_mora = $val["moras_letras"] - $val["moras_amortizado"];
				
			$totaltotal += $total;
			$totalcuota += $val['amortizado'];
			$totalmora += $val['moras_amortizado'];
			$totalsaldo += $saldo;

			// $this->pdf->Ln(); 
		}
		/************************** BODY *****************************************/
		
		/************************** PIE *****************************************/
		$this->pdf->Ln(1);
		$this->pdf->SetDrawColor(0, 0, 0);
		$this->pdf->Cell(($whit_fecha+$whit_zona+$whit_credt+$whit_clien+$whit_monto+$whit_desct),5,'TOTAL',0,0,'R');
		$this->pdf->Cell(20,5,number_format($totaltotal, 2),1,0,'R');
		$this->pdf->Cell(20,5,number_format($totalcuota, 2),1,0,'R');
		$this->pdf->Cell(16,5,number_format($totalmora, 2),1,0,'R');
		$this->pdf->Cell(20,5,number_format($totalsaldo, 2),1,0,'R');
		// $this->pdf->SetFont('Arial','B',8);
		// $this->pdf->Cell(($whit_compr + $whit_fecha + $whit_prov),5,"TOTAL",0,0,'R');
		// foreach($monedas as $k=>$v){
			// $filtro = $this->totales_monedas(" AND idmoneda='$v[idmoneda]' ");
			// $this->pdf->Cell($whit_min,5,number_format($filtro,2,'.',','),0,0,'R');			
		// }
		/************************** PIE *****************************************/
		$this->pdf->Output();
	}
}
?>