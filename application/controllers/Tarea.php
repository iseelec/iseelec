<?php

include_once "Controller.php";

class Tarea extends Controller {
	
	/**
	 * Datos iniciales del controlador
	 */
	public function init_controller() {
		$this->set_title("Mantenimiento Total de Estudiantes por Sección");
		$this->set_subtitle("Lista Estudiantes por Sección y numero de Aulas");
	}
	
	/**
	 * Datos finales del controlador antes de renderizar la plantilla
	 */
	public function end_controller() {
		$this->js('form/'.$this->controller.'/index');
	}
	
	/**
	 * Metodo que retorna el formulario
	 */
	public function form($data = null, $prefix = "") {
		if(!is_array($data)) {
			$data = array();
		}
		
		$this->load->library('combobox');
		$this->load_model(array("proyecto.tarea", "proyecto.tarea_fase",  "proyecto.proyecto", "proyecto.fase"));
		
			// combo areas
		$this->combobox->setAttr("id",$prefix."idfase");
		$this->combobox->setAttr("name","idfase");
		$this->combobox->setAttr("class","form-control");
		$this->combobox->setAttr("required","");
		
		$this->db->select('idfase,descripcion')->order_by("idfase", "asc");
	
		$query = $this->db->where("estado","A")->get("proyecto.fase");
		$this->combobox->addItem("","Seleccione...");
		$data['fase'] = $this->combobox->getObject();
		$this->combobox->addItem($query->result_array());
		$data['fases'] = $this->combobox->getAllItems();
			//$data['fase'] = $this->combobox->getObject();
		//fin fase
		
		// 

		//$data['sucursales'] = $this->get_var_session("sucursal");
		// combo proyectos
		$this->combobox->init();
		$this->combobox->setAttr("id",$prefix."idproyecto");
		$this->combobox->setAttr("name","idproyecto");
		$this->combobox->setAttr("class","form-control");
		$this->combobox->setAttr("required","");
		
		$this->db->select('idproyecto,descripcion')->order_by("idproyecto", "asc");
		$query = $this->db->where("estado","A")->get("proyecto.proyecto");
		$this->combobox->addItem("","Seleccione...");
		$this->combobox->addItem($query->result_array());
		$data['proyectoss'] = $this->combobox->getAllItems();
		$data['proyectos'] = $this->combobox->getObject();
		//fin proyectos

		//fin series
		//COMBO SECCION PRUEBA
		// $query = $this->db->select('idseccion, descripcion')
			// ->where("estado", "A")
			// ->order_by("descripcion", "asc")->get("compra.seccion");
		$this->combobox->removeAllItems();
		$this->combobox->setAttr("id", "idestatus");
		$this->combobox->setAttr("name", "idestatus");
		// $this->combobox->removeItems(1);
		
		$this->combobox->setAttr("class", "form-control input-xs");
		//$this->combobox->setAttr("required", "");
		//$this->combobox->addItem($query->result_array());
		$this->combobox->addItem(" ", "Selecione..");
		$this->combobox->addItem("T", "Trabajando");
		$this->combobox->addItem("E", "Echo");
		$this->combobox->addItem("S", "Espera Supervición");
		$this->combobox->addItem("A", "Atascado");
		
		$data["combo_estatus"] = $this->combobox->getObject();
	
		//combo prioridad
		$this->combobox->removeAllItems();
		 $this->combobox->setAttr("id", "idprioridad");
		 $this->combobox->setAttr("name", "idprioridad");
		// $this->combobox->removeItems(1);
		
		$this->combobox->setAttr("class", "form-control input-xs");
		//$this->combobox->setAttr("required", "");
		//$this->combobox->addItem($query->result_array());
		$this->combobox->addItem(" ", "Selecione..");
		$this->combobox->addItem("U", "Urgente");
		$this->combobox->addItem("A", "Alta");
		$this->combobox->addItem("M", "Media");
		$this->combobox->addItem("B", "Baja");
		
		$data["combo_prioridad"] = $this->combobox->getObject();
	//COMBO RESPONSABLE
		$this->combobox->removeAllItems();
		$this->combobox->setAttr(array("id"=>"idusuario","name"=>"idusuario","class"=>"form-control","required"=>""));
		$this->db->select('idusuario,nombres')->order_by("nombres", "asc");
		$query = $this->db->where("estado","A")->get("seguridad.usuario");
		$this->combobox->addItem("","Seleccione..");
		$this->combobox->addItem($query->result_array());
		$data['usuarioes'] = $this->combobox->getAllItems();
		if( isset($data["proyecto"]["idusuario"]) ) {
			$this->combobox->setSelectedOption($data["proyecto"]["idusuario"]);
		}
		$data['combo_usuario'] = $this->combobox->getObject();
	
		
		$data["controller"] = $this->controller;
		$data["prefix"] = $prefix;

		$this->css('plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox');
		$this->css("plugins/datapicker/datepicker3");
		$this->css('plugins/iCheck/custom');
		
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		$this->js('plugins/iCheck/icheck.min');
		$this->js('form/'.$this->controller.'/index');
			
		$this->js("<script>var prefix_{$this->controller} = '".$data["prefix"]."';</script>", false);
		return $this->load->view($this->controller."/form", $data, true);
		
		
	
	}

	/**
	 * Retornamos la grilla
	 */
	public function grilla() {
		// cargamos el modelo y la libreria
		$this->load_model("proyecto.view_tarea");
		$this->load->library('datatables');
		
		// indicamos el modelo al datatables
		$this->datatables->setModel($this->view_tarea);
		
		// filtros adicionales para la tabla de la bd (perfil en este caso)
		$this->datatables->where('estado', '=', 'A');
		
		// indicamos las columnas a mostrar de la tabla de la bd
		$this->datatables->setColumns(array('descripcion','nombre_ie','anio','total_aulas','total_est','observacion','estado'));
		
		// columnas de la tabla, si no se envia este parametro, se muestra el 
		// nombre de la columna de la tabla de la bd
		$columnasName = array('Nivel','Institucion','Añio','Aulas','Estudiantes','Observacion','Estado',);
		/*$columnasName = array(
			array('Observacion','55%')
			,array('Añio','20%')
			,array('Estado','25%')*/
			//,array('Banco','40%')
			// array('Descripci&oacute;n', '95%') // ancho de la columna
		//);

		// generamos la tabla y el script para el dataTables
		$table = $this->datatables->createTable($columnasName);
		// $table = $this->datatables->createTable();
		$script = "<script>".$this->datatables->createScript()."</script>";
		
		// agregamos los css para el dataTables
		$this->css('plugins/dataTables/dataTables.bootstrap');
		$this->css('plugins/dataTables/dataTables.responsive');
		$this->css('plugins/dataTables/dataTables.tableTools.min');
		
		// agregamos los scripts para el dataTables
		$this->js('plugins/dataTables/jquery.dataTables');
		$this->js('plugins/dataTables/dataTables.bootstrap');
		$this->js('plugins/dataTables/dataTables.responsive');
		$this->js('plugins/dataTables/dataTables.tableTools.min');
		$this->js($script, false);
		
		return $table;
	}
	
	/**
	 * Metodo para registrar un nuevo registro
	 */
	/*public function nuevo($prefix = "",$modal = "", $id = "") {
		$this->set_title("Registrar Proyectos");
		$this->set_subtitle("");
		
		$this->set_content($this->form($data));
		$this->index("content");
	}*/
	public function nuevo() {
		$this->set_title("Registrar Tareas por Fase");
		$this->set_subtitle("");
		
		$this->set_content($this->form());
		$this->index("content");
	}
	/**
	 * Metodo para editar registro
	 */
	public function editar($id) {
		$this->set_title("Modificar Progreso Anual de Aprendizaje");
		$this->set_subtitle("");
	
		$this->load_model(array("proyecto.tarea", "proyecto.tarea_fase" , "proyecto.proyecto", "proyecto.fase"));
		//$this->load->library('combobox');
		
		$data = $this->tarea->find($id);
		$data["tarea"] = $this->tarea->find($id);
	//	$idsucursal = $this->get_var_session("idsucursal");
		//$data["proyectos"] = $this->proyectos->find($data["tarea"]["idproyecto"]);
		// echo '<pre>';print_r($data);echo '</pre>';exit;
		$proyectos = $this->proyectos->find($data["tarea"]["idsucursal"]);
		$fase = $this->fase->find($data["tarea"]["idsucursal"]);
		//$grad = $this->tarea->find($data["tarea"]["idproyecto"]);
		
		$data["tr_tarea"] = $this->table_tr_tarea($id, $proyectos["descripcion"], $fase["descripcion"]);
		//echo '<pre>';print_r($proyectosw);echo '</pre>';exit;
		$this->set_content($this->form($data));
		$this->index("content");
	}
	
	public function table_tr_tarea($idtarea, $gdesc) {
		$rs = $this->tarea->proyectosis($idtarea);
		$html = '';
		if(!empty($rs)) {
			/*$html .= '<tr>';
				$html .= '<td><strong>Edad/Proyecto</td>';
				$html .= '<td><strong>Matriculados 2017</strong></td>';
				$html .= '<td><strong>Concluyeron 2017</strong></td>';
				$html .= '<td><strong>Matriculados 2018</strong></td>';				
				$html .= '<td><strong>Concluyeron 2018</strong></td>';
				$html .= '<td><strong>Proyeccion 2019</strong></td>';				
				$html .= '<td><strong>Acción</strong></td>';		
				$html .= '</tr>';*/
			foreach($rs as $val) {
				$html .= '<tr index="'.$val["idproyecto"].'" index1="'.$val["idfase"].'">';
				$html .= '<td><input type="hidden" name="tot_proyectos[]" class="tot_proyectos" value="'.$val["idproyecto"].'"><input type="hidden" name="proyectos[]" class="proyectos" value="'.$val["descripcion"].'"><font color="red">'.$val["descripcion"].'</font></td>';
				$html .= '<td><input type="hidden" name="tot_fase[]" class="tot_fase" value="'.$val["idfase"].'"><font color="red">'.$val["fase"].'</font></td>';
			    
		
				$html .= '<td><input type="text" name="nombre_sec[]" class="nombre_sec form-control input-xs required" value="'.$val["nombre_sec"].'"></td>';
			
				//$html .= '<td><label class="col-lg-2 control-label required">Concluyeron 2018</label></td>';
				$html .= '<td><input type="text" name="cantidad[]" class="cantidad form-control input-xs required" value="'.$val["cantidad"].'"></td>';				
				$html .= '<td><input type="text" name="cantidad[]" class="cantidad form-control input-xs required" value="'.$val["cantidad"].'"></td>';				
				$html .= '<td><input type="text" name="cantidad[]" class="cantidad form-control input-xs required" value="'.$val["cantidad"].'"></td>';				
				$html .= '<td><input type="text" name="cantidad[]" class="cantidad form-control input-xs required" value="'.$val["cantidad"].'"></td>';				
				$html .= '<td><input type="text" name="cantidad[]" class="cantidad form-control input-xs required" value="'.$val["cantidad"].'"></td>';				
				$html .= '<td><button class="btn btn-danger btn-xs btn-del-proyectos"><i class="fa fa-trash"></i></button></td>';
				$html .= '</tr>';
			}
		}
		return $html;
	}
	
	
	
	/**
	 * Metodo para guardar un registro
	 */
	public function guardar() {
		$this->load_model(array("proyecto.tarea", "proyecto.fase", "proyecto.proyecto", "proyecto.tarea_fase"));
		//$this->load_model($this->controller);

		$fields = $this->input->post();
		//$fields['idtarea'] = $fields['idtarea'];
		$fields['estado'] = "A";
		$fields['anio'] = date("Y");
		$fields['sucursal'] = $this->get_var_session("sucursal");
		$fields['idsucursal'] = $this->get_var_session("idsucursal");
		$fields['idempresa'] = $this->get_var_session("idempresa");
		$fields["codigo_te2"] = $fields["anio"].$fields["idsucursal"].$fields["idempresa"];
		// guardamos el tarea
		if(empty($fields["idtarea"])) {
			if($this->tarea->exists(array("codigo_te2"=>$fields["codigo_te2"])) == false) {
				$id = $this->tarea->insert($fields);
				$fields["idtarea"] = $id;
			}
			else {
				$this->exception("<font color='red'> El total de estudiantes por sección </font> para el año ".date("Y")." ".$fields["idtarea"]." ya existe. intente modificar el registro");
			}
		}	
		else {
			$this->tarea->update($fields);
				
		}
	
		/*
		if(empty($fields["idtarea"])) {
			$this->tarea->insert($fields);
		}
		else {
			$this->tarea->update($fields);
		}
		*/
		$this->response($this->tarea->get_fields());
	
		//$this->response($this->tarea->get_fields());
		
		// eliminamos los proyectos asignadas anteriormente
		$this->db->delete("proyecto.tarea_fase", array("idtarea"=>$fields["idtarea"]));

		// asignamos las proyectos
		if(!empty($fields["tot_proyectos"])) {
			$param["idtarea"] = $fields["idtarea"];
			foreach($fields["tot_proyectos"] as $k=>$idproyecto) {
			 //foreach($fields["tot_fase"] as $k=>$idfase){
				if(!empty($fields["cantidad"][$k])) {
					$param["idproyecto"] = $idproyecto;
					$param["idfase"] = floatval($fields["tot_fase"][$k]);
					$param["nombre_sec"] = $fields["nombre_sec"][$k];
					$param["cantidad"] = floatval($fields["cantidad"][$k]);
					//$param["idtarea"] = floatval($fields["idtarea"][$k]);
					//print $param;
					$this->tarea_fase->insert($param, false);
				}
			  //}
			}
		}

		$this->db->trans_complete();
		
		$this->response($fields);	
		//$this->db->trans_complete();
		
		//$this->response($fields);
		//conserva $this->response($this->tarea->get_fields());
		/*if(empty($fields["idtarea"])) {
			$this->tarea->insert($fields);
		}
		else {
			$this->tarea->update($fields);
		}*/
		// conserva $this->response($this->tarea->get_fields());
	
	}
	
	/**
	 * Metodo para eliminar un registro
	 */
	public function eliminar($id) {
		$this->load_model($this->controller);
	//	$this->load_model(array("proyecto.totalestudiantes", "proyecto.proyecto", "proyecto.totalestudiantes_proyectos"));
		//$this->load_model($this->controller);

		$fields = $this->input->post();
		
		// cambiamos de estado
		$fields['idtarea'] = $id;
		$fields['estado'] = "I";
		$fieldsr['anio'] = date("Y");
		$fieldsr['sucursal'] = $this->get_var_session("idsucursal");
		$fieldsr['idempresa'] = $this->get_var_session("idempresa");
		$fields['codigo_te2'] = $fieldsr['anio'].$fieldsr['idsucursal'].$fieldsr['idempresa'].$fields['estado'];
		$this->tarea->update($fields);
		// eliminamos los proyectos asignadas anteriormente
		$this->db->delete("proyecto.tarea_fase", array("idtarea"=>$fields["idtarea"]));

		
		$this->response($fields);
	}
	
}
?>