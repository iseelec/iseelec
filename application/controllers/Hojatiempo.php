<?php

include_once "Controller.php";

class Hojatiempo extends Controller {
	
	public function form($data = null) {
		if( ! is_array($data))
			$data = array();
		
		$data["controller"] = $this->controller;
		$data["options_proyecto"] = $this->options_proyecto();
		$data["date"] = date("Y-m-d");
		
		$this->css('plugins/clockpicker/clockpicker');
		$this->js('plugins/clockpicker/clockpicker');
		
		$this->css("plugins/datapicker/datepicker3");
		$this->js("plugins/datapicker/bootstrap-datepicker");
		$this->js("plugins/datapicker/bootstrap-datepicker.es");
		
		$this->js('form/'.$this->controller.'/index', true, true, $data); // js incluimos
		
		return $this->load->view($this->controller."/index", $data, true);
		
	}
	
	public function grilla() {
		return false;
	}
	
	public function index($tpl = "") {
		$data = array(
			"menu_title" => $this->menu_title
			,"menu_subtitle" => $this->menu_subtitle
			,"content" => $this->form()
			,"with_tabs" => $this->with_tabs
		);
		
		$str = $this->load->view("content_empty", $data, true);
		$this->show($str);
	}
	
	public function options_proyecto() {
		$this->db->select('idproyecto,descripcion')->order_by("fecha_inicio", "asc");
		$query = $this->db->where("estado","A")->get("proyecto.proyecto");
		
		if( ! isset($this->combobox))
			$this->load_library('combobox');
		
		$this->combobox->addItem($query->result_array());
		return $this->combobox->getAllItems();
	}
	
	public function options_empleado() {
		$sql = "select idusuario, nombres||coalesce(' '||appat, '')||coalesce(' '||apmat, '') as nombres
			from seguridad.usuario
			where estado='A' and baja='N' and idusuario in (
				select idusuario from proyecto.proyecto_usuario where estado='A' and idproyecto=?
			) order by nombres";
		$query = $this->db->query($sql, array(intval($this->input->post("idproyecto"))));
		
		if( ! isset($this->combobox))
			$this->load_library('combobox');
		
		$this->combobox->addItem($query->result_array());
		
		$this->response($this->combobox->getAllItems());
		
		return $this->combobox->getAllItems();
	}
	
	public function options_fase() {
		$sql = "select f.idfase, f.descripcion
			from proyecto.proyecto_fase p
			join proyecto.fase f on f.idfase=p.idfase
			where p.estado='A' and p.idproyecto=? order by 1";
		$query = $this->db->query($sql, array(intval($this->input->post("idproyecto"))));
		
		if( ! isset($this->combobox))
			$this->load_library('combobox');
		
		$this->combobox->addItem($query->result_array());
		
		$this->response($this->combobox->getAllItems());
		
		return $this->combobox->getAllItems();
	}
	
	public function resume() {
		$post = $this->input->post();
		if(empty($post["fecha"]))
			$post["fecha"] = date("Y-m-d");
		
		$date = new DateTime($post["fecha"]);
		
		$res["query"] = array("idproyecto"=>$post["idproyecto"], "idfase"=>$post["idfase"], "idusuario"=>$post["idusuario"], "fecha"=>$post["fecha"]);
		$res["today"] = date("Y-m-d");
		$res["current_date"] = $date->format("Y-m-d");
		$res["string_date"] = ucfirst(getDaysName($date->format("N")))." ".$date->format("d/m/Y");
		$res["tasks"] = array();
		$res["current_rows"] = array();
		$res["previous_rows"] = array();
		
		// obtenemos las tareas
		$sql = "select idtarea, descripcion_fase as descripcion 
			from proyecto.proyecto_tareaf
			where estado='A' and idproyecto=? and idfase=? and idusuario=?";
		$query = $this->db->query($sql, array($post["idproyecto"], $post["idfase"], $post["idusuario"]));
		if($query->num_rows() > 0)
			$res["tasks"] = $query->result_array(); 
		
		
		// obtenemos los registros (YYYY-MM-DDTHH:MM:SSZ)
		$sql = "select h.*, t.descripcion_fase as tarea,
			extract(EPOCH from (h.fecha::text||' '||h.hora_fin::text)::timestamp - (h.fecha::text||' '||h.hora_inicio::text)::timestamp) / 3600 as horas
			from proyecto.hojatiempo h
			join proyecto.proyecto_tareaf t on t.idproyecto=h.idproyecto and t.idfase=h.idfase 
			and t.idtarea=h.idtarea and t.idusuario=h.idusuario
			where h.estado='A' and h.idproyecto=? and h.idfase=? and h.idusuario=?
			order by fecha, idhojatiempo";
		$query = $this->db->query($sql, array($post["idproyecto"], $post["idfase"], $post["idusuario"]));
		if($query->num_rows() > 0) {
			$rows = array();
			foreach($query->result_array() as $v) {
				if( ! array_key_exists($v["fecha"], $rows))
					$rows[$v["fecha"]] = array();
				
				list($y, $m, $d) = explode("-", $v["fecha"]);
				$date->setDate(intval($y), intval($m), intval($d));
				$v["string_date"] = ucfirst(getDaysName($date->format("N")))." ".$date->format("d/m/Y");
				
				$rows[$v["fecha"]][] = $v;
			}
			
			if(array_key_exists($res["current_date"], $rows)) {
				$res["current_rows"] = $rows[$res["current_date"]];
				unset($rows[$res["current_date"]]);
			}
			
			if( ! empty($rows)) {
				krsort($rows);
				$res["previous_rows"] = $rows;
			}
		}
		
		$this->response($res);
		
		return $res;
	}
	
	protected function getMaxId($p) {
		$sql = "select coalesce(max(idhojatiempo), 0) as id from proyecto.hojatiempo where idproyecto=? and idusuario=?";
		$query = $this->db->query($sql, array($p["idproyecto"], $p["idusuario"]));
		return ($query->num_rows() > 0) ? intval($query->row()->id) : 0;
	}
	
	public function guardar() {
		$p = $this->input->post();
		$p["fecha_registro"] = date("Y-m-d H:i:s");
		$p["idusuario_registro"] = $this->get_var_session("idusuario");
		$p["idsucursal"] = $this->get_var_session("idsucursal");
		$p["estado"] = "A";
		
		$this->load_model("proyecto.hojatiempo");
		$this->hojatiempo->text_uppercase(false);
		$this->hojatiempo->set($p);
		
		// inactivo para todos los registros
		$sql = "update proyecto.hojatiempo set estado='I' where idproyecto=? and idfase=? and idusuario=? and fecha=?";
		$this->db->query($sql, array($p["idproyecto"], $p["idfase"], $p["idusuario"], $p["fecha"]));
		
		if( ! empty($p["tareas"])) {
			$id = $this->getMaxId($p);
			foreach($p["tareas"] as $v) {
				$this->hojatiempo->set($v);
				if( ! empty($v["idhojatiempo"]))
					$this->hojatiempo->update();
				else {
					$this->hojatiempo->set("idhojatiempo", ++$id);
					$this->hojatiempo->insert();
				}
			}
		}
		
		$this->response($this->hojatiempo->get_fields());
		return $this->hojatiempo->get_fields();
	}
}
