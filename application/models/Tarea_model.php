<?php

include_once "Model.php";

class Tarea_model extends Model {
	public function init() {
		$this->set_schema("proyecto");
	}
/**
	 * Obtener los proyectos  asignados al total estduaintes 
	 * @param integer $idtarea
	 * @return array resultset
	 */
	public function proyectosis($idtarea, $idproyecto = FALSE, $idfase = FALSE) {
		$this->db
			->select("tes.idproyecto, tes.idfase, g.descripcion, s.descripcion fase, tes.nombre_sec, tes.cantidad, tes.idtarea")
			->where("tes.idtarea", $idtarea);
		
		if($idproyecto !== FALSE)
			$this->db->where("tes.idproyecto", $idproyecto);
		
		if($idfase!== FALSE)
			$this->db->where("tes.idfase", $idfase);
		
		$query = $this->db->where("g.estado", "A")
			->join("proyecto.proyecto g", "g.idproyecto=tes.idproyecto")
			->join("proyecto.fase s", "s.idfase=tes.idfase")
			->order_by("g.descripcion", "asc")
			->get("proyecto.tarea_fase tes");
		return $query->result_array();
	}	
	
}
?>