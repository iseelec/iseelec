<?php

include_once "Model.php";

class Detalle_proforma_model extends Model {

	public function init() {
		$this->set_schema("compra");
	}
	
	public function get_item_proforma($idproforma) {
		$sql = "SELECT dp.idproducto, p.descripcion, dp.idunidad, py.idproyecto, dp.cantidad, dp.precio_compra,
				p.descripcion_detallada, ma.descripcion AS marca, mo.descripcion AS modelo,
				coalesce(dp.cantidad, 0.00)*coalesce(dp.precio_compra, 0.00) as importe
				FROM compra.detalle_proforma AS dp
				INNER JOIN compra.producto AS p ON p.idproducto = dp.idproducto
				INNER JOIN proyecto.proyecto AS py ON py.idproyecto = dp.idproyecto
				INNER JOIN general.marca AS ma ON ma.idmarca = p.idmarca
				INNER JOIN general.modelo AS mo ON mo.idmodelo = p.idmodelo
			WHERE dp.estado = ? AND dp.idproforma = ?";
		
		$query = $this->db->query($sql, array("A", $idproforma));
		return $query->result_array();
	}
}
?>