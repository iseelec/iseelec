<?php

include_once "Model.php";

class Detalle_preventa_model extends Model {

	public function init() {
		$this->set_schema("venta");
	}
	
	public function get_items($idpreventa, $idsucursal = NULL) {
		if(empty($idsucursal))
			$idsucursal = $this->ci->get_var_session("idsucursal");
		
		// $sql = "SELECT dv.iddetalle_preventa as iddetalle, dv.idproducto, dv.idseccion, p.descripcion_detallada, 
			// dv.idunidad, dv.cantidad, coalesce(dv.precio,0.00) as precio, p.tipo, p.controla_serie, p.controla_stock,
			// dv.idalmacen, dv.serie, dv.oferta, dv.codgrupo_igv, dv.codtipo_igv, pu.precio_compra, 
			// coalesce(pu.precio_venta,0.00) as precio_venta
			// FROM venta.detalle_preventa dv
			// JOIN compra.producto p ON p.idproducto = dv.idproducto
			// LEFT JOIN compra.producto_precio_unitario pu on pu.idproducto=p.idproducto and pu.idsucursal=?
			// WHERE dv.estado = ? AND dv.idpreventa = ?
			// ORDER BY iddetalle_preventa";	
			
			$sql = "SELECT
					dv.iddetalle_preventa AS iddetalle,
					dv.idproducto,
					dv.idseccion,
					--p.descripcion_detallada,
					dv.idunidad,
					dv.cantidad,
					coalesce(dv.precio,0.00) AS precio,
					p.tipo,
					p.controla_serie,
					p.controla_stock,
					dv.idalmacen,
					dv.serie,
					dv.oferta,
					dv.codgrupo_igv,
					dv.codtipo_igv,
					p.precio_compra,
					p.precio_mercado,
					dv.descripcion_cli as desc_cli,
					coalesce(dv.descuento, 0.00) as descuento,
					compra.unidad.descripcion as unidad,
					general.marca.descripcion AS marca,
					general.modelo.descripcion as modelo
					FROM
					venta.detalle_preventa AS dv
					JOIN compra.producto AS p ON p.idproducto = dv.idproducto
					LEFT JOIN compra.unidad ON dv.idunidad = compra.unidad.idunidad and p.idsucursal=?
					INNER JOIN general.marca ON general.marca.idmarca = p.idmarca
					INNER JOIN general.modelo ON general.modelo.idmodelo = p.idmodelo
			WHERE dv.estado = ? AND dv.idpreventa = ?
			ORDER BY iddetalle_preventa";
		
		$query = $this->db->query($sql, array($idsucursal, "A", $idpreventa));
		return $query->result_array();
	}
}
?>