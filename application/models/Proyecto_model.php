<?php

include_once "Model.php";

class Proyecto_model extends Model {

	public function init() {
		$this->set_schema("proyecto");
	}
	
	/**
	 * Obtener la cantidad disponible en usuarioes minimas. Para obtener el stock
	 * en otras usuarioes se debe hacer la conversion dividiendo esta cantidad por la
	 * cantidad de la usuario que se desea convertir.
	 * @param integer $idproyecto
	 * @return integer numero que representa la cantidad disponible en usuarioes minimas
	 */

	
	/**
	 * Obtener las usuarioes de responsable asignados al proyecto
	 * @param integer $idproyecto
	 * @return array resultset
	 */
	public function usuarioes($idproyecto, $idusuario = FALSE) {
		$this->db
			->select("p.idusuario, u.nombres, u.appat, p.dias_usuario, p.sueldo_usuario, p.viatico_usuario")
			->where("p.idproyecto", $idproyecto);
		
		if($idusuario !== FALSE)
			$this->db->where("p.idusuario", $idusuario);
		
		$query = $this->db->where("u.estado", "A")
			->join("seguridad.usuario u", "u.idusuario=p.idusuario")
			->order_by("u.nombres", "asc")
			->get("proyecto.proyecto_usuario p");
		return $query->result_array();
	}
	
		/**
	 * Obtener las fases asignados al proyecto
	 * @param integer $idproyecto
	 * @return array resultset
	 */
public function fasees($idproyecto, $idfase = FALSE) {
		$this->db
			->select("f.idfase, f.idusuario, u.nombres as nombres, pf.descripcion, pf.descripcion, f.fecha_inicio, f.fecha_fin")
			->where("f.idproyecto", $idproyecto);
		
		if($idfase !== FALSE)
			$this->db->where("f.idfase", $idfase);
		
		$query = $this->db->where("pf.estado", "A")
			->join("proyecto.fase pf", "pf.idfase=f.idfase")
			->join("seguridad.usuario u", "u.idusuario=f.idusuario")
			->order_by("pf.descripcion", "asc")
			->get("proyecto.proyecto_fase f");
		return $query->result_array();
	}
	
	////fases 
	public function tareasf($idproyecto) {
		$query = $this->db
			->select("pt.idusuario, f.descripcion as fase, pt.idfase, pt.descripcion_fase, pt.estatus, pt.tiempo_dias, pt.fecha_vencimiento, pt.prioridad")
			->join("proyecto.fase f", "f.idfase=pt.idfase")
			->where("pt.idproyecto", $idproyecto)
			//->where("pt.idfase", $idfase)
			->order_by("fecha_vencimiento", "asc")
			->get("proyecto.proyecto_tareaf pt");
		return $query->result_array();
	}
	
	/**
	 * Obtener los precios de proyecto asignados al proyecto, segun usuario de responsable, 
	 * moneda y sucursal
	 * @param integer $idproyecto
	 * @param integer $idsucursal
	 * @param integer $idmoneda opcional
	 * @return array resultset
	 */
public function get_items($idpreventa, $idsucursal = NULL) {
		if(empty($idsucursal))
			$idsucursal = $this->ci->get_var_session("idsucursal");
		
		// $sql = "SELECT dv.iddetalle_preventa as iddetalle, dv.idproducto, dv.idseccion, p.descripcion_detallada, 
			// dv.idunidad, dv.cantidad, coalesce(dv.precio,0.00) as precio, p.tipo, p.controla_serie, p.controla_stock,
			// dv.idalmacen, dv.serie, dv.oferta, dv.codgrupo_igv, dv.codtipo_igv, pu.precio_compra, 
			// coalesce(pu.precio_venta,0.00) as precio_venta
			// FROM venta.detalle_preventa dv
			// JOIN compra.producto p ON p.idproducto = dv.idproducto
			// LEFT JOIN compra.producto_precio_unitario pu on pu.idproducto=p.idproducto and pu.idsucursal=?
			// WHERE dv.estado = ? AND dv.idpreventa = ?
			// ORDER BY iddetalle_preventa";	
			
			$sql = "SELECT
					dv.iddetalle_preventa AS iddetalle,
					dv.idproducto,
					dv.idseccion,
					--p.descripcion_detallada,
					dv.idunidad,
					dv.cantidad,
					coalesce(dv.precio,0.00) AS precio,
					p.tipo,
					p.controla_serie,
					p.controla_stock,
					dv.idalmacen,
					dv.serie,
					dv.oferta,
					dv.codgrupo_igv,
					dv.codtipo_igv,
					p.precio_compra,
					p.precio_mercado,
					dv.descripcion_cli as desc_cli,
					coalesce(dv.descuento, 0.00) as descuento,
					compra.unidad.descripcion as unidad,
					general.marca.descripcion AS marca,
					general.modelo.descripcion as modelo
					FROM
					venta.detalle_preventa AS dv
					JOIN compra.producto AS p ON p.idproducto = dv.idproducto
					LEFT JOIN compra.unidad ON dv.idunidad = compra.unidad.idunidad and p.idsucursal=?
					INNER JOIN general.marca ON general.marca.idmarca = p.idmarca
					INNER JOIN general.modelo ON general.modelo.idmodelo = p.idmodelo
			WHERE dv.estado = ? AND dv.idpreventa = ?
			ORDER BY iddetalle_preventa";
		
		$query = $this->db->query($sql, array($idsucursal, "A", $idpreventa));
		return $query->result_array();
	}
	
	/**
	 * Obtener los precios de venta asignados al proyecto
	 * @param integer $idproyecto
	 * @param integer $idsucursal
	 * @return array resultset
	 */

	
	/**
	 * Obtener el precio de proyecto en usuarioes segun el proyecto
	 
	 */


	/**
	 * Obtener el precio de costo unitario de un proyecto, esta funcion es alternativo a
	 * la funcion "get_precio_proyecto_unitario", utilizamos nueva tabla para hacer el calculo.
	 */

}

?>