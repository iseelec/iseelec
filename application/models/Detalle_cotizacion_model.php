<?php

include_once "Model.php";

class Detalle_cotizacion_model extends Model {

	public function init() {
		$this->set_schema("cotizacion");
	}
	
	public function get_items($idcotizacion) {
		$sql = "SELECT dp.iddetalle_cotizacion, dp.idproducto, p.descripcion, dp.idunidad, dp.cantidad, 
			dp.precio as precio_cotizacion, p.descripcion_detallada, p.idtipo_producto, 
			dp.afecta_stock as controla_stock, dp.afecta_serie as controla_serie, 
			array_to_string(array_agg(dps.serie), '|'::text) AS serie
			FROM cotizacion.detalle_cotizacion dp 
			INNER JOIN compra.producto p ON p.idproducto = dp.idproducto
			LEFT JOIN cotizacion.detalle_cotizacion_serie dps ON dps.idcotizacion = dp.idcotizacion 
				AND dps.iddetalle_cotizacion = dp.iddetalle_cotizacion AND dps.estado = 'A'
			WHERE dp.estado = ? AND dp.idcotizacion = ?
			GROUP BY dp.iddetalle_cotizacion, dp.idproducto, p.descripcion, dp.idunidad, dp.cantidad, 
				dp.precio, p.descripcion_detallada, p.idtipo_producto, dp.afecta_stock, dp.afecta_serie
			ORDER BY dp.iddetalle_cotizacion";
		
		$query = $this->db->query($sql, array("A", $idcotizacion));
		return $query->result_array();
	}
}
?>