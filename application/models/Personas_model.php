<?php

include_once "Model.php";

class Personas_model extends Model {

	public function init() {
		$this->set_schema("venta");
	}
	
    public function obtener_cliente_id($id){
		// $query = $this->db->get('seguridad.perfil', 10);
        // return $query->result();
	}

	public function obtener_cliente(){
        $query = $this->db->get('venta.cliente');
        return $query->result();
	}
	
	public function saldo($idcliente) {
		$sql = "SELECT COALESCE(SUM(c.monto_credito),0) AS total, COALESCE(SUM(a.monto),0) AS pagado
			FROM credito.credito c
			JOIN credito.amortizacion a ON a.idcredito=c.idcredito AND a.estado=?
			WHERE c.estado=? AND c.pagado=? AND c.idcliente=?";
		$query = $this->db->query($sql, array("A", "A", "N", $idcliente));
		
		if ($query->num_rows() > 0) {
			$row = $query->row_array();
			$en_credito = $credito = $row["total"] - $row["pagado"];
			
			$query = $this->db->get_where("venta.cliente", array("idcliente"=>$idcliente));
			if($query->num_rows() > 0) {
				$row = $query->row_array();
				$credito = $row["limite_credito"];
			}
			
			return ($credito - $en_credito);
		}
		
		return 0;
	}
}
?>