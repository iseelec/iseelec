<?php
    
    require_once ("constants.php");
    require_once (NUSOAP_DIR."/nusoap.php");

    function getUserLogin($usu_id, $sis_id, $cookie) {
        $response = array();
        
        $acceso = new Generic('sesion', 'seguridad');
        $acceso->usu_id = $usu_id;
        $acceso->sis_id = $sis_id;
        $acceso->select('', 'usu_id, sis_id');
        $acceso->updateValuesWithResultSet(0);
        
		if($acceso->cookie == $cookie) {
			$usuario = new Generic('usuario', 'seguridad');
			$usuario->find($acceso->usu_id);
			
			$perfil = new Generic('perfil', 'seguridad');
			$perfil->find($usuario->per_id);
			
			$response['usuario'] = $usuario->getDataValues();
			$response['perfil'] = $perfil->getDataValues();
		}
        
        return json_encode($response);
    }

    $server = new soap_server();
    $server->configureWSDL("server", "urn:server");

    $server->register(
            "getUserLogin",
            array("usu_id" => "xsd:int", 'sis_id' => 'xsd:int', 'cookie' => 'xsd:int'),
            array("return" => "xsd:string"),
            "urn:server",
            "urn:server#getUserLogin",
            "rpc",
            "encoded",
            "Devuelve los datos del usuario almacenado en la cookie"
    );

    $postdata = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : file_get_contents("php://input");
    $server->service($postdata);
    
?>