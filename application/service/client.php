<?php
require_once "nusoap.php";

function call($url, $function, $params) {
	if( ! starts_with($url, "http")) { // funcion localizada bajo ./helpers/general_helper.php
		return false;
	}
	
	$cliente = new nusoap_client($url);

	$error = $cliente->getError();
	if ($error) {
		return false;
		// return $error;
	}

	$result = $cliente->call($function, $params);
	
	if ($cliente->fault) {
		return false;
		// return $result;
	}
	else {
		$error = $cliente->getError();
		if ($error) {
			return false;
			// return $error;
		}
		else {
			return $result;
		}
	}
}
?>