﻿DELETE FROM general.tipo_operacion;

INSERT INTO general.tipo_operacion(codtipo_operacion, descripcion) VALUES ('0101', 'VENTA INTERNA');

UPDATE seguridad.param SET valor='EXO' WHERE idparam='default_igv';

ALTER TABLE general.tipo_igv ADD COLUMN codgrupo_igv character varying(10);

UPDATE general.tipo_igv SET codgrupo_igv = substr(descripcion,1,3);

CREATE TABLE venta.resumen_diario
(
  idresumen_diario serial NOT NULL,
  idsucursal integer,
  idusuario integer,
  fecha date,
  correlativo integer,
  estado character varying(1),
  CONSTRAINT pkresumen_diario PRIMARY KEY (idresumen_diario)
);

CREATE TABLE venta.detalle_resumen_diario
(
  iddetalle_resumen_diario integer NOT NULL,
  idresumen_diario integer NOT NULL,
  idreferencia integer,
  referencia character varying(45),
  tip_docu_modifica character varying(5),
  serie_docu_modifica character varying(4),
  nro_docu_modifica character varying(8),
  estado_docu character varying(1),
  estado character varying(1),
  CONSTRAINT pkdetalle_resumen_diario PRIMARY KEY (iddetalle_resumen_diario, idresumen_diario)
);

ALTER TABLE venta.facturacion ADD COLUMN resumen_value text;
ALTER TABLE venta.facturacion ADD COLUMN resumen_firma text;
ALTER TABLE venta.facturacion ADD COLUMN tip_docu_cliente character varying(5);
ALTER TABLE venta.facturacion ADD COLUMN num_docu_cliente character varying(15);
ALTER TABLE venta.facturacion ADD COLUMN estado character varying(1);
ALTER TABLE venta.facturacion ADD COLUMN fecha date;
ALTER TABLE venta.facturacion ADD COLUMN actualizado integer;
ALTER TABLE venta.facturacion ADD COLUMN xml_content text;
ALTER TABLE venta.facturacion ADD COLUMN pdf_content text;
ALTER TABLE venta.facturacion ADD COLUMN codsede character varying(3);
ALTER TABLE venta.facturacion ADD COLUMN gravado numeric(10,2);
ALTER TABLE venta.facturacion ADD COLUMN inafecto numeric(10,2);
ALTER TABLE venta.facturacion ADD COLUMN exonerado numeric(10,2);
ALTER TABLE venta.facturacion ADD COLUMN suma_igv numeric(10,2);
ALTER TABLE venta.facturacion ADD COLUMN suma_descuento numeric(10,2);
ALTER TABLE venta.facturacion ADD COLUMN descuento_global numeric(10,2);
ALTER TABLE venta.facturacion ADD COLUMN gratuito numeric(10,2);
ALTER TABLE venta.facturacion ADD COLUMN imagen_qr text;
ALTER TABLE venta.facturacion ADD COLUMN html_content text;
ALTER TABLE venta.facturacion ADD COLUMN razon_cliente character varying(200);
ALTER TABLE venta.facturacion ADD COLUMN direccion_cliente character varying(200);
ALTER TABLE venta.facturacion ADD COLUMN valor_venta numeric(10,2);
ALTER TABLE venta.facturacion ADD COLUMN idsucursal integer;
ALTER TABLE venta.facturacion ADD COLUMN fecha_registro timestamp without time zone;

ALTER TABLE venta.documento_baja ADD COLUMN estado character varying(1);

ALTER TABLE general.grupo_igv ADD COLUMN nombre character varying(30);
ALTER TABLE general.grupo_igv ADD COLUMN codsunat character varying(30);
ALTER TABLE general.grupo_igv ADD COLUMN codinternacional character varying(30);

UPDATE general.grupo_igv SET nombre='EXO', codsunat='9997', codinternacional='VAT' WHERE codgrupo_igv='EXO';
UPDATE general.grupo_igv SET nombre='IGV', codsunat='1000', codinternacional='VAT' WHERE codgrupo_igv='GRA';
UPDATE general.grupo_igv SET nombre='INA', codsunat='9998', codinternacional='FRE' WHERE codgrupo_igv='INA';

ALTER TABLE venta.notacredito ADD COLUMN fecha_registro timestamp without time zone;
ALTER TABLE venta.notacredito ADD COLUMN motivo_anulacion text;
ALTER TABLE venta.notacredito ADD COLUMN fecha_hora_anulacion timestamp without time zone;
ALTER TABLE venta.notacredito ADD COLUMN idusuario_anulacion integer;

ALTER TABLE seguridad.empresa ADD COLUMN nombre_comercial character varying(250);
ALTER TABLE seguridad.empresa ADD COLUMN ubigeo character varying(15);
ALTER TABLE seguridad.empresa ADD COLUMN codigo_pais character varying(15);
ALTER TABLE seguridad.empresa ADD COLUMN departamento character varying(45);
ALTER TABLE seguridad.empresa ADD COLUMN provincia character varying(45);
ALTER TABLE seguridad.empresa ADD COLUMN distrito character varying(45);
ALTER TABLE seguridad.empresa ADD COLUMN urbanizacion character varying(100);
ALTER TABLE seguridad.empresa ADD COLUMN cod_local character varying(15);
ALTER TABLE seguridad.empresa ADD COLUMN email character varying(45);
ALTER TABLE seguridad.empresa ADD COLUMN usuario_sol character varying(30);
ALTER TABLE seguridad.empresa ADD COLUMN clave_sol character varying(30);
ALTER TABLE seguridad.empresa ADD COLUMN send_to_produccion character varying(1);

-- Nota: llenar los datos de la empresa en la tabla [seguridad.empresa], (Conforme a los datos de la web Consultas Ruc de Sunat)

CREATE TABLE general.tipo_comprobante
(
  tip_docu character varying(5) NOT NULL,
  descripcion character varying(45),
  abreviatura character varying(30),
  CONSTRAINT pktipo_comprobante PRIMARY KEY (tip_docu)
);

INSERT INTO general.tipo_comprobante VALUES ('01', 'Factura', 'F');
INSERT INTO general.tipo_comprobante VALUES ('03', 'Boleta de Venta', 'BV');
INSERT INTO general.tipo_comprobante VALUES ('07', 'Nota de Credito', 'NC');
INSERT INTO general.tipo_comprobante VALUES ('08', 'Nota de Debito', 'ND');
INSERT INTO general.tipo_comprobante VALUES ('RA', 'Comunicacion de Baja', 'CB');
INSERT INTO general.tipo_comprobante VALUES ('20', 'Retencion', 'R');
INSERT INTO general.tipo_comprobante VALUES ('40', 'Percepcion', 'P');
INSERT INTO general.tipo_comprobante VALUES ('RC', 'Resumen de Boletas', 'RC');
INSERT INTO general.tipo_comprobante VALUES ('RR', 'Resumen de Reversion', 'RR');
INSERT INTO general.tipo_comprobante VALUES ('09', 'Guia de Remision Remitente', 'GR');

CREATE TABLE general.situacion
(
  ind_situ character varying(5) NOT NULL,
  descripcion character varying(45),
  generar character varying(1),
  enviar character varying(1),
  baja character varying(1),
  restablecer character varying(1),
  CONSTRAINT pksituacion PRIMARY KEY (ind_situ)
);

INSERT INTO general.situacion VALUES ('03', 'Enviado y Aceptado SUNAT', NULL, NULL, 'S', NULL);
INSERT INTO general.situacion VALUES ('04', 'Enviado y Aceptado SUNAT con Obs.', NULL, NULL, 'S', NULL);
INSERT INTO general.situacion VALUES ('08', 'Enviado a SUNAT Por Procesar', NULL, NULL, NULL, NULL);
INSERT INTO general.situacion VALUES ('09', 'Enviado a SUNAT Procesando', NULL, NULL, NULL, NULL);
INSERT INTO general.situacion VALUES ('01', 'Por Generar XML', 'S', NULL, NULL, 'S');
INSERT INTO general.situacion VALUES ('02', 'XML Generado', NULL, 'S', NULL, 'S');
INSERT INTO general.situacion VALUES ('06', 'Con Errores', 'S', NULL, NULL, 'S');
INSERT INTO general.situacion VALUES ('07', 'Por Validar XML', 'S', NULL, NULL, 'S');
INSERT INTO general.situacion VALUES ('10', 'Rechazado por SUNAT', 'S', 'S', NULL, 'S');
INSERT INTO general.situacion VALUES ('11', 'Enviado y Aceptado SUNAT', NULL, NULL, 'S', NULL);
INSERT INTO general.situacion VALUES ('12', 'Enviado y Aceptado SUNAT con Obs.', NULL, NULL, 'S', NULL);
INSERT INTO general.situacion VALUES ('05', 'Rechazado por SUNAT', NULL, NULL, 'S', 'S');

CREATE OR REPLACE VIEW venta.notacredito_view AS 
 SELECT n.idnotacredito, n.fecha, 
    (n.serie::text || '-'::text) || n.numero::text AS nrodocumento, 
    c.nombres::text || COALESCE(' '::text || c.apellidos::text, ''::text) AS cliente, 
    n.subtotal, n.igv, n.subtotal + n.igv AS total, n.descripcion AS motivo, 
    (((t.abreviatura::text || ' '::text) || n.serie_ref::text) || '-'::text) || n.numero_ref::text AS tipodoc_ref, 
    n.estado, n.idsucursal, n.idusuario, n.idmoneda, m.descripcion AS moneda, 
    t.descripcion AS tipo_documento_ref, c.dni, c.ruc, 
    c.direccion_principal AS direccion, n.serie, n.numero, 
    n.numero AS correlativo, 
    c.nombres::text || COALESCE(' '::text || c.apellidos::text, ''::text) AS full_nombres, 
    to_char(n.fecha::timestamp with time zone, 'DD/MM/YYYY'::text) AS fecha_venta_format, 
    venta.tipo_documento AS documento_modifica, 
    venta.comprobante AS comprobante_modifica, n.descuento, 
    n.total_con_descuento AS total_desc, n.fecha_registro, n.serie_ref, 
    n.numero_ref AS correlativo_ref
   FROM venta.notacredito n
   JOIN venta.cliente c ON c.idcliente = n.idcliente
   JOIN venta.tipo_documento t ON t.idtipodocumento = n.iddocumento_ref
   JOIN venta.venta_view venta ON venta.idventa = n.idventa
   JOIN general.moneda m ON m.idmoneda = n.idmoneda;
